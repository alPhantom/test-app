# [Start Bootstrap](../../index.phpootstrap.com/) - [SB Admin 2](../../index.phpootstrap.com/template-overviews/sb-admin-2/)
[![CDNJS](../../index.phphields.io/cdnjs/v/startbootstrap-sb-admin-2.svg)](../../index.php.com/libraries/startbootstrap-sb-admin-2)

[SB Admin 2](../../index.phpootstrap.com/template-overviews/sb-admin-2/) is an open source, admin dashboard template for [Bootstrap](../../index.phptstrap.com/) created by [Start Bootstrap](../../index.phpootstrap.com/).

## Getting Started

To begin using this template, choose one of the following options to get started:
* [Download the latest release on Start Bootstrap](../../index.phpootstrap.com/template-overviews/sb-admin-2/)
* Clone the repo: `git clone https://github.com/BlackrockDigital/startbootstrap-sb-admin-2.git`
* Fork the repo

## Using the Source Files

After cloning the repo take a look at the `gulpfile.js` and check out the tasks available:
* `gulp` The default task will compile the LESS and JS into the `dist` directory and minify the output, and it will copy all vendor libraries from `bower_components` into the `vendor` directory
* `gulp dev` The dev task will serve up a local version of the template and will watch the LESS, JS, and HTML files for changes and reload the browser windo automatically

To update dependencies, run `bower update` and then run `gulp copy` to copy the updated dependencies into the `vendor` directory

## Bugs and Issues

Have a bug or an issue with this template? [Open a new issue](../../index.phpb.com/BlackrockDigital/startbootstrap-sb-admin-2/issues) here on GitHub or leave a comment on the [template overview page at Start Bootstrap](../../index.phpootstrap.com/template-overviews/sb-admin-2/).

## Creator

Start Bootstrap was created by and is maintained by **[David Miller](../../index.phpiller.io/)**, Owner of [Blackrock Digital](../../index.phpockdigital.io/).

* https://twitter.com/davidmillerskt
* https://github.com/davidtmiller

Start Bootstrap is based on the [Bootstrap](../../index.phptstrap.com/) framework created by [Mark Otto](../../index.phper.com/mdo) and [Jacob Thorton](../../index.phper.com/fat).

## Copyright and License

Copyright 2013-2016 Blackrock Digital LLC. Code released under the [MIT](../../index.phpb.com/BlackrockDigital/startbootstrap-sb-admin-2/blob/gh-pages/LICENSE) license.
