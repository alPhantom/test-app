;$(function() {

    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    /**
     * Обработка кнопки добавления новой строки с картой.
     * Рисует строку в блоке card_info_block, содержащую три колонки с полем для ввода номера карты,
     * суммы и кнопки "удалить" соответственно
     */
    $('#add_card').on('click', function (e) {

        $('#card_info_block').append(
            $('<div/>', {'class': 'form-group row card-block required'})
                .append(
                    $('<div/>', {'class' : 'row '})
                        .append(
                            $('<div/>', {'class': 'col-lg-5 col-md-4'})
                                .append(
                                    $('<label/>', {'class': 'control-label', 'for': 'box'})
                                        .append('Карта/кошелек ПКЦ::')))
                        .append(
                            $('<div/>', {'class': 'col-lg-6 col-md-5'})
                                .append(
                                    getBoxes()
                                ))
                        .append(
                            $('<div/>', {'class':'col-lg-1 col-md-2 col-sm-1 col-xs-1'})
                                .append(
                                    $('<div/>', {'class':'delete-card-btn pointer'})
                                        .append(
                                            $('<i/>', {'class':'fa fa-trash fa-2x'})
                                        ))))
                .append(
                    $('<div/>', {'class' : 'row'})
                        .append(
                            $('<div/>', {'class': 'col-lg-5 col-md-5'})
                                .append(
                                    $('<label/>', {'class': 'control-label', 'for': 'card_number'})
                                        .append('Карта/кошелек клиента:')))
                        .append(
                            $('<div/>', {'class': 'col-lg-6 col-md-6'})
                                .append(
                                    $('<input/>', {'class':'form-control', 'type':'text',
                                        'id':'card_number', 'name':'card_number[]', 'required': 'required',
                                        'maxlength': 30, 'autocomplete': 'Off'}))))
                .append(
                    $('<div/>', {'class' : 'row'})
                        .append(
                            $('<div/>', {'class': 'col-lg-5 col-md-5'})
                                .append(
                                    $('<label/>', {'class': 'control-label', 'for': 'card_time'})
                                        .append('Дата и время зачисления:')))
                        .append(
                            $('<div/>', {'class': 'col-lg-6 col-md-6'})
                                .append(
                                    $('<input/>', {'class':'form-control', 'type':'datetime-local',
                                        'id':'card_time', 'name':'card_time[]', 'autocomplete': 'Off'}))))
                .append(
                    $('<div/>', {'class' : 'row'})
                        .append(
                            $('<div/>', {'class': 'col-lg-5 col-md-5'})
                                .append(
                                    $('<label/>', {'class': 'control-label', 'for': 'amount'})
                                        .append('Сумма:')))
                        .append(
                            $('<div/>', {'class': 'col-lg-6 col-md-6'})
                                .append(
                                    $('<input/>', {'class':'form-control', 'type':'number',
                                        'id':'amount', 'name':'amount[]', 'min':'0', 'required': 'required',
                                        'autocomplete': 'Off'}))))
        );
        e.preventDefault();

    });

    /**
     * Обработка событий в блоке card_info_block
     */
    $('#card_info_block').on('click', '.delete-card-btn', function () {

        discardCommission();

        let totalAmount = $('#total_amount');

        totalAmount.val(deductAmount(parseInt(totalAmount.val()),
            parseInt($(this).parent().parent().parent().find('#amount').val()))); //Указатель возвращается к предку - общему блоку
        // и берёт значение из инпута с суммой
        $('#commission_amount').val('');
        $(this).parent().parent().parent().remove();

    })
        .on('keyup', '#amount', function () {
            let totalAmount = 0;
            $('input[name^="amount"]').each(function() {
                totalAmount += Number( $(this).val() );
            });
            $('#total_amount').val(totalAmount);
            $('#commission_amount').val('');
        });


    function discardCommission() {

        let commission_block = $('#commission_block');
        if (commission_block.children().length > 0) {

            $('#calc_commission')[0].innerText = 'Рассчитать комиссию';
            commission_block.empty();
        }
    }

    function deductAmount(currentValue, amount ) {

        if (currentValue > 0){
            if (amount > 0) {
                return currentValue - amount;
            }
            else return currentValue;
        } else {
            return 0;
        }
    }

    function getBoxes() {
        let boxes = $('<select/>', {'class': 'form-control', 'id':'box', 'name':'box_id[]', 'required': 'required'});

        $.ajax({
            url: '/withdrawal/listbox',
            method: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'json',
            success: function(response) {
                console.dir('success my');
                console.dir(response.code);
                if (response.code === 'success') {
                    boxes.append( $('<option></option>').attr('value', '').text('Выберите из списка') );
                    for (item of response.result) {
                        boxes.append(
                            $('<option></option>').attr('value', item.id).text(item.unique_number)
                        );
                    }
                } else {
                    errorMessageBox(response.message, true);
                }
            },
            error: function( jqXHR, exception ) {
                console.log(jqXHR.responseText);
                let msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status === 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status === 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed. ';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                errorMessageBox(msg, true);
            }
        });

        return boxes;
    }

    function errorMessageBox (text) {
        $('#error-message-box').removeAttr('hidden').find('#error-text')[0].innerText = text;
    }

});
