;$(function() {

    $('#send_cash_payment').on('submit', function (e) {
        // console.log('subnm');
        let valid = true;
        $('#debt').find('input[id $= "_rest"]').each(function () {
            // console.log(Number(this.value) > 0);
            if (Number(this.value) > 0) {
                $('#error-text').text('Заявка должна быть полностью оплачена!');
                $('#error-message-box').removeAttr('Hidden');
                valid = false;
            }
        });
        if (!valid) e.preventDefault();
         // e.preventDefault();
    });


    /**
     * Обработка кнопки добавления новой строки с картой.
     * Рисует строку в блоке card_info_block, содержащую три колонки с полем для ввода номера карты,
     * суммы и кнопки "удалить" соответственно
     */
    $('#add_card').on('click', function (e) {

        // $('#calc-amount-block').removeAttr('hidden');
        discardCommission();

        $('#card_info_block').append(
            // $('<br/>'))
            // .append(
            $('<div/>', {'class': 'form-group row card-block required'})
                .append(
                $('<div/>', {'class' : 'row '})
                    .append(
                    $('<div/>', {'class': 'col-lg-5 col-md-4'})
                        .append(
                        $('<label/>', {'class': 'control-label'})
                            .append('Номер карты/кошелька:')))
                    .append(
                    $('<div/>', {'class': 'col-lg-6 col-md-5'})
                        .append(
                        $('<input/>', {'class': 'form-control',
                                        'type': 'text','id':'card_number',
                                        'name':'card_number[]', 'required': 'required',
                                        'autocomplete':'off'})))
                    .append(
                        $('<div/>', {'class':'col-lg-1 col-md-2 col-sm-1 col-xs-1'})
                            .append(
                            $('<div/>', {'class':'delete-card-btn pointer'})
                                .append(
                                $('<i/>', {'class':'fa fa-trash fa-2x'})
                                )
                            )))
                .append(
                $('<div/>', {'class' : 'row'})
                    .append(
                    $('<div/>', {'class': 'col-lg-5 col-md-5'})
                        .append(
                        $('<label/>', {'class': 'control-label'})
                            .append('Сумма:')))
                    .append(
                    $('<div/>', {'class': 'col-lg-6 col-md-6'})
                        .append(
                        $('<input/>', {'class':'form-control', 'type':'number',
                                        'id':'amount', 'name':'amount[]', 'required': 'required'}))))
                .append(
                    $('<div/>', {'class' : 'row'})
                        .append(
                            $('<div/>', {'class': 'col-lg-5 col-md-5'})
                                .append(
                                    $('<label/>', {'class': 'control-label'})
                                        .append('Комментарий:')))
                        .append(
                            $('<div/>', {'class': 'col-lg-6 col-md-6'})
                                .append(
                                    $('<input/>', {'class':'form-control', 'type':'text', 'value':'',
                                        'name':'comment[]', 'required': 'required', 'autocomplete':'off'}))))
        );
        e.preventDefault();

    });


    /**
     * Обработка событий в блоке card_info_block
     */
    $('#card_info_block').on('click', '.delete-card-btn', function () {

        discardCommission();

        let totalAmount = $('#total_amount');

        totalAmount.val(deductAmount(parseInt(totalAmount.val()),
            parseInt($(this).parent().parent().parent().find('#amount').val()))); //Указатель возвращается к предку - общему блоку
                                                                        // и берёт значение из инпута с суммой
        $('#commission_amount').val('');
        $(this).parent().parent().parent().remove();

    })
        .on('keyup', '#amount', function () {
            let totalInput = $('#total_amount');
            totalInput[0].value = 0;
            let totalAmount = 0;
            $('input[name="amount\[\]"]').each(function() {
                console.log(this.value);
                totalAmount += Number( $(this).val() );
            });
            totalInput.val(totalAmount);
            $('#commission_amount').val('');
            discardCommission();
        });

    /**
     * Вычитание суммы
     * currentValue int
     *  текущее значение
     * amount int
     *  вычитаемая сумма
     */
    function deductAmount(currentValue, amount ) {

        if (currentValue > 0){
            if (amount > 0) {
                return currentValue - amount;
            }
            else return currentValue;
        } else {
            return 0;
        }
    }

    function discardCommission() {

        let commission_block = $('#commission_block');
        if (commission_block.children().length > 0) {

            $('#calc_commission')[0].innerText = 'Рассчитать комиссию';
            commission_block.empty();
        }
    }

    $('.readonly').keydown(function(e){
        e.preventDefault();
    });


});
