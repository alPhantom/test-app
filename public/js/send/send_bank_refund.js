;$(function() {

    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $('#add_refund').on('click', function (e) {

        let boxes = '';
        let cards = '';
        let id = $('#id').val();

        let parameters = {
            _token: CSRF_TOKEN,
            id: id,
            currency_id: $('#currency_id').val()
        };

        $.ajax({
            url: '/send/bank_refund',
            method: 'POST',
            data: parameters,
            dataType: 'json',
            success: function (response) {
                if (response.code === 'success') {

                    for (row of response.boxes) {
                        boxes += '<option value="' + row.id + '"> ' + row.unique_number + '</option>'
                    }

                    for (row of response.cards) {
                        cards += '<option value="' + row.id + '"> ' + row.card_number + '</option>';
                    }

                    drawElements(id, boxes, cards);

                } else {
                    showError(response.message)
                }
            },
            error: function (jqXHR, exception) {
                showError(exception);
            }
        });

        e.preventDefault();
    });

    function drawElements(id, boxes, cards) {

        $('#bank_refund_block')
            .append(
                $('<div/>', {'class': 'form-group row required repayment-row'})
                    .append(
                        $('<div/>', {'class': 'col-lg-4 col-md-4'})
                            .append(
                                $('<div/>', {'class': 'form-group input-group-sm'})
                                    .append(
                                        $('<label/>', {'class': 'control-label'}).append('Карта клиента:'))
                                    .append($('<select/>', {
                                        'class': 'form-control',
                                        'id': 'cardlist',
                                        'name': 'send_info[]',
                                        'required': 'required'
                                    })
                                        .append($('<option/>').append('Выберите из списка'))
                                        .append(cards))
                            )
                    ).append(
                    $('<div/>', {'class': 'col-lg-3 col-md-3'})
                        .append(
                            $('<div/>', {'class': 'form-group input-group-sm'})
                                .append($('<label/>', {'class': 'control-label'}).append('Сумма:'))
                                .append($('<input/>', {
                                    'class': 'form-control',
                                    'name': 'refund[]',
                                    'type': 'number',
                                    'required': 'required'
                                }))
                        )
                ).append(
                    $('<div/>', {'class': 'col-lg-4 col-md-4'})
                        .append(
                            $('<div/>', {'class': 'form-group input-group-sm'})
                                .append(
                                    $('<label/>', {'class': 'control-label'}).append('Карта ПКЦ:'))
                                .append($('<select/>', {
                                    'class': 'form-control',
                                    'id': 'boxlist',
                                    'name': 'box_id[]',
                                    'required': 'required'
                                })
                                    .append($('<option/>').append('Выберите из списка'))
                                    .append(boxes))
                        )
                ).append(
                    $('<div/>', {'class': 'col-lg-1 col-md-1 col-sm-1 col-xs-1'})
                        .append($('<div/>', {'class': 'delete-box-btn pointer repayment-button'})
                            .append($('<i/>', {'class': 'fa fa-trash fa-2x'})))
                )
            );
    }

    function showError(text) {

        $('#error-text').html(text);
        $('#error-message-box').removeAttr('hidden');

    }
});