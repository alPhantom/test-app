;$(function() {

    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $('[id^="add_box"]').on('click', function (e) {

        let boxes = '';
        let id = this.id.match(/[0-9]+/)[0];

        let parameters = {
            _token: CSRF_TOKEN,
            currency_id: $('#currency_id').val()
        };

        $.ajax({
            url: '/box/boxlist',
            method: 'POST',
            data: parameters,
            dataType: 'json',
            success: function (response) {
                if (response.code === 'success') {

                    for (row of response.result) {
                        boxes += '<option value="' + row.id + '"> ' + row.unique_number + '</option>'
                    }
                    drawElements(id, boxes);

                } else {
                    showError(response.message)
                }

            },
            error: function (jqXHR, exception) {
                showError(exception);
            }
        });

        e.preventDefault();
    });

    function drawElements(id, boxes) {

        $('#repayment_block_' + id)
            .append(
                $('<div/>', {'class': 'form-group row repayment-row'})
            .append(
                $('<div/>', {'class': 'col-lg-3 col-md-3'})
                    .append(
                    $('<div/>', {'class': 'form-group input-group-sm'})
                        .append(
                            $('<label/>', {'class': 'control-label'}).append('Карта ПКЦ:'))
                        .append($('<select/>', {'class': 'form-control', 'id': 'boxlist' ,'name': 'box_id[]', 'required' : 'required'})
                            .append($('<option/>').append('Выберите из списка'))
                            .append(boxes))
                )
            ).append(
                $('<div/>', {'class': 'col-lg-2 col-md-2'})
                    .append(
                    $('<div/>', {'class': 'form-group input-group-sm'})
                        .append($('<label/>', {'class': 'control-label'}).append('Сумма:'))
                        .append($('<input/>', {'type':'hidden', 'name': 'send_info_id[]', 'value': id}))
                        .append($('<input/>', {'class': 'form-control', 'name' : 'repayment_amount[]', 'type': 'number', 'required' : 'required'}))
                    )
            ).append(
                    $('<div/>', {'class': 'col-lg-2 col-md-2'})
                        .append(
                            $('<div/>', {'class': 'form-group input-group-sm'})
                                .append($('<label/>', {'class': 'control-label'}).append('Комиссия:'))
                                .append($('<input/>', {'class': 'form-control', 'name' : 'commission_amount[]', 'type': 'number'}))
                        )
            ).append(
                $('<div/>', {'class': 'col-lg-2 col-md-2 col-sm-2 col-xs-2'})
                    .append($('<div/>', {'class' : 'delete-box-btn pointer repayment-button'})
                    .append($('<i/>', {'class':'fa fa-trash fa-2x'})))
                )
            );
    }

    $('[id^="repayment_block"]').on('click', '.delete-box-btn', function () {

        $(this).parent().parent().remove();

    });

    $('#close').on('click', function (btn) {

        try {
            $('[id^="request_info"]').each(function (index, elem) {

                let card = $(this).find('[name^="amount_"]').val();

                $(this).find('.repayment-row').each(function (index, elem) {
                    card -=  $(this).find('[name^="repayment_amount"]').val();
                });

                if (card !== 0) {
                    showError('Необходимо погасить задолженность клиенту по карте ' +  $(this).find('[name^="card_number"]').val());
                    btn.preventDefault();
                }

            });

        } catch (e) {
            console.log(e);
            btn.preventDefault();
        }

    });

    function showError(text){

        $('#error-text').html(text);
        $('#error-message-box').removeAttr('hidden');

    }

});