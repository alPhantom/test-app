;(function($) {

    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $('#group').on('change', function () {

        updateContent(this.value);

    });

    function updateContent(groupId) {

        $.ajax({
            url: 'rate/show',
            method: 'POST',
            data: {_token: CSRF_TOKEN, id: groupId},
            dataType: 'json',
            success: function( response ) {

                if ( response.code === 'success' ) {

                    fillTheTable(JSON.parse(response.cashRetail), 'cash_retail');
                    fillTheTable(JSON.parse(response.cashWholesale), 'cash_wholesale');
                    fillTheTable(JSON.parse(response.noncashRetail), 'noncash_retail');
                    fillTheTable(JSON.parse(response.noncashWholesale), 'noncash_wholesale');

                } else {
                    console.dir(response);
                }
            },
            error: function( jqXHR, exception ) {
                console.dir(jqXHR);
            }
        });
    }

    function fillTheTable(rates, type) {

        for (row of rates){

            $('#' + type +'_buy_' + row.exchange_direction_id).val(Number(row.buy).toFixed(5));
            $('#' + type + '_buy_minus_' + row.exchange_direction_id).val(Number(row.buy_minus).toFixed(5));
            $('#' + type + '_buy_plus_' + row.exchange_direction_id).val(Number(row.buy_plus).toFixed(5));
            $('#' + type + '_sale_' + row.exchange_direction_id).val(Number(row.sale).toFixed(5));
            $('#' + type + '_sale_minus_' + row.exchange_direction_id).val(Number(row.sale_minus).toFixed(5));
            $('#' + type + '_sale_plus_' + row.exchange_direction_id).val(Number(row.sale_plus).toFixed(5));

        }
    }

    function getRates() {

        let rates = [];
        let group = $('#group').val();

        $( "tr.rate-row" ).each(function () {

            let direction = /[0-9]+/.exec(this.id).toString(),
                type = /\D+/.exec(this.id).toString();

            let row = {};
            row.group_id = group;
            row.is_retail = this.id.includes('retail') ? 1 : 0;
            row.is_cash = this.id.includes('noncash') ? 0 : 1;
            row.exchange_direction_id = direction;
            row.buy = $(this).find('#' + type +'buy_' + direction).val();
            row.buy_minus = $(this).find('#' + type + 'buy_minus_' + direction).val();
            row.buy_plus = $(this).find('#' + type + 'buy_plus_' + direction).val();
            row.sale = $(this).find('#' + type + 'sale_' + direction).val();
            row.sale_minus = $(this).find('#' + type + 'sale_minus_' + direction).val();
            row.sale_plus = $(this).find('#' + type + 'sale_plus_' + direction).val();

            rates.push(row);

        });

        return rates;

    }

    $('#edit_button').on('click', function () {

        editMode(true);

    });

    $('#save_cancel_block').on('click', '#cancel_button', function () {

        editMode(false);
        updateContent($('#group').val());

    }).on('click', '#save_button', function () {

        let valid = true;
        $('input[type="number"]').filter('[required]').each(function () {
            if (this.value === '' || this.value === null){
                valid = false;
                console.dir('empty');
            }
        });
        if (valid) {
            let parameters = {
                _token: CSRF_TOKEN,
                group_id: $('#group').val(),
                rates: getRates()
            };


            $.ajax({
                url: 'rate/store',
                method: 'POST',
                data: parameters,
                dataType: 'json',
                success: function (response) {

                    if (response.code === 'success') {

                        editMode(false);
                        successMessageBox('Данные были успешно обновлены!')

                    } else {
                        errorMessageBox(response.message);
                    }
                },
                error: function (jqXHR, exception) {
                    errorMessageBox(exception);
                }
            });
        } else {
            errorMessageBox('Должны быть заполнены все поля!')
        }
    });

    function editMode(on) {

        if (on) {

            $('#edit_block').attr('hidden', 'true');
            $('#group').attr('disabled', 'true');
            $('#group_edit').attr('disabled', 'true');
            $('#save_cancel_block').removeAttr('hidden');
            $('#content').removeAttr('disabled');

        } else {

            $('#save_cancel_block').attr('hidden', 'true');
            $('#edit_block').removeAttr('hidden');
            $('#group').removeAttr('disabled');
            $('#group_edit').removeAttr('disabled');
            $('#content').attr('disabled', 'true');
        }
    }

    //
    $('#group_modal').on('hidden.bs.modal', function () {

        let list = $('#group');
        let selected = list.val();

        $.ajax({
            url: 'office_group/list',
            method: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'json',
            success: function(response) {
                console.dir(response);
                if (response.code === 'success') {

                    list[0].innerHTML = '';

                    list.append($('<option/>', {'value': 'null'}).append('Выберите из списка'));

                    for (item of response.result){

                        list.append(
                            $('<option/>', {'value': item.id, 'selected': (item.id === Number(selected))}).append(item.descr)
                        );
                    }

                    if (list.val() === 'null'){
                        list.val(0);
                        updateContent(0)
                    }

                } else {
                    errorMessageBox(response.message, true);
                }
            },
            error: function( jqXHR, exception ) {
                errorMessageBox(exception, true);
            }
        });

    });



    function errorMessageBox (text) {
        $('#error-message-box').removeAttr('hidden').find('#error-text')[0].innerText = text;
    }

    function successMessageBox (text) {
        $('#success-message-box').removeAttr('hidden').find('#success-text')[0].innerText = text;
    }

})(jQuery);