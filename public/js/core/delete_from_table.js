/**
 * Удаление строк из любой таблицы используя хранимую процедуру или delete запрос.
 */

;$(function() {

    // id удаляемой строки.
    var currentId = null;

    // Запоминаем строку которую будем удалять.
    $( '.button-delete' ).click( function() {
        currentId = this.id;
        $( '#confirm-code' ).text( currentId );
    } );

    // Если подтвердили, что будут удалять.
    $( '#confirm-delete' ).click( function() {

        if ( currentId !== null ) {

            var clientRequest = {
                'client_request': JSON.stringify({
                    table_sp: $( 'table' ).attr( 'table_sp' ),
                    table_id: currentId
                })
            };

            $.ajax({
                url: '/delete',
                method: 'POST',
                data: clientRequest,
                dataType: 'json',
                success: function( serverResponse ) {
                    var jsonErrorSQL = JSON.parse( JSON.stringify( serverResponse ) );

                    if ( jsonErrorSQL['error_text'].length === 0 ) {
                        $( '#row' + String( currentId ) ).remove();
                        $( '#success-text' ).html( 'Запись из базы данных успешно удалена.' );
                        $( '#success-time' ).html( ( new Date ).toLocaleString() );
                        $( '#success-message-box' ).removeAttr( 'hidden' );
                    } else {
                        $( '#error-text' ).html( jsonErrorSQL['error_text'] );
                        $( '#error-time' ).html( jsonErrorSQL['error_time'] );
                        $( '#error-message-box' ).removeAttr( 'hidden' );
                    }
                },
                error: function ( jqXHR, exception ) {
                    var msg = '';
                    var now = ( new Date ).toLocaleString();

                    if ( jqXHR.status === 0 ) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if ( jqXHR.status === 404 ) {
                        msg = 'Requested page not found. [404]';
                    } else if ( jqXHR.status === 500 ) {
                        msg = 'Internal Server Error [500].';
                    } else if ( exception === 'parsererror' ) {
                        msg = 'Requested JSON parse failed.';
                    } else if ( exception === 'timeout' ) {
                        msg = 'Time out error.';
                    } else if ( exception === 'abort' ) {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }

                    $( '#error-text' ).text( msg );
                    $( '#error-time' ).text( now );
                    $( '#error-message-box' ).removeAttr( 'hidden' );
                }
            });

        }

    });

});