/**
 * Открывает окно печати бланка.
 *
 * Например:
 * location.pathname - /exchange имя контроллера.
 * /blank - имя экшина.
 * this.id - 15
 *
 * Итого: /exchange/blank/15
 */
;$(function() {

    $( '.pkc-blanks' ).click(function() {

        // Открыть страницу с бланком в новом окне.
        var urlWindow = location.pathname + '/blank/' + this.id,
            nameWindow = 'modal_print_blank',
            printWindow = window.open( urlWindow, nameWindow );

        // Модально открыть окно предварительного просмотра печати.
        printWindow.addEventListener( 'load', function(){
            printWindow.print();
            printWindow.close();
        }, true);

    }); // end: $( '.pkc-blanks' ).click

});