/**
 * Если у тега table есть класс show-search-pagination
 * то таблица становится "отзывчевой".
 * К ней добавляется:
 *  Сколько строк показывать.
 *  Поле поиска.
 *  Пагинация.
 *
 */
;$(function() {

    $('.show-search-pagination').DataTable({
        responsive: true,
        pageLength: 50,
        language: {
            processing: "Подождите...",
            search: "Поиск:",
            lengthMenu: "Показать _MENU_ записей",
            info: "Записи с _START_ до _END_ из _TOTAL_ записей",
            infoEmpty: "Записи с 0 до 0 из 0 записей",
            infoFiltered: "(отфильтровано из _MAX_ записей)",
            infoPostFix: "",
            loadingRecords: "Загрузка записей...",
            zeroRecords: "Записи отсутствуют.",
            emptyTable: "В таблице отсутствуют данные",
            paginate: {
                first: "Первая",
                previous: "Предыдущая",
                next: "Следующая",
                last: "Последняя"
            },
            aria: {
                sortAscending: ": активировать для сортировки столбца по возрастанию",
                sortDescending: ": активировать для сортировки столбца по убыванию"
            }
            // url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Russian.json'
        }
    });

});