
;$(function() {

    //Раскрывает выпадающий список
    $('.dropdown dt input[type="text"]').on('click', function () {
        $(".dropdown dd ul").slideToggle('fast');
    });

    //Прячет выпадающий список, если был клик за областью списка
    $(document).bind('click', function (e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
    });

    //Заполнение инпутов при check uncheck элемента checkbox'a
    $('.mutli_select input[type="checkbox"]').on('click', function () {


        let checked = $('#checked')[0];
        let checkboxValue = this.dataset['text'];
        let inputValue = checked.value;

        if ($(this).is(':checked')) {
            inputValue += ',' + checkboxValue;

        } else {
            inputValue = inputValue.replace(checkboxValue, '');
        }

        //Убирает запятые в начале, в конце и двойные и записывает значения
        checked.value = inputValue.replace(/^,+/g,'').replace(',,',',').replace(/,+$/g,'');

    });

    $('.readonly').on('keydown', function(e){
        e.preventDefault();
    });

});