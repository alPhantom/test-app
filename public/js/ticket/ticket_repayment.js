;$(function() {

    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $('[id^="add_box"]').on('click', function (e) {

        let boxes = '';
        let parent = $(this).parent().parent();
        // console.log(parent);
        let parameters = {
            _token: CSRF_TOKEN,
            currency_id: $('#currency_id').val()
        };
        // e.preventDefault();

        $.ajax({
            url: '/box/boxlist',
            method: 'POST',
            data: parameters,
            dataType: 'json',
            success: function (response) {
                if (response.code === 'success') {

                    for (row of response.result) {
                        boxes += '<option value="' + row.id + '">' + row.unique_number + '</option>'
                    }
                    drawElements(parent, boxes);

                } else {
                    showError(response.message)
                }

            },
            error: function (jqXHR, exception) {
                showError(exception);
            }
        });

        e.preventDefault();
    });

    function drawElements(parent, boxes) {
        parent.find('[id^="repayment_block"]')
            .append(
                $('<div/>', {'class': 'form-group row repayment-row'})
                    .append(
                        $('<div/>', {'class': 'col-lg-6 col-md-6'})
                            .append(
                                $('<div/>', {'class': 'form-group input-group-sm'})
                                    .append(
                                        $('<label/>', {'class': 'control-label'}).append('Карта ПКЦ:'))
                                    .append($('<select/>', {'class': 'form-control', 'id': 'boxlist' ,'name': 'box_id[]', 'required' : 'required'})
                                        .append($('<option/>').append('Выберите из списка'))
                                        .append(boxes))
                            )
                    ).append(
                    $('<div/>', {'class': 'col-lg-4 col-md-4'})
                        .append(
                            $('<div/>', {'class': 'form-group input-group-sm'})
                                .append($('<label/>', {'class': 'control-label'}).append('Сумма:'))
                                .append($('<input/>', {'class': 'form-control', 'name' : 'repayment_amount[]', 'type': 'number', 'required' : 'required'}))
                        )
                ).append(
                    $('<div/>', {'class': 'col-lg-2 col-md-2 col-sm-2 col-xs-2'})
                        .append($('<div/>', {'class' : 'delete-box-btn pointer repayment-button'})
                            .append($('<i/>', {'class':'fa fa-trash fa-2x'})))
                )
            );
    }

    $('[id^="repayment_block"]').on('click', '.delete-box-btn', function () {

        $(this).parent().parent().remove();

    });

    $('#close').on('click', function (btn) {

        try {
            let total = Number($('#total_amount').val());
            let repayed = 0;
            $('#repayment_block').find('[name^="repayment_amount"]').each(function (index, elem) {

                repayed += elem.value;

            });
            console.log(total);
            console.log(repayed);

            if (repayed < total) {
                showError('Заявка должна быть полностью оплачена!');
                btn.preventDefault();
            }

        } catch (e) {
            console.log(e);
            btn.preventDefault();
        }

    });

    function showError(text){

        $('#error-text').html(text);
        $('#error-message-box').removeAttr('hidden');

    }

});