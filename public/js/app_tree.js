;(function($) {

    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    // Инициализируем нужные переменные
    var urlGetTree = '/node/get_tree',
        urlUpdateNode = '/node/update_node',
        urlCreateNode = '/node/create_node',
        urlDeleteNode = '/node/delete_node',
        currentId = 123,
        currentParent=null,
        ui = {
            node_tree: $( '#node_tree' )
        };

    // Инициализация дерева категорий с помощью jstree
    function initTree( data ) {
        ui.node_tree.jstree({
            core: {
                check_callback: true,
                multiple: false,
                data: data
            },
            plugins: ['dnd']
        }).bind( 'changed.jstree', function( e, data ) {

            if (data.node === undefined) {
                $('#description').attr('hidden', true);
                return;
            }

            currentId = data.node.id;
            currentParent = data.node.parent;
            console.dir(currentId);
            $('#description').removeAttr('hidden');
            $('#descr').val(data.node.li_attr['path']);
            $('#note').val(data.node.text);

            if (data.node.li_attr['show'] === 1)
                $('#show').attr('checked','checked');
            else
                $('#show').removeAttr('checked');

            $('#menu_descr').val(data.node.li_attr['menu_descr']);

            if (data['node']['icon']) $('#icon').val(data.node.icon);
        }).bind( 'move_node.jstree', function( e, data ) {

            ui.node_tree.jstree("select_node", "#" + data.node.id);
            currentParent = data.node.parent;
        });

    }

    // Загрузка категорий с сервера
    function loadTree() {
        $.ajax({
            url: urlGetTree,
            method: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'json',
            success: function( response ) {
                // Инициализируем дерево категорий
                if ( response.code === 'success' ) {
                    initTree( response.result );
                } else {
                    showError(response.message);
                }
            }, // end: success: function
            error: function( jqXHR, exception ) {
                showError(exception);
            } // end: error: function
        }); // end: $.ajax
    }

    //
    function updateTree(params, url){

        $.ajax({
            url: url,
            method: 'POST',
            data: params,
            dataType: 'json',
            success: function( response ) {
                // Инициализируем дерево категорий
                if ( response.code === 'success' ) {

                        ui.node_tree.jstree(true).settings.core.data = response.result;
                        ui.node_tree.jstree(true).refresh();

                } else {
                    showError(response.message);
                }
            }, // end: success: function
            error: function( jqXHR, exception ) {
                showError(exception);
            } // end: error: function
        });
    }


    $( '#button_create' ).on('click', function() {

        ui.node_tree.jstree(true).create_node(currentId);

    });

    $('#button_update').on('click', function () {

        let show = $('#show').is(":checked") ? 1 : 0;

        let params = {
            _token: CSRF_TOKEN,
            id: currentId,
            parent_id: currentParent,
            descr: $('#descr').val(),
            note: $('#note').val(),
            show: show,
            menu_descr: $('#menu_descr').val(),
            icon: $('#icon').val()
        };

        let url = (/^[0-9]+$/.test(currentId)) ? urlUpdateNode : urlCreateNode;

        updateTree(params, url);

    });
    
    $('#button_delete').on('click', function () {

        if((/^[0-9]+$/.test(currentId))) {

            let params = {
                _token: CSRF_TOKEN,
                id: currentId
            };
            updateTree(params, urlDeleteNode);

        } else ui.node_tree.jstree(true).refresh();
    });

    // Инициализация приложения
    function init() {
        loadTree();
    }

    // Загружаем дерево при старте
    init();

    function showError(exception) {

        let err =  $('#error');

        $('#img_load').attr('hidden', true);

       err.removeAttr('hidden');
       err[0].innerHTML = exception;

    }

})(jQuery);
