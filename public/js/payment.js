;$(function() {

    $('[id$="payment_block"]').on('keyup', 'input[name ^= "payment"]', function () {

        let block = $(this).parent().parent().parent().parent();
        let total = Number(block.prev().prev().find('[id $= "full_amount"]')[0].innerText);
        let equals = 0;

        block.find('input[name ^= "payment"]').each( function () {

            let row = $(this).parent().parent().parent();
            let amount = this.value;
            let rate;
            let elem;

            if(amount >= Number(this.dataset['border'])) {
                elem = row.find('[id $="wholesale"]')[0];

            } else {
                elem = row.find('[id $="retail"]')[0];
            }

            let rateRub = elem.dataset['rub_rate'];
            rate = Number(elem.innerText);
            setRate(row, amount, rate, rateRub);

            equals += amount*rate;

        });

        let rest = total - equals;

        if (rest === 0){

            block.next().find('input[id $= "_rest"]').each(function (){

                this.value = 0;
            });
        } else {

            let currency = $('#operation_currency')[0].innerText;
            let input = block.find('[id $="'+currency+'_amount"]')[0];

            block.next().find('input[id $= "_rest"]').each(function () {

                let row = $('[id$="' + this.dataset['currency']+'_payment"]');
                console.log('keyup');
                let rate;
                if(rest >= Number(input.dataset['border'])) {
                    rate = Number(row.find('[id $="wholesale"]')[0].innerText);
                } else {
                    rate = Number(row.find('[id $="retail"]')[0].innerText);
                }
                this.value = Math.round(rest/rate * 10000) / 10000;

            });

        }

    });

    function setRate(row, amount, rate, rateRub){
        if (amount !==''){
            row.find('[name^="rub_rate"]').val(rateRub);
            row.find('[name^="rate"]').val(rate);
        } else {
            row.find('[name^="rub_rate"]').val('');
            row.find('[name^="rate"]').val('');
        }
    }

});