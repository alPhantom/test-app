;$(function () {

    $('#user_form').on('submit', function (e) {

        if ($('#password').val() !== $('#new_password').val()) {

            $( '#error-text' ).text( 'Пароли должны совпадать! ' );
            $( '#error-time' ).text(  ( new Date ).toLocaleString() );
            $( '#error-message-box' ).removeAttr( 'Hidden' );

            e.preventDefault();
        }

    });

    $('#phone').on('keyup', function () {

        $('#login_phone').val(this.value.match(/[0-9]+/g).join(''));

    });

    $('#password').on('keyup', function () {

        let newPass = $('#new_password');
        let labelPass = $('#pass_label');

        if (this.value !== '') {
            newPass.attr('type', 'password');
            newPass.val('');
            labelPass.removeAttr('hidden');

        } else {
            newPass.attr('type', 'hidden');
            labelPass.attr('hidden', 'true');
            newPass.val('');

        }

        newPass[0].style.boxShadow = null;

    });

    $('#new_password').on('keyup', function () {

        let password = $('#password').val();

        if (password === this.value) {
            this.style.boxShadow = "1px 1px 10px green";
        } else {
            this.style.boxShadow = "1px 1px 10px red";
        }

    });

});