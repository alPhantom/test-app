;(function($) {

    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


    $('#add_group_button').on('click', function(){

        $('#group_block').append(
            $('<div/>', {'class': 'form-group input-group-sm row'}).append(
                $('<div/>', {'class': 'col-lg-4'}).append(
                    $('<input/>', {'type': 'text', 'class': 'form-control', 'name': 'descr', 'autocomplete': 'off'})
                )).append(
                $('<div/>', {'class': 'col-lg-2'}).append(
                    $('<label/>', {'class': 'form-check-label'}).append(
                        $('<input/>', {'type': 'checkbox', 'class': 'form-check-input', 'name': 'is_archive'})
                ).append(' Архив'))).append(
                $('<div/>', {'class': 'col-lg-2'}).append(
                    $('<button/>', {'class': 'btn btn-default', 'name':'change_group_button'}).append('Подвердить'))
                ).append(
                $('<div/>', {'class': 'col-lg-2' , 'align': 'right'}).append(
                    $('<button/>', {'class': 'btn btn-default', 'name':'delete_group_button'}).append('Удалить'))
                ).append(
                    $('<input/>', {'type':'hidden', 'name': 'descr_old'})
                ).append(
                    $('<input/>', {'type':'hidden', 'name': 'is_archive_old'})
                ).append(
                    $('<input/>', {'type':'hidden', 'name': 'id'})
                )
            );
    });

    /*
     События в блоке с группами отделений
      */
    // Кнопка "Изменить"
    $('#group_block').on('click', '[name^="change_group_button"]', function () {

        let parent = $(this).parent().parent();
        let descr = parent.find('[name="descr"]');
        let isArchive = parent.find('[name="is_archive"]');
        let id = parent.find('[name^="id"]');

        // Активировать строку для изменения
        if ('Изменить' === this.innerText) {
            this.innerText = 'Подтвердить';
            descr.removeAttr('disabled');
            isArchive.removeAttr('disabled');
            parent.append(
                $('<div/>', {'class':'col-lg-2'}).append(
                    $('<button/>', {'class': 'btn btn-link', 'name':'discard_group_button'}).append('Отмена'))
            );
        } else {
            // Подтвердить изменения - заблокировать строку и добавить db
            if (validInput(descr.val())) {

                let parameters = {
                    _token: CSRF_TOKEN,
                    descr: descr.val(),
                    is_archive: Number(isArchive.is(':checked')),
                    id: id.val()
                };

                let button = this;

                sendChanges('office_group/update', parameters, function (response) {

                    button.innerText = 'Изменить';
                    descr.attr('disabled', 'true');
                    isArchive.attr('disabled', 'true');
                    parent.find('[name^="descr_old"]').val(response.descr);
                    parent.find('[name^="id"]').val(response.id);
                    parent.find('[name^="discard_group_button"]').remove();
                    parent.find('[name^="is_archive_old"]').prop('checked', response.is_archive === 1);
                });
            }
        }

    //    Кнопка Удалить (для существующих групп)
    }).on('click', '[name="delete_group_button"]', function () {

        let parent =  $(this).parent().parent();
        let currentId = parent.find('[name^="id"]').val();
        let parameters = {
            _token: CSRF_TOKEN,
            id: currentId
        };
        if (parent.find('[name^="id"]').val() === ''){
            parent.remove();

        } else {
            sendChanges('office_group/delete', parameters, function () {
                parent.remove();
            });
        }

    //    Кнопка Отмена - заблокировать строку и отменить не применённые изменения
    }).on('click', '[name^="discard_group_button"]', function () {

        let parent =  $(this).parent().parent();
        returnOldValues(parent);
        parent.find('[name^="change_group_button"]')[0].innerText = 'Изменить';

        $(this).parent().remove();

    });

    function sendChanges(url, parameters, callback) {

        $.ajax({
            url: url,
            method: 'POST',
            data: parameters,
            dataType: 'json',
            success: function(response) {
                if (response.code === 'success') {
                    callback(response.result);
                    showError(response.message, false);
                } else {
                    showError(response.message, true);
                }
            },
            error: function( jqXHR, exception ) {
                showError(exception, true);
            }
        });
    }

    function returnOldValues(parent) {

        let descr = parent.find('[name="descr"]');
        let isArchive = parent.find('[name="is_archive"]');
        descr.attr('disabled', 'true')
            .val(parent.find('[name^="descr_old"]').val());
        isArchive.attr('disabled', 'true')
            .prop('checked', parent.find('[name^="is_archive_old"]').val() === '1');
    }

    function validInput(input){

        if (input === undefined || input.replace(/\s/g, '').length === 0){
            showError('Недопустимо пустое название группы!', true);
            return false;
        }

        return true;
    }

    // Отменить все изменения формы и очистить массивы объекта groups
    $('#cancel_group_button').on('click', function () {

        // todo: reload list of groups in select
    });


    function showError(text, error) {
        let cl = error ? 'alert alert-danger' : 'alert alert-success';
        $('#group_error_block').attr('class', cl).removeAttr('hidden')[0].innerText = text;
    }

})(jQuery);