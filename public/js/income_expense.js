;$(function() {

    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $('#box_id').on('change', function () {

        let parameters = {
            box_id: $(this).find(':selected').val(),
            _token: CSRF_TOKEN
        };

        $.ajax({
            url: '/nbu/rate',
            method: 'POST',
            data: parameters,
            dataType: 'json',
            success: function (response) {

            },
            error: function (jqXHR, exception) {

                let msg = '';

                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status === 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status === 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed. ';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }

                showError(msg);
            }
        });

    });

    function showError(text){

        $('#error-text').html(text);
        $('#error-message-box').removeAttr('hidden');

    }

});