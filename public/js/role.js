;(function($) {

    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    let urlGetTree = '/node/get_tree',
        urlGetPermissions = '/role/get_permissions',
        urlSaveRole = '/role/create',
        urlUpdateRole = $(location).attr("href"),
        node_tree = $( '#node_tree' );

    let permissions = {
        data: [],
        deleteByNodeId: function(id){
            this.data.forEach(function(item, i, arr) {
                if (item['node_id'] === id){
                    arr.splice(i, 1);
                }
            });
        },
        update: function (id, access) {
            let hasMatches = false;
            this.data.forEach(function(item, i, arr) {
                if (item['node_id'] === id){
                    hasMatches = true;
                    arr[i]['access_type'] = access;
                }
            });

            if (!hasMatches)
                this.data.push({
                    node_id: id,
                    access_type: access
                });
        },
        getByNodeId: function (id) {
            return this.data.find(function (element, index, array) {
                return element['node_id'] === id;
            });
        }
    };
    let roleId = $('#id').val();
    let nodeId;


    // Инициализация дерева категорий с помощью jstree
    function initTree( data ) {
        node_tree.jstree({
            core: {
                check_callback: true,
                multiple: false,
                data: data
            }
        }).bind( 'changed.jstree', function( e, data ) {

            nodeId = Number(data.node.id);
            $('#description').removeAttr('hidden');

            let permission = permissions.getByNodeId(nodeId);

            if (permission !== undefined){
                $('#'+ permission['access_type']).prop("checked", true);
            } else {
                $('#none').prop("checked", true);
            }
        });
    }

    // Загрузка категорий с сервера
    function loadTree() {
        $.ajax({
            url: urlGetTree,
            method: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'json',
            success: function( response ) {

                if ( response.code === 'success' ) {

                    initTree( response.result );
                } else {
                    showError(response.message);
                }
            },
            error: function( jqXHR, exception ) {
                showError(jqXHR.responseText);
            }
        }); // end: $.ajax
    }

    function loadPermissions() {

        $.ajax({
            url: urlGetPermissions,
            method: 'POST',
            data: {_token: CSRF_TOKEN, id: roleId},
            dataType: 'json',
            success: function( response ) {

                if ( response.code === 'success' ) {
                    permissions.data = response.result;
                //
                // } else {
                //     showError(response.message);
                }
            },
            error: function( jqXHR, exception ) {
                showError(jqXHR.responseText);
            }
        }); // end: $.ajax

    }
    
    $('input:radio[name="access"]').on('change', function () {

        if (this.value !== 'none')
            permissions.update(nodeId, this.value);
        else permissions.deleteByNodeId(nodeId);
        
    });
    
    $('#button_save').on('click', function () {

        if (validateSubmit()) {

            let role = $('#role').val();
            let roleNote = $('#note').val();
            let url = roleId === '' ? urlSaveRole : urlUpdateRole;

            $.ajax({
                url: url,
                method: 'POST',
                data: {_token: CSRF_TOKEN, id: roleId, descr: role, note: roleNote, data: permissions.data},
                dataType: 'json',
                success: function (response) {
                    window.location.href = response.location;
                },
                error: function (jqXHR, exception) {
                    showError(jqXHR.responseText);

                }
            }); // end: $.ajax
        }

    });


    // Инициализация приложения
    function init() {
        loadTree();
        loadPermissions();
    }

    // Загружаем дерево при старте
    init();

    function validateSubmit() {

        let role = $('#role').val();
        let roleNote = $('#note').val();

        if (role === '')
            showError('Заполните поле "Роль"!');
        else if (roleNote=== '')
            showError('Заполните поле "Описание"!');
        else if (!/^[A-Za-z_]+$/.test(role))
            showError('Поле "Роль" может содержать только латинские буквы и символ нижнего подчёркивания!');
        else return true;
        return false;

    }

    function showError(exception) {

        let err =  $('#error');

        $('#img_load').attr('hidden', true);

        err.removeAttr('hidden');
        err[0].innerHTML = exception;

    }

})(jQuery);
