;$(function() {

    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    /**
     * Расчёт комиссии - кнопка
     */
    $('#calc_commission').on('click', function () {

        if (this.innerText === 'Рассчитать комиссию') {
            calculateCommission(this);
        } else {
            acceptCommission(this);
        }

    });


    /**
     *  Расчёт комиссии - ajax; отрисовка элементов формы
     * @param btn кнопка (т.к. меняется название при нажатии)
     */
    function calculateCommission(btn){

        let serviceType = $('[name = "cross_service_id"]').val();
        let amount = $('#total_amount').val();
        let currency = $('[name = "currency_id"]').val();

        if (!serviceType) {
            showCommissionError('Не заполнено поле (Вид операции)');
        } else if (amount === 0) {
            showCommissionError('Не заполнено ни одного поля с суммой');
        } else if (!currency) {
            showCommissionError('Не выбрана валюта');
        } else {

            let parameters = {
                _token: CSRF_TOKEN,
                service_type_id: serviceType,
                amount: amount,
                currency_id: currency
            };

            $.ajax({
                url: '/tariff/commission',
                method: 'POST',
                data: parameters,
                dataType: 'json',
                success: function (response) {

                    if (response.code === 'success') {

                        let commission_block = $('#commission_block');

                        commission_block.empty();

                        let commission = response.commission;
                        let discounts = response.discounts;

                        if (commission.length === 0 && discounts.length === 0){
                            commission_block.append(
                                '<div class="form-group" align="center">\n' +
                                '     <label class="help-block"> Тарифы не заданы для данного вида операции</label>\n' +
                                '</div>\n ');
                        } else {
                            // Вывести необходимые тарифы
                            for (row of commission) {

                                commission_block.append(
                                    '<div class="row commission-row">\n' +
                                    '     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">\n' +
                                    '          <label class="control-label">' + row.tariff_type_descr + ':</label>\n' +
                                    '          <input\n' +
                                    '                type="hidden"\n' +
                                    '                name="tariff_id[]"\n' +
                                    '                id="tariff_id' + row.id + '"\n' +
                                    '                value="' + row.id + '"\n' +
                                    '          />' +
                                    '     </div>\n' +
                                    '     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">\n' +
                                    '          <input\n' +
                                    '                type="number"\n' +
                                    '                class="form-control"\n' +
                                    '                name="tariff[]"\n' +
                                    '                id="tariff_sum' + row.id + '"\n' +
                                    '                value="' + row.tariff + '"\n' +
                                    '                data-sign="' + row.plus_minus + '"\n' +
                                    '                data-type="' + row.percent_unit + '"\n' +
                                    '                data-value="' + row.tariff + '"\n' +
                                    '          />\n' +
                                    '     </div>\n' +
                                    '     <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">\n' +
                                    '          <input\n' +
                                    '                type="text"\n' +
                                    '                class="form-control"\n' +
                                    '                name="tariff_comment[]"\n' +
                                    '                id="tariff_comment' + row.id + '"\n' +
                                    '                autocomplete="off"\n' +
                                    '                placeholder="Введите комментарий"\n' +
                                    '           />\n' +
                                    '     </div>\n' +
                                    '</div>');
                            }

                            //Если запрос вернул акции/скидки
                            if (discounts.length > 0) {
                                //нарисовать выпадающий список
                                commission_block.append(
                                    '<div class="row commission-row" id="discount_block">\n' +
                                    '    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">\n' +
                                    '          <input\n' +
                                    '                type="hidden"\n' +
                                    '                name="tariff_id[]"\n' +
                                    '                id="tariff_id_s"\n' +
                                    '                value=""\n' +
                                    '          />' +
                                    '         <select class="form-control" id="tariff_discount">\n' +
                                    '           <option value="">Выбрать акцию</option>\n' +
                                    '         </select>\n' +
                                    '    </div>\n' +
                                    '</div>\n'
                                );
                                //заполнить его значениями
                                for (row of discounts) {
                                    $('#tariff_discount').append($('<option/>')
                                        .val(row.id)
                                        .text(row.tariff_type_descr)
                                        .attr('data-tariff', row.tariff)
                                        .attr('data-sign', row.plus_minus)
                                        .attr('data-type', row.percent_unit));
                                }
                            }
                        }

                        commission_block.append('<br/>').append(
                            '<div class="form-group input-group">\n' +
                            '    <span class="input-group-addon"> Сумма с комиссией</span>\n' +
                            '    <input type="number" class="form-control readonly" name="commission_amount" id="commission_amount" required>\n' +
                            '</div>'
                        );

                        btn.innerText = 'Подтвердить';

                    } else {

                        showCommissionError(response.message);
                    }

                },
                error: function (jqXHR, exception) {

                    let msg = '';

                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status === 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status === 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed. ';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }

                    showCommissionError(msg);
                }
            });
        }
    }

    /**
     * Обработка подтверждения комиссии
     * Пересчитывает "Сумму с комисией" и блокирует поля в Комиссии
     */
    function acceptCommission(btn){

        //  Проверить на изменение и на наличие комментария
        let valid = true;
        let tariffs = [];
        $('[id^="tariff_sum"]').each(function (index, elem) {

            if (elem.value !== elem.dataset['value'] && $(this).parent().next().find('[name^="tariff_comment"]')[0].value === ''){
                valid = false;
            }

            tariffs.push({
                tariff: Number(elem.value),
                sign: elem.dataset['sign'],
                type: elem.dataset['type']
            });
        });

        if (valid) {

            let clearAmount = Number($('#total_amount').val());
            let totalAmount = clearAmount;
            // Расчитать итоговую сумму
            tariffs.forEach(function (value) {

                if (value.sign === 'plus') {
                    if (value.type === 'unit') {
                        totalAmount += value.tariff;
                    } else {
                        totalAmount += (clearAmount/100)*value.tariff;
                    }
                } else{
                    if (value.type === 'unit') {
                        totalAmount -= value.tariff;
                    } else {
                        totalAmount -= (clearAmount/100)*value.tariff;
                    }
                }

            });
            // Записать итоговую сумму
            $('#commission_amount').val(Math.ceil(totalAmount));
            // Заблокировать элементы
            $('[id ^= "tariff"]').each(function () {
                $(this).attr('readonly', 'true');
            });
            $('#tariff_discount').attr('disabled', 'true');

            btn.innerText = 'Рассчитать комиссию';

        } else {

            showCommissionError('При изменении значения тарифа необходимо указать причину в комментариях!');
        }

    }

    /**
     * Обработка события вобор акции из выпадающего списка
     * Выводит инпут с выбранным значением акции/скидки
     */
    $('#commission_block').on('change', '#tariff_discount', function () {

        let option = $(this).find(':selected')[0];
        let hiddenId = $(this).parent().find('#tariff_id_s')[0];

        hiddenId.value = option.value;

        $('#discount_value').remove();

        if (option.dataset['tariff'] !== undefined) {

            $('#discount_block').append(
                '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" id="discount_value">\n' +
                '    <input \n' +
                '           type="number"\n' +
                '           class="form-control readonly"\n' +
                '           name="tariff[]"\n' +
                '           id="tariff_sum' + (option.value + 100) + '"\n' +
                '           value="' + option.dataset['tariff'] + '"\n' +
                '           data-sign="' + option.dataset['sign'] + '"\n' +
                '           data-type="' + option.dataset['type'] + '"\n' +
                '           data-value="' + option.dataset['tariff'] + '"\n' +
                '           readonly\n' +
                '    />\n' +
                '</div>\n'
            );

        }
    }).on('keydown', '.readonly', function(e){
        e.preventDefault();
    });

    function discardCommission() {

        let commission_block = $('#commission_block');
        if (commission_block.children().length > 0) {

            $('#calc_commission')[0].innerText = 'Рассчитать комиссию';
            commission_block.empty();
        }
    }

    function checkCommission(){

        let amount = $('#commission_amount');

        return amount !== undefined && amount.val().toString() !== '';

    }

    // To make field readonly
    $('.readonly').keydown(function(e){
        e.preventDefault();
    });

    function showCommissionError(text){

        $('#error-text').html(text);
        $('#error-message-box').removeAttr('hidden');

    }

});