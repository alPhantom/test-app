-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: express_db
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book_bush`
--

DROP TABLE IF EXISTS `book_bush`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_bush` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(80) NOT NULL COMMENT 'Куст',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_bush$descr` (`descr`),
  KEY `fk$book_bush$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$book_bush$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$book_bush$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_bush$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Куст';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_bush`
--

LOCK TABLES `book_bush` WRITE;
/*!40000 ALTER TABLE `book_bush` DISABLE KEYS */;
INSERT INTO `book_bush` VALUES (1,'Касса',0,'2017-09-29 13:30:18','express_user@%',NULL,NULL,NULL,NULL),(2,'Нечаев',0,'2017-09-29 13:30:18','express_user@%',NULL,NULL,NULL,NULL),(3,'Попов',0,'2017-09-29 13:30:18','express_user@%',NULL,'2017-11-06 08:34:11','express_user@%',NULL),(4,'Кристина',0,'2017-09-29 13:30:18','express_user@%',NULL,'2017-12-19 12:55:27','express_user@%',NULL);
/*!40000 ALTER TABLE `book_bush` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_bush$before_ins
BEFORE INSERT
ON book_bush FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_bush$before_upd
BEFORE UPDATE
ON book_bush FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_category`
--

DROP TABLE IF EXISTS `book_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(60) NOT NULL COMMENT 'Название категории',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_category$descr` (`descr`),
  KEY `fk$book_category$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$book_category$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$book_category$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$book_category$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$book_category$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_category$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_category$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_category$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Категории систем и сервисов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_category`
--

LOCK TABLES `book_category` WRITE;
/*!40000 ALTER TABLE `book_category` DISABLE KEYS */;
INSERT INTO `book_category` VALUES (1,'Карты',NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:12:25','express_user@%',NULL,NULL),(2,'Кошельки',NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:12:25','express_user@%',NULL,NULL),(3,'Международные переводы',NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:12:25','express_user@%',NULL,NULL),(4,'Кассы',NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:12:25','express_user@%',NULL,NULL),(5,'Продукты ПКЦ',NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:12:25','express_user@%',NULL,NULL),(6,'Колибри',NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:12:25','express_user@%',NULL,NULL),(7,'Расчетный счет',NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:31:24','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `book_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_category$before_ins
BEFORE INSERT
ON book_category FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_category$before_upd
BEFORE UPDATE
ON book_category FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_city`
--

DROP TABLE IF EXISTS `book_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_city` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(80) NOT NULL COMMENT 'Город',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_city$descr` (`descr`),
  KEY `fk$book_city$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$book_city$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$book_city$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_city$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='Города';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_city`
--

LOCK TABLES `book_city` WRITE;
/*!40000 ALTER TABLE `book_city` DISABLE KEYS */;
INSERT INTO `book_city` VALUES (1,'Луганск',0,'2017-09-29 13:29:49','express_user@%',NULL,'2017-11-06 08:34:54','express_user@%',NULL),(2,'Алчевск',0,'2017-09-29 13:29:49','express_user@%',NULL,NULL,NULL,NULL),(3,'Молодогвардейск',0,'2017-09-29 13:29:49','express_user@%',NULL,NULL,NULL,NULL),(4,'Ровеньки',0,'2017-09-29 13:29:49','express_user@%',NULL,NULL,NULL,NULL),(5,'Красный Луч',0,'2017-09-29 13:29:49','express_user@%',NULL,NULL,NULL,NULL),(6,'Лутугино',0,'2017-09-29 13:29:49','express_user@%',NULL,NULL,NULL,NULL),(7,'Перевальск',0,'2017-09-29 13:29:49','express_user@%',NULL,NULL,NULL,NULL),(8,'Свердловск',0,'2017-09-29 13:29:49','express_user@%',NULL,NULL,NULL,NULL),(9,'Суходольск',0,'2017-09-29 13:29:49','express_user@%',NULL,NULL,NULL,NULL),(10,'Стаханов',0,'2017-09-29 13:29:49','express_user@%',NULL,NULL,NULL,NULL),(11,'Краснодон',0,'2017-09-29 13:29:49','express_user@%',NULL,NULL,NULL,NULL),(12,'Антрацит',0,'2017-09-29 13:29:49','express_user@%',NULL,NULL,NULL,NULL),(13,'Брянка',0,'2017-09-29 13:29:49','express_user@%',NULL,'2017-12-26 08:57:39','express_user@%',NULL),(14,'Все города',0,'2018-01-26 10:23:17','express_user@%',NULL,NULL,NULL,NULL),(15,'Первомайск',0,'2018-02-08 13:44:02','express_user@%',NULL,NULL,NULL,NULL),(20,'фыв',0,'2018-07-09 06:54:52','express_user@%',NULL,NULL,NULL,NULL),(21,'фыв2',0,'2018-07-09 07:24:58','express_user@%',NULL,NULL,NULL,NULL),(24,'фыв4',0,'2018-07-09 07:51:49','express_user@%',NULL,'2018-07-09 11:00:43','express_user@%',NULL);
/*!40000 ALTER TABLE `book_city` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_city$before_ins
BEFORE INSERT
ON book_city FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_city$before_upd
BEFORE UPDATE
ON book_city FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_country`
--

DROP TABLE IF EXISTS `book_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(80) NOT NULL COMMENT 'Страна',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_country$descr` (`descr`),
  KEY `fk$book_country$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$book_country$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$book_country$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_country$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Страна';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_country`
--

LOCK TABLES `book_country` WRITE;
/*!40000 ALTER TABLE `book_country` DISABLE KEYS */;
INSERT INTO `book_country` VALUES (1,'Украина',0,'2017-10-05 08:38:02','express_user@%',NULL,'2017-11-06 08:32:13','express_user@%',NULL),(2,'Россия',0,'2017-10-05 08:38:02','express_user@%',NULL,NULL,NULL,NULL),(3,'Беларусь',0,'2017-10-05 08:38:02','express_user@%',NULL,NULL,NULL,NULL),(4,'США',0,'2017-10-05 08:38:02','express_user@%',NULL,'2017-11-06 08:32:19','express_user@%',NULL),(5,'Германия',1,'2017-10-05 08:38:02','express_user@%',NULL,'2017-11-22 06:51:52','express_user@%',NULL);
/*!40000 ALTER TABLE `book_country` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_country$before_ins
BEFORE INSERT
ON book_country FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_country$before_upd
BEFORE UPDATE
ON book_country FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_currency`
--

DROP TABLE IF EXISTS `book_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_currency` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(30) NOT NULL COMMENT 'Английское наименование',
  `shortdescr` varchar(60) NOT NULL COMMENT 'Короткое наименование',
  `fulldescr` varchar(80) NOT NULL COMMENT 'Полное наименование',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `create_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user_id` int(10) unsigned DEFAULT NULL,
  `create_office_id` int(10) unsigned DEFAULT NULL,
  `change_date_time` timestamp NULL DEFAULT NULL,
  `change_user_id` int(10) unsigned DEFAULT NULL,
  `change_office_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_currency$descr` (`descr`),
  UNIQUE KEY `uk$book_currency$shortdescr` (`shortdescr`),
  UNIQUE KEY `uk$book_currenbook_currencycy$fulldescr` (`fulldescr`),
  KEY `fk$book_currency$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$book_currency$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$book_currency$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$book_currency$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$book_currency$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_currency$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_currency$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_currency$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Валюта';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_currency`
--

LOCK TABLES `book_currency` WRITE;
/*!40000 ALTER TABLE `book_currency` DISABLE KEYS */;
INSERT INTO `book_currency` VALUES (1,'RUB','руб','Рубль',0,'2017-09-29 13:28:06','express_user@%',NULL,NULL,'2018-07-18 07:49:10',NULL,NULL,NULL,NULL,NULL),(2,'UAH','грн','Гривна',0,'2017-09-29 13:28:06','express_user@%',NULL,NULL,'2018-07-18 07:49:10',NULL,NULL,NULL,NULL,NULL),(3,'USD','дол','Доллар',0,'2017-09-29 13:28:06','express_user@%',NULL,NULL,'2018-07-18 07:49:10',NULL,NULL,NULL,NULL,NULL),(4,'EUR','евро','Евро',0,'2017-09-29 13:28:06','express_user@%',NULL,NULL,'2018-07-18 07:49:10',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `book_currency` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_currency$before_ins
BEFORE INSERT
ON book_currency FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_currency$before_upd
BEFORE UPDATE
ON book_currency FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_exchange_direction`
--

DROP TABLE IF EXISTS `book_exchange_direction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_exchange_direction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `from_currency_id` int(10) unsigned NOT NULL COMMENT 'ИЗ какой валюты',
  `to_currency_id` int(10) unsigned NOT NULL COMMENT 'В какую валюту',
  `descr` varchar(61) NOT NULL COMMENT 'Описание. Например USD/RUB',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `create_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user_id` int(10) unsigned DEFAULT NULL,
  `create_office_id` int(10) unsigned DEFAULT NULL,
  `change_date_time` timestamp NULL DEFAULT NULL,
  `change_user_id` int(10) unsigned DEFAULT NULL,
  `change_office_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_exchange_direction$from_currency_id$to_currency_id` (`from_currency_id`,`to_currency_id`),
  KEY `fk$book_exchange_direction$to_currency_id$book_currency$id` (`to_currency_id`),
  KEY `fk$book_exchange_direction$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$book_exchange_direction$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$book_exchange_direction$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$book_exchange_direction$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$book_exchange_direction$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_exchange_direction$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_exchange_direction$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_exchange_direction$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_exchange_direction$from_currency_id$book_currency$id` FOREIGN KEY (`from_currency_id`) REFERENCES `book_currency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_exchange_direction$to_currency_id$book_currency$id` FOREIGN KEY (`to_currency_id`) REFERENCES `book_currency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Направление обмена';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_exchange_direction`
--

LOCK TABLES `book_exchange_direction` WRITE;
/*!40000 ALTER TABLE `book_exchange_direction` DISABLE KEYS */;
INSERT INTO `book_exchange_direction` VALUES (1,3,1,'USD/RUB',0,'2017-09-29 13:28:48','express_user@%',NULL,NULL,'2018-07-18 07:29:41',NULL,NULL,NULL,NULL,NULL),(2,4,1,'EUR/RUB',0,'2017-09-29 13:28:48','express_user@%',NULL,NULL,'2018-07-18 07:29:41',NULL,NULL,NULL,NULL,NULL),(3,2,1,'UAH/RUB',0,'2017-09-29 13:28:48','express_user@%',NULL,NULL,'2018-07-18 07:29:41',NULL,NULL,NULL,NULL,NULL),(4,3,2,'USD/UAH',0,'2017-09-29 13:28:48','express_user@%',NULL,NULL,'2018-07-18 07:29:41',NULL,NULL,NULL,NULL,NULL),(5,4,2,'EUR/UAH',0,'2017-09-29 13:28:48','express_user@%',NULL,NULL,'2018-07-18 07:29:41',NULL,NULL,NULL,NULL,NULL),(6,4,3,'EUR/USD',0,'2017-09-29 13:28:48','express_user@%',NULL,NULL,'2018-07-18 07:29:41',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `book_exchange_direction` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_exchange_direction$before_ins
BEFORE INSERT
ON book_exchange_direction FOR EACH ROW
BEGIN  
  DECLARE from_currency_descr varchar(30);
  DECLARE to_currency_descr varchar(30);
  
  SET from_currency_descr = '';
  SET to_currency_descr = '';
  
  SET NEW.ins_user = CURRENT_USER();
  
  IF NEW.from_currency_id IS NOT NULL THEN  
	SELECT descr 
	FROM book_currency 
	WHERE id = NEW.from_currency_id
	INTO from_currency_descr;
  END IF;
  
  IF  NEW.to_currency_id IS NOT NULL THEN
	SELECT descr
	FROM book_currency 
	WHERE id = NEW.to_currency_id
	INTO to_currency_descr;
  END IF; 
 
  SET NEW.descr = CONCAT(from_currency_descr, '/', to_currency_descr);  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_exchange_direction$before_upd
BEFORE UPDATE
ON book_exchange_direction FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_marketing`
--

DROP TABLE IF EXISTS `book_marketing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_marketing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `attraction` varchar(80) NOT NULL COMMENT 'Источник привлечения',
  `sorting` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Порядок сортировки',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_office$attraction` (`attraction`),
  KEY `fk$book_marketing$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$book_marketing$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$book_marketing$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_marketing$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COMMENT='Маркетинг';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_marketing`
--

LOCK TABLES `book_marketing` WRITE;
/*!40000 ALTER TABLE `book_marketing` DISABLE KEYS */;
INSERT INTO `book_marketing` VALUES (89,'Листовка',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2018-07-13 07:19:22','express_user@%',NULL),(90,'Объявления',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(91,'Радио',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(92,'Газета',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(93,'ТВ',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(94,'Наружная реклама',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(95,'Фасад',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(96,'Интернет',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(97,'Соц сети',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(98,'Агент',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(99,'От сотрудника',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(100,'Визитка',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(101,'СМС',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(102,'Холодный звонок',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(103,'Сарафан',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL),(104,'Постоянный клиент',1,0,'2017-10-24 14:14:54','express_user@%',NULL,'2017-10-30 09:17:26','express_user@%',NULL);
/*!40000 ALTER TABLE `book_marketing` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_marketing$before_ins
BEFORE INSERT
ON book_marketing FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_marketing$before_upd
BEFORE UPDATE
ON book_marketing FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_nbu`
--

DROP TABLE IF EXISTS `book_nbu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_nbu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `rate_uah` decimal(65,30) unsigned NOT NULL COMMENT 'Курс гривны',
  `rate_usd` decimal(65,30) unsigned NOT NULL COMMENT 'Курс доллара',
  `rate_eur` decimal(65,30) unsigned NOT NULL COMMENT 'Курс евро',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  KEY `fk$book_nbu$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$book_nbu$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$book_nbu$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_nbu$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Курс валют НБУ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_nbu`
--

LOCK TABLES `book_nbu` WRITE;
/*!40000 ALTER TABLE `book_nbu` DISABLE KEYS */;
INSERT INTO `book_nbu` VALUES (3,2.180000000000000000000000000000,59.050000000000000000000000000000,73.160000000000000000000000000000,0,'2017-11-22 13:30:28','express_user@%',NULL,'2018-07-13 11:25:50','express_user@%',NULL);
/*!40000 ALTER TABLE `book_nbu` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`book_nbu$before_ins` 
BEFORE INSERT ON express_db.book_nbu
FOR EACH ROW
BEGIN
  declare count_row int unsigned;
  
  select count(*)
  into count_row
  from book_nbu;
  
  if count_row > 0 then
    signal sqlstate '22003' 
    set message_text = 'Ошибка добавления. Курсы НБУ. В таблицу нельзя добавлять более одной записи!';
  end if;
  
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`book_nbu$before_upd` 
BEFORE UPDATE ON express_db.book_nbu FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_office`
--

DROP TABLE IF EXISTS `book_office`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_office` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `item` smallint(5) unsigned NOT NULL COMMENT 'Номер отделения',
  `descr` varchar(80) DEFAULT NULL COMMENT 'Описание отделения',
  `city_id` int(10) unsigned NOT NULL COMMENT 'В каком городе отделение',
  `bush_id` int(10) unsigned DEFAULT NULL COMMENT 'К какому кусту принадлежит отделение',
  `boss_id` int(10) unsigned DEFAULT NULL COMMENT 'Руководитель отделения',
  `tariff_group_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Ссылка на book_tariff_group',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'В какой группе отделение',
  `border_uah` decimal(19,2) unsigned NOT NULL DEFAULT '1.00' COMMENT 'Граница Гривна. Начинается опт.',
  `border_usd` decimal(19,2) unsigned NOT NULL DEFAULT '1.00' COMMENT 'Граница Доллар. Начинается опт.',
  `border_eur` decimal(19,2) unsigned NOT NULL DEFAULT '1.00' COMMENT 'Граница Евро. Начинается опт.',
  `address` varchar(160) DEFAULT NULL COMMENT 'Физический адрес',
  `guide` varchar(160) DEFAULT NULL COMMENT 'Ориентир',
  `latitude` decimal(9,6) DEFAULT NULL COMMENT 'Географическая широта',
  `longitude` decimal(9,6) DEFAULT NULL COMMENT 'Географическая долгота',
  `vodafone` char(12) DEFAULT NULL COMMENT 'Номер телефона отделения Vodafone',
  `lugacom` char(12) DEFAULT NULL COMMENT 'Номер телефона отделения Lugacom',
  `opening_date` date DEFAULT NULL COMMENT 'Дата открытия отделения',
  `closing_date` date DEFAULT NULL COMMENT 'Дата закрытия отделения',
  `color` char(7) NOT NULL DEFAULT '#0678be' COMMENT 'Цвета для андроид приложения',
  `monday` varchar(30) DEFAULT NULL COMMENT 'Понедельник',
  `tuesday` varchar(30) DEFAULT NULL COMMENT 'Вторник',
  `wednesday` varchar(30) DEFAULT NULL COMMENT 'Среда',
  `thursday` varchar(30) DEFAULT NULL COMMENT 'Четверг',
  `friday` varchar(30) DEFAULT NULL COMMENT 'Пятница',
  `saturday` varchar(30) DEFAULT NULL COMMENT 'Суббота',
  `sunday` varchar(30) DEFAULT NULL COMMENT 'Воскресенье',
  `pic_map` mediumblob COMMENT 'Отделение на карте',
  `pic_photo` mediumblob COMMENT 'Фото фасада отделения',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_office$item` (`item`),
  UNIQUE KEY `uk$book_office$descr` (`descr`),
  KEY `fk$book_office$city_id$book_city$id` (`city_id`),
  KEY `fk$book_office$bush_id$book_bush$id` (`bush_id`),
  KEY `fk$book_office$group_id$book_office_group$id` (`group_id`),
  KEY `fk$book_office$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$book_office$bush_id$book_bush$id` FOREIGN KEY (`bush_id`) REFERENCES `book_bush` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_office$city_id$book_city$id` FOREIGN KEY (`city_id`) REFERENCES `book_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_office$group_id$book_office_group$id` FOREIGN KEY (`group_id`) REFERENCES `book_office_group` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk$book_office$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='Отделения';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_office`
--

LOCK TABLES `book_office` WRITE;
/*!40000 ALTER TABLE `book_office` DISABLE KEYS */;
INSERT INTO `book_office` VALUES (1,111,'Cто одиннадцатое',1,3,NULL,1,47,5000.00,1000.00,500.00,'ул. Советская 249',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'#0678be',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-03-26 09:45:06','express_user@%',NULL),(2,888,'Восемьсот восемьдесят восьмое',1,1,NULL,1,47,5000.00,1000.00,500.00,'ул. Советская 249',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'#0678be',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-03-26 09:45:06','express_user@%',NULL),(3,1,'Первое',1,3,NULL,2,45,5000.00,1000.00,500.00,'ул. Советская, 59А','Напротив сити-центра, бывшее кафе \"Арктика\"',48.568426,39.301413,'380660659050','380721085601',NULL,NULL,'#5cbf73','08:00-18:00','08:00-18:00','08:00-18:00','08:00-18:00','08:00-18:00','08:30-16:00','08:30-14:00',NULL,NULL,NULL,0,NULL,NULL,NULL,1,1,'2018-05-14 07:07:38','2017-09-29 13:30:53','express_user@%','2018-05-14 07:07:38','express_user@%',NULL),(4,3,'Третье',1,3,NULL,1,0,5000.00,1000.00,500.00,'кв. Солнечный, 3','Возле абсолюта, за цветочн. Маг., бывш отд. Приват-банка',48.566978,39.383742,'380992239077','380721085603',NULL,NULL,'#5cbabf','08:30-16:30','08:30-16:30','08:30-16:30','08:30-16:30','08:30-16:30','09:30-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(5,4,'Четвертое',1,3,NULL,1,45,5000.00,1000.00,500.00,'кв. Ленинского Комсомола, 14','Напротив магазина  \"Блеск\", возле торгового центра',48.552738,39.298722,'380501968281','380721085604',NULL,NULL,'#bfb05c','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:30','08:30-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(6,5,'Пятое',1,4,NULL,1,0,5000.00,1000.00,500.00,'поселок Юбилейный, кв. Королева, 12','Возле маг. Народный, напротив рынка',48.559766,39.189344,'380667647845','380721272873',NULL,NULL,'#5c76bf','08:00-17:00','08:00-17:00','08:00-17:00','08:00-17:00','08:00-17:00','08:00-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(7,6,'Шестое',1,3,NULL,1,0,5000.00,1000.00,500.00,'ул. Артема, 86','Комплекс Юность, на 2эт. Бывш. Отд. укркоммунбанка',48.558313,39.186425,'380500179082','380721085609',NULL,NULL,'#5cbfb7','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-14:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(8,7,'Седьмое',1,3,NULL,1,0,5000.00,1000.00,500.00,'кв. Шевченко, 62','на остановке авиацентр, возле аптеки, бывш. Отд.приватбанка',48.579207,39.320642,'380953444986','380721085611',NULL,NULL,'#bf785c','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(9,8,'Восьмое',2,4,NULL,1,0,5000.00,1000.00,500.00,'ул. Ленина, 56','Ресторан Кристалл, агентство недвижимости Гудвилл',48.466436,38.823596,'380668420565','380721085612',NULL,NULL,'#99bf5c','08:30-18:00','08:30-18:00','08:30-18:00','08:30-18:00','08:30-18:00','08:30-14:00','08:30-14:00',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(10,9,'Девятое',2,4,NULL,1,0,5000.00,1000.00,500.00,'ул. Фрунзе, 31','Остановка московская, перекресток Фрунзе и Московской, ',48.468422,38.800745,'380669544691','380721085613',NULL,NULL,'#875cbf','08:00-17:00','08:00-17:00','08:00-17:00','08:00-17:00','08:00-17:00','08:30-14:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(11,10,'Десятое',1,3,NULL,1,0,5000.00,1000.00,500.00,'кв. Гагарина, 1','в монолите на 1 этаже',48.586731,39.179471,'380953166818','380721085614',NULL,NULL,'#bf5c6b','08:00-17:00','08:00-17:00','08:00-17:00','08:00-17:00','08:00-17:00','08:30-16:00','08:30-14:00',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(12,11,'Одиннадцатое',13,4,NULL,1,0,5000.00,1000.00,500.00,'ул. Крымская, 1','здание налоговой',48.565910,38.621614,'380502377125','380721085615',NULL,NULL,'#b7bf5c','08:30-15:30','08:30-15:30','08:30-15:30','08:30-15:30','08:30-15:30','08:00-15:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(13,12,'Двенадцатое',3,2,NULL,1,0,5000.00,1000.00,500.00,'ул. Ленина, 18','дк Ленина, парк',48.347611,39.655023,'380662062398','380721085616',NULL,NULL,'#5cbf8f','08:00-17:00','08:00-17:00','08:00-17:00','08:00-17:00','08:00-17:00','08:30-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(14,13,'Тринадцатое',4,2,NULL,1,0,5000.00,1000.00,500.00,'ул.Кооперативная, 6','первая парковка на рынке, в районе УТСЗН',48.087532,39.359548,'380502788783','380721085617',NULL,NULL,'#4b9eb1',NULL,'08:00-15:00','08:00-15:00','08:00-15:00','08:00-15:00','08:00-15:00','08:00-14:00',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(15,14,'Четырнадцатое',1,2,NULL,1,0,5000.00,1000.00,500.00,'ул.Оборонная, 1','напротив пед. Института, здание МЖК',48.565293,39.314211,'380957082107','380721085619',NULL,NULL,'#834bb1','08:00-18:00','08:00-18:00','08:00-18:00','08:00-18:00','08:30-16:00','08:30-14:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(16,15,'Пятнадцатое',4,2,NULL,1,0,5000.00,1000.00,500.00,'ул.Карла Маркса, 115','рядом с Абсолютом, напротивцентральной поликлиники',48.087049,39.373581,'380954473708','380721085620',NULL,NULL,'#b14b80','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(17,17,'Семнадцатое',5,2,NULL,1,0,5000.00,1000.00,500.00,'ул. Магистральная, 62д','напротив Дома быта',48.135330,38.932014,'380668157380','380721085622',NULL,NULL,'#b18f4b','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:30-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(18,18,'Восемнадцатое',6,2,NULL,1,0,5000.00,1000.00,500.00,'ул.Ленина, 116','За Остановкой, рядом с маг.\"Конфискат\"\"',48.401993,39.206959,'380661574061','380721275928',NULL,NULL,'#b4b96b','07:45-16:00','07:45-16:00','07:45-16:00','07:45-16:00','07:45-16:00','07:45-14:00','07:45-14:00',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(19,19,'Девятнадцатое',1,3,NULL,1,0,5000.00,1000.00,500.00,'кв.Молодежный, 5','Рядом Брусниця',48.570892,39.374018,'380500179258','380721085625',NULL,NULL,'#6b96b9','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(20,20,'Двадцатое',1,4,NULL,1,0,5000.00,1000.00,500.00,'кв. Гаевого, 2б','в здании \"Планеты обуви\"',48.559149,39.258155,'380501762605','380721085627',NULL,NULL,'#6bb9a0','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(21,23,'Двадцать третье',7,4,NULL,1,0,5000.00,1000.00,500.00,'ул. Ленина, 39','рядом Сбербанк, пенсионный фонд',48.435761,38.809378,'380661527212','380721085621',NULL,NULL,'#6b89b9','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-14:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(22,24,'Двадцать четвертое',1,3,NULL,1,0,5000.00,1000.00,500.00,'ул. Ленина, 39','возле абсолюта, бывшее отделение банка',48.567092,39.291988,'380666772473','380721085628',NULL,NULL,'#9f6bb9','08:30-16:30','08:30-16:30','08:30-16:30','08:30-16:30','08:30-16:30','08:30-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(23,25,'Двадцать пятое',1,3,NULL,1,0,5000.00,1000.00,500.00,'ул. Фрунзе, 107',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'#6ba2b9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-03-26 09:45:06','express_user@%',NULL),(24,29,'Двадцать девятое',1,2,NULL,1,0,5000.00,1000.00,500.00,'ул. Советская, 15','перед мостом',48.564212,39.274579,'380662062396','380721085630',NULL,NULL,'#70b96b','08:00-17:00','08:00-17:00','08:00-17:00','08:00-17:00','08:00-17:00','08:30-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(25,30,'Тридцатое',3,2,NULL,1,0,5000.00,1000.00,500.00,'кв. Олега Кошевого, 5','в районе центрального рынка',48.349147,39.658706,'380501737842','380721085592',NULL,NULL,'#c7c738','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:30-14:00',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(26,31,'Тридцать первое',8,2,NULL,1,0,5000.00,1000.00,500.00,'ул. Пирогова, 1','Бывший Гостиный Двор',48.074213,39.650171,'380507138160','380721085631',NULL,NULL,'#93c738','08:00-15:00','08:00-15:00','08:00-15:00','08:00-15:00','08:00-15:00','08:00-15:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(27,32,'Тридцать второе',9,2,NULL,1,0,5000.00,1000.00,500.00,'ул. Ленина 27','бывш АТБ',48.352172,39.733708,'380500193643','380721363427',NULL,NULL,'#4d5dd7','08:30-16:00','08:30-16:00','08:30-16:00','08:30-16:00','08:30-16:00','08:30-14:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(28,33,'Тридцать третье',10,4,NULL,1,0,5000.00,1000.00,500.00,'ул. Ленина 27',' здание Абсолюта со стороны центр.рынка',48.561779,38.651877,'380500179301','380721050046',NULL,NULL,'#56d2ce',NULL,'08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-14:00',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(29,34,'Тридцать четвёртое',1,3,NULL,1,0,5000.00,1000.00,500.00,'ул. Оборонная, 20А','В 9 этажке,перед входом в Поляну',48.551459,39.325311,'380957442993','380721085602',NULL,NULL,'#b382de','08:30-17:00','08:30-17:00','08:30-17:00','08:30-17:00','08:30-17:00','08:30-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(30,35,'Тридцать пятое',1,4,NULL,1,0,5000.00,1000.00,500.00,'кв. Заречный, 18','возле пенс.фонда,за Абсолютом',48.527551,39.269028,'380662245273','380721085589',NULL,NULL,'#7bceba','08:30-17:00','08:30-17:00','08:30-17:00','08:30-17:00','08:30-17:00','08:30-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(31,36,'Тридцать шестое',11,2,NULL,1,0,5000.00,1000.00,500.00,'ул. Артема, 9','район центрального рынка',48.286753,39.742893,'380662374517','380721201625',NULL,NULL,'#ce9f7b','08:30-16:00','08:30-16:00','08:30-16:00','08:30-16:00','08:30-16:00','08:00-14:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(32,37,'Тридцать седьмое',4,2,NULL,1,0,5000.00,1000.00,500.00,'кв. Шахтерский, 4','напротив заправки ТНК',48.071681,39.398260,'380501737128','380721363354',NULL,NULL,'#b17bce','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:30-16:00','08:30-14:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(33,38,'Тридцать восьмое',10,4,NULL,1,0,5000.00,1000.00,500.00,'проспект 50 Лет Октября, 1','остановка Универсам',48.578787,38.665640,'380957998702','380721277246',NULL,NULL,'#7bce87','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:30','08:00-16:30','08:00-15:30',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(34,39,'Тридцать девятое',1,3,NULL,1,0,5000.00,1000.00,500.00,'кв. Жукова, 4','напротив 1 корпуса маш.университета',48.592158,39.307236,'380508298503','380721275932',NULL,NULL,'#d66c6c','08:30-17:00','08:30-17:00','08:30-17:00','08:30-17:00','08:30-17:00','08:30-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-29 13:30:53','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(35,40,'Сороковое',4,NULL,NULL,1,0,5000.00,1000.00,500.00,'ул.Коммунистическая 15/4','Возле Ирис Центра',48.088785,39.364682,'380501737066','380721364076',NULL,NULL,'#8fae63','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-02-08 13:45:03','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(36,41,'Сорок первое',12,NULL,NULL,1,0,5000.00,1000.00,500.00,'ул.Петровского 34 П',NULL,48.124956,39.089703,NULL,NULL,NULL,NULL,'#63a9ae',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-02-08 13:45:03','express_user@%','2018-03-26 09:45:06','express_user@%',NULL),(37,42,'Сорок второе',15,NULL,NULL,1,0,5000.00,1000.00,500.00,'кв.40 Лет Победы 4А','ТЦ Первомайск Сити 2-й этаж',48.631019,38.529324,'380950454240','380721468213',NULL,NULL,'#9d6cb1','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-02-08 13:45:03','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(38,43,'Сорок третье',1,NULL,NULL,1,0,5000.00,1000.00,500.00,'ул.Оборонная 7','Авозле бывш.бара Мафия',48.556638,39.321198,'380950454235','380721468211',NULL,NULL,'#6c78b1','08:30-16:00','08:30-16:00','08:30-16:00','08:30-16:00','08:30-16:00','08:30-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-02-08 13:45:03','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(39,44,'Сорок четвёртое',1,NULL,NULL,1,0,5000.00,1000.00,500.00,'ул.Коцюбинского 8','медиана',48.571805,39.305284,'380950454234','380721468212',NULL,NULL,'#b2b66a','08:30-17:00','08:30-17:00','08:30-17:00','08:30-17:00','08:30-17:00','08:30-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-02-08 13:45:03','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(40,45,'Сорок пятое',10,NULL,NULL,1,0,5000.00,1000.00,500.00,'пер.Базарный 3/7','ц.Рынок ,малая автостанция',48.559446,38.642541,'380957730006','380720000111',NULL,NULL,'#b6765c',NULL,'08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-14:00',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-02-08 13:45:03','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(41,46,'Сорок шестое',8,NULL,NULL,1,0,5000.00,1000.00,500.00,'скул.Косиора 5','бывший Укрсоцбанк',48.076911,39.647262,'380959232125','380721468210',NULL,NULL,'#b65c7e',NULL,'08:00-16:00','08:00-16:00','08:00-16:00','08:00-16:00','08:00-14:00','08:00-16:00',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-02-08 13:45:03','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(42,47,'Сорок седьмое',10,NULL,NULL,1,0,5000.00,1000.00,500.00,'пр. Ленина 54','остановка Ромашка',48.545450,38.622775,'380667036850','380721468299',NULL,NULL,'#5baf88',NULL,'08:30-16:00','08:30-16:00','08:30-16:00','08:30-16:00','08:30-16:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-02-08 13:45:03','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(43,48,'Сорок восьмое',2,NULL,NULL,1,0,5000.00,1000.00,500.00,'ул.Гмыри 20/помещение 46','Бывший офис Алчевск',48.468202,38.831362,'380667034653','380721474572',NULL,NULL,'#5b9baf',NULL,'08:30-17:00','08:30-17:00','08:30-17:00','08:30-17:00','08:30-14:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-02-08 13:45:03','express_user@%','2018-05-07 11:44:21','express_user@%',NULL),(44,106,'сто шесть',2,NULL,NULL,1,44,16.00,17.00,18.00,'оборонная 1','мжк',NULL,NULL,'380661234567','380721234567',NULL,NULL,'#0678be',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'2018-07-17 11:43:44',NULL,NULL,'2018-07-17 11:47:27','2018-07-17 11:43:44','express_user@%','2018-07-17 11:47:27','express_user@%',NULL);
/*!40000 ALTER TABLE `book_office` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`book_office$before_ins` BEFORE INSERT ON express_db.book_office FOR EACH ROW
BEGIN
  
  if NEW.border_uah < 1 then
    signal sqlstate '22003' 
    set message_text = 'Ошибка добавления. Курс валют. Граница для курса Гривны должна быть больше 1!';
  end if;
  
  if NEW.border_usd < 1 then
    signal sqlstate '22003' 
    set message_text = 'Ошибка добавления. Курс валют. Граница для курса Доллара должна быть больше 1!';
  end if;  
  
  if NEW.border_eur < 1 then
    signal sqlstate '22003' 
    set message_text = 'Ошибка добавления. Курс валют. Граница для курса Евро должна быть больше 1!';
  end if;    
  
  SET NEW.ins_user = CURRENT_USER();
  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`book_office$before_upd` 
BEFORE UPDATE ON express_db.book_office 
FOR EACH ROW
BEGIN
  
  if NEW.border_uah < 1 then
    signal sqlstate '22003' 
    set message_text = 'Ошибка изменения. Курс валют. Граница для курса Гривны должна быть больше 1!';
  end if;
  
  if NEW.border_usd < 1 then
    signal sqlstate '22003' 
    set message_text = 'Ошибка изменения. Курс валют. Граница для курса Доллара должна быть больше 1!';    
  end if;  
  
  if NEW.border_eur < 1 then
    signal sqlstate '22003' 
    set message_text = 'Ошибка изменения. Курс валют. Граница для курса Евро должна быть больше 1!';
  end if;    

  SET NEW.upd_user = CURRENT_USER();  
  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_office_group`
--

DROP TABLE IF EXISTS `book_office_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_office_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(80) NOT NULL COMMENT 'Наименование группы',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `create_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user_id` int(10) unsigned DEFAULT NULL,
  `create_office_id` int(10) unsigned DEFAULT NULL,
  `change_date_time` timestamp NULL DEFAULT NULL,
  `change_user_id` int(10) unsigned DEFAULT NULL,
  `change_office_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_office_group$descr` (`descr`),
  KEY `fk$book_office_group$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$book_office_group$upd_php$teh_user$id` (`upd_php`),
  KEY `fk$book_office_group$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$book_office_group$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$book_office_group$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$book_office_group$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$book_office_group$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_office_group$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_office_group$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_office_group$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_office_group$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_office_group$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='Группа для отделений';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_office_group`
--

LOCK TABLES `book_office_group` WRITE;
/*!40000 ALTER TABLE `book_office_group` DISABLE KEYS */;
INSERT INTO `book_office_group` VALUES (0,'Без группы',0,'2017-12-04 08:04:32','express_user@%',NULL,'2017-12-04 08:05:31','express_user@%',NULL,'2018-07-31 06:40:38',NULL,NULL,NULL,NULL,NULL),(44,'Без названия',0,'2017-12-09 06:29:40','express_user@%',NULL,'2018-07-21 09:02:34','express_user@%',NULL,'2018-07-31 06:40:38',NULL,NULL,NULL,NULL,NULL),(45,'Новая группа',0,'2017-12-11 07:10:45','express_user@%',NULL,'2017-12-19 13:04:07','express_user@%',NULL,'2018-07-31 06:40:38',NULL,NULL,NULL,NULL,NULL),(47,'Группа центр',0,'2017-12-26 08:07:09','express_user@%',NULL,NULL,NULL,NULL,'2018-07-31 06:40:38',NULL,NULL,NULL,NULL,NULL),(49,'Вторая группа',0,'2018-07-10 11:42:23','express_user@%',NULL,'2018-07-31 07:47:53','express_user@%',NULL,'2018-07-31 06:40:38',NULL,NULL,'2018-07-31 07:47:53',1,1),(53,'Новые',0,'2018-07-31 12:15:14','express_user@%',NULL,'2018-07-31 12:23:43','express_user@%',NULL,'2018-07-31 12:15:14',1,1,'2018-07-31 12:23:43',1,1),(54,'Новая 2',0,'2018-07-31 12:23:50','express_user@%',NULL,NULL,NULL,NULL,'2018-07-31 12:23:50',1,1,'2018-07-31 12:23:50',NULL,NULL);
/*!40000 ALTER TABLE `book_office_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_office_group$before_ins
BEFORE INSERT
ON book_office_group FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`book_office_group$after_ins` AFTER INSERT ON express_db.book_office_group FOR EACH ROW
begin
  insert into doc_rate
  (group_id, exchange_direction_id, is_cash, is_retail, buy, sale)
    select
      new.id, exchange_direction_id, is_cash, is_retail, buy, sale
    from doc_rate
    where group_id = 0;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`book_office_group$before_upd` BEFORE UPDATE ON express_db.book_office_group FOR EACH ROW
BEGIN


  

  SET NEW.upd_user = CURRENT_USER();  

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`book_office_group$before_del` BEFORE DELETE ON express_db.book_office_group FOR EACH ROW
begin
  if old.id = 0 then
    signal sqlstate '22004' 
    set message_text = 'Ошибка удаления. Группы отделений. Невозможно удалить запись (без групп)!';
  end if;
  
  update book_office
  set group_id = 0
  where group_id = old.id;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_product`
--

DROP TABLE IF EXISTS `book_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `service_id` int(10) unsigned NOT NULL COMMENT 'Услуга. Ссылка на book_service',
  `system_id` int(10) unsigned NOT NULL COMMENT 'К какой системе относиться продукт. Ссылка на book_system',
  `currency_id` int(10) unsigned NOT NULL COMMENT 'Валюта продукта. Ссылка на book_currency',
  `exdir_id` int(10) unsigned NOT NULL COMMENT 'Направление обмена. Ссылка на book_exchange_direction',
  `sorting` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Порядок сортировки',
  `many` enum('single','multi') NOT NULL DEFAULT 'single' COMMENT 'Для ДопУслуг. single одна заявка одна услуга. multi много услуг в одной заявке.',
  `is_archive` int(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  KEY `fk$book_product$service_id$book_service$id` (`service_id`),
  KEY `fk$book_product$exdir_id$book_exchange_direction$id` (`exdir_id`),
  KEY `fk$book_product$currency_id$book_currency$id` (`currency_id`),
  KEY `fk$book_product$system_id$book_system$id` (`system_id`),
  KEY `fk$book_product$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$book_product$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$book_product$currency_id$book_currency$id` FOREIGN KEY (`currency_id`) REFERENCES `book_currency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_product$exdir_id$book_exchange_direction$id` FOREIGN KEY (`exdir_id`) REFERENCES `book_exchange_direction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_product$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_product$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8 COMMENT='Продукты';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_product`
--

LOCK TABLES `book_product` WRITE;
/*!40000 ALTER TABLE `book_product` DISABLE KEYS */;
INSERT INTO `book_product` VALUES (69,1,34,2,1,1,'single',0,'2017-12-12 11:33:21','express_user@%',NULL,NULL,NULL,NULL),(70,1,34,3,1,1,'single',0,'2017-12-12 11:33:53','express_user@%',NULL,NULL,NULL,NULL),(71,1,34,4,1,1,'single',0,'2017-12-12 11:34:14','express_user@%',NULL,NULL,NULL,NULL),(72,1,34,1,1,1,'single',0,'2017-12-12 11:35:04','express_user@%',NULL,NULL,NULL,NULL),(73,1,36,2,1,1,'single',0,'2017-12-12 12:05:56','express_user@%',NULL,NULL,NULL,NULL),(74,1,36,1,1,1,'single',0,'2017-12-12 12:06:06','express_user@%',NULL,NULL,NULL,NULL),(75,1,37,2,1,1,'single',0,'2017-12-12 12:06:43','express_user@%',NULL,NULL,NULL,NULL),(76,1,37,1,1,1,'single',0,'2017-12-12 12:07:04','express_user@%',NULL,NULL,NULL,NULL),(77,1,38,2,1,1,'single',0,'2017-12-12 12:07:26','express_user@%',NULL,NULL,NULL,NULL),(78,1,38,1,1,1,'single',0,'2017-12-12 12:07:41','express_user@%',NULL,NULL,NULL,NULL),(79,1,3,1,1,1,'single',0,'2017-12-12 12:08:15','express_user@%',NULL,NULL,NULL,NULL),(80,1,1,2,1,1,'single',0,'2017-12-12 12:09:14','express_user@%',NULL,NULL,NULL,NULL),(81,1,1,1,1,1,'single',0,'2017-12-12 12:09:32','express_user@%',NULL,NULL,NULL,NULL),(82,1,6,1,1,1,'single',0,'2017-12-12 12:14:39','express_user@%',NULL,NULL,NULL,NULL),(83,1,9,3,1,1,'single',0,'2017-12-12 12:14:59','express_user@%',NULL,NULL,NULL,NULL),(84,1,9,4,1,1,'single',0,'2017-12-12 12:15:26','express_user@%',NULL,NULL,NULL,NULL),(85,1,14,3,1,1,'single',0,'2017-12-12 12:15:42','express_user@%',NULL,NULL,NULL,NULL),(86,1,14,4,1,1,'single',0,'2017-12-12 12:16:08','express_user@%',NULL,NULL,NULL,NULL),(87,1,8,2,1,1,'single',0,'2017-12-12 12:16:45','express_user@%',NULL,NULL,NULL,NULL),(88,1,8,3,1,1,'single',0,'2017-12-12 12:17:14','express_user@%',NULL,NULL,NULL,NULL),(89,1,8,4,1,1,'single',0,'2017-12-12 12:17:35','express_user@%',NULL,NULL,NULL,NULL),(90,1,8,1,1,1,'single',0,'2017-12-12 12:17:51','express_user@%',NULL,NULL,NULL,NULL),(91,1,13,3,1,1,'single',0,'2017-12-12 12:18:16','express_user@%',NULL,NULL,NULL,NULL),(92,1,13,1,1,1,'single',0,'2017-12-12 12:18:52','express_user@%',NULL,NULL,NULL,NULL),(93,1,7,1,1,1,'single',0,'2017-12-12 12:19:16','express_user@%',NULL,NULL,NULL,NULL),(94,1,5,1,1,1,'single',0,'2017-12-12 12:19:38','express_user@%',NULL,NULL,NULL,NULL),(95,1,5,2,1,1,'single',0,'2017-12-12 12:20:04','express_user@%',NULL,NULL,NULL,NULL),(96,1,5,3,1,1,'single',0,'2017-12-12 12:20:24','express_user@%',NULL,'2017-12-18 16:20:58','express_user@%',NULL),(97,1,4,1,1,1,'single',0,'2018-01-12 11:12:31','express_user@%',NULL,NULL,NULL,NULL),(99,2,3,1,1,1,'single',0,'2018-01-22 11:07:10','express_user@%',NULL,NULL,NULL,NULL),(119,4,39,1,1,1,'multi',0,'2018-02-05 11:42:03','express_user@%',NULL,'2018-02-06 11:14:04','express_user@%',NULL),(120,4,40,1,1,1,'single',0,'2018-02-05 11:45:07','express_user@%',NULL,NULL,NULL,NULL),(121,4,41,1,1,1,'multi',0,'2018-02-05 11:45:45','express_user@%',NULL,'2018-02-06 11:16:08','express_user@%',NULL),(122,4,42,1,1,1,'single',0,'2018-02-05 11:46:39','express_user@%',NULL,NULL,NULL,NULL),(124,4,43,1,1,1,'single',0,'2018-02-05 11:48:31','express_user@%',NULL,NULL,NULL,NULL),(125,4,44,1,1,1,'single',0,'2018-02-05 11:49:12','express_user@%',NULL,NULL,NULL,NULL),(126,4,45,1,1,1,'single',0,'2018-02-05 11:52:31','express_user@%',NULL,NULL,NULL,NULL),(127,4,46,1,1,1,'single',0,'2018-02-05 11:53:15','express_user@%',NULL,NULL,NULL,NULL),(128,4,47,1,1,1,'single',0,'2018-02-05 11:53:48','express_user@%',NULL,NULL,NULL,NULL),(129,4,48,1,1,1,'multi',0,'2018-02-05 11:55:31','express_user@%',NULL,'2018-02-06 11:16:41','express_user@%',NULL),(130,4,49,1,1,1,'multi',0,'2018-02-05 11:56:53','express_user@%',NULL,'2018-02-06 11:17:05','express_user@%',NULL),(131,4,50,1,1,1,'multi',0,'2018-02-05 11:57:47','express_user@%',NULL,'2018-02-06 11:17:17','express_user@%',NULL),(132,4,51,1,1,1,'multi',0,'2018-02-05 11:58:20','express_user@%',NULL,'2018-02-06 11:17:42','express_user@%',NULL),(133,4,52,1,1,1,'single',0,'2018-02-05 11:59:35','express_user@%',NULL,NULL,NULL,NULL),(134,4,52,2,1,1,'single',0,'2018-02-05 12:00:49','express_user@%',NULL,NULL,NULL,NULL),(135,4,53,1,1,1,'single',0,'2018-02-05 12:01:26','express_user@%',NULL,NULL,NULL,NULL),(136,4,54,1,1,1,'multi',0,'2018-02-05 12:02:27','express_user@%',NULL,'2018-02-06 11:18:07','express_user@%',NULL),(137,4,55,1,1,1,'single',0,'2018-02-05 12:04:35','express_user@%',NULL,NULL,NULL,NULL),(138,4,56,1,1,1,'single',0,'2018-02-05 12:05:37','express_user@%',NULL,NULL,NULL,NULL),(139,4,56,2,1,1,'single',0,'2018-02-05 12:05:51','express_user@%',NULL,NULL,NULL,NULL),(140,4,57,1,1,1,'multi',0,'2018-02-05 12:06:34','express_user@%',NULL,'2018-02-06 11:18:25','express_user@%',NULL),(141,4,58,1,1,1,'single',0,'2018-02-05 12:07:38','express_user@%',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `book_product` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_product$before_ins
BEFORE INSERT
ON book_product FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_product$before_upd
BEFORE UPDATE
ON book_product FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_service`
--

DROP TABLE IF EXISTS `book_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_service` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `category_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_category',
  `descr` varchar(60) NOT NULL COMMENT 'Название услуги',
  `is_archive` tinyint(1) DEFAULT '0' COMMENT 'В архиве?',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_service$descr` (`descr`),
  KEY `fk$book_service$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$book_service$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$book_service$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$book_service$change_office_id$book_office$id` (`change_office_id`),
  KEY `fk$book_service$category_id$book_category$id` (`category_id`),
  CONSTRAINT `fk$book_service$category_id$book_category$id` FOREIGN KEY (`category_id`) REFERENCES `book_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_service$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_service$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_service$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_service$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COMMENT='Справочник услуг';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_service`
--

LOCK TABLES `book_service` WRITE;
/*!40000 ALTER TABLE `book_service` DISABLE KEYS */;
INSERT INTO `book_service` VALUES (1,1,'Приват-карта',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-07-14 09:53:01','express_user@%'),(2,1,'УКР карта',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-10 10:19:07','express_user@%'),(3,1,'РФ карта',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-10 10:29:29','express_user@%'),(4,1,'Сбер карта',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(5,2,'QIWI',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-07-16 13:06:02','express_user@%'),(6,2,'Яндекс',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(7,2,'WebMoney',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(8,2,'Payeer',0,NULL,NULL,NULL,1,1,'2018-05-14 23:59:56','2018-05-07 06:32:22','express_user@%','2018-05-15 11:59:54','express_user@%'),(9,3,'Contact',0,NULL,NULL,NULL,1,1,'2018-08-08 07:20:55','2018-05-07 06:32:22','express_user@%','2018-08-08 07:20:55','express_user@%'),(10,3,'Unistream',0,NULL,NULL,NULL,1,1,'2018-08-08 07:20:47','2018-05-07 06:32:22','express_user@%','2018-08-08 07:20:47','express_user@%'),(11,3,'Western Union',0,NULL,NULL,NULL,1,1,'2018-08-08 07:17:46','2018-05-07 06:32:22','express_user@%','2018-08-08 07:17:46','express_user@%'),(12,3,'Золотая Корона',0,NULL,NULL,NULL,1,1,'2018-08-08 07:17:56','2018-05-07 06:32:22','express_user@%','2018-08-08 07:17:56','express_user@%'),(13,3,'Money Gram',0,NULL,NULL,NULL,1,1,'2018-08-08 07:20:37','2018-05-07 06:32:22','express_user@%','2018-08-08 07:20:37','express_user@%'),(14,3,'RIA',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(15,5,'Игры',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(16,5,'AVON РФ',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(17,5,'Faberlic РФ',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(18,5,'Oriflame РФ',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(19,5,'Mary Kay РФ',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(20,5,'Faberlic УКР',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(21,5,'Oriflame УКР',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(22,5,'Mary Kay УКР',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(23,5,'Amway УКР',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(24,5,'AVON УКР',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(25,5,'Контракт',0,NULL,NULL,NULL,1,1,'2018-08-07 06:16:03','2018-05-07 06:32:22','express_user@%','2018-08-07 06:16:03','express_user@%'),(26,6,'Провайдер I-Team',0,NULL,NULL,NULL,1,1,'2018-08-08 12:15:12','2018-05-07 06:32:22','express_user@%','2018-08-08 12:15:12','express_user@%'),(27,6,'Провайдер Лугалинк',0,NULL,NULL,NULL,1,1,'2018-08-08 12:15:19','2018-05-07 06:32:22','express_user@%','2018-08-08 12:15:19','express_user@%'),(28,6,'Провайдер Холинет',0,NULL,NULL,NULL,1,1,'2018-08-08 12:15:26','2018-05-07 06:32:22','express_user@%','2018-08-08 12:15:26','express_user@%'),(29,6,'Провайдер Hiperline',0,NULL,NULL,NULL,1,1,'2018-08-08 12:15:32','2018-05-07 06:32:22','express_user@%','2018-08-08 12:15:32','express_user@%'),(30,6,'Провайдер Матрица-Лугань',0,NULL,NULL,NULL,1,1,'2018-08-08 12:15:37','2018-05-07 06:32:22','express_user@%','2018-08-08 12:15:37','express_user@%'),(31,6,'Провайдер Алческ net',0,NULL,NULL,NULL,1,1,'2018-08-08 12:15:42','2018-05-07 06:32:22','express_user@%','2018-08-08 12:15:42','express_user@%'),(32,6,'Провайдер Эверест',0,NULL,NULL,NULL,1,1,'2018-08-08 12:15:47','2018-05-07 06:32:22','express_user@%','2018-08-08 12:15:47','express_user@%'),(33,6,'Провайдер СКС Антрацит',0,NULL,NULL,NULL,1,1,'2018-08-08 12:15:51','2018-05-07 06:32:22','express_user@%','2018-08-08 12:15:51','express_user@%'),(34,6,'Провайдер IPcom',0,NULL,NULL,NULL,1,1,'2018-08-08 12:15:57','2018-05-07 06:32:22','express_user@%','2018-08-08 12:15:57','express_user@%'),(35,5,'Выписка со счета',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(36,5,'Пополнение мобильных операторо РФ',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(37,5,'Удаление электронного пропуска',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(38,5,'Регистрация в онлайн-банкинге',0,NULL,NULL,NULL,1,1,'2018-05-16 06:28:10','2018-05-07 06:32:22','express_user@%','2018-05-16 06:28:07','express_user@%'),(39,5,'Оформление VIP-карт',0,NULL,NULL,NULL,1,1,'2018-05-16 06:28:44','2018-05-07 06:32:22','express_user@%','2018-05-16 06:28:41','express_user@%'),(40,5,'Проверка купюр на подлинность',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-10 09:48:24','express_user@%'),(41,5,'Оформление интернет-кошелька',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-10 09:48:45','express_user@%'),(42,5,'Ксерокопия',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(43,5,'Панда по ЛНР',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(44,5,'Панда по Луганску',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(45,5,'Пропуск',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(46,5,'Размен купюр',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(47,5,'Перевод с карты на карту',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-10 09:51:40','express_user@%'),(48,5,'Перевыпуск карт',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(49,5,'Идентификация',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(50,5,'Разблокировка карт',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(51,5,'Продажа стартового пакета Лугаком',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(52,5,'Продажа стартового пакета Vodafone Red',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(53,5,'Продажа стартового пакета Vodafone Red Лайт',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(54,5,'Смена тарифного плана МТС и Vodafone',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(55,5,'Провайдер Магеал',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(56,5,'Лугаком',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(57,5,'Провайдер Мегалинк',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(58,5,'Интертелеком',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(59,5,'Lifecell',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(60,5,'Киестар',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(61,5,'Провайдер Тримоб',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(62,5,'Vodafon/МТС',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(63,5,'Провайдер Пульсар',0,NULL,NULL,NULL,1,1,'2018-08-08 12:17:03','2018-05-07 06:32:22','express_user@%','2018-08-08 12:17:03','express_user@%'),(64,5,'Провайдер Луганет',0,NULL,NULL,NULL,1,1,'2018-08-08 12:17:07','2018-05-07 06:32:22','express_user@%','2018-08-08 12:17:07','express_user@%'),(65,5,'Провайдер Диджитл',0,NULL,NULL,NULL,1,1,'2018-08-08 12:17:13','2018-05-07 06:32:22','express_user@%','2018-08-08 12:17:13','express_user@%'),(66,5,'Провайдер Флайнет',0,NULL,NULL,NULL,1,1,'2018-08-08 12:17:17','2018-05-07 06:32:22','express_user@%','2018-08-08 12:17:17','express_user@%'),(67,5,'Viasat',0,NULL,NULL,NULL,1,1,'2018-08-08 12:17:24','2018-05-07 06:32:22','express_user@%','2018-08-08 12:17:24','express_user@%'),(68,5,'Extra TV',0,NULL,NULL,NULL,1,1,'2018-08-08 12:17:29','2018-05-07 06:32:22','express_user@%','2018-08-08 12:17:29','express_user@%'),(69,5,'РЦК-осток',0,NULL,NULL,NULL,1,1,'2018-08-08 12:17:33','2018-05-07 06:32:22','express_user@%','2018-08-08 12:17:33','express_user@%'),(70,5,'Соц сети ОК',0,NULL,NULL,NULL,1,1,'2018-08-08 12:17:37','2018-05-07 06:32:22','express_user@%','2018-08-08 12:17:37','express_user@%'),(71,5,'Соц сети ВК',0,NULL,NULL,NULL,1,1,'2018-05-16 06:29:13','2018-05-07 06:32:22','express_user@%','2018-05-16 06:29:10','express_user@%'),(72,5,'Соц сети Мой мир',0,NULL,NULL,NULL,1,1,'2018-08-08 12:17:47','2018-05-07 06:32:22','express_user@%','2018-08-08 12:17:47','express_user@%'),(73,5,'Соц сети Мail.ru агент',0,NULL,NULL,NULL,1,1,'2018-08-08 12:17:52','2018-05-07 06:32:22','express_user@%','2018-08-08 12:17:52','express_user@%'),(74,5,'Соц сети НТ-online',0,NULL,NULL,NULL,1,1,'2018-08-08 12:17:56','2018-05-07 06:32:22','express_user@%','2018-08-08 12:17:56','express_user@%'),(75,5,'Ж/д, авиа билет РФ',0,NULL,NULL,NULL,1,1,'2018-08-04 08:39:50','2018-05-07 06:32:22','express_user@%','2018-08-04 08:39:50','express_user@%'),(76,5,'Ж/д, авиа билет',0,NULL,NULL,NULL,1,1,'2018-08-04 08:39:16','2018-05-07 06:32:22','express_user@%','2018-08-04 08:39:16','express_user@%'),(77,5,'Телефон РФ',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(78,5,'БУМ',0,NULL,NULL,NULL,1,1,'2018-08-08 07:23:45','2018-05-07 06:32:22','express_user@%','2018-08-08 07:23:45','express_user@%'),(79,5,'Объявление OLX',0,NULL,NULL,NULL,1,1,'2018-08-06 06:26:22','2018-05-07 06:32:22','express_user@%','2018-08-06 06:26:22','express_user@%'),(80,5,'БУМ ПАРТНЕР',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(81,7,'РФ р/с ',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%'),(82,7,'УКР р/с ',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:32:22','express_user@%','2018-05-07 07:16:46','express_user@%');
/*!40000 ALTER TABLE `book_service` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_service$before_ins
BEFORE INSERT
ON book_service FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_service$before_upd
BEFORE UPDATE
ON book_service FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_service_type`
--

DROP TABLE IF EXISTS `book_service_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_service_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(60) NOT NULL COMMENT 'Тип услуги',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_service_type$descr` (`descr`),
  KEY `fk$book_service_type$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$book_service_type$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$book_service_type$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$book_service_type$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$book_service_type$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_service_type$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_service_type$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_service_type$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Типы услуг';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_service_type`
--

LOCK TABLES `book_service_type` WRITE;
/*!40000 ALTER TABLE `book_service_type` DISABLE KEYS */;
INSERT INTO `book_service_type` VALUES (1,'отправка',NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:33:10','express_user@%',NULL,NULL),(2,'снятие',NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:33:10','express_user@%',NULL,NULL),(3,'доп. услуга',NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:33:10','express_user@%','2018-08-08 07:16:18','express_user@%'),(4,'колибри',NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-08 07:16:18','express_user@%',NULL,NULL),(5,'билеты',NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-08 07:16:18','express_user@%',NULL,NULL),(6,'панда',NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-08 07:16:18','express_user@%',NULL,NULL),(7,'бум',NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-08 07:16:18','express_user@%',NULL,NULL),(8,'межд. перев.',NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-08 07:16:18','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `book_service_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_service_type$before_ins
BEFORE INSERT
ON book_service_type FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_service_type$before_upd
BEFORE UPDATE
ON book_service_type FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_speed`
--

DROP TABLE IF EXISTS `book_speed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_speed` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(80) NOT NULL COMMENT 'Скорость/Особенность выполнения заявки',
  `sorting` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Порядок сортировки',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_speed$descr` (`descr`),
  KEY `fk$book_speed$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$book_speed$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$book_speed$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_speed$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Скорость/Особенность выполнения заявки';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_speed`
--

LOCK TABLES `book_speed` WRITE;
/*!40000 ALTER TABLE `book_speed` DISABLE KEYS */;
INSERT INTO `book_speed` VALUES (1,'Обычный',1,0,'2017-10-06 08:11:50','express_user@%',NULL,NULL,NULL,NULL),(2,'Срочный',1,0,'2017-10-06 08:11:50','express_user@%',NULL,NULL,NULL,NULL),(3,'Супер срочный',1,0,'2017-10-06 08:11:50','express_user@%',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `book_speed` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_speed$before_ins
BEFORE INSERT
ON book_speed FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_speed$before_upd
BEFORE UPDATE
ON book_speed FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_state`
--

DROP TABLE IF EXISTS `book_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_state` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(80) NOT NULL COMMENT 'Состояние операции',
  `sorting` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Порядок сортировки',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_state$descr` (`descr`),
  KEY `fk$book_state$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$book_state$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$book_state$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_state$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Состояние операции';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_state`
--

LOCK TABLES `book_state` WRITE;
/*!40000 ALTER TABLE `book_state` DISABLE KEYS */;
INSERT INTO `book_state` VALUES (1,'Создана',1,0,'2017-10-06 09:13:10','express_user@%',NULL,NULL,NULL,NULL),(2,'Проведена',1,0,'2017-10-06 09:13:10','express_user@%',NULL,NULL,NULL,NULL),(3,'Удалена',1,0,'2017-10-06 09:13:10','express_user@%',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `book_state` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_state$before_ins
BEFORE INSERT
ON book_state FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_state$before_upd
BEFORE UPDATE
ON book_state FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_status`
--

DROP TABLE IF EXISTS `book_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(30) NOT NULL COMMENT 'Название статуса',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Статусы заявок';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_status`
--

LOCK TABLES `book_status` WRITE;
/*!40000 ALTER TABLE `book_status` DISABLE KEYS */;
INSERT INTO `book_status` VALUES (1,'Создана',NULL,'2018-05-07 10:33:46','express_user@%',NULL,NULL),(2,'К оплате',NULL,'2018-05-07 10:33:46','express_user@%',NULL,NULL),(3,'К выполнению',NULL,'2018-05-07 10:33:46','express_user@%',NULL,NULL),(4,'К возврату',NULL,'2018-05-07 10:33:46','express_user@%',NULL,NULL),(5,'Выполнена',NULL,'2018-05-07 10:33:46','express_user@%',NULL,NULL),(6,'Выполнена с возвратом',NULL,'2018-05-07 10:33:46','express_user@%',NULL,NULL),(7,'В архив',NULL,'2018-05-07 10:33:46','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `book_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_status$before_ins
BEFORE INSERT
ON book_status FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_status$before_upd
BEFORE UPDATE
ON book_status FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_stock`
--

DROP TABLE IF EXISTS `book_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_stock` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(80) NOT NULL COMMENT 'Описание акции',
  `percent` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Процент по акции',
  `discount` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Скидка по акции',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_stock$descr` (`descr`),
  KEY `fk$book_stock$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$book_stock$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$book_stock$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_stock$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='Акции';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_stock`
--

LOCK TABLES `book_stock` WRITE;
/*!40000 ALTER TABLE `book_stock` DISABLE KEYS */;
INSERT INTO `book_stock` VALUES (1,'Зимняя скидка',NULL,NULL,0,'2017-11-21 11:42:13','express_user@%',NULL,NULL,NULL,NULL),(2,'Весенняя скидка',3.00,NULL,0,'2017-11-21 11:42:13','express_user@%',NULL,NULL,NULL,NULL),(3,'Супер срочный',NULL,2.00,0,'2017-11-21 11:42:13','express_user@%',NULL,NULL,NULL,NULL),(8,'asd1',1.00,1.00,1,'2017-12-18 11:45:30','express_user@%',NULL,NULL,NULL,NULL),(9,'asd2',0.92,NULL,1,'2017-12-18 11:49:39','express_user@%',NULL,'2017-12-18 12:08:51','express_user@%',NULL),(11,'asd3',1.00,2.00,1,'2017-12-26 06:46:49','express_user@%',NULL,'2017-12-26 06:46:59','express_user@%',NULL);
/*!40000 ALTER TABLE `book_stock` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`book_stock$before_ins` 
BEFORE INSERT 
ON express_db.book_stock 
FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`book_stock$before_upd` 
BEFORE UPDATE 
ON express_db.book_stock 
FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_system`
--

DROP TABLE IF EXISTS `book_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_system` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `category_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_category',
  `descr` varchar(60) NOT NULL COMMENT 'Название услуги',
  `is_archive` tinyint(1) DEFAULT '0' COMMENT 'В архиве?',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_service$descr` (`descr`),
  KEY `fk$book_system$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$book_system$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$book_system$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$book_system$change_office_id$book_office$id` (`change_office_id`),
  KEY `fk$book_system$category_id$book_category$id` (`category_id`),
  CONSTRAINT `fk$book_system$category_id$book_category$id` FOREIGN KEY (`category_id`) REFERENCES `book_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_system$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_system$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_system$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_system$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='Справочник систем ПКЦ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_system`
--

LOCK TABLES `book_system` WRITE;
/*!40000 ALTER TABLE `book_system` DISABLE KEYS */;
INSERT INTO `book_system` VALUES (1,1,'Приват карта',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(2,1,'Приват Platinum карта',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(3,1,'Альфа РФ карта',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(4,1,'Альфа УКР карта',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(5,1,'Тинькофф карта',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(6,1,'ПУМБ карта',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(7,1,'Unicredit карта',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(8,1,'Сбер карта (Ростов)',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(9,1,'Сбер карта (Краснодар)',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(10,1,'Сбер карта (Москва)',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(11,2,'QIWI',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(12,2,'Яндекс',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(13,2,'WebMoney',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(14,2,'Payeer',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(15,3,'Норета',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(16,3,'Наталья РФ',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(17,3,'Ирина УКР',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(18,4,'Кассовая зона 1',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(19,4,'Касовая зона 2',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL),(20,6,'Колибри',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 06:17:56','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `book_system` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_system$before_ins
BEFORE INSERT
ON book_system FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_system$before_upd
BEFORE UPDATE
ON book_system FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_system_category`
--

DROP TABLE IF EXISTS `book_system_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_system_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(80) NOT NULL COMMENT 'Описание категории системы',
  `fulldescr` varchar(160) NOT NULL COMMENT 'Полное описание категории системы',
  `sorting` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Порядок сортировки',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_system_category$descr` (`descr`),
  UNIQUE KEY `uk$book_system_category$fulldescr` (`fulldescr`),
  KEY `fk$book_system_category$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$book_system_category$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$book_system_category$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_system_category$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Категория системы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_system_category`
--

LOCK TABLES `book_system_category` WRITE;
/*!40000 ALTER TABLE `book_system_category` DISABLE KEYS */;
INSERT INTO `book_system_category` VALUES (1,'Карта','Банковская карта',1,0,'2017-10-03 13:58:55','express_user@%',NULL,NULL,NULL,NULL),(2,'Кошелек','Электронный кошелек',1,0,'2017-10-03 13:58:55','express_user@%',NULL,NULL,NULL,NULL),(3,'МП','Международные переводы',1,0,'2017-10-03 13:58:55','express_user@%',NULL,NULL,NULL,NULL),(4,'Колибри','Денежные переводы Колибри',1,0,'2017-10-03 13:58:55','express_user@%',NULL,NULL,NULL,NULL),(5,'ПКЦ продукты','ПКЦ продукты',1,0,'2017-10-03 13:58:55','express_user@%',NULL,NULL,NULL,NULL),(6,'Касса','Касса',1,0,'2017-10-04 07:56:11','express_user@%',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `book_system_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_system_category$before_ins
BEFORE INSERT
ON book_system_category FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_system_category$before_upd
BEFORE UPDATE
ON book_system_category FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_tariff`
--

DROP TABLE IF EXISTS `book_tariff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_tariff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `tariff` decimal(19,2) unsigned NOT NULL COMMENT 'Значение тарифа',
  `sum_min` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Минимальное значение диапазона сумм',
  `sum_max` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Максимальное значение диапазона сумм',
  `date_begin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата начала действия тарифа',
  `date_end` timestamp NULL DEFAULT NULL COMMENT 'Дата окончания действия тарифа',
  `currency_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_currency',
  `service_type_id` int(10) unsigned NOT NULL COMMENT 'Ссылка cross_service_type',
  `tariff_type_id` int(10) unsigned NOT NULL COMMENT 'Ссылка book_tariff_type',
  `note` varchar(60) DEFAULT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_tariff$tariff$` (`tariff`,`date_begin`,`currency_id`,`service_type_id`,`tariff_type_id`),
  KEY `fk$book_tariff$currency_id$book_currency$id` (`currency_id`),
  KEY `fk$book_tariff$service_type_id$cross_service_type$id` (`service_type_id`),
  KEY `fk$book_tariff$tariff_type_id$book_tariff_type$id` (`tariff_type_id`),
  KEY `fk$book_tariff$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$book_tariff$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$book_tariff$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$book_tariff$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$book_tariff$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_tariff$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_tariff$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_tariff$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_tariff$currency_id$book_currency$id` FOREIGN KEY (`currency_id`) REFERENCES `book_currency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_tariff$service_type_id$cross_service_type$id` FOREIGN KEY (`service_type_id`) REFERENCES `cross_service_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_tariff$tariff_type_id$book_tariff_type$id` FOREIGN KEY (`tariff_type_id`) REFERENCES `book_tariff_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='Тарифы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_tariff`
--

LOCK TABLES `book_tariff` WRITE;
/*!40000 ALTER TABLE `book_tariff` DISABLE KEYS */;
INSERT INTO `book_tariff` VALUES (1,50.00,0.00,5000.00,'2018-03-31 21:00:00',NULL,3,6,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:24:47','express_user@%',NULL,NULL),(2,3.00,0.00,5000.00,'2018-03-31 21:00:00',NULL,3,6,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-18 05:34:23','2018-05-07 07:24:47','express_user@%','2018-07-18 05:34:23','express_user@%'),(3,10.00,1.00,5000.00,'2018-05-16 10:30:00','2018-12-30 21:00:00',2,1,1,NULL,1,1,'2018-05-15 22:30:19',1,1,'2018-05-23 09:04:39','2018-05-16 10:30:19','express_user@%','2018-05-23 09:04:38','express_user@%'),(4,1.00,1.00,5000.00,'2018-05-23 07:17:00','2018-12-12 07:17:00',2,1,4,NULL,1,1,'2018-05-23 07:18:14',NULL,NULL,NULL,'2018-05-23 07:18:13','express_user@%',NULL,NULL),(5,20.00,0.00,8000.00,'2018-05-28 05:18:00','2020-07-02 05:18:00',2,35,1,NULL,1,1,'2018-05-28 05:20:56',NULL,NULL,NULL,'2018-05-28 05:20:56','express_user@%',NULL,NULL),(6,10.00,80001.00,15000.00,'2018-05-28 05:20:00','2020-03-27 05:20:00',2,35,1,NULL,1,1,'2018-05-28 05:21:53',NULL,NULL,NULL,'2018-05-28 05:21:53','express_user@%',NULL,NULL),(7,5.00,15001.00,55000.00,'2018-05-28 05:22:00','2020-08-27 05:22:00',2,35,1,NULL,1,1,'2018-05-28 05:22:58',NULL,NULL,NULL,'2018-05-28 05:22:58','express_user@%',NULL,NULL),(8,0.00,55001.00,9999999999.00,'2018-05-28 05:23:00','2020-05-27 05:23:00',2,35,1,NULL,1,1,'2018-05-28 05:24:06',NULL,NULL,NULL,'2018-05-28 05:24:06','express_user@%',NULL,NULL),(9,5.00,0.00,8000.00,'2018-05-28 05:24:00','2019-11-27 05:24:00',2,35,2,NULL,1,1,'2018-05-28 05:25:06',NULL,NULL,NULL,'2018-05-28 05:25:06','express_user@%',NULL,NULL),(10,4.00,8001.00,15000.00,'2018-05-28 05:25:00','2020-04-30 05:25:00',2,35,2,NULL,1,1,'2018-05-28 05:25:51',NULL,NULL,NULL,'2018-05-28 05:25:51','express_user@%',NULL,NULL),(11,3.00,15001.00,55000.00,'2018-05-28 05:25:00','2020-07-30 05:25:00',2,35,2,NULL,1,1,'2018-05-28 05:26:46',NULL,NULL,NULL,'2018-05-28 05:26:46','express_user@%',NULL,NULL),(12,2.00,55000.00,9999999999.00,'2018-05-28 05:28:00','2020-05-27 05:28:00',2,35,2,NULL,1,1,'2018-05-28 05:29:33',NULL,NULL,NULL,'2018-05-28 05:29:33','express_user@%',NULL,NULL),(13,1.00,0.00,9999999999.00,'2018-05-28 05:29:00','2021-02-23 05:29:00',2,35,4,NULL,1,1,'2018-05-28 05:30:27',NULL,NULL,NULL,'2018-05-28 05:30:27','express_user@%',NULL,NULL),(14,5.00,0.00,5000.00,'2018-04-28 13:10:00','2018-12-28 13:10:00',2,1,5,NULL,1,1,'2018-05-28 01:10:58',1,1,'2018-07-17 13:48:32','2018-05-28 13:10:58','express_user@%','2018-07-17 13:48:32','express_user@%'),(15,5.00,1.00,10000.00,'1970-09-22 00:00:00',NULL,1,3,2,NULL,NULL,NULL,'2018-07-17 13:49:33',NULL,NULL,'2018-07-21 09:16:10','2018-07-17 13:49:33','express_user@%','2018-07-21 09:16:10','express_user@%'),(17,5.00,0.00,5000.00,'2018-07-25 21:00:00','2019-01-03 09:12:00',1,39,2,NULL,1,1,'2018-07-27 09:43:15',1,1,'2018-07-27 11:04:51','2018-07-27 09:43:15','express_user@%','2018-07-27 11:04:51','express_user@%'),(18,8.00,5000.00,10000.00,'2017-12-31 21:00:00','2020-02-20 00:15:00',2,1,2,NULL,1,1,'2018-07-30 09:27:38',1,1,'2018-07-30 09:31:29','2018-07-30 09:27:38','express_user@%','2018-07-30 09:31:29','express_user@%'),(19,30.00,0.00,5000.00,'2018-08-01 21:00:00',NULL,1,38,3,NULL,1,1,'2018-08-02 12:09:47',NULL,NULL,'2018-08-02 12:09:47','2018-08-02 12:09:47','express_user@%',NULL,NULL),(20,20.00,5001.00,10000.00,'2018-08-01 21:00:00',NULL,1,38,3,NULL,1,1,'2018-08-02 12:11:26',NULL,NULL,'2018-08-02 12:11:26','2018-08-02 12:11:26','express_user@%',NULL,NULL),(21,10.00,10001.00,15000.00,'2018-08-01 21:00:00',NULL,1,38,1,NULL,1,1,'2018-08-02 12:13:07',NULL,NULL,'2018-08-02 12:13:07','2018-08-02 12:13:07','express_user@%',NULL,NULL),(22,3.00,0.00,3000.00,'2018-08-01 21:00:00',NULL,1,38,2,NULL,1,1,'2018-08-02 12:18:14',NULL,NULL,'2018-08-02 12:18:14','2018-08-02 12:18:14','express_user@%',NULL,NULL),(23,2.00,3001.00,6000.00,'2018-08-01 21:00:00',NULL,1,38,2,NULL,1,1,'2018-08-02 12:19:55',NULL,NULL,'2018-08-02 12:19:55','2018-08-02 12:19:55','express_user@%',NULL,NULL),(24,1.00,6001.00,99999999.00,'2018-08-01 21:00:00',NULL,1,38,2,NULL,1,1,'2018-08-02 12:20:58',NULL,NULL,'2018-08-02 12:20:58','2018-08-02 12:20:58','express_user@%',NULL,NULL),(25,1.00,0.00,1000.00,'2018-08-04 05:00:00',NULL,3,5,4,NULL,1,1,'2018-08-04 06:43:08',NULL,NULL,'2018-08-04 06:43:08','2018-08-04 06:43:08','express_user@%',NULL,NULL),(26,50.00,0.00,999999999.00,'2018-01-01 05:00:00',NULL,2,153,1,NULL,1,1,'2018-08-06 08:57:18',1,1,'2018-08-06 11:07:06','2018-08-06 08:57:18','express_user@%','2018-08-06 11:07:06','express_user@%'),(27,100.00,0.00,99999999.00,'2018-08-06 05:00:00','2018-08-30 21:12:00',1,153,1,NULL,1,1,'2018-08-06 08:59:51',1,1,'2018-08-06 10:47:08','2018-08-06 08:59:51','express_user@%','2018-08-06 10:47:08','express_user@%'),(28,1.00,10.00,50000.00,'2018-08-06 05:00:00',NULL,2,153,5,NULL,1,1,'2018-08-06 10:55:21',NULL,NULL,'2018-08-06 10:55:21','2018-08-06 10:55:21','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `book_tariff` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_tariff$before_ins
BEFORE INSERT
ON book_tariff FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_tariff$before_upd
BEFORE UPDATE
ON book_tariff FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_tariff_group`
--

DROP TABLE IF EXISTS `book_tariff_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_tariff_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(60) NOT NULL COMMENT 'Название группы тарифа',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_tariff_group$descr` (`descr`),
  KEY `fk$book_tariff_group$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$book_tariff_group$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$book_tariff_group$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$book_tariff_group$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$book_tariff_group$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_tariff_group$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_tariff_group$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_tariff_group$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Справочник групп тарифов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_tariff_group`
--

LOCK TABLES `book_tariff_group` WRITE;
/*!40000 ALTER TABLE `book_tariff_group` DISABLE KEYS */;
INSERT INTO `book_tariff_group` VALUES (1,'Базовая группа',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:27:44','express_user@%',NULL,NULL),(2,'Тарифы Луганск',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:27:44','express_user@%',NULL,NULL),(3,'Тарифы Ровеньки',0,NULL,NULL,NULL,1,1,'2018-05-14 09:18:03','2018-05-07 07:27:44','express_user@%','2018-05-14 09:18:03','express_user@%');
/*!40000 ALTER TABLE `book_tariff_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_tariff_group$before_ins
BEFORE INSERT
ON book_tariff_group FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_tariff_group$before_upd
BEFORE UPDATE
ON book_tariff_group FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `book_tariff_type`
--

DROP TABLE IF EXISTS `book_tariff_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_tariff_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(60) NOT NULL COMMENT 'Название типа тарифа',
  `percent_unit` enum('percent','unit') NOT NULL COMMENT 'Тариф в процентах или д.е.',
  `plus_minus` enum('plus','minus') NOT NULL COMMENT 'Знак тарифа',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве? 0-нет, 1-да',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$book_tariff_type$descr` (`descr`),
  KEY `fk$book_tariff_type$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$book_tariff_type$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$book_tariff_type$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$book_tariff_type$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$book_tariff_type$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_tariff_type$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_tariff_type$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$book_tariff_type$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Справочник типов тарифов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_tariff_type`
--

LOCK TABLES `book_tariff_type` WRITE;
/*!40000 ALTER TABLE `book_tariff_type` DISABLE KEYS */;
INSERT INTO `book_tariff_type` VALUES (1,'Комиссия ПКЦ д.е.','unit','plus',0,NULL,NULL,NULL,1,1,'2018-07-23 11:15:17','2018-05-07 07:19:52','express_user@%','2018-07-23 11:15:17','express_user@%'),(2,'Комиссия ПКЦ %','percent','plus',0,NULL,NULL,NULL,1,1,'2018-07-23 11:15:22','2018-05-07 07:19:52','express_user@%','2018-07-23 11:15:22','express_user@%'),(3,'Комиссия системы д.е','unit','plus',0,NULL,NULL,NULL,1,1,'2018-07-23 11:15:27','2018-05-07 07:19:52','express_user@%','2018-07-23 11:15:27','express_user@%'),(4,'Комиссия системы %','percent','plus',0,NULL,NULL,NULL,1,1,'2018-07-23 11:15:32','2018-05-07 07:19:52','express_user@%','2018-07-23 11:15:32','express_user@%'),(5,'Акция %','percent','minus',0,NULL,NULL,NULL,1,1,'2018-07-23 11:15:37','2018-05-07 07:19:52','express_user@%','2018-07-23 11:15:37','express_user@%'),(6,'Акция д.е.','unit','minus',0,NULL,NULL,NULL,1,1,'2018-07-23 11:15:43','2018-05-07 07:19:52','express_user@%','2018-07-23 11:15:43','express_user@%');
/*!40000 ALTER TABLE `book_tariff_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_tariff_type$before_ins
BEFORE INSERT
ON book_tariff_type FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER book_tariff_type$before_upd
BEFORE UPDATE
ON book_tariff_type FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cross_service_type`
--

DROP TABLE IF EXISTS `cross_service_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cross_service_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `service_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_service',
  `type_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_service_type',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$cross_service_type$service_id$type_id` (`service_id`,`type_id`),
  KEY `fk$cross_service_type$type_id$book_service_type$id` (`type_id`),
  KEY `fk$cross_service_type$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$cross_service_type$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$cross_service_type$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$cross_service_type$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$cross_service_type$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_service_type$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_service_type$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_service_type$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_service_type$service_id$book_service$id` FOREIGN KEY (`service_id`) REFERENCES `book_service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk$cross_service_type$type_id$book_service_type$id` FOREIGN KEY (`type_id`) REFERENCES `book_service_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8 COMMENT='Кросс таблица услуг и типов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cross_service_type`
--

LOCK TABLES `cross_service_type` WRITE;
/*!40000 ALTER TABLE `cross_service_type` DISABLE KEYS */;
INSERT INTO `cross_service_type` VALUES (1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(2,2,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(3,3,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(4,4,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(5,5,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(6,6,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(7,7,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(14,14,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(15,15,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(16,16,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(17,17,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(18,18,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(19,19,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(20,20,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(21,21,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(22,22,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(23,23,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(24,24,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(26,26,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(27,27,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(28,28,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(29,29,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(30,30,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(31,31,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(32,32,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(33,33,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(34,34,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(35,1,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(36,2,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(37,3,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(38,4,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(39,5,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(40,6,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(41,7,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(48,14,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(49,81,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(50,82,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(51,25,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(61,35,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(62,36,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(63,37,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(66,40,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(67,41,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(68,42,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(69,43,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(70,44,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(71,45,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(72,46,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(73,47,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(74,48,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(75,49,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(76,50,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(77,51,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(78,52,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(79,53,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(80,54,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(81,55,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(82,56,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(83,57,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(84,58,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(85,59,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(86,60,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(87,61,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(88,62,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(102,76,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(103,77,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(106,80,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:17:12','express_user@%','2018-05-07 07:23:21','express_user@%'),(120,8,2,1,1,'2018-05-14 23:59:56',NULL,NULL,NULL,'2018-05-15 11:59:54','express_user@%',NULL,NULL),(121,8,1,1,1,'2018-05-14 23:59:56',NULL,NULL,NULL,'2018-05-15 11:59:54','express_user@%',NULL,NULL),(126,38,3,1,1,'2018-05-16 06:28:10',NULL,NULL,NULL,'2018-05-16 06:28:07','express_user@%',NULL,NULL),(127,39,3,1,1,'2018-05-16 06:28:44',NULL,NULL,NULL,'2018-05-16 06:28:41','express_user@%',NULL,NULL),(153,76,1,1,1,'2018-08-04 08:39:17',NULL,NULL,'2018-08-04 08:39:17','2018-08-04 08:39:17','express_user@%',NULL,NULL),(157,11,8,1,1,'2018-08-08 07:17:46',NULL,NULL,'2018-08-08 07:17:46','2018-08-08 07:17:46','express_user@%',NULL,NULL),(158,12,8,1,1,'2018-08-08 07:17:56',NULL,NULL,'2018-08-08 07:17:56','2018-08-08 07:17:56','express_user@%',NULL,NULL),(159,13,8,1,1,'2018-08-08 07:20:37',NULL,NULL,'2018-08-08 07:20:37','2018-08-08 07:20:37','express_user@%',NULL,NULL),(160,10,8,1,1,'2018-08-08 07:20:47',NULL,NULL,'2018-08-08 07:20:47','2018-08-08 07:20:47','express_user@%',NULL,NULL),(161,9,8,1,1,'2018-08-08 07:20:55',NULL,NULL,'2018-08-08 07:20:55','2018-08-08 07:20:55','express_user@%',NULL,NULL),(162,75,5,1,1,'2018-08-08 07:23:09',NULL,NULL,'2018-08-08 07:23:09','2018-08-08 07:23:09','express_user@%',NULL,NULL),(163,76,5,1,1,'2018-08-08 07:23:22',NULL,NULL,'2018-08-08 07:23:22','2018-08-08 07:23:22','express_user@%',NULL,NULL),(164,78,7,1,1,'2018-08-08 07:23:45',NULL,NULL,'2018-08-08 07:23:45','2018-08-08 07:23:45','express_user@%',NULL,NULL),(165,79,5,1,1,'2018-08-08 07:23:56',NULL,NULL,'2018-08-08 07:23:56','2018-08-08 07:23:56','express_user@%',NULL,NULL),(166,26,4,1,1,'2018-08-08 12:15:12',NULL,NULL,'2018-08-08 12:15:12','2018-08-08 12:15:12','express_user@%',NULL,NULL),(167,27,4,1,1,'2018-08-08 12:15:19',NULL,NULL,'2018-08-08 12:15:19','2018-08-08 12:15:19','express_user@%',NULL,NULL),(168,28,4,1,1,'2018-08-08 12:15:26',NULL,NULL,'2018-08-08 12:15:26','2018-08-08 12:15:26','express_user@%',NULL,NULL),(169,29,4,1,1,'2018-08-08 12:15:32',NULL,NULL,'2018-08-08 12:15:32','2018-08-08 12:15:32','express_user@%',NULL,NULL),(170,30,4,1,1,'2018-08-08 12:15:37',NULL,NULL,'2018-08-08 12:15:37','2018-08-08 12:15:37','express_user@%',NULL,NULL),(171,31,4,1,1,'2018-08-08 12:15:42',NULL,NULL,'2018-08-08 12:15:42','2018-08-08 12:15:42','express_user@%',NULL,NULL),(172,32,4,1,1,'2018-08-08 12:15:47',NULL,NULL,'2018-08-08 12:15:47','2018-08-08 12:15:47','express_user@%',NULL,NULL),(173,33,4,1,1,'2018-08-08 12:15:51',NULL,NULL,'2018-08-08 12:15:51','2018-08-08 12:15:51','express_user@%',NULL,NULL),(174,34,4,1,1,'2018-08-08 12:15:57',NULL,NULL,'2018-08-08 12:15:57','2018-08-08 12:15:57','express_user@%',NULL,NULL),(175,63,1,1,1,'2018-08-08 12:17:03',NULL,NULL,'2018-08-08 12:17:03','2018-08-08 12:17:03','express_user@%',NULL,NULL),(176,63,4,1,1,'2018-08-08 12:17:03',NULL,NULL,'2018-08-08 12:17:03','2018-08-08 12:17:03','express_user@%',NULL,NULL),(177,64,1,1,1,'2018-08-08 12:17:07',NULL,NULL,'2018-08-08 12:17:07','2018-08-08 12:17:07','express_user@%',NULL,NULL),(178,64,4,1,1,'2018-08-08 12:17:07',NULL,NULL,'2018-08-08 12:17:07','2018-08-08 12:17:07','express_user@%',NULL,NULL),(179,65,1,1,1,'2018-08-08 12:17:13',NULL,NULL,'2018-08-08 12:17:13','2018-08-08 12:17:13','express_user@%',NULL,NULL),(180,65,4,1,1,'2018-08-08 12:17:13',NULL,NULL,'2018-08-08 12:17:13','2018-08-08 12:17:13','express_user@%',NULL,NULL),(181,66,1,1,1,'2018-08-08 12:17:17',NULL,NULL,'2018-08-08 12:17:17','2018-08-08 12:17:17','express_user@%',NULL,NULL),(182,66,4,1,1,'2018-08-08 12:17:17',NULL,NULL,'2018-08-08 12:17:17','2018-08-08 12:17:17','express_user@%',NULL,NULL),(183,67,1,1,1,'2018-08-08 12:17:24',NULL,NULL,'2018-08-08 12:17:24','2018-08-08 12:17:24','express_user@%',NULL,NULL),(184,67,4,1,1,'2018-08-08 12:17:24',NULL,NULL,'2018-08-08 12:17:24','2018-08-08 12:17:24','express_user@%',NULL,NULL),(185,68,1,1,1,'2018-08-08 12:17:29',NULL,NULL,'2018-08-08 12:17:29','2018-08-08 12:17:29','express_user@%',NULL,NULL),(186,68,4,1,1,'2018-08-08 12:17:29',NULL,NULL,'2018-08-08 12:17:29','2018-08-08 12:17:29','express_user@%',NULL,NULL),(187,69,1,1,1,'2018-08-08 12:17:33',NULL,NULL,'2018-08-08 12:17:33','2018-08-08 12:17:33','express_user@%',NULL,NULL),(188,69,4,1,1,'2018-08-08 12:17:34',NULL,NULL,'2018-08-08 12:17:34','2018-08-08 12:17:34','express_user@%',NULL,NULL),(189,70,1,1,1,'2018-08-08 12:17:37',NULL,NULL,'2018-08-08 12:17:37','2018-08-08 12:17:37','express_user@%',NULL,NULL),(190,70,4,1,1,'2018-08-08 12:17:37',NULL,NULL,'2018-08-08 12:17:37','2018-08-08 12:17:37','express_user@%',NULL,NULL),(191,71,1,1,1,'2018-08-08 12:17:42',NULL,NULL,'2018-08-08 12:17:42','2018-08-08 12:17:42','express_user@%',NULL,NULL),(192,71,4,1,1,'2018-08-08 12:17:42',NULL,NULL,'2018-08-08 12:17:42','2018-08-08 12:17:42','express_user@%',NULL,NULL),(193,72,1,1,1,'2018-08-08 12:17:47',NULL,NULL,'2018-08-08 12:17:47','2018-08-08 12:17:47','express_user@%',NULL,NULL),(194,72,4,1,1,'2018-08-08 12:17:47',NULL,NULL,'2018-08-08 12:17:47','2018-08-08 12:17:47','express_user@%',NULL,NULL),(195,73,1,1,1,'2018-08-08 12:17:52',NULL,NULL,'2018-08-08 12:17:52','2018-08-08 12:17:52','express_user@%',NULL,NULL),(196,73,4,1,1,'2018-08-08 12:17:52',NULL,NULL,'2018-08-08 12:17:52','2018-08-08 12:17:52','express_user@%',NULL,NULL),(197,74,1,1,1,'2018-08-08 12:17:56',NULL,NULL,'2018-08-08 12:17:56','2018-08-08 12:17:56','express_user@%',NULL,NULL),(198,74,4,1,1,'2018-08-08 12:17:56',NULL,NULL,'2018-08-08 12:17:56','2018-08-08 12:17:56','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `cross_service_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_service_type$before_ins
BEFORE INSERT
ON cross_service_type FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_service_type$before_upd
BEFORE UPDATE
ON cross_service_type FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cross_system_service`
--

DROP TABLE IF EXISTS `cross_system_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cross_system_service` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `system_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_service',
  `service_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_system',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$cross_system_service$system_id$service_id` (`system_id`,`service_id`),
  KEY `fk$cross_system_service$service_id$book_service$id` (`service_id`),
  KEY `fk$cross_system_service$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$cross_system_service$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$cross_system_service$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$cross_system_service$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$cross_system_service$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_system_service$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_system_service$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_system_service$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_system_service$service_id$book_service$id` FOREIGN KEY (`service_id`) REFERENCES `book_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_system_service$system_id$book_system$id` FOREIGN KEY (`system_id`) REFERENCES `book_system` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица связывает системы и сервисы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cross_system_service`
--

LOCK TABLES `cross_system_service` WRITE;
/*!40000 ALTER TABLE `cross_system_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `cross_system_service` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_system_service$before_ins
BEFORE INSERT
ON cross_system_service FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_system_service$before_upd
BEFORE UPDATE
ON cross_system_service FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cross_tariff_deal`
--

DROP TABLE IF EXISTS `cross_tariff_deal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cross_tariff_deal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `deal_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на doc_deal',
  `tariff_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_tariff',
  `changed_tariff` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Изменённый тариф',
  `explanation` varchar(300) DEFAULT NULL COMMENT 'Объяснение изменения тарифа',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$cross_tariff_additional$tariff_id$book_tariff$id` (`tariff_id`),
  KEY `fk$cross_tariff_additional$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$cross_tariff_additional$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$cross_tariff_additional$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$cross_tariff_additional$change_office_id$book_office$id` (`change_office_id`),
  KEY `fk$cross_tariff_deal$deal_id$doc_deal$id` (`deal_id`),
  CONSTRAINT `fk$cross_tariff_additional$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_additional$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_additional$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_additional$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_additional$tariff_id$book_tariff$id` FOREIGN KEY (`tariff_id`) REFERENCES `book_tariff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_deal$deal_id$doc_deal$id` FOREIGN KEY (`deal_id`) REFERENCES `doc_deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Кросс таблица тарифов для additional';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cross_tariff_deal`
--

LOCK TABLES `cross_tariff_deal` WRITE;
/*!40000 ALTER TABLE `cross_tariff_deal` DISABLE KEYS */;
INSERT INTO `cross_tariff_deal` VALUES (1,2,3,10.00,NULL,1,1,'2018-08-08 07:33:15',NULL,NULL,'2018-08-08 07:33:15','2018-08-08 07:33:15','express_user@%',NULL,NULL),(2,2,4,1.00,NULL,1,1,'2018-08-08 07:33:15',NULL,NULL,'2018-08-08 07:33:15','2018-08-08 07:33:15','express_user@%',NULL,NULL),(3,5,3,10.00,NULL,1,1,'2018-08-08 09:00:26',NULL,NULL,'2018-08-08 09:00:26','2018-08-08 09:00:26','express_user@%',NULL,NULL),(4,5,4,5.00,'tak nado',1,1,'2018-08-08 09:00:26',NULL,NULL,'2018-08-08 09:00:26','2018-08-08 09:00:26','express_user@%',NULL,NULL),(5,5,14,5.00,NULL,1,1,'2018-08-08 09:00:26',NULL,NULL,'2018-08-08 09:00:26','2018-08-08 09:00:26','express_user@%',NULL,NULL),(7,25,3,10.00,NULL,1,1,'2018-08-13 10:34:54',NULL,NULL,'2018-08-13 10:34:54','2018-08-13 10:34:54','express_user@%',NULL,NULL),(8,25,4,1.00,NULL,1,1,'2018-08-13 10:34:54',NULL,NULL,'2018-08-13 10:34:54','2018-08-13 10:34:54','express_user@%',NULL,NULL),(9,27,3,10.00,NULL,1,1,'2018-08-13 10:35:35',NULL,NULL,'2018-08-13 10:35:35','2018-08-13 10:35:35','express_user@%',NULL,NULL),(10,27,4,1.00,NULL,1,1,'2018-08-13 10:35:35',NULL,NULL,'2018-08-13 10:35:35','2018-08-13 10:35:35','express_user@%',NULL,NULL),(11,32,3,10.00,NULL,1,1,'2018-08-13 12:47:40',NULL,NULL,'2018-08-13 12:47:40','2018-08-13 12:47:40','express_user@%',NULL,NULL),(12,32,4,1.00,NULL,1,1,'2018-08-13 12:47:40',NULL,NULL,'2018-08-13 12:47:40','2018-08-13 12:47:40','express_user@%',NULL,NULL),(13,34,3,10.00,NULL,1,1,'2018-08-16 11:11:04',NULL,NULL,'2018-08-16 11:11:04','2018-08-16 11:11:04','express_user@%',NULL,NULL),(14,34,4,1.00,NULL,1,1,'2018-08-16 11:11:04',NULL,NULL,'2018-08-16 11:11:04','2018-08-16 11:11:04','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `cross_tariff_deal` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_tariff_additional$before_ins
BEFORE INSERT
ON `cross_tariff_deal` FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_tariff_additional$before_upd
BEFORE UPDATE
ON `cross_tariff_deal` FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cross_tariff_group`
--

DROP TABLE IF EXISTS `cross_tariff_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cross_tariff_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `tariff_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_tariff',
  `group_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_tariff_group',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$cross_tariff_group$tariff_id$group_id` (`tariff_id`,`group_id`),
  KEY `fk$cross_tariff_group$group_id$book_tariff_group$id` (`group_id`),
  KEY `fk$cross_tariff_group$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$cross_tariff_group$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$cross_tariff_group$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$cross_tariff_group$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$cross_tariff_group$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_group$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_group$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_group$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_group$group_id$book_tariff_group$id` FOREIGN KEY (`group_id`) REFERENCES `book_tariff_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk$cross_tariff_group$tariff_id$book_tariff$id` FOREIGN KEY (`tariff_id`) REFERENCES `book_tariff` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COMMENT='Кросс таблица услуг и типов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cross_tariff_group`
--

LOCK TABLES `cross_tariff_group` WRITE;
/*!40000 ALTER TABLE `cross_tariff_group` DISABLE KEYS */;
INSERT INTO `cross_tariff_group` VALUES (1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:28:58','express_user@%',NULL,NULL),(2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:28:58','express_user@%',NULL,NULL),(3,2,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-07 07:28:58','express_user@%',NULL,NULL),(18,4,1,1,1,'2018-05-23 07:18:14',NULL,NULL,NULL,'2018-05-23 07:18:13','express_user@%',NULL,NULL),(19,4,2,1,1,'2018-05-23 07:18:14',NULL,NULL,NULL,'2018-05-23 07:18:13','express_user@%',NULL,NULL),(20,3,1,1,1,'2018-05-23 09:04:39',NULL,NULL,NULL,'2018-05-23 09:04:38','express_user@%',NULL,NULL),(21,3,2,1,1,'2018-05-23 09:04:39',NULL,NULL,NULL,'2018-05-23 09:04:38','express_user@%',NULL,NULL),(22,3,3,1,1,'2018-05-23 09:04:39',NULL,NULL,NULL,'2018-05-23 09:04:38','express_user@%',NULL,NULL),(23,5,1,1,1,'2018-05-28 05:20:56',NULL,NULL,NULL,'2018-05-28 05:20:56','express_user@%',NULL,NULL),(24,6,1,1,1,'2018-05-28 05:21:53',NULL,NULL,NULL,'2018-05-28 05:21:53','express_user@%',NULL,NULL),(25,7,1,1,1,'2018-05-28 05:22:58',NULL,NULL,NULL,'2018-05-28 05:22:58','express_user@%',NULL,NULL),(26,8,1,1,1,'2018-05-28 05:24:06',NULL,NULL,NULL,'2018-05-28 05:24:06','express_user@%',NULL,NULL),(27,9,1,1,1,'2018-05-28 05:25:06',NULL,NULL,NULL,'2018-05-28 05:25:06','express_user@%',NULL,NULL),(28,10,1,1,1,'2018-05-28 05:25:51',NULL,NULL,NULL,'2018-05-28 05:25:51','express_user@%',NULL,NULL),(29,11,1,1,1,'2018-05-28 05:26:46',NULL,NULL,NULL,'2018-05-28 05:26:46','express_user@%',NULL,NULL),(30,12,1,1,1,'2018-05-28 05:29:33',NULL,NULL,NULL,'2018-05-28 05:29:33','express_user@%',NULL,NULL),(31,13,1,1,1,'2018-05-28 05:30:27',NULL,NULL,NULL,'2018-05-28 05:30:27','express_user@%',NULL,NULL),(36,14,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-17 13:34:17','express_user@%',NULL,NULL),(37,14,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-17 13:34:31','express_user@%',NULL,NULL),(38,14,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-17 13:48:32','express_user@%',NULL,NULL),(39,15,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-17 13:49:33','express_user@%',NULL,NULL),(43,15,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-21 09:16:10','express_user@%',NULL,NULL),(45,17,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-27 09:43:15','express_user@%',NULL,NULL),(46,17,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-27 09:43:15','express_user@%',NULL,NULL),(47,17,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-27 09:43:15','express_user@%',NULL,NULL),(48,18,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 09:27:38','express_user@%',NULL,NULL),(49,18,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 09:29:59','express_user@%',NULL,NULL),(50,18,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 09:30:52','express_user@%',NULL,NULL),(51,19,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-02 12:09:47','express_user@%',NULL,NULL),(52,20,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-02 12:11:26','express_user@%',NULL,NULL),(53,21,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-02 12:13:07','express_user@%',NULL,NULL),(54,22,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-02 12:18:14','express_user@%',NULL,NULL),(55,23,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-02 12:19:55','express_user@%',NULL,NULL),(56,24,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-02 12:20:58','express_user@%',NULL,NULL),(57,25,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-04 06:43:08','express_user@%',NULL,NULL),(58,25,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-04 06:43:08','express_user@%',NULL,NULL),(59,25,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-04 06:43:08','express_user@%',NULL,NULL),(60,26,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-06 08:57:19','express_user@%',NULL,NULL),(61,26,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-06 08:57:19','express_user@%',NULL,NULL),(62,26,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-06 08:57:19','express_user@%',NULL,NULL),(63,27,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-06 08:59:51','express_user@%',NULL,NULL),(64,27,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-06 08:59:51','express_user@%',NULL,NULL),(65,27,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-06 08:59:51','express_user@%',NULL,NULL),(66,28,1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-06 10:55:21','express_user@%',NULL,NULL),(67,28,2,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-06 10:55:21','express_user@%',NULL,NULL),(68,28,3,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-06 10:55:21','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `cross_tariff_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_tariff_group$before_ins
BEFORE INSERT
ON cross_tariff_group FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_tariff_group$before_upd
BEFORE UPDATE
ON cross_tariff_group FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cross_tariff_send`
--

DROP TABLE IF EXISTS `cross_tariff_send`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cross_tariff_send` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `send_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на doc_send',
  `tariff_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_tariff',
  `changed_tariff` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Изменённый тариф',
  `explanation` varchar(300) DEFAULT NULL COMMENT 'Объяснение изменения тарифа',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$cross_tariff_send$send_id$tariff_id` (`send_id`,`tariff_id`),
  KEY `fk$cross_tariff_send$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$cross_tariff_send$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$cross_tariff_send$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$cross_tariff_send$change_office_id$book_office$id` (`change_office_id`),
  KEY `ix$cross_tariff_send$tariff_id$send_id` (`tariff_id`,`send_id`),
  CONSTRAINT `fk$cross_tariff_send$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_send$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_send$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_send$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_send$send_id$doc_send$id` FOREIGN KEY (`send_id`) REFERENCES `doc_send` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_send$tariff_id$book_tariff$id` FOREIGN KEY (`tariff_id`) REFERENCES `book_tariff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='Кросс таблица тарифов для отправки';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cross_tariff_send`
--

LOCK TABLES `cross_tariff_send` WRITE;
/*!40000 ALTER TABLE `cross_tariff_send` DISABLE KEYS */;
INSERT INTO `cross_tariff_send` VALUES (33,1,3,10.00,'',1,1,'2018-05-31 12:51:00',NULL,NULL,NULL,'2018-05-31 12:50:57','express_user@%',NULL,NULL),(34,1,4,1.00,'',1,1,'2018-05-31 12:51:00',NULL,NULL,NULL,'2018-05-31 12:50:57','express_user@%',NULL,NULL),(41,2,3,10.00,'',1,1,'2018-05-31 13:16:00',NULL,NULL,NULL,'2018-05-31 13:15:57','express_user@%',NULL,NULL),(42,2,4,1.00,'',1,1,'2018-05-31 13:16:00',NULL,NULL,NULL,'2018-05-31 13:15:57','express_user@%',NULL,NULL),(43,2,14,5.00,'',1,1,'2018-05-31 13:16:00',NULL,NULL,NULL,'2018-05-31 13:15:57','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `cross_tariff_send` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_tariff_send$before_ins
BEFORE INSERT
ON cross_tariff_send FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_tariff_send$before_upd
BEFORE UPDATE
ON cross_tariff_send FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cross_tariff_ticket`
--

DROP TABLE IF EXISTS `cross_tariff_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cross_tariff_ticket` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `ticket_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на doc_send',
  `tariff_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_tariff',
  `changed_tariff` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Изменённый тариф',
  `explanation` varchar(300) DEFAULT NULL COMMENT 'Объяснение изменения тарифа',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$cross_tariff_ticket$tariff_id$book_tariff$id` (`tariff_id`),
  KEY `fk$cross_tariff_ticket$ticket_id$doc_send$id` (`ticket_id`),
  KEY `fk$cross_tariff_ticket$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$cross_tariff_ticket$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$cross_tariff_ticket$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$cross_tariff_ticket$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$cross_tariff_ticket$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_ticket$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_ticket$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_ticket$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_ticket$tariff_id$book_tariff$id` FOREIGN KEY (`tariff_id`) REFERENCES `book_tariff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_ticket$ticket_id$doc_send$id` FOREIGN KEY (`ticket_id`) REFERENCES `doc_ticket` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Кросс таблица тарифов для билетов/олх';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cross_tariff_ticket`
--

LOCK TABLES `cross_tariff_ticket` WRITE;
/*!40000 ALTER TABLE `cross_tariff_ticket` DISABLE KEYS */;
INSERT INTO `cross_tariff_ticket` VALUES (1,1,26,50.00,NULL,1,1,'2018-08-06 11:28:54',NULL,NULL,'2018-08-06 11:28:54','2018-08-06 11:28:54','express_user@%',NULL,NULL),(2,2,26,100.00,'x2',4,11,'2018-08-07 10:45:14',NULL,NULL,'2018-08-07 10:45:14','2018-08-07 10:45:14','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `cross_tariff_ticket` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_tariff_ticket$before_ins
BEFORE INSERT
ON cross_tariff_ticket FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_tariff_ticket$before_upd
BEFORE UPDATE
ON cross_tariff_ticket FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cross_tariff_withdrawal`
--

DROP TABLE IF EXISTS `cross_tariff_withdrawal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cross_tariff_withdrawal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `withdrawal_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на doc_withdrawal',
  `tariff_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_tariff',
  `changed_tariff` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Изменённый тариф',
  `explanation` varchar(300) DEFAULT NULL COMMENT 'Объяснение изменения тарифа',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$cross_tariff_withdrawal$withdrawal_id$tariff_id` (`withdrawal_id`,`tariff_id`),
  KEY `fk$cross_tariff_withdrawal$tariff_id$book_tariff$id` (`tariff_id`),
  KEY `fk$cross_tariff_withdrawal$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$cross_tariff_withdrawal$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$cross_tariff_withdrawal$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$cross_tariff_withdrawal$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$cross_tariff_withdrawal$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_withdrawal$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_withdrawal$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_withdrawal$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_withdrawal$tariff_id$book_tariff$id` FOREIGN KEY (`tariff_id`) REFERENCES `book_tariff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$cross_tariff_withdrawal$withdrawal_id$doc_withdrawal$id` FOREIGN KEY (`withdrawal_id`) REFERENCES `doc_withdrawal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='Кросс таблица тарифов для снятия';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cross_tariff_withdrawal`
--

LOCK TABLES `cross_tariff_withdrawal` WRITE;
/*!40000 ALTER TABLE `cross_tariff_withdrawal` DISABLE KEYS */;
INSERT INTO `cross_tariff_withdrawal` VALUES (12,15,11,3.00,'gfgg',1,1,'2018-06-01 05:48:08',NULL,NULL,NULL,'2018-06-01 05:48:08','express_user@%',NULL,NULL),(13,15,7,5.00,'gfgg',1,1,'2018-06-01 05:48:08',NULL,NULL,NULL,'2018-06-01 05:48:08','express_user@%',NULL,NULL),(14,15,13,1.00,'gfgg',1,1,'2018-06-01 05:48:08',NULL,NULL,NULL,'2018-06-01 05:48:08','express_user@%',NULL,NULL),(15,18,9,5.00,'',1,1,'2018-06-07 13:22:47',NULL,NULL,NULL,'2018-06-07 13:22:47','express_user@%',NULL,NULL),(16,18,5,20.00,'',1,1,'2018-06-07 13:22:47',NULL,NULL,NULL,'2018-06-07 13:22:47','express_user@%',NULL,NULL),(17,18,13,1.00,'',1,1,'2018-06-07 13:22:47',NULL,NULL,NULL,'2018-06-07 13:22:47','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `cross_tariff_withdrawal` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_tariff_withdrawal$before_ins
BEFORE INSERT
ON cross_tariff_withdrawal FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER cross_tariff_withdrawal$before_upd
BEFORE UPDATE
ON cross_tariff_withdrawal FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_additional`
--

DROP TABLE IF EXISTS `doc_additional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_additional` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `cross_service_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на таблицу cross_service_type',
  `currency_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на таблицу book_currency',
  `status_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_status',
  `phone` varchar(12) DEFAULT NULL COMMENT 'Номер телефона клиента',
  `last_name` varchar(60) DEFAULT NULL COMMENT 'Фамилия',
  `first_name` varchar(60) DEFAULT NULL COMMENT 'Имя',
  `middle_name` varchar(60) DEFAULT NULL COMMENT 'Отчество',
  `marketing_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на book_marketing',
  `amount` decimal(19,2) unsigned DEFAULT NULL,
  `commission` decimal(19,2) unsigned DEFAULT NULL,
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$doc_additional$cross_service_id$cross_service_type$id` (`cross_service_id`),
  KEY `fk$doc_additional$status_id$book_status$id` (`status_id`),
  KEY `fk$doc_additional$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_additional$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_additional$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_additional$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$doc_additional$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_additional$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_additional$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_additional$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_additional$cross_service_id$cross_service_type$id` FOREIGN KEY (`cross_service_id`) REFERENCES `cross_service_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_additional$status_id$book_status$id` FOREIGN KEY (`status_id`) REFERENCES `book_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Доп. услуги + колибри';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_additional`
--

LOCK TABLES `doc_additional` WRITE;
/*!40000 ALTER TABLE `doc_additional` DISABLE KEYS */;
INSERT INTO `doc_additional` VALUES (1,61,1,5,'21312321','qqqqqqqqqw','wwwwwwwww','eeeeeeee',91,10.00,0.00,NULL,1,1,'2018-08-07 13:37:29',1,1,'2018-08-08 06:43:12','2018-08-07 13:37:29','express_user@%','2018-08-08 06:43:12','express_user@%');
/*!40000 ALTER TABLE `doc_additional` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_additional$before_ins
BEFORE INSERT
ON doc_additional FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_additional$before_upd
BEFORE UPDATE
ON doc_additional FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_bonus`
--

DROP TABLE IF EXISTS `doc_bonus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_bonus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `card_code` smallint(5) unsigned NOT NULL COMMENT 'Код карты. Уникальный в пределах отделения.',
  `card_number` varchar(7) NOT NULL COMMENT 'Номер карты. Пять цифр',
  `client_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на doc_client',
  `office_id` int(10) unsigned NOT NULL COMMENT 'Кто выдал карту. Ссылка на book_office',
  `balance` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Баланс бонусной карты',
  `works` enum('no','yes') NOT NULL DEFAULT 'yes' COMMENT 'yes - работает, no - не работате',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$doc_bonus$card_code$office_id` (`card_code`,`office_id`),
  UNIQUE KEY `uk$doc_bonus$card_number` (`card_number`),
  KEY `fk$doc_bonus$office_id$book_office$id` (`office_id`),
  KEY `fk$doc_bonus$ckient_id$doc_client$id` (`client_id`),
  KEY `fk$doc_bonus$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$doc_bonus$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$doc_bonus$ckient_id$doc_client$id` FOREIGN KEY (`client_id`) REFERENCES `doc_bonus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_bonus$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_bonus$office_id$book_office$id` FOREIGN KEY (`office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_bonus$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Бонусные карты';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_bonus`
--

LOCK TABLES `doc_bonus` WRITE;
/*!40000 ALTER TABLE `doc_bonus` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_bonus` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_bonus$before_ins
BEFORE INSERT
ON doc_bonus FOR EACH ROW
BEGIN
  
  declare local_item smallint unsigned;
  
  declare local_card_code smallint unsigned;
  
  SET NEW.ins_user = CURRENT_USER();
  
  
  select item
  into local_item
  from book_office
  where id = new.office_id;  
  
  if ( local_item = 111 ) or ( local_item = 888 ) then
    signal sqlstate '45000' 
    set message_text = 'Ошибка. Бонусные карты могут выдавать только отделения!';
  end if;
  
  if ( local_item > 99 ) then
    signal sqlstate '45000' 
    set message_text = 'Ошибка. Номер отделения трехзначное число!';
  end if;
  
  
  select max( card_code )
  into local_card_code
  from doc_bonus
  where office_id = new.office_id;
  
  if local_card_code is not null then
    set local_card_code = local_card_code + 1;
  else
    set local_card_code = 1;
  end if;
  
  set new.card_code = local_card_code;
  
  
  
  
  if new.card_number is null then
    set new.card_number = concat( lpad(local_item, 2, 0), lpad(local_card_code, 4, 0) );
  end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_bonus$before_upd
BEFORE UPDATE
ON doc_bonus FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_box`
--

DROP TABLE IF EXISTS `doc_box`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_box` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `system_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_system',
  `currency_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_currency',
  `office_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_office',
  `unique_number` varchar(30) NOT NULL COMMENT 'Уникальный номер бокса',
  `balance` decimal(19,2) NOT NULL DEFAULT '0.00' COMMENT 'Баланс',
  `rate` decimal(65,30) unsigned NOT NULL DEFAULT '1.000000000000000000000000000000' COMMENT 'Курс валюты',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$doc_box$unique_number` (`unique_number`),
  KEY `fk$doc_box$currency_id$book_currency$id` (`currency_id`),
  KEY `fk$doc_box$office_id$book_office$id` (`office_id`),
  KEY `fk$doc_box$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_box$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_box$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_box$change_office_id$book_office$id` (`change_office_id`),
  KEY `fk$doc_box$system_id$book_system$id` (`system_id`),
  CONSTRAINT `fk$doc_box$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_box$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_box$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_box$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_box$currency_id$book_currency$id` FOREIGN KEY (`currency_id`) REFERENCES `book_currency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_box$office_id$book_office$id` FOREIGN KEY (`office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_box$system_id$book_system$id` FOREIGN KEY (`system_id`) REFERENCES `book_system` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='Коробки с деньгами';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_box`
--

LOCK TABLES `doc_box` WRITE;
/*!40000 ALTER TABLE `doc_box` DISABLE KEYS */;
INSERT INTO `doc_box` VALUES (1,8,1,1,'1234 5678',0.00,1.000000000000000000000000000000,'Тестовый бокс',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-18 07:27:33','express_user@%','2018-08-13 10:01:15','express_user@%'),(2,8,1,1,'8800 8800',0.00,1.000000000000000000000000000000,'Тестовый бокс',0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-18 07:27:58','express_user@%','2018-08-13 13:07:20','express_user@%'),(3,18,2,1,'Касса_грн_111_1',2968.00,2.330000000000000000000000000000,NULL,0,NULL,NULL,NULL,1,1,'2018-06-04 11:15:30','2018-06-04 10:22:43','express_user@%','2018-08-16 11:11:29','express_user@%'),(4,18,1,1,'Касса_руб_111_1',-6985.00,50.087967072297780959198282032928,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-06-04 10:22:43','express_user@%','2018-08-13 13:49:41','express_user@%'),(5,18,3,1,'Касса_дол_111_1',100.00,51.900000000000000000000000000000,NULL,0,NULL,NULL,NULL,1,1,'2018-06-04 11:15:18','2018-06-04 10:22:43','express_user@%','2018-08-13 13:15:48','express_user@%'),(6,18,4,1,'Касса_евр_111_1',0.00,67.321304347826086956521739130435,NULL,0,NULL,NULL,NULL,1,1,'2018-06-04 11:15:03','2018-06-04 10:22:43','express_user@%','2018-08-13 13:07:20','express_user@%'),(7,1,2,1,'0101 0101 0101 0101',5552.00,2.116794185087911350914287278504,NULL,0,1,1,'2018-06-05 13:37:29',NULL,NULL,NULL,'2018-06-05 13:37:29','express_user@%','2018-08-16 11:11:47','express_user@%'),(8,1,2,1,'Приват-карта 01',0.00,5.912671225159113450533771461125,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-06-07 11:21:45','express_user@%','2018-08-13 13:07:20','express_user@%'),(9,1,2,1,'Приват-карта 02',0.00,2.351978021978021978021978021978,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-06-07 11:21:45','express_user@%','2018-08-13 13:07:20','express_user@%'),(10,1,2,1,'Приват-карта 03',0.00,1.812824840764331210191082802548,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-06-07 11:21:45','express_user@%','2018-08-08 08:59:30','express_user@%'),(11,2,2,2,'1231232132131',34565.00,2.166822174507124731602576615265,NULL,0,2,5,'2018-06-22 14:01:49',1,1,'2018-06-23 06:22:20','2018-06-22 14:01:49','express_user@%','2018-08-08 08:59:30','express_user@%'),(12,1,3,2,'private usd',9450.00,5.164857165073269686938456283083,NULL,0,1,1,'2018-07-27 08:52:07',NULL,NULL,'2018-07-27 08:52:07','2018-07-27 08:52:07','express_user@%','2018-08-08 08:59:30','express_user@%'),(13,3,1,2,'1818181818181818',1500.00,1.000000000000000000000000000000,NULL,0,1,1,'2018-07-30 09:26:39',NULL,NULL,'2018-07-30 09:26:39','2018-07-30 09:26:39','express_user@%',NULL,NULL),(14,5,1,1,'Tinkoff 001',0.00,1.000000000000000000000000000000,NULL,0,1,1,'2018-08-02 11:01:39',NULL,NULL,'2018-08-02 11:01:39','2018-08-02 11:01:39','express_user@%',NULL,NULL),(15,18,2,11,'Касса_грн_10_1',0.00,2.150000000000000000000000000000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-04 05:55:52','express_user@%','2018-08-13 13:07:20','express_user@%'),(16,18,1,11,'Касса_руб_10_1',0.00,53.477748467732243720706645835837,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-04 05:55:52','express_user@%','2018-08-13 13:07:20','express_user@%'),(17,18,3,11,'Касса_дол_10_1',0.00,1.000000000000000000000000000000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-04 05:55:52','express_user@%',NULL,NULL),(18,18,4,11,'Касса_евр_10_1',0.00,1.000000000000000000000000000000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-04 05:55:52','express_user@%',NULL,NULL),(19,20,2,1,'Kolibri UAH',150000.00,1.000000000000000000000000000000,NULL,0,1,1,'2018-08-08 13:36:52',NULL,NULL,'2018-08-08 13:36:52','2018-08-08 13:36:52','express_user@%','2018-08-13 13:07:20','express_user@%'),(20,20,1,1,'Kolibri RUB',150000.00,1.000000000000000000000000000000,NULL,0,1,1,'2018-08-08 13:37:18',NULL,NULL,'2018-08-08 13:37:18','2018-08-08 13:37:18','express_user@%','2018-08-13 13:07:20','express_user@%');
/*!40000 ALTER TABLE `doc_box` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_box$before_ins
BEFORE INSERT
ON doc_box FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`doc_box$before_upd` BEFORE UPDATE ON express_db.doc_box FOR EACH ROW
BEGIN

  
  declare average_sum decimal(19,4);
  
  
  declare average_rate decimal(19,4);
  
  SET NEW.upd_user = CURRENT_USER();  

  

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_bum_info`
--

DROP TABLE IF EXISTS `doc_bum_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_bum_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `head_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на таблицу doc_deal',
  `unique_key` varchar(60) NOT NULL COMMENT 'Уникальное поле',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$doc_bum_info$unique_key` (`unique_key`),
  KEY `fk$doc_bum_info$head_id$doc_deal$id` (`head_id`),
  KEY `fk$doc_bum_info$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_bum_info$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_bum_info$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_bum_info$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$doc_bum_info$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_bum_info$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_bum_info$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_bum_info$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_bum_info$head_id$doc_deal$id` FOREIGN KEY (`head_id`) REFERENCES `doc_deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Доп. таблица к Буму';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_bum_info`
--

LOCK TABLES `doc_bum_info` WRITE;
/*!40000 ALTER TABLE `doc_bum_info` DISABLE KEYS */;
INSERT INTO `doc_bum_info` VALUES (3,18,'18/120180810020845',1,1,'2018-08-10 11:07:45',NULL,NULL,'2018-08-10 11:07:45','2018-08-10 11:07:45','express_user@%',NULL,NULL),(4,19,'19/120180810020818',1,1,'2018-08-10 11:49:18',NULL,NULL,'2018-08-10 11:49:18','2018-08-10 11:49:18','express_user@%',NULL,NULL),(5,20,'20/120180810030826',1,1,'2018-08-10 12:41:26',NULL,NULL,'2018-08-10 12:41:26','2018-08-10 12:41:26','express_user@%',NULL,NULL),(6,33,'33/120180813040809',1,1,'2018-08-13 13:27:09',NULL,NULL,'2018-08-13 13:27:09','2018-08-13 13:27:09','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `doc_bum_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_bum_info$before_ins
BEFORE INSERT
ON doc_bum_info FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_bum_info$before_upd
BEFORE UPDATE
ON doc_bum_info FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_cash_register`
--

DROP TABLE IF EXISTS `doc_cash_register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_cash_register` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `box_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на doc_box',
  `balance` int(21) unsigned NOT NULL DEFAULT '0' COMMENT 'Баланс кассы',
  `rate` decimal(65,30) unsigned NOT NULL DEFAULT '0.000000000000000000000000000000' COMMENT 'Курс',
  `period` tinyint(1) unsigned DEFAULT '0' COMMENT 'Признак открытие/закрытие (0/1)',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$doc_cash_register$box_id$doc_box$id` (`box_id`),
  CONSTRAINT `fk$doc_cash_register$box_id$doc_box$id` FOREIGN KEY (`box_id`) REFERENCES `doc_box` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Инвентаризация дс (Свод)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_cash_register`
--

LOCK TABLES `doc_cash_register` WRITE;
/*!40000 ALTER TABLE `doc_cash_register` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_cash_register` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_cash_register$before_ins
  BEFORE INSERT
  ON doc_cash_register FOR EACH ROW
  BEGIN
    SET NEW.ins_user = CURRENT_USER();
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_cash_register$before_upd
  BEFORE UPDATE
  ON doc_cash_register FOR EACH ROW
  BEGIN
    SET NEW.upd_user = CURRENT_USER();
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_cashflow`
--

DROP TABLE IF EXISTS `doc_cashflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_cashflow` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `amount` decimal(19,2) NOT NULL COMMENT 'Сумма операции',
  `rate` decimal(19,4) unsigned DEFAULT '1.0000' COMMENT 'Курс операции',
  `rub_rate` decimal(19,4) unsigned DEFAULT '1.0000' COMMENT 'Курс по отношению к рублю',
  `box_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на doc_box',
  `send_info_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на doc_send_info',
  `withdrawal_info_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на doc_withdrawal_info',
  `unfinished_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на doc_unfinished',
  `additional_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на doc_additional',
  `deal_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на doc_deal',
  `ticket_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на таблицу doc_ticket',
  `global_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на doc_global',
  `panda_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на doc_panda',
  `bum_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на doc_bum',
  `partner_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на doc_partner',
  `income_expense_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на doc_income_expense',
  `encashment_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на doc_encashment',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$doc_cashflow$unfinished_id` (`unfinished_id`),
  KEY `fk$doc_cashflow$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_cashflow$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_cashflow$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_cashflow$change_office_id$book_office$id` (`change_office_id`),
  KEY `fk$doc_cashflow$box_id$doc_box$id` (`box_id`),
  KEY `fk$doc_cashflow$send_info_id$doc_send_info$id` (`send_info_id`),
  KEY `fk$doc_cashflow$withdrawal_info_id$doc_withdrawal_info$id` (`withdrawal_info_id`),
  KEY `fk$doc_cashflow$unfinished_id$doc_unfinished$id` (`unfinished_id`),
  KEY `fk$doc_cashflow$ticket_id$doc_ticket$id` (`ticket_id`),
  KEY `fk$doc_cashflow$additional_id$doc_additional$id` (`additional_id`),
  KEY `fk$doc_cashflow$deal_id$doc_deal$id` (`deal_id`),
  CONSTRAINT `fk$doc_cashflow$additional_id$doc_additional$id` FOREIGN KEY (`additional_id`) REFERENCES `doc_additional` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_cashflow$box_id$doc_box$id` FOREIGN KEY (`box_id`) REFERENCES `doc_box` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_cashflow$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_cashflow$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_cashflow$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_cashflow$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_cashflow$deal_id$doc_deal$id` FOREIGN KEY (`deal_id`) REFERENCES `doc_deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_cashflow$send_info_id$doc_send_info$id` FOREIGN KEY (`send_info_id`) REFERENCES `doc_send_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_cashflow$ticket_id$doc_ticket$id` FOREIGN KEY (`ticket_id`) REFERENCES `doc_ticket` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_cashflow$unfinished_id$doc_unfinished$id` FOREIGN KEY (`unfinished_id`) REFERENCES `doc_unfinished` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_cashflow$withdrawal_info_id$doc_withdrawal_info$id` FOREIGN KEY (`withdrawal_info_id`) REFERENCES `doc_withdrawal_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=utf8 COMMENT='Движение денежных средств';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_cashflow`
--

LOCK TABLES `doc_cashflow` WRITE;
/*!40000 ALTER TABLE `doc_cashflow` DISABLE KEYS */;
INSERT INTO `doc_cashflow` VALUES (210,100.00,1.0000,51.9000,5,NULL,NULL,NULL,NULL,20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'2018-08-13 13:15:48',NULL,NULL,'2018-08-13 13:15:48','2018-08-13 13:15:48','express_user@%',NULL,NULL),(214,500.00,1.0000,2.3300,3,NULL,NULL,NULL,NULL,33,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'2018-08-13 13:27:19',NULL,NULL,'2018-08-13 13:27:19','2018-08-13 13:27:19','express_user@%',NULL,NULL),(216,-1085.00,0.4608,2.1700,4,NULL,NULL,NULL,NULL,33,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'2018-08-13 13:27:54',NULL,NULL,'2018-08-13 13:27:54','2018-08-13 13:27:54','express_user@%',NULL,NULL),(220,-5900.00,0.0170,58.9000,4,NULL,NULL,NULL,NULL,20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'2018-08-13 13:49:41',NULL,NULL,'2018-08-13 13:49:41','2018-08-13 13:49:41','express_user@%',NULL,NULL),(221,2468.00,1.0000,2.3300,3,NULL,NULL,NULL,NULL,34,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'2018-08-16 11:11:29',NULL,NULL,'2018-08-16 11:11:29','2018-08-16 11:11:29','express_user@%',NULL,NULL),(222,-2453.00,1.0000,2.3300,7,156,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'2018-08-16 11:11:47',NULL,NULL,'2018-08-16 11:11:47','2018-08-16 11:11:47','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `doc_cashflow` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_cashflow$before_ins
BEFORE INSERT
ON doc_cashflow FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `doc_cashflow$after_ins`
AFTER INSERT ON doc_cashflow
FOR EACH ROW
  BEGIN 
  update doc_box
  set rate = ((balance*rate) + (new.amount*new.rub_rate))/(balance + new.amount),
  balance = balance + new.amount
  where id = new.box_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_cashflow$before_upd
BEFORE UPDATE
ON doc_cashflow FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`doc_cashflow$after_upd` 
AFTER UPDATE ON express_db.doc_cashflow FOR EACH ROW
BEGIN 
  update doc_box
  set balance = balance - old.amount
  where id = old.box_id;
  
  update doc_box
  set balance = balance + new.amount
  where id = new.box_id;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`doc_cashflow$after_del` 
AFTER DELETE ON express_db.doc_cashflow FOR EACH ROW
BEGIN 
  update doc_box
  set balance = balance - old.amount
  where id = old.box_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_client`
--

DROP TABLE IF EXISTS `doc_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_client` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `phone` varchar(12) NOT NULL COMMENT 'Телефон',
  `email` varchar(320) DEFAULT NULL COMMENT 'E-mail',
  `last_name` varchar(60) DEFAULT NULL COMMENT 'Фамилия',
  `first_name` varchar(60) DEFAULT NULL COMMENT 'Имя',
  `middle_name` varchar(60) DEFAULT NULL COMMENT 'Отчество',
  `birthday` date DEFAULT NULL COMMENT 'День рождения',
  `passport_number` char(8) DEFAULT NULL COMMENT 'Серия и номер паспорта',
  `passport_by` varchar(200) DEFAULT NULL COMMENT 'Кем когда выдан паспорт',
  `passport_when` date DEFAULT NULL COMMENT 'Когда выдан паспорт',
  `tin` char(12) DEFAULT NULL COMMENT 'ИНН',
  `formal_country_id` int(10) unsigned DEFAULT NULL COMMENT 'Прописка. Страна. Ссылка на book_country',
  `formal_city_id` int(10) unsigned DEFAULT NULL COMMENT 'Прописка. Город. Ссылка на book_city',
  `formal_address` varchar(160) DEFAULT NULL COMMENT 'Прописка. Адрес: кв/ул дом квартира',
  `fact_country_id` int(10) unsigned DEFAULT NULL COMMENT 'Проживание. Страна. Ссылка на book_country',
  `fact_city_id` int(10) unsigned DEFAULT NULL COMMENT 'Проживание. Город. Ссылка на book_city',
  `fact_address` varchar(160) DEFAULT NULL COMMENT 'Проживание. Адрес: кв/ул дом квартира',
  `marketing_id` int(10) unsigned DEFAULT NULL COMMENT 'Источник привлечения. Ссылка book_marketing',
  `swindler` enum('no','yes') NOT NULL DEFAULT 'no' COMMENT 'no - не мошенник, yes - мошенник',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$doc_client$phone` (`phone`),
  UNIQUE KEY `uk$doc_people$email` (`email`),
  UNIQUE KEY `uk$doc_people$passport_number` (`passport_number`),
  UNIQUE KEY `uk$doc_people$tin` (`tin`),
  KEY `fk$doc_client$marketing_id$book_marketing$id` (`marketing_id`),
  KEY `fk$doc_client$formal_country_id$book_country$id` (`formal_country_id`),
  KEY `fk$doc_client$formal_city_id$book_city$id` (`formal_city_id`),
  KEY `fk$doc_client$fact_country_id$book_country$id` (`fact_country_id`),
  KEY `fk$doc_client$fact_city_id$book_city$id` (`fact_city_id`),
  KEY `fk$doc_client$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$doc_client$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$doc_client$fact_city_id$book_city$id` FOREIGN KEY (`fact_city_id`) REFERENCES `book_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_client$fact_country_id$book_country$id` FOREIGN KEY (`fact_country_id`) REFERENCES `book_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_client$formal_city_id$book_city$id` FOREIGN KEY (`formal_city_id`) REFERENCES `book_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_client$formal_country_id$book_country$id` FOREIGN KEY (`formal_country_id`) REFERENCES `book_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_client$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_client$marketing_id$book_marketing$id` FOREIGN KEY (`marketing_id`) REFERENCES `book_marketing` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_client$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Клиенты';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_client`
--

LOCK TABLES `doc_client` WRITE;
/*!40000 ALTER TABLE `doc_client` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_client` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_client$before_ins
BEFORE INSERT
ON doc_client FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_client$before_upd
BEFORE UPDATE
ON doc_client FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_contractor`
--

DROP TABLE IF EXISTS `doc_contractor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_contractor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `people_id` int(10) unsigned NOT NULL COMMENT 'Человек. Ссылка на doc_people',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  KEY `fk$doc_contractor$people_id$doc_people$id` (`people_id`),
  KEY `fk$doc_contractor$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$doc_contractor$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$doc_contractor$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_contractor$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Контрагенты';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_contractor`
--

LOCK TABLES `doc_contractor` WRITE;
/*!40000 ALTER TABLE `doc_contractor` DISABLE KEYS */;
INSERT INTO `doc_contractor` VALUES (1,1,0,'2017-10-07 08:42:47','express_user@%',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `doc_contractor` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_contractor$before_ins
BEFORE INSERT
ON doc_contractor FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_contractor$before_upd
BEFORE UPDATE
ON doc_contractor FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_deal`
--

DROP TABLE IF EXISTS `doc_deal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_deal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `cross_service_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на таблицу cross_service_type',
  `currency_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на таблицу book_currency',
  `status_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_status',
  `phone` varchar(12) DEFAULT NULL COMMENT 'Номер телефона клиента',
  `last_name` varchar(60) DEFAULT NULL COMMENT 'Фамилия',
  `first_name` varchar(60) DEFAULT NULL COMMENT 'Имя',
  `middle_name` varchar(60) DEFAULT NULL COMMENT 'Отчество',
  `marketing_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на book_marketing',
  `amount` decimal(16,2) unsigned DEFAULT NULL COMMENT 'Сумма сделки',
  `commission` decimal(16,2) unsigned DEFAULT NULL COMMENT 'Комиссия от сделки',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$doc_deal$cross_service_id$cross_service_type$id` (`cross_service_id`),
  KEY `fk$doc_deal$status_id$book_status$id` (`status_id`),
  KEY `fk$doc_deal$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_deal$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_deal$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_deal$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$doc_deal$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_deal$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_deal$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_deal$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_deal$cross_service_id$cross_service_type$id` FOREIGN KEY (`cross_service_id`) REFERENCES `cross_service_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_deal$status_id$book_status$id` FOREIGN KEY (`status_id`) REFERENCES `book_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='Сделка';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_deal`
--

LOCK TABLES `doc_deal` WRITE;
/*!40000 ALTER TABLE `doc_deal` DISABLE KEYS */;
INSERT INTO `doc_deal` VALUES (1,67,1,5,'2323212332','name','name','name',91,300.00,0.00,NULL,1,1,'2018-08-08 07:02:19',1,1,'2018-08-08 07:06:33','2018-08-08 07:02:19','express_user@%','2018-08-08 07:06:33','express_user@%'),(2,1,2,5,'name','name','name','name',90,500.00,15.00,NULL,1,1,'2018-08-08 07:28:23',1,1,'2018-08-08 07:33:49','2018-08-08 07:28:23','express_user@%','2018-08-08 07:33:49','express_user@%'),(5,1,2,6,'familia','familia','familia','familia',90,900.00,10.00,NULL,1,1,'2018-08-08 09:00:23',1,1,'2018-08-08 10:54:32','2018-08-08 09:00:23','express_user@%','2018-08-08 10:54:32','express_user@%'),(6,163,2,6,'fam','fam','fam','fam',90,500.00,0.00,NULL,1,1,'2018-08-08 11:18:38',1,1,'2018-08-08 11:59:28','2018-08-08 11:18:38','express_user@%','2018-08-08 11:59:28','express_user@%'),(7,51,2,5,'name','name','name','name',93,200.00,0.00,NULL,1,1,'2018-08-08 12:19:00',1,1,'2018-08-08 12:19:16','2018-08-08 12:19:00','express_user@%','2018-08-08 12:19:16','express_user@%'),(12,188,2,6,'name','name','name','name',104,500.00,0.00,NULL,1,1,'2018-08-09 08:23:12',1,1,'2018-08-09 12:02:34','2018-08-09 08:23:12','express_user@%','2018-08-09 12:02:34','express_user@%'),(13,180,1,3,'mane','mane','mane','mane',98,1000.00,0.00,NULL,1,1,'2018-08-09 12:46:24',1,1,'2018-08-09 12:47:51','2018-08-09 12:46:24','express_user@%','2018-08-09 12:47:51','express_user@%'),(14,166,2,5,'11111111','11111111','11111111111','11111111111',89,322.00,0.00,NULL,1,1,'2018-08-09 13:19:44',1,1,'2018-08-10 06:41:38','2018-08-09 13:19:44','express_user@%','2018-08-10 06:41:38','express_user@%'),(15,166,1,5,'111111','1111111111','1111111111','1111111111',92,3434.00,0.00,NULL,1,1,'2018-08-10 07:00:42',1,1,'2018-08-10 07:22:15','2018-08-10 07:00:42','express_user@%','2018-08-10 07:22:15','express_user@%'),(16,198,1,6,NULL,'321321','213213','2312321',94,1111.00,0.00,NULL,1,1,'2018-08-10 07:23:05',1,1,'2018-08-10 07:28:13','2018-08-10 07:23:05','express_user@%','2018-08-10 07:28:13','express_user@%'),(17,192,2,5,'client','client','client','client',92,500.00,0.00,NULL,4,11,'2018-08-10 07:31:08',2,11,'2018-08-10 07:32:38','2018-08-10 07:31:08','express_user@%','2018-08-10 07:32:38','express_user@%'),(18,164,2,5,'14321132','262626','2323232','1323122312',92,500.00,0.00,NULL,1,1,'2018-08-10 10:22:50',1,1,'2018-08-10 11:11:41','2018-08-10 10:22:50','express_user@%','2018-08-10 11:11:41','express_user@%'),(19,164,2,5,'111111111','1111','1111','11111',97,500.00,0.00,NULL,1,1,'2018-08-10 10:53:35',1,1,'2018-08-10 12:31:53','2018-08-10 10:53:35','express_user@%','2018-08-10 12:31:53','express_user@%'),(20,164,3,5,'321312','1123213','213231321','21323321',97,100.00,0.00,NULL,1,1,'2018-08-10 12:41:26',1,1,'2018-08-13 13:49:41','2018-08-10 12:41:26','express_user@%','2018-08-13 13:49:41','express_user@%'),(21,165,2,5,'4868','3211111111111','1533333333','4666',90,200.00,0.00,NULL,1,1,'2018-08-11 08:48:57',1,1,'2018-08-11 08:49:30','2018-08-11 08:48:57','express_user@%','2018-08-11 08:49:30','express_user@%'),(25,1,2,1,'212121','21212','12121','212121',90,111.00,12.00,NULL,1,1,'2018-08-13 10:20:06',1,1,'2018-08-13 10:34:54','2018-08-13 10:20:06','express_user@%','2018-08-13 10:34:54','express_user@%'),(27,1,2,1,'212121','21212','12121','212121',90,111.00,12.00,NULL,1,1,'2018-08-13 10:35:35',NULL,NULL,'2018-08-13 10:35:35','2018-08-13 10:35:35','express_user@%',NULL,NULL),(32,1,2,6,'fam','fam','fam','fam',91,623.00,17.00,NULL,1,1,'2018-08-13 12:47:40',1,1,'2018-08-13 13:00:06','2018-08-13 12:47:40','express_user@%','2018-08-13 13:00:06','express_user@%'),(33,164,2,5,'1111111','1111','1111111','11111111',94,500.00,0.00,NULL,1,1,'2018-08-13 13:27:09',1,1,'2018-08-13 13:27:54','2018-08-13 13:27:09','express_user@%','2018-08-13 13:27:54','express_user@%'),(34,1,2,5,'13213123','12321321','3123123123','213123213',91,2433.00,35.00,NULL,1,1,'2018-08-16 11:11:04',1,1,'2018-08-16 11:11:47','2018-08-16 11:11:04','express_user@%','2018-08-16 11:11:47','express_user@%');
/*!40000 ALTER TABLE `doc_deal` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_deal$before_ins
BEFORE INSERT
ON doc_deal FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_deal$before_upd
BEFORE UPDATE
ON doc_deal FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_exchange`
--

DROP TABLE IF EXISTS `doc_exchange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_exchange` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `buy_sale` enum('buy','sale') NOT NULL COMMENT 'buy-покупка, sale-продажа',
  `ex_dir_id` int(10) unsigned NOT NULL COMMENT 'Направление обмена. Ссылка на book_exchange_direction',
  `in_sum` decimal(19,2) unsigned NOT NULL COMMENT 'Сумма к принятию',
  `in_currency` int(10) unsigned NOT NULL COMMENT 'Валюта к принятию. Ссылка на book_currency',
  `out_sum` decimal(19,2) unsigned NOT NULL COMMENT 'Сумма к выдачи',
  `out_currency` int(10) unsigned NOT NULL COMMENT 'Валюта к выдачи. Ссылка на book_currency',
  `rate` decimal(19,2) unsigned NOT NULL COMMENT 'Курс. Копируется из doc_rate',
  `situation` tinyint(1) unsigned NOT NULL COMMENT '1-проведена, 2-запрос спецКура, 3-отказано, 4-согласовано',
  `rate_note` varchar(1200) DEFAULT NULL COMMENT 'Ответ Гасяна',
  `posit` enum('short','long') DEFAULT NULL COMMENT 'short - короткая, long - длиннная позиция',
  `tariff_id` int(10) unsigned DEFAULT NULL COMMENT 'Тариф. Ссылка на doc_tariff',
  `tariff_percent` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Процент по тарифу',
  `tariff_note` varchar(3000) DEFAULT NULL COMMENT 'Номера купюр',
  `note` varchar(1200) DEFAULT NULL COMMENT 'Комментарий',
  `create_user` int(10) unsigned NOT NULL COMMENT 'Кем создана?',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда создана?',
  `create_office` int(10) unsigned NOT NULL COMMENT 'Где создана?',
  `confirm_user` int(10) unsigned DEFAULT NULL COMMENT 'Кем проверена?',
  `confirm_date` timestamp NULL DEFAULT NULL COMMENT 'Когда проверена?',
  `confirm_office` int(10) unsigned DEFAULT NULL COMMENT 'Где проверена?',
  `complete_user` int(10) unsigned DEFAULT NULL COMMENT 'Кем проведена?',
  `complete_date` timestamp NULL DEFAULT NULL COMMENT 'Когда проведена?',
  `complete_office` int(10) unsigned DEFAULT NULL COMMENT 'Где проведена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$doc_exchange$ex_dir_id$book_exchange_direction$id` (`ex_dir_id`),
  KEY `fk$doc_exchange$in_currency$book_currency$id` (`in_currency`),
  KEY `fk$doc_exchange$out_currency$book_currency$id` (`out_currency`),
  KEY `fk$doc_exchange$tariff_id$doc_tariff$id` (`tariff_id`),
  KEY `fk$doc_exchange$create_user$teh_user$id` (`create_user`),
  KEY `fk$doc_exchange$create_office$book_office$id` (`create_office`),
  KEY `fk$doc_exchange$check_user$teh_user$id` (`confirm_user`),
  KEY `fk$doc_exchange$check_office$book_office$id` (`confirm_office`),
  KEY `fk$doc_exchange$complete_user$teh_user$id` (`complete_user`),
  KEY `fk$doc_exchange$complete_office$book_office$id` (`complete_office`),
  KEY `i$doc_exchange$create_date` (`create_date`),
  CONSTRAINT `fk$doc_exchange$check_office$book_office$id` FOREIGN KEY (`confirm_office`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_exchange$check_user$teh_user$id` FOREIGN KEY (`confirm_user`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_exchange$complete_office$book_office$id` FOREIGN KEY (`complete_office`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_exchange$complete_user$teh_user$id` FOREIGN KEY (`complete_user`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_exchange$create_office$book_office$id` FOREIGN KEY (`create_office`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_exchange$create_user$teh_user$id` FOREIGN KEY (`create_user`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_exchange$ex_dir_id$book_exchange_direction$id` FOREIGN KEY (`ex_dir_id`) REFERENCES `book_exchange_direction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_exchange$in_currency$book_currency$id` FOREIGN KEY (`in_currency`) REFERENCES `book_currency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_exchange$out_currency$book_currency$id` FOREIGN KEY (`out_currency`) REFERENCES `book_currency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_exchange$tariff_id$doc_tariff$id` FOREIGN KEY (`tariff_id`) REFERENCES `doc_tariff` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='Валютообмен';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_exchange`
--

LOCK TABLES `doc_exchange` WRITE;
/*!40000 ALTER TABLE `doc_exchange` DISABLE KEYS */;
INSERT INTO `doc_exchange` VALUES (1,'buy',1,125.00,3,7273.75,1,58.19,1,NULL,'long',40,50.00,NULL,NULL,1,'2018-03-29 12:25:58',1,NULL,NULL,NULL,1,'2018-03-29 12:25:58',1,'2018-03-29 12:25:58','express_user@%',NULL,NULL),(2,'buy',1,580.00,3,33750.20,1,58.19,1,NULL,'long',40,50.00,NULL,NULL,1,'2018-03-29 12:57:08',1,NULL,NULL,NULL,1,'2018-03-29 12:57:08',1,'2018-03-29 12:57:08','express_user@%',NULL,NULL),(3,'sale',2,23546.70,1,323.00,4,72.90,1,NULL,'long',NULL,NULL,NULL,NULL,1,'2018-03-29 13:22:41',1,NULL,NULL,NULL,1,'2018-03-29 13:22:41',1,'2018-03-29 13:22:41','express_user@%',NULL,NULL),(4,'sale',1,61900.00,3,1000.00,1,61.90,1,NULL,'long',NULL,NULL,NULL,NULL,1,'2018-03-30 07:57:16',1,NULL,NULL,NULL,1,'2018-03-30 07:57:16',1,'2018-03-30 07:57:16','express_user@%',NULL,NULL),(5,'buy',1,212.00,3,34041.15,1,58.19,1,NULL,'long',NULL,NULL,NULL,NULL,1,'2018-03-30 10:07:55',1,NULL,NULL,NULL,1,'2018-03-30 10:07:55',1,'2018-03-30 10:07:55','express_user@%',NULL,NULL),(6,'buy',3,20000.00,2,43400.00,1,2.17,2,NULL,'long',NULL,NULL,NULL,NULL,1,'2018-03-30 10:12:08',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-03-30 10:12:08','express_user@%',NULL,NULL),(7,'sale',2,7290.00,1,100.00,4,72.90,2,NULL,'long',43,20.00,NULL,NULL,1,'2018-03-30 11:37:42',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-03-30 11:37:42','express_user@%',NULL,NULL),(8,'buy',1,100.00,3,5819.00,1,58.19,1,NULL,'long',NULL,NULL,NULL,NULL,1,'2018-03-30 13:28:57',1,NULL,NULL,NULL,1,'2018-03-30 13:28:57',1,'2018-03-30 13:28:57','express_user@%',NULL,NULL),(9,'buy',6,150.00,4,171.00,3,1.14,1,NULL,NULL,NULL,NULL,NULL,NULL,1,'2018-04-05 11:02:39',1,NULL,NULL,NULL,1,'2018-04-05 11:02:39',1,'2018-04-05 11:02:39','express_user@%',NULL,NULL),(10,'sale',6,180.00,3,150.00,4,1.45,4,'йцукен','long',NULL,NULL,NULL,NULL,1,'2018-04-05 11:56:50',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-05 11:56:50','express_user@%','2018-04-06 13:19:55','express_user@%'),(11,'buy',6,157.00,4,178.00,3,5.00,4,'фывап','long',NULL,NULL,NULL,'Запросить спец курс',1,'2018-04-06 13:21:46',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-06 13:21:46','express_user@%','2018-04-06 13:22:24','express_user@%'),(12,'buy',1,100.00,3,5820.00,1,50.00,4,NULL,'long',NULL,NULL,NULL,'100 баксов',1,'2018-04-06 13:30:30',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-06 13:30:30','express_user@%','2018-04-06 13:30:50','express_user@%'),(13,'sale',1,30950.00,3,500.00,1,65.00,4,NULL,'long',NULL,NULL,NULL,'Клиент просит по 65',1,'2018-04-06 13:34:20',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-06 13:34:20','express_user@%','2018-04-07 06:54:15','express_user@%'),(14,'sale',3,12815.00,1,5500.00,2,2.35,4,'оло','long',NULL,NULL,NULL,'Прошу 2.2',1,'2018-04-07 06:55:09',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-07 06:55:09','express_user@%','2018-04-07 07:00:01','express_user@%'),(15,'buy',2,154.00,4,10451.00,1,68.20,2,NULL,'long',NULL,NULL,NULL,'Прошиу вас по 65',1,'2018-04-07 07:03:46',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-07 07:03:46','express_user@%',NULL,NULL),(16,'buy',1,151.00,3,8763.00,1,58.20,2,NULL,'long',NULL,NULL,NULL,'67 hghg',1,'2018-04-07 07:24:16',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-07 07:24:16','express_user@%',NULL,NULL),(17,'sale',1,15475.00,3,250.00,1,61.90,2,NULL,'long',NULL,NULL,NULL,'jhkhkgk',1,'2018-04-07 09:23:36',1,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-07 09:23:36','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `doc_exchange` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_exchange$before_ins
BEFORE INSERT
ON doc_exchange FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_exchange$before_upd
BEFORE UPDATE
ON doc_exchange FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_kolibri_info`
--

DROP TABLE IF EXISTS `doc_kolibri_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_kolibri_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `head_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на таблицу doc_deal',
  `k_number` varchar(60) NOT NULL COMMENT 'Номер счёта / телефона',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$doc_kolibri_info$head_id$doc_deal$id` (`head_id`),
  KEY `fk$doc_kolibri_info$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_kolibri_info$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_kolibri_info$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_kolibri_info$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$doc_kolibri_info$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_kolibri_info$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_kolibri_info$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_kolibri_info$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_kolibri_info$head_id$doc_deal$id` FOREIGN KEY (`head_id`) REFERENCES `doc_deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Номера/Счета для колибри';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_kolibri_info`
--

LOCK TABLES `doc_kolibri_info` WRITE;
/*!40000 ALTER TABLE `doc_kolibri_info` DISABLE KEYS */;
INSERT INTO `doc_kolibri_info` VALUES (1,12,'5980874542',1,1,'2018-08-09 08:23:12',NULL,NULL,'2018-08-09 08:23:12','2018-08-09 08:23:12','express_user@%',NULL,NULL),(2,13,'3232343243',1,1,'2018-08-09 12:46:24',NULL,NULL,'2018-08-09 12:46:24','2018-08-09 12:46:24','express_user@%',NULL,NULL),(3,14,'323212312',1,1,'2018-08-09 13:19:44',NULL,NULL,'2018-08-09 13:19:44','2018-08-09 13:19:44','express_user@%',NULL,NULL),(4,14,'323212312',1,1,'2018-08-09 13:23:57',NULL,NULL,'2018-08-09 13:23:57','2018-08-09 13:23:57','express_user@%',NULL,NULL),(5,15,'21212312',1,1,'2018-08-10 07:00:42',NULL,NULL,'2018-08-10 07:00:42','2018-08-10 07:00:42','express_user@%',NULL,NULL),(6,15,'21212312',1,1,'2018-08-10 07:00:50',NULL,NULL,'2018-08-10 07:00:50','2018-08-10 07:00:50','express_user@%',NULL,NULL),(7,16,'22222',1,1,'2018-08-10 07:23:05',NULL,NULL,'2018-08-10 07:23:05','2018-08-10 07:23:05','express_user@%',NULL,NULL),(8,17,'2555555555',4,11,'2018-08-10 07:31:08',NULL,NULL,'2018-08-10 07:31:08','2018-08-10 07:31:08','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `doc_kolibri_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_kolibri_info$before_ins
BEFORE INSERT
ON doc_kolibri_info FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_kolibri_info$before_upd
BEFORE UPDATE
ON doc_kolibri_info FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_news`
--

DROP TABLE IF EXISTS `doc_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `public_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата публикации',
  `title` varchar(255) NOT NULL COMMENT 'Заголовок новости',
  `body` varchar(4096) DEFAULT NULL COMMENT 'Текст новости',
  `photo` mediumblob COMMENT 'Картинка новости',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве? 0-нет, 1-да',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  KEY `fk$doc_news$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$doc_news$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$doc_news$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_news$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Новости/Акции для андроид приложения';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_news`
--

LOCK TABLES `doc_news` WRITE;
/*!40000 ALTER TABLE `doc_news` DISABLE KEYS */;
INSERT INTO `doc_news` VALUES (1,'2018-02-02 21:00:00','ВРЕМЯ ПОДАРКОВ: ДЕНЬ РОЖДЕНИЯ ПКЦ. МЫ СОБИРАЕМ ВСЕХ НА ПРАЗДНИК','Услуги –  от нас, подарки –  для вас!\nПКЦ проводит большой «марафон щедрости» в честь 3-летней годовщины работы. Всё просто: в наш праздник подарки получаете  Вы! Единственное «условие» –  Ваше присутствие и желание. За три года своей успешной работы  компания по предоставлению финансовых услуг превратилась в лидера финансового рынка нашей Республики, количество отделений стремительно растёт (недавно открыто 48 отделение), а спектр предоставляемых услуг перевалил за полторы сотни! Наши желания совпадают  с возможностями, и мы хотим поощрить всех своих клиентов  за выбор в пользу Первого Коммерческого Центра!\n\nСимпатия без взаимности – это не симпатия! Так мы решили и создали особые «акционные дни»! Наша признательность -  в поступках. Клиентов-гостей в каждом отделении ПКЦ щедро одарят подарками, приятными бонусами и праздничной атмосферой. Не упустите свой шанс! Воспользуйтесь нашими услугами с пользой для себя!\n\nСогласитесь,  очень  заманчиво: прийти просто пополнить счёт на мобильном телефоне, а уйти с подарком!\n\nПриходите, и никто не останется незамеченным! Бонусы действуют для всех!\n\nВ нашем центре Вы можете воспользоваться услугой на выбор, а также получить профессиональную консультацию и помощь. Переводы любой сложности, снятие денег с карт, обмен валют без ограничений, работа с электронными кошельками и пополнения мобильного оператора - для нас нет ничего невозможного! Безопасность, доступность и качество - вот главные принципы работы ПКЦ!\n\nОтпразднуем наше 3-летие вместе и с максимальной выгодой для Вас!','�\��\�\0JFIF\0\0`\0`\0\0�\��Exif\0\0MM\0*\0\0\0\01\0\0\0\0\0\0>�i\0\0\0\0\0\0\\\�\0\0\0\0\0\02\0\0\0\0\�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0Adobe Photoshop CS6 (Windows)\0\0\�\0\0\0\0\0n\0\0\0\0\�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�\�	�http://ns.adobe.com/xap/1.0/\0<?xpacket begin=\'﻿\' id=\'W5M0MpCehiHzreSzNTczkc9d\'?>\r\n<x:xmpmeta xmlns:x=\"adobe:ns:meta/\"><rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"><rdf:Description rdf:about=\"uuid:faf5bdd5-ba3d-11da-ad31-d33d75182f1b\" xmlns:xmp=\"http://ns.adobe.com/xap/1.0/\"><xmp:CreatorTool>Adobe Photoshop CS6 (Windows)</xmp:CreatorTool></rdf:Description></rdf:RDF></x:xmpmeta>\r\n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                            <?xpacket end=\'w\'?>�\�\0C\0		\n\r\Z\Z $.\' \",#(7),01444\'9=82<.342�\�\0C			\r\r2!!22222222222222222222222222222222222222222222222222��\0P{\"\0�\�\0\0\0\0\0\0\0\0\0\0\0	\n�\�\0�\0\0\0}\0!1AQa\"q2���#B��R\��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz�����������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\������������\�\0\0\0\0\0\0\0\0	\n�\�\0�\0\0w\0!1AQaq\"2�B����	#3R�br\�\n$4\�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz������������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�����������\�\0\0\0?\0�z�v+Ƕ\�?�1H��8�E�\n�\�\���\\X\�\��:mb3�cJ#ub\��\�\�#\�w\�\�Hd��\�?\�VB�\0H+�\�\�}A�²i�vB\nH�c	@B\�%GT>�J�^tR+\0�F�\"�s�HW\�	�oB*ER��\��\�YɝT\�jE\Z#$pA�\0LT\�G��PG�\�?�?:�b\n	��=�\�FП��������\�R�����A�\r\��N0?�\��TrG\�g8#���Q\��\0\�zH\�@\� ���\�P�1�<�p?���Y\�:/R�U*�Kw\'�\n�⯸UR�s\�1����T\0�\�os��>R<�Fh\�I-.H\"�\�\nH_�9��\�8�����N\0\'\�ҤKg%�+bDx\���~^�v\�\�6�D�PJ�ŖQ�x<\�#����m$:�d:�b�I\0�a�\�3\�Z���ثm��\�\�DX�FJ���\�\�~��m�5\���7a�����f��[MZ\'����J1\�G\0g\�ϸ5j	�\rNi�\Z\��R��\��h�@�1���i%X�8�\�\�\0=s��jFP)\0�\�\�k���+;C\�\�\�1�\�\�POԃڮImmq��\�)\\�Bp�\�\�a�\�\�\�4s4�S���_\�i\�|\�\n� y\�GcT�[(\�]Յ���� �\����� \�.=\�H �\�@�(f\��g�\"��}:\�=�q�\\<-��k)Ǩ<Δ�H`[\�\�\�\�-\�\�\�ݹ\�x5:5\�t�\"�M\�!C�\�S�FO�#��\�+	m,���\�p\��f\0�|S\�D�!���\�\�; ���{g�1\�W+\�\�.\� \0�7H@R8W9 L�l��$E4\0� ����\\Je�i\�\�\�!�\�YGB�\�ޡ�]\�\'��$�\�y<g��\�q���i\�X\�R3.R\�v�2\�\��\"�\�M2\��Xڒ6\�\�9\���לz\�#5�<e.]\ZP\��4�3�$d�������t��Yv1]�E��##�\�$��\�V\Ze���H#eu7(\�98\�I\0\��\�80\�\�\\Mi,Q1ಐ}zq\�V�el���%˜nYCH�3�9\�Ͽj��-J\�@��`G�d�}7�\�4E(�\�s�V���l�@&Iv0h=\�?�\�\�~\���\�#� Ls�V\�\�\'b�RT�h8\'I�ւB�\�Ҹ��A��?\�L���s+,h$g�\0�y���j\�UH؂GA�\n�\�6���z�:\�\�kS,H~T�\0I\�OA�Vb�%A\�v��q\�\��5�\�(\�w\�¹��-s\�q\�=jRNJۖ\�\��u<ƨ\�[��#��2:t\�)iqb0�\Zأ��;�\rrs]	�1RmmY\�\�H �\nRy�U�RK3?\�@jY�\nX���\�Sϖ�\�\��i@ޞ\�%Z�\�p}H\��4B\0A�b¡F\0K�{�O�\�Dw9\'�8��v\�z\�Z�B��(o�?Z���2{GҪ\�\�䌌z���\�Vw*����>�\�8��\��r\�! �.8?�G$X�~��9\���x\��1nT\\b��OS��J\ZnTػ�\�<�\�R�#\�y@=je\�o��\�v^��$~`U{�\�`8t*@\�0\��x����9\\TS�3n$U$#\�*��\\�v#ri�RĂpFr\rT�����J�4G���m�\0I��ն���fH/m�^>Їzs\�\0~5N\�-^B�M*�_/h\�ܓ�+V\�\�m����ʹ��\����Uϱā�S)l۴�䲂�\�Q���\�Io� ���A�F|�>\��\�3L�TP4�Ē<�8\�x\�c\0��QI(���h�\�96	\�9\�׃\�ҡA���J֋�#ġ���Td��a���J�\�,]�\�p�\�$m�}2G\�RN)m\�`�YY�UI% \��\�\�O\�Mtx�h�Q��q\�\�H\��\0��\�(�㭤GeYd+o\0\�\�Č3\��\�`	��P\�p3J\�*���z�ԟ\�\�\�ӌ\��T$�`T1\�$\�\�A�ӟJю;\�P\�ʍԴ�\"�\�E98\�\�ڔ��>�-in\\\�\�\�H��Ku\n�\�A�ǽdN!iX�FI�r	Ċ\���C\�\�]\�ĜFF\�\�?��\�\\��dM���vf`=\�@�*��\�4��\�e�\�S]���\�\��	��\�!\�+p�\�GD\�J��\���*�G\�,q�\�5\�밈\�\��ث�ʼ�\�R2�pv>� �ҭVz\�o���3��K(\n8�\�\n\��\�\Z\�\ZsLy\�\�}}+M�V$G\�V����ݹIW�\�?�y�l]JSN绎\�)զ\�.�\�\�tOkz��\��Iy6\�G\�z\�\���<E�[�\�hP�5 }[\'\�\�o�4\�=.\�[l0A�\�\n@�\���j\��&\�w�Mg��!�\�c�GJ�>�)����\�XJ��e\��)�PpK�w21:�=	\����Ee��R\"4\�Q\�	LWg\����\�5�\�\��-�g8\��\�W\�\��Q#\�\\gm�m�Զ3�\Z\�%5b����\�\�&��\�.̐;�#?\�4�\�/.ul�O\��TH\�\�=�\�zh�xغ*�r3���O_֧2\\$�UG,\�+�r\�*@�c�c\�j\�O0\�uHe�5\�q;�(;��,x\�}M:\� �\�-��g����N1��{ƲE-�P�2�\�G\��Q���sW����X\n\�\0$U�~�ǧnƓ\�jZ�đ8E��\�2dB\�C\�q\�>\�:>�<(Z\�ȇ�ĨJ79\\�r28\�ۚ6�p���\�F\���\'\0\�>����\�\�H-+9��lbT\�b2�\�:\�k�\�Ek�>\�H�����l��I\�R9\0\�:�\�u�D\��2�G|�\�d�U�w�[@\�{k�\�d$B�Hn�\�\�s\�\�\�z\�;���&^yu�%I�`֑~\�\�Ͱ@\�Mڰ<d\��q��DM�Y�A4��/S�	�j[(�\�(I�\�H\�\��5b\�\r\�\�v�\�v</\���b���\Z�R Vb�\�\�\�\��>�g�j<kK�\0G\0\��5�c\�;��\�\0z�\��\'�>\�t\�%é\01U$dpA\'�⸪\�s=|6-S\"��ˈ\�@\0\�\�9\�\�f\�\"H`���vw��ifdJ� \�s\�=zsU��WW �UC�B�r@\�N��K���ƺ�v1��X\�f8%Ku�\�\�WXَ�\�8\�Z�FeP�\�9\�\�W��g�S0�0�\�sm�\�~��5ޟs\�r2��H#�F\�\�Vd��HT�pO��MC�V���T\0�<�\�N�2\0[\n}H\�\�M\0�\��J�8��`\0�p�e%�\�\�M݂�\�!w=\�ɫI\r#pv/\Z�F�t��\�\�r���\�\�R��ؐ�!\�)\��\�`y��\�g�\�8-w��Jv�C\Z�U,C�z�\�`�H�\�\�GO�\�ROB�\�\�h\�#��r\�s�\�O�1=��\�I�\�9�y�pL\�!\�j=�m\�BP���\�\�].�iog\Z�*�\�\�# {\�\�U4\��\�g-#nI� p3�\�\��M\�Q-.�6\�m��\�F}�^|C��C\��H�u�LٓX�#��\�cjW��S�}${�\�\Z\�\�\�I�Y]H GB+ԿSyЊ\�\��4���K	�@\�\�\0\�=��5\�I\�\� ��\�]����N�\�-�\�\�)�\�5\�x�:u��^\�a�Rd\'8`z\�FOc\�{xw,\\f|�w����\���;M�h\�h����\ZGnFpT?\�\�\�\�֒#@2\�H\�ԁ\�\�����\�\�b\�Aj>P3��4넲��\�qr��PEm��\�\�|\�\�|�vۨxaH_�f8 z�Ĝ{�*�\�ɕpe�A�e\�T�2I�z׍�aip.��D�P\�\�\�\008\�@#��[�M�$O;\�\��T19c�|�:�$};Wm*���.Z<�V�\�,�/�	�\�|\�\0\��9\�\�\�Ya�\�\"C�1:�\n\�1�$�{g�։\�!�YQ8\�\�\�OL�2G=���N\�@H\\��s\Z�w.R}qZ\�.b����e��Oݒ�x9\0��ۏJ���\��9 �#�*A��l��1�Y�8�dVʒ0:��\�\�LS�ّ�\�e���џC��\�G����s\�M\�^A�%�� ��^>�f��\�.Z\�im�##\� ���?��*(\"��X\�I\�Y� Ds�I<ma�y\��Z�x\nym�}kU\�\�����NG_q\�׭aVp�na�7�9;8�\nw@<\�\�\�\�H<g�ɪ�z�\�H0>\�\�g���@ռ%iaf\��\�\�(Ȍ\�8\�\0편\�CT�}#Ph.C(bJ8\�7=��y8�V��L�\�}K\�\�\�[���wy�\�\�$�QޓMա,\�v���\0�ų��{�r\��*	\'\�\0+z94\�1\0Ka+�	a�~�U�ʥZ<\�C�2\��t*rA\\�\���5\�\�\�$\�RO�\0^�\"\�Ѐ8��)mGO�u1[k�a��\n3��\�\��_o\�0j\��Ī��P#=I\�5�|$��\��\�u�\0�*6���F�\������f\0!|�)\��\�Џrk\�\�gۈ�3�`��$\�\��\�<\0\'5\���\��\�V��X�\�h�\�\�³�\�H����W\�\�i���!�iR��>XA\�#\�\� }sצ\�I\�qJ���\�\�(�\�-\"��\�kY���/9\\m,k��\�q\��\�j�\r\�e��H��@BJ �U�80�1\�3\�\�g�\�\�\�-\�E$�[�B�<\�O\��W{����;�\�9\�r��L��e\����ʞ?\�S$�\�z`�����̊NT�Q��\�<T�$�ȑ>HfNRA��$�J�֏\n\�, �PU\�s\��\�A�pG�K�JNŅ�]\�eR`~U�\�Gf=r1\�\�Gz#l\�|�@+��$F=pibGX\�kPDO�h$?t\�\� �\�\��<�A�\�a�<g���\��\�XI��\�\�\�\�$�ٝumm<[�W\n#z��\0����1���k�\�\�q\�*#��7~$w\�\�Ie0G{��}\�~V\nv�\�}}MMq�\�@X����e6\�d}��`��m_\�U�\�wVѹ�V@\�\\\�l\��؀OnǥRH�FP�VP8$�߹�_OH�7�\�\�}{\�XP�\� aG,��~��iC3�iT\�\�\�\�\�r��*�fE[e`|\�\�?\�q\�� q��\�\��[TBU$7l�\�\�\�=ɫ6ѳ*�	\�\\\�7)\�\�\�2q�\�5\�i\����c�F�\r1<aס#?J�\�V�WoC\�_��q��\�ŪZF\r\��?\�\�B2q\���\�qw\�*��$mQ�\0\�zz\�{�\�W�S2G\��(ۛ\�\0��\�rrE,2:\\�#�$�[�ǰ\�<~�T\�B_ԉ�u2d�-ԓ�p\�v\0{��Q�L�Ǚd�p8�]0NU�X��\�*[$���=�5�[xǚ\�9\�\0\�;w8\�]\�\�ȣ5�<h����\\��o~8\�]J��x�;�\���@N|�;��Oo�k%���\�k��Er�q�`	\0g���,�h0J\n���=\�\�\�\�kE�H[��\�T\�����\�(�&a�f�q�\0˟_�<\Z\�\�M\�E�����[\�\"h��Vo���\�}�\�ڙ�d�r��\��g�3�|�U�\n:\�U�=�⅍�\�*	\�;��9b/#۞\�\�\�1��0T\�S\�\n�Ǐҥ\�`2\� R\�.\�zd��\�銘.��\"��F\�d��?O�\�lxn\�f n��\������\��R�[-�ƪ8�E�.�2 �	\�\�\���\�\�\�2j1\�r1�U�+���\�9N\�0y�\��*�\�\�\�L\��zf�M\�c��*^\�\�lў\�P�Ґ\���$���*��pBv��ï|Vr�oDwNq�o2ݓyw��Nk���#\�Z��.#W �>a��\�\�Z\�\Zt*A�Pk\�{��P~\�\�\�?ƴ��x���\�;\�w��j�\�+3��\�\���5b\rKTBf�ϻ\�k\'\�ͺ\�\"z��\0:\�\�\0�^�v?;��\�5<;x\�\�Ӧ�y%� f{�;g9�2�\�|;\�[}\�5\�/�`D�`�}OR{�צi�e\�L8 ��\�a|`P�z^͢I�Ǹ\�!x\'�\�g�\0�k�&��rT�cs\�ATɝʒ�゠\�;\�\�,���\���1�\�^\� Ƨ�\�m��\�\�9\�g��t\�HX\�Hޠ�+\0?@kֲl\���\\�V7�D��9X�@�\�c��\�Q\�!oP^<\�p8h\�ꧧ?��\�ȑ2�v�\�T�g��ǭ;I�E�B\�h���XdA b\0#ЊS�$[*�\�T�S\�\�\�>\Z�{\�����$6�\�ȹ!\�c�Ny\���N\�\�ڢ����\��\0\�k\�G�q�\0Z\�%9\�.@1�\���\�U=\�\�a��\n���\�:��,,^)\�:���^C\�RIm8\r*3 $�1\�J�x�\�N��`\�\�\�\�x�n�\��\�hIh\�eN=FH�\�5�)\�U#�T*\�)%�\�xF\�]YT*�@O\\����kq���Fk�\��\�,\�Z2\\�A8k�\�u(\�`,�I\�W\�Pq\�J/C\�3\Z8�ueQ\�g\�i\��\�8��onE����IC�	 :ի\�\�&w \n\��}R+�--\�\�!��#�\�W>`�\�;��V��P��F\�֯\�FW\�S\��\�\�Wt�k��\�\r�@VnJ�r?���6E�#\��\0��V��\��=�\�_\�(\�\��j\�ƭ\�џsj-��͈o-�\��`�\��\��\Z�*6�gn]r\�Q��\�bH9犟_���\�\")=\�U��Myx�vr\�,1nVD\n\�\'МW\�S���\�\���Zu�A=�3\�\�!fY4�\0H`@\�\�\�}�\n\�Y\�1\�\�9F9�=sߟb=\0���b��p[�I2� ��=3W7��V��.\0\n\�z�G!3W$�aY��4ћx\�U�1�Cr=�:`�?.x�BY��D��\�6z��2O~X�#\�&�Q����6W��rHǿ��5}�Ɯ�p$��\�>\�q\�\�p\�k\�s\�xrp��F/{6[\�TZ\�Kc\0\�F=+�+�5\�k�\�rG\�\�����-|\�\���^Ο��\rn\�K`\Z\��9q���(\�i�1H\��Ғv5�F7=�mce�܉�B\�	�-\�\'\r�\��ɮ\�\��ī\�.�\�1�\��׈k7\�k=�#\�88\��\0��SG�\�B��H\�N@\�ѓqI��\�8h³�z���t\�9��YH�`��M�\0��\�W�J�@����)$�:���k--\���+��}�y \�Z9v�c\��`��\�r��礛ўKq2��\�\�&��w6s\�Y�\��HX����@�� WS\�\"-?\�7)�EY�:��\�j\�\Z?(\�z��}\�ߣiEH�Քd\�V%m�ح�y�)	�:��A\�s�ld�kP�;v�(?�M\�\�9\�Z\�hs�K��\�e\0$\� ���\��\ri[eTG\'�$g\�~C��qC�@x\�nW^? 	�̎!(\�oR088�:~U\�Z�1\�\�q~\�\\��ʂ6H:�\����ɗ\�C,,Nd@0ˎ2�5�CR�\"�@a���?��\�<;ჩ\\Eq<�lX\��\�s\�=�z\�8\���\�\�0�E\�s��O<\�t$\� �� d~#�jh�4p\�\0�	=y�\�ج�1�\�D\0��F\�8\��\0Z�\���\��\�A˜P�8�}��amt<�Nb\Z教\�>$� �YT�e\0�\�A �\0Oαc�\0�r>�\�x�B�\�*��h�|�\��\�+\��\�\\<\nc�\n��kƭF2��z�{�\�jR�N5\�V�\�]j	L#�\�`\ZW��31\��i�sp\�_31\�\��\�鶖���Eg=\�nA#�#�1]x,��#�7\�%\���,,5�T�\0\� \�\���Z��\�%\�ܕI\�\n\�<�\�5�>��\� F\0�?�\�#`\�\n����U\�,��[*��_ݶ����=�\�\��\'�\�\�1\�Mv#Q�\r,���f��ꋪ\�o\Z\���� �=���{y\r\�����\�=�\�\�+��t�4v;0u&V����\Z\�;�VH\�:� \�W5�WSW���@�\�F{�b3�\0?\Z\��O���i]�\�c�\�ҤԴ۽[XmVe3��]�!@\�d;\n\�\�\�)B�\�ά^S��-F73��QV�\�\�\�\�� �\0/ֳ\Zڧ�\0\�Lx�\�\'?�j��Wvn\�P\��d\0Cg� \�\�5^F۫���@a\�\0��5\�ҩ\Z�\�\�\�\�Võ\n\�\�dM\ZRO@H�?�\0�&�i\�Yޱ\�\�d�3�1\���C\�QE\n��)�\�@X�S��>�#�5�a�8X��˶]\�?1<�=��d�t�8\�5a\�u��1x��\�w�ܠ�#\��\�)\�П�\0^+\�4?Yk�jʓ)8|�\�p}\r|����=\�c\n0\��\�>\Z���Ֆi\�!�\"D!�nǎ�\�bpP\����OK0��U����\�\�u�P\�\�oK\�\�!\0��\�z�\�VMơ`\��9\0\�@8�+WR,`�S�\��\�b��\\\�\�K��\��3���(\�sz��\��٩R�;H\�=:�i4	\�;�Bʜ�\��\�1ʬ@NՐ$��\�&/��%|�\�ӓ\�z\�^-Ƨ,އ\��VM�u)�ʺƦe��Iا\n9>��h�_�W\Z\�\�\\܉N6\"�\�OaU\�tS.��\�YA �zW�\�z%��]�\�\�\�>a$e�8##�\0z\�n+��t>R�\�\�\r��\�[;t�X�N�$f\�c�\�Z\��_\�\�\�M�YL\�\�[7\�-\�:�9�\�|\�\"��S\��1G3�\�#A2��X\�w�5Ӄi�z43L]�O}���\�\�]�K9f��\������9\�\�ȅ�σ� \�\�\�kKS\�\rWS���\�2F�z�p	\�\�+)U7\0\�\�ryǰ�\�\�\\U��\�W�\�6\��d\�y\"[4G���*\�\�F8\�z��Z\�XR%GP?���s\�\�?*�9S�P \��_ֺ]�:��U\�4qK7z�?j���5yO\�U�,u2�4S\��\�$-\�z�=\Z\�\�ͧ�_�\r�\����u��+�[\��������s��\�5\�\�EΚ�[�%�\�B��d���N\"�zN��=ܮ�#	�R\�Ge��\�\�\�\�iv\�|�gґ�\�2HǱ�j\�孮�K+�x#�ҵ\�\�ߑׯJ�\�e#�\Zy\�\�/u&W1\�\�����k��	�%�\�v\�U���Q|�V2q��[\�\�\�]E���YF\ZE@\�7�O\�^�,�X\�\�|\�a\�P\�U\��F����|Qz�8\����Wgᛨ~\�BT2��a�	뎵�����$�\�\��~Y��ZZ\�[\�\�\�w	1\�\�#\��\�ufJ�ў\\�cev�s\�\�`s[Z4�\��+\�n<]3J�c��]ׄ�\�\��\\\�\�\ZH,\�6=3\\\r�3w��\'2W3�\"̗>*u��E\'h<\���{\�!$e@epT�*ďק\�Zڅ��ڍ\�Ӳ9�˅f\0O\0r8\0c\"�\\����kw\�\���u�z�H��&|�+[�S�Zԥ�<��\��\0�*����P\�BWk\�p�\�y���\�!\�k�-KC�ǖ\�\�\�\�\�\�`��ϼGΧ�<��>˒��s�B2��\0T�g\'�U�TWg�\�8�q\r�\�}O\�\rgVj�\�;p\�牭\Z4��\�\��ޘ���gk0�܌\�7%@\'$0@#�Z�;:+2���G��+��[�#\�WZ�ѫO$�We��\�{�^� \�\��꘩b�\��j\�\�\�ʒwkS�����/\�\�䐊\�j\���n\�,�jЩ �b�\n\�\�`�ż�\0\��\�:�g�kKn\�\�F&*�#p\�~�\�{\Z�\�\�\�%Ӵ���\�tdwA�\\�B�Zd\�\0��=H�\�d-<�N@\'ַ�8\�\r�\�\�Q\�\�\�:k\�\"�\�J�P��u?\�ҽU�-tԵ�\�ǧR~��\�\'\��.��#=���\�]���2�=\n�0\�\� �>c7�/l��9��!�j	4ȏc��t {TRA\�]g�T�\�\�>\�\�Ȯ�\�\�A=��R\�t��*\�A88\��H�i�Ńҥ,@�V鰁���\�6&��6�o,\�Mb!\'ѓi\�\�vVH񠌰\�r\����\�|H9�lw\�+��v�U����Oj\�K\\��\'\�_Ur;3��5Oo\������ɑp�@n�\�Դ�m\�\��\�\�\�;��\�>��s��\�;~�\�\�\'�h\0\��j�&%Ҫ�sf�dqxIB�ק�\�i����	!C}p��\�M\�]���=O&�\��a�:�O�\�H=\�pG\�+J7�I\�t#���5��\�\�\�\�1�\\�b3U��eD1_J\�l\�k0\� *��l��\�\�|&YҲ5\r*�߷\�;�jqUX8�y.f�US�\�;��FK\�$����]Z\�`�\�\�Z:�:�u\�	�?Q����Ո*A�\"�2�	R��?s�ftqTcRs[_3\'�T���T�ee\�*�f>�d�ַmt�.&UU\�OS�S]F�s�xCAɲ{�BV9\�ĒK\�y\�\�eJ6�3<�\�\�(}^��g�i��M\�B@\�l�\�^ǥ؋k\� n\'\�y�\�l\�Q�P��v�w#A\�\0t\�ڽ@\0�1]\\ɻ�\�+FP�ܖ��-�\�Oʪ�^\\�a\�/$\����\�@\�<zל�\�\�$\�t\�\�i��u��\�R�i(\�v*�0�s�8��q\�}F�\�\�gw\�\�\�Ⱥ�%\��0y����E̦S\�\'\'g\��B+%Ԁ6��?�\��z׽Vp\��ɽ<<%\n\����\�I�\��\0�<>���\�c\�X�X���W�O\�,�\�f9\0�O_q�U\�\�q���%��\�8\�����?1Tv5\�W\�:�R\�{�pk�\�\�s���&���dA\�\�\�k�\��#�n� \�$|2�P_�uWvH�\��q\\桦ېI�r}2+5cUt\�yo��{{\�y\'�.# -©$d\�\�ڼ�MK����bA\�\'<�Z\�|S!�\�x#%c�=G�sz`D\�g�9P	\�5\�K\n�8��*\\�}Q\�i�4Q\�F\�\�~P��@v\����f��a�\�\0�I\�u�l�J�Lk��e2:W�d|]\\\\�VR�\�ɦ>	\�3��[G=�\�s)\�C�\�\�],���4#9�i3zX\�\�E�\�J�\�DB������\���\�\�,,m!I%a3c##\���\�+��\ŗL����F�`��\�B�np1_+�\�\�̶?P\�lL*a�d�\�\�]n\�c2\�o~U��\�uv�fU��L\�z����r9 U�Dkk�\�r��;��\�*\�\r8�O����^��\�0:��@\�\�\�$x?B��������\�;�>u �>�\��`Λgq�������Rg\�\�\�Z�u�$�OOG�\0V%8\�dǩ JэKh\�\��\�* ���5LU\���	�rUpc�\�\�\�њ_\�\�\�O\�/5�\�\�o���\0��H�\�0<�\�\�\ru�q���\0x�\�N\�cO�2\�~�ğ��z���\r�`�\�\�gi����\�hN�%���G\\��v�K[\� �Re~OE�]U\�|��W�x���\�ı-��\�8\�\\\�ʮr\��μ�R!\�5�]KM��\"EB��q\��+\�o_tRy`zWm�=�	e�\�F	�V��N�\"9\�C�������N�CU\���|m�\�\\�P�Ӧ+y|B\�>\�B\�O,[�X��,\�с��U#�\�j�Hs�H �j�p\�踥\���\�&��\0OF��K���AR*W`V�]�S	?+�}G�\0Z�\�$e��I�V2RW>\'��\Z܄R�JY\�\0=j����\�69,ǿ��\�F�\�!$\�G@+�\�,JN	��uc7\���\�j�5��-p��\0$�\�\�FF+#\����\"������8�J\��y\�\�t��u\�Wq\'�\"�:�)I\�\�\�Y^g��N0�2�+o\�\�\�\�̻���m�P<G�\�8ϦzV����,�\�j��\��=+SnI%�\�\�+\�����+X\�ԓ�&wX�Ѻ�\���I�\��FA����K+\�ܳ1b}\�\�=\"ȯ��y`��\�\�sU�\�V\�\���\"h۹C�\��5f1j0w\�H���\�PEnN8�K=5\�`p=qJVKVr�a�Y+�·V�\�HnT*LKG!�\'8>��w\�Ԓ\�V\�h����\�\� �\�\�\���\�V\�l&[�8�	\�q�:c�G�Cu\�\�:68D�*\��A#�<\��\�\�\�S�\�9I�\�*ӣiK���\�BX$�\�D;�U�\�t\�r��V�E\�1/�a\����튈�\�2��0�\0�y\�\���V�1�a�ki�$\�p!������y�1�:�(J\���\��Xj��W\�qt��*�n\�\�\�)\��R��`z�q\�t\�[w\�\'��\�\�\�ID�\�ʪ}\0\�z��q\�PD\�jі���edr\n�{�F+R\�gBO�|�\�~�����8ZR\�\�\�\�9ϪV�\�S��\�_\�\ZƬ��qE	\�N\�\���jE�Ne���\�s�Ǡ�jܶ�$���p+ߧJ0V��O\�%\�-_�^YZP\0��!�\���z���F�����v�I?����ڴ\ZE�6W-�B?�Ey�\�j�].}�5<\�M\�\�\��㢺ɣZ2\�R��rE\�\�\\�<[j\�\�`\�K\���\�\�?���\�gI�\�u\�CX�zX\�2�^T\�2\�\�PO\�j�Քr�F\�t\��zWA⻖�O���b>\��\0\�^7{څ\�\�\�\�-���:�,\��e�\�/t\��\\�\�( �;��\rrֳ���g#�Wu?�g��\�{���\�\���{P�ƞ\�\��\�\�pA\�l�\�]T1�U}[#�\�U\�5\�\�jb�&BƠ�\����[;���dt ��מ\�9F\\u�lh׭\rҩ\'k��v\�c�\�\�P\�Ʀ\�\�\�:�\\��;\' �ϹuPKu\�N\�\0���9&��N�\�\�Ca���\�Wg��\�J��QU��|>\�7��>��\0<V^����<\�\�Xw�N�\\*I]\���\��u\Z��j1*XX\�Ź,F?C\�\�_3�\�\�\�\�\��+ �0r\\\�ƒ��Z\��\0�;\��K�\�N�\�ݔ���,rG!�D*\��#^�\��G\�LeD}\�E�2i3\�\�y�RY�\�\�G$\�\������\�9\'�~tm�5\�a�J�t>+8�\��wW�\�2G~l\�\�\�R\�\��RA&�\�H�CqH\0>i$\�t ��\�V�\�\Z\�#wv\�05S���ҳf\�\Z\�G��/J��SUcil~_�\�}V��\rf�����\�)�\�#\�F%��\�]O�<tI�O����\�#�\'\�~���N�o\'2\�9\�\�&�%��\�	9}A�\Z�:t\�\���y�#C�G\�G\�05��p�Z5,Of\�\�ڼ�ȗ\\��\�%&(������\�+��湩_Z\�gs t�q\'v\��\�\��\�lZi^T|N޽\�|\�+Y�c�\�\�*\n����\�ڜ�\�\0\�61�U)5\�$���\n�\�\�GT!�\�\�$ϯ�ķss4\�븎��@\����錥n\"\\3�:8\��\�Dpx\�5*�_b��t܅IP8!�n��*҄�\�n]\ZԜg�\�i,E�dg$�pkSW�*�q\�\�?\�S4\�d�C\�$\'I�=�*\r^\�ĥ�\�b\��rFC��ӹ���^1��s�,N	\�\�%�\�\�\�:�\r\�\�\0v\�ڻxW{�\�\�\�=@`O��f�wLY\�Ĳ( ��\��[?[[h�I�=\�\�\0\�ʠp=GL�Z�j\�M�KS��$ݖ�	\��kmM\�\�앗\�9`R@\�Փ\�P�a-�e\�\0�UB=dvbz�֤:<\�0O�5\�\'\�\�Ľա\�Λ\�[\�`�<�\�\�K�w1#h#�\�=}x\�\�i/�N��X$�\�s\�`8�>�ߞ��\�i�F��5� \�\�sL�^�\�r�1F\�\�\�a\���\0\�V�%U�䂽LEL4��JG��f�\�qQib\�\�\�6�T�W^�#�>�>��\r�\�i\r��s_H��\�\�\�(\�\�˖D�u���\�y�MKS�Тī%\�\0�=��\0�>\�\�-Gk�\0\�\�,JFr\�u>é�>�Ԟ\'�{\�\�cf��l�en�޿����\�0���z��\��a���\��\0���k~,�iT3 8i\�8E�s��}?ᆛ}q5\��\nv/\�\�\�]���6�$0F�ĀU\0\��z�n�s{ % �� w�&��x\Zq\\\�5g\�\�\�\�\�\\\�5f0�/�\�\��9O�v\'�\�φ�\�>B\�l\�G$~M��kO�Z\�\Z\����Kyk�F2Ѐx=\�\\׵���\�/=\����$@i\�XzɮT��]?uhy^�\��g\�\�o,\�k���, ��=G\\}	����\�\�\�\�\�EV帎Q��{\�}�\Z�%e�+V\�5\�9�j�մ\��J�<� �`9,=\�\�ׯXt�a_�ý:�\n�7Es\�\�v+\�\Z ���1�䧷(\�A�\��+�.����\�\Z�\�\0\��u\�܎����Z���\�\�m���\��xr�O���XRJ���M\�,]�\�#\�E\�}Ϡ��5+�q\�\�	`�\�=�8\�޽\�b�\�I\�w�\�X��G|\��5\�ͩ\�@?\�d��\�]�Iht\�2�VR\�?yu3.5��:�9�\�S$\r�ѓ�A\���ڽo��:�R��e\0K��=\rx�̂Y�m\�Oj�\�X]-Ť\�\�x �G�\�pW\�}\�},�ձ֝W\�޼q�%ͬV4\�	\'�{}z�\�]\�tXc\�ZG���\�\�}EY\�$��\�wtCJ#}=��\�U��\'�碮?�|\�ozM��\�xuF�iy]���jw�~W\�=\0��^ݺ2JL�z�jԖ\�J\�2�W=�Oe\�\�%\��Mv\�UR$T�ϵe\�K�Ն0�vv\�gW m<0\�Z\�\�l��I!XU�\�p2O��[+������\�M�(\�u��,귦;q\Z�/Ԏ¹��ydX\�v\�Os\�[���s\�\�2�Pd\0O����ޚ�^	A��@���s�O݋>��4y�\�\�𯄁x\�n|�ʫ.NO�\�^Ǣ=��-\�\�r\�2�\�\�`=3\�\�Z\�v)�ۭ���LyJ\��u\0��<T\�\�p�\�S^T\�)h�T\ZwoRg��j�OS��b���x�K�P#�>\�B\0!H�NrqӞ}F�i����F=�ְ��\�%E\�T󁌎\�qMY�zU\�R��$\�8˛w�f��g�J\�LUcg\�V\�\�+I\�\�ҵ]>g�y%0ˎQ�á#��\�5�(�D\�.d\�\�ba��l�\�,�AJ�\�0�\�vl�!ܭ�>�\�\�hT��ЇP2G�.0rsO\n⾲*\���2�96�g/{�DQ�΀�W->Cx\�z-\�����\�7��\� �r�\�v�kŸ\�}.M�S����τ\n\�rz\�ʺ�\�\�:�\�a{�\\/�\�\Z��\�Q���\0\'��\�Hʒ1���lj���䓅L$;�����\Z��m\�w��-��\�V���sZ\�s���0~\�@��ѳE;�3��Kk���]�\�����r=+E,D\Z$�\�34�W#�$�OJ\�<=�M���*�\�� �\��SZ\Z֜�Ome���$c<�I?\�z8v\�5s�<\�\�8\�X\��֞��P��G$׮�~\������ \�?Ҹ���\�v�ň\�5\�Zu����1��\�OZ�f\�\�\�Y%1�f\0t�d�\�L\\{\�/9P�g�2\ZzU\�\�U=0s�\�+\�\�\�Sծƕ�\�]1\0�D.���\�+��J	\��G��rc h$1�GU\�+��\��1\��I\�/\�\\���pÀx�F\�\�(>�Z�+\�\�z,)\�K\�]Nō&�\�[m\"\��Y��Gm\�FI�\�\�k\��\�]E�\�\n\��3����S^]\�8?�>(\\\�����^y\�~Y�+\��m�x3UX/�v8��\��Zm\�S��:��(\�uz�4\�uK-R5�\�W	\�tl\�\�y�\�a|L�>�4�6�3��\�3rP�@ ���1�V?�5Y�\�i�S�\r\�$\\�\�\�dz�A\�\�^Ө�cJԯ���\��d2N��r`��pJP��\�IicxJx���\Zg�j\�\0\�t��\����ώA��0AS\�P}�k��5WQo\�\�^\�\�\�퍈\�`�\�8\0\�^�}⳧x�\�=f\�\�\Z\�\�\�Y]F\�\�\\\�U�=Nv���\�A7�\�\�\�Z\��\�ZE0�\�� �\�\�\�#�\0\�Gj%��vN�SM�^\�(<Yw\�-6\�@KT����ܕFrH�q\�\��=�Z����2\��\rxo�<]k7��Դ\�:\�C��W\0�\�P\0eR@\0c�p\�G�:�\��:��\�Q\�!���\n�q�	8\�=�S\�\�Nn��S�*w�\�p7ֿ��|E���,�X@�p\��<�lW[��\�8\�\'?\�ڳ�-Z�i��>x�h�=g�\0e�MM��m�\��\�VϹ\0�\0�u`%YS\�sƟ��R���\�[]�\�A\��b���n\�=\�	�\�1�*�\0E}}\�#\�\�Q�#�\�mVw�(9\�J\�\�*\�H�B\�l�\�\���}c\\-\��+#��}�\�<D��\�d��\�<�\�\�zmчO��PK��=*6,\�Y�I\�Y��zY��\��@\�r��ʵJ\��\�\�փ�\�Y�n�*\�U#\�b��\�cڀ3\�VGb�L˒6�\�d\�r_NԕU,0H ��b�7�>x\�x[BmNy�$�!3��O��\�h�y\�\�H���~\�Mk]*\�R�\nĹ\�q�@+o\�zp���\'���\�wME0ZD�b�nbH$�2}I��\��Jsu�)\�\�=;z�U\�\��?<ôӹ\�h�l#�\�ӁZ\�o��楳�X`H\�`(��\�Q{hc%\�\�d\�^*�C�ȭw��E@`\�w�M3�\�\��,d�>\�\�b�\�e#�·�\0����x��G\"�/���Msi��-��@?�z~�?�y\�W���j\n�O�^\�\�l�i�	J0!�\�\�0#�i\�h\�y`1q؞�\�\��`Iֲ�YJ��Wl*\'�g�_\r̹\�y�\�\�Y:��\�d0�\�A�kq�sH\�|֏S���)sGs��L���VP2�*\�x\�\��\�\�u\�4��\�Fʽ�\�\�Ee\�\"�9U�\n\�\��Z&�b��\�\0r\�\��\�\�8HT�\�\�{ÜAV�e	�,�ќ\�ғetWZ3H�H�7�������\�D$<z��IE\�ٟ�P\�R�h2�ܚ�,^\�\Z$f\\ըle�ªI\�ֺ\r*\�D\�/�:�\�$���O��\0s\�zQ\�뱎3���\�΋GІ��CjX\�7\���\�מ��]���e��i*�5��)\���@H$� ƴ|[�=n#�\�\���\���e*@\�B\�\'\���yD���!�\r����\�}k�ڨ�T|�,��nlEe�>�\�t�b�\\\�`brI�Eth�@�\��r/xr\��J�J�G�a���\"�\Z�\�cǩFN2\�-5�0�U{��l\�i�%H\�Q�\�p	7�3�I]�b$�\�yw�<F��\�M�|\��\�v\�\�\��\�\�S��\�-v�Y酣�\�<\����\��WKH\�PI5\�`pn?���G\�\�Y����6ii�8\�zW�\�?dWa\�>��oi3^Ί��Ԃώ\0�\0\Z�\�H\�aT\0*q�vGV��$�\�x1~\��7T�n	��\0���u\�5�5熴\�\�\�\'Ԅ��\�$$cŀO?Lz�\\��\����k\'嶺 HǠ\�k�@�^�sw���s�X\"C#?���\�\�\�N*P}μ�\�}\�~\�QՓ]�I�ӭ-�[� h�\� ����H\�\�q�\�	\�+�񞫮\�I�?�nD�\�ܑ,�˸B�\�\n\�A\�LJ�����\�r\�a���\�?)�\�G��\�g\�W�\�W�-��&\�d\Z&�\�xAXo\�H\� ��n�Q�T�M�\Za\�Q�t\�_\�u\�1�\�C�\�O�\�b{IQ�-����\0#�2��v�\�\�W�\�>\�6�i�i�F�\�\�^`�� *�sԟ�\�\�\�\\ֵ�Z�$+gl\r���2H�\�\�8=��x�^�\�p�sE`ڽ\�\�Lel\�\�#{�Qܑ�:\�6\�BPZ}\�\�鶗WV�\�j\"т\�\n�\�\�\�ѓ�@\�7H\�\����\����&y��\��oR��\r�\�\�\��s[�\�-��I\��tYZ\�т��820`�T���I\�z�޻�+MЦk:u��7$I\�C0ns�w\�a\�}�\�v<ׇ^Ӛ��\�\�+8>��}\�\\\\+�n\�bI�\��V%P}\0W=\�b<A\�\�+GO�+ n.1\�\�\�?\�\�\�\0\�\�\�\�\�d\�sV��G�\�PIo;+�WRA���Z�mgC�\�b!�I�\�\�Oc\�+\��\rj\Zl�\�h�àʟ˧\�_M�\�E�Ya�9K\�@�\�s�\�\�t�n�\���pE[&U\�\�)�Ų#\�+�\�ʧ\Z�eu�0\�b�G���\��\�r8���ţ�\���mи\�=�\���)F^N\�W�\�>\Z\Zv��{:��	I<�d\n�jiJ;���\�wZ�\\kk�(Fs@����$�\�ʷb%`XFð\�ӏ|V}\��G��\�V\���\�2GL�O<p:\�\�8\�t?A��Ш��\�\ZiUI$�f�3FСѴ�\�;P�\�J\�@\0�d�\�`k����\�4�k\�=��\�	.	@�\0�\�CЀA�W\�O\���k+�\�\�IV�\�!d�\�X�\�?A\�ZE�N\�s\�\�{lѪt~��\�/�Q֢�L\'\��,� \�p\�F����>�\�\Z-�F\��\nD�.r\�G�\�|\�.��d�3\�װ|7�l)�[\�Z�\�;�S���\�q\�#�3\�ZB��\�\�̲�ai)Eh�NQ��)��ު^j�V(^\�\�(�}��Rrz?)(�\�ض@=k��/���2~W�p|��\�}O��j�\�>#A4ZRy�\�i\n>���?\Z�k\�F{ۇ�y\ZY_\�15\�ar�I�T\�&;6�S�\r_q��R\�\�Kq;��F,\��5K\�-\�ii\�=\�p#�&bO\'\�Oj\�m�c�H\� 1	��ӫ��F\�<\�6\n�d\�\'k�E����t{{\�+<A��a��o}�\���p;W�\�\�\�3э�\��,ĵ���\���\�e\�\�=Z3�`;���? \nQ\�AZ\�R(�`\�KV�*�\�:�\��Pw.��\�\�tmKG\�na�;yaxе\�[�\�(���<��8\��\�S�\00y��:��ԋL\�\rBex��\�\���{Y�\�\r�H# �\�U�\�m��U��\��#�ׂ\rhOc�G3�Z9��I\��\�Tl5�\rV\�G��G�@A\�X;�8ʂ\0\0�\�\�Ȕ\�$�\��\�J���+齌\�4\�U�MD�u%�ۄ\r\�\0͒H3��\�:sj\��-�\�@�q�	Rk�1�\0I#��֒HÀ�\�s�}\�1V\�	E,\�q\�F�Mm�<\�� HB�`�=y\�K�]mX\�>�c��\�~%�t7�(l\�%CFy\���=�C\�T\�����p�\\\���	\��>;G\�^}Js�\�s\�gxj\��\����2�����\�.Y@P��LN\�9���\��\0ǳi�<\�D�\�O�Ɇ&8rH݃�\�8#\�Q\�.����6�\�IN\�7v\�{9\�\�Z�\�\�\�D�\�\�\Z����_q]\�X4\�>\�\�f�\�&��}�\�ĝa�D\�R\�L`��gS�s�\�^_\�Zġ\�.d��\0�\�\�^�-��\�<�#��8�\�b\�l# ��\� �b�&�%\�\�S\�*\�g^^�ݏ,�\�f�`\'v=�&�M3�\�\�vDI\�j�\��\n\�R4�@EU_a�JELuI-\r�`�S\�jS�\�ಁb�0�;\�>��5q\�\�\��\�rmݝq�F7�|?��)m	:��1u\�L��\�\�X\�\�\�S\�\��η�m���)F|\�\�9끀}F\rv\�\\Ǌ<�ĩ�i�M^c�N7�\�7�\�\�r8�j�י:r�����$<)ȝt�$\�i]���\'\�]Lq�H����\0\n\0\�\�^:�\�u\�<Un\���\��\�O|�^�\�v\�\�\�+,2���\�d`A�J�gCj2�-a�<\�M�է\�\�.�c�n���C	\�\�$RÎ�\�\n�̺�%\���\�Y�o:�\�In\����ɼe1\�9��\��l\�_A\0\�?:�n�gmM\"�cľ\��W<W\Z��\�՚(\�s���z�\��\�7�\����\��ɵ��,\�\�s\�\��S5�\Z\��|�\�\�v��1\�[[|\�?\���~\�KH�\�\���]e��j�\�n=�\����\��mGsϝD\�--\�\�\�W6�\Z��3�j\r\�K��9\�\�;t\�]#�Ԭy\�j��kXG�X#MB<����c Ђ2\rLA4�Oz\�;♑u\�\�2\�-�a�R���+=���\�Y@��\0֮��\�+X֚Z2%N/ts�\�\��&�\�\�E+�A��F2\���[M5t\�,Zi\�Uڬ\�b[��\�>�$ֹ�\�`�\�u%)���P|�M�E���\�\�ڄ�d\�\�t�B�L�I\�0~\\c�\���\�KIl\�ٍ\�0\��� \�zS\�^(-&�]�8�\�K��s�¼\��\rSI��9�Iݤ�%$��<\�`p1\\�Ɩ�\�篁�V\�\�Tth��\�h�쥊\�AB���1\�\�^Y\�\\i+\�\�_-�\��arzg�\�xc\��>%��Y�\�d�\�H� ��\�+WP���m!\�!VRI�#\0Oq\�J\�T\�V*Qf�\��Vt�n\�\�xޝb�)�2X�\�\�=Et�>��f-}��:;�{�:s]���h\�{��<��Af�\��zW�\��B�Ե8�(��F�UO\�Ќ���\�\�\\U�(��j},~+2��T\����:�_\�W\�v�V���\�V[\�\�����JNH#\'#��d\�x�d>E�L��RC�_\�]���A=��y�\�\�͒ѷ+�r?]{x<STӦ|g���*u�g�G��\0Rs�n Q\���\nٰ�\r��W��\�`rUF\�~�����\�.���<�xj4\�\�!�����Em\nƀc�>��9$��Q\\\�]Y\�X\\\�QE\n(��\n*9�!�P\�J��\�\�:Ο\�zl9c#Ȥ�\�\�Vq4i|r�\�KdV��\�\�\�Ӣ\\I\�\�?7\�v��$\�ڼ�S�\�\�çHLq(!\�=���k����\�HYb*�\�0Ðs\�s^v�#\�Ɂ��\�<��\�bkƴ���G\��=��,2oW#\�<\'���[\�\�GpA-\�@���=}k�><�\�\0�\�O���¼3S�+W�\�\�rH���\�W%�Ū�.����\0UoKYEr��\��l�9K\���\�>)�ȖQ�Fc\�;rI#ny\0d�\�\�ָ[{��K\�u3�9m\�\�Ė�\�<�\�MP��xʆbA\�?JTH\�e�G�M\�?\�\\\�eR����\�`�\�\\5N�׹\�\�\�S���\�hm���\�˹�\�\0r\0<\�\��v\�\0\0\0\0\08�\�\�W�z|�9`\�\"�!$�	\�\�5\�6�͌\�\�\�I\�\��\�\�z8,e%Y\�\��,�\��fi � Ux\�Iua\�\Z�y\�zM]j�\":h<\�\�y\�+̵��Z及.�}?N��h\�\�\Z\�vv\���O^��/�|qO4�T�,\�o \0�I\���W��oȅ��ߑ\�`\�8cҸ\�\0���\�v��\�AF\�\�TDg ���\�+����\�6ׯ4\�?J��x%tUHݘ�� 7\�4\�&	)w4�\�R\�z\�>\�\�5嶾5�\�A7��q\�*�9��\��\�g9��Ǿ,�𝝜ְA1�\�K�021�=\���qr\�Tq\�r\�u\�u��n`���\�#�2(?��\�\\\��=����\��-CLc\�\�f%?z\�2�>�٭�xn6�*:\�!ʑ��Ls�WG\�ψx�g��\�취��ܮ��A�\�Q\�f�R�H�zeg�\�\�j\��\0	S��G{(هя?�K�\0<�|j\�!\�\�S<ǿ\�F� u�b\�\�G�O�ﴍ7I���	\�\0Wb�X��\�L�\�w�4k�\�[��B�\�\0\�N=	$Q\�i+��\�\�}=OH\��3F�\�\�\�\�\�}X\�\�\�\�5�5\�\�<)&�bSj�Ʈ:�\�\���W;\�\Z_��k伂\�%�T+\���\�$�WB�\�{�^ҜZ�\�v�Q���\�ڼ\�\�=Դ�Ǣ\�mj\�3Ļ�6\�>3Ё\�Ojs�$\�T�i�\��\r�\�\�\�:�\���\�&h\r����i�yS�\0\��\�Rx�\�7�\�\�.l\��S4�6��\�x�����o\Zh\�Z֫�D/.b#\�̙�\�<c�j\'RN\\�z�)ɹ�\�\�i2�ּKw�\�[9\�\�U\�# dc�I\�\�]#/�yψ��?\�\�\�zo�l-Mă}ѐ�?��\��pOs^���H͍\�A t�qUF�3qoTM9^�oTFF*\�G�tPI�Y^(\�&\�\�hB�4�,FH�N�\�\�k�7���bI\�?!�\\��\�v\�k�Uޤ\�,\�\�c�\�\"� ��d�hێ{u=+̮.\�7F\�\��\��`g<}1[���%��mp��\�X`B�q\�Oҹ�\'\�x�If$�O?�y����y�d��,4ZV��\�t�i%o-fki��9\'\�\�޺�ԯ\n�?�dd&1\�qS\�jz��\�EX�p>�\�|!w*�-\�\r�\�R�E)Ň\���F\�O\�5/�iu��\�\�\\��ٺ��\���k.[�2�5\�d\��W\�װ�\�ѷ�J�\�iM\�m\�	`9\����G5����ٶ7\�N*�]���\��<�KK�eUO:\�\�K#.X��	\�rz�xV��ߙ��UCD���s�\�^�m�]C\Z�\�YT��NO\�kjY�®J�\�>[��u\�T��Ί\�ѵ\�\�\�0�n\'9�\0\nկ{\r��\"��\r��\�\\��EW@��(��(�Ǔ<�,�Fc`}�G�\�*��H��OC]O\�5�\�G�V�	X�<�\�_�SR\�\�籓\�\'�����o\�X\�,���\��\Z�lb:���O&�\�\�oW�\�\�<�Ze���X\�F\������t۹\�r�\�T�OzI�Sӡ�s�O��\�\�\r	���9\r?\�{���δö��V�1OK���\�sVc\�dR��#�k�u�˩�3*��\�2��\"�Rs�p	박?�\�<��бC��0�\�k�7��F�\�FMB�ɝ\�q��2�_\�\�*\�w�#�\�4æޔv\�IRA\�lv��믃P���\"\�^���\�x�YR\�1�0\�PA\��\�V��x\�J\��a\�\��\�eZn�57�>����a�=�\�tz��\�\�Vc�Ɂ���\�\�+\�jF��rˑ����\�\n���.8X&|$�Si\�\"�m���\�t��@+\�<XvxCWa\�ZI�\0���˗\�dg��A�\��K\�/�5�\�\��\0Pk:_\rO�\�C\��8��#:^���\0-��\0A�V\�^�\��=B\�)%�g�6DI,@\�@\�u_�%���J��\0?\�X������O}4P�\�\\)y�*\�\'�<v�.��=L�\�\�;ׇ�#\�x�[�M����\�Y�ɷ{k#\�0Ƌ�K��\0\�Mu���V\�5]5_�\�*�\�ҹ��+\�V\�iف�\�\��WEV���\�uV��\�\����=��?M�\�PF��\�U��K6@\�0:�s��\��^k\�n\�؃!N8\�\�A��\�x;\���֙}6�o5İ+�\�\�\�\��:�WoQ�\�,h�\n�\0\0{D)N|�}g>YO�\�Pk�����\�7I+B�3�,J	9$�w��2񭗍\r��l�c�Iw�\�\�\��as\�\'�\�.�=�_��\�\�H>\�rL@_\�\�\'\�]ċ�\nM\��V,dԷ/�mʳ����\�1��̓p��_c�s{9Y\�}��\�\�\�M�y%��\�qv2be �%I$cԓ^I\�O\�>7ͧ\��jhO3f\�\�8�O\�]���\����\�\�e��\��ݜ�\��9���:��������\�2\�A�EPq�=}2*\�\�J\�\�O�T\�v:M❆�~�\Z�����M\�\�	 s\�#�W\�8�\0��e\�[o\�*�\��?�\�\�=l�M$C!\�q\�\�G=?*�\�\�c��d\�d�\�1�?.ML\�JQ\�zّR��Ykf��\� ö$�E\�\�)��A\�o��C��kUX\"=\�:�a��\n�\�+�\����\�}>SY\�|36�5���\r\�[\"\�e��\r�\�G�ɭ$\�\�ڏTk\'/�5\r\�8�h��`�\�%g�\�CO*�U\��r;p;WЫ\�HM��W��c kz_�q�\0�\0\�W���\��\���L,yjMz\n�y*M/#���͕����~��y�\�\��׫�\�\�]h�1b\Z\�2\0wv#ۭy˱bX���\'3��\�v}I���ħZV�\�\�\��ȯuvK	Eg\�+ʏ_\��\�j�Q�F\�7�\0@\�R\�\�\�\��\0�\�5��n\�\��\0*\�Vt\�G\�\��akԥ��\���:��a\n\�v\�ZI�����\�VF8\�(\�<י*i����\�Ρl\�E�.\�8Ik\���R��d�\0�T�u�F\�cu$у�\�\r6\�� �VB7�\�\���f\�T�IK�\�ː�O����\0\'�s\Zʩ\'�x�F�{&�\�2P\0Ot\�^��eA)n}��:�:4\���k���\n/\�.�Քg׃�5\�\�?\�\�eА\��\�O\�\\c\�a���:w\�t��e7K�\��t��iS\�(��(��\0(��\0\��k\n\�ᛃ��\n�>`\ry3\�\�\�e���a�R\��\�\�\� )��W��y\�_9�&!z�_	d\��2o~\�F\�9�\�Q\��	�*q�U\�Aј�\�αJ�\�gH0s�Ƹ�Zt\�g\�\��>MU�\��\0C�\�\�@\�G�\0CgنZ�w1g=8�&�|��\��3�)f`I8�@\�oןҫ\�@\r��%��~U\���\��\�\�\�:�\�\��[\�;��\�\�$\�@�ik]\�0\���;w�\0¹}0\�\�\��ҽ\n\0���T�Gc\�q\�儥�tީ_�;�ͬ4\�-�\�(EY�\��\�Ziڠ��`򩗂k�����\�|�\�\�M\�[��oxf]lx��Ϟ.�\�\�\r�h \�:\��\�\�a\�4\��\�L�}��j�\�/�Z���\�\�JkR��.\��\�Y	\�p�q�z�{�\����\rF\�=v-@Ek����@�\�\�W$��\�r*�I��1���\�xsºo�#�4�\0;�\�\�>\�F@ǧSX\��={y=\�\�j�fv���$�qǩ�F\�5��\�-���#P�h��1�\0G�\�+\�9\�T6�!\�`:^�\�WSAkc]A�|\�\�\���vԹSj\� \�Rk�\�\��\0�K\�A\�\����\0\�V��\�M7\�v\�\��\�]�\��\�oQ�x\�)uMr\�O�\�\��\�5A\����bR�X��8�jv\�&��8\�<B\�\�[��y\�\���\�` �~����n]N�M(\�z>�a���alɁ&㓁\�{ռ�Ҽ\�G�\�sYZ\�Z\�-\�3\�Ƣy����.BH9#�3Ҳ_Y\�%�\�\��Q�\�\�kqp�M\0i���8S�\0S�k_l�\�C_�$�:{\�\�/�/&k�,�4��P\�98p2M>\�ᇅ\�\�Y\Z\�K�S�&���p0\��YK�;\�6pk�Զ�\�\�-淕��\�C���\�z\��\�m_Tm|Csq:\�Y�1:ɒH_�\";!\'9\�\'�:\����L\�F�\�;\�\�m\�\�e�(\�H�\"���:u�9����ԅ�wwmtex\�\"C�\�2r6\�t\'�ؚ[-g^�4�\�L�\�\�\�	%v\�\�7�z*��<v9n��k3��晝\"��\�g�\0Ys�\�\����=O҉Jj�	ΜڼN�F�׆�;|��0D�\�!u29yYG�I8\�b�\\xG\��\"\�\�_K4\�E\�\�0ٹ\�ЌƠֵM/\��u{2\���w�L�\\\�\�\'k�K��\�m.Z;a�J��$��Y��3�J��r�\�4\�(G\�hr�N>\�Z�\�\��\'��G\�)n_-��s\�i[[\�ei\r���$�;\00+\�m5-f+�\n;K�u$�Xjc�\nT3\�\�\��\�<T+.�u\��]J�.e��4�]\�\0P\��\�H\0�T�F�H�m\�-N�\�^ҼKu\��\0��$ؾ[\�\�9\�\�95����3�\0�p+ε\�ϱkz���\��+�oh�\�4�8�\"H�s\�\�\�R\�K�YIz�n�2�0C3�\�\�kJQsvD\�qrvZ�x�nЯ�?\�\�f�~\�0瑐+ص��F���\0���G\0\�^x\�jD\�kfd\�\�#��%g{i#tPͼ)�A\�5�|��pk\�&?�\��5Ňw��\�\�:\�Uׯ\�o�\�U,�I�*A��H?CDq����,*H\�\�\�Wj\�\�X\�B$\�(\�JeĲ�lQp@\��U�bDQ�{RN���\�\\*K�kc���{�C#��\�\0\0\�\�\�l�\�c(*Wz�W��f���\\\�\���\�(\�w\�\�g��\�$\�\��\�]��֜�4\�UJ?AVj+A�\�\�F�\�T��xujQ�>Rm�\�\n(��$(��\0*d���!ps\�gkzտ��9�K��N\�c\�77�\�=�k\�l�|_�Q�8\�\�\�>\\\�\�#�zq\������%S�*Wg��\�\�\�Ru\�%.��=gI��H���\��$\\.\��\�A\�j\��z\��ءq겏\��\�+K�K\�i���;\�?��<�\�϶z\�S��\'\�\Z�\�\�\�\�[ku%\�w�\0��\�\�k\�\�F8�\�8\�{58oFq�*\�J[hjK��T��d��P�	�I\�\�\�\�Eq�\�/<;�[\�\�\�p\�9P���>���z}��\�ղ��{�1��%$(\�	9<Wi\�W�)t\�Y\�t\�v\� \�J�\�}+eN%(��$�\�<�\�jI�\�\��\�\Z�*\n�%\�\�)�)�\�.=ExM\�ύ�����\�Su��d�j@3���a� �\�+�.\�L\���$�^\�BO��Mwq}\��\n\�h�j0&>\�b\0k��tc�a�\�wb\�\�Z\'\�g\�\�\�=��DQ����` S�w�Т�H�{�@�Q��\��M�ʮ\�1�8�\�4�4\�\�rܴɃ/P\0<��\�FTm\\t\�*\�n>�r����`APCuq�\�u\�m{�\���YZĹH\�\r�Yv��\�\�Wc��\�8\'�i\�L(\0\00\0�ʎ�Y�<�0�qI��֎T;��2+\�\0\�\�N\��00Nzw��wQ��T\�ۆ\0��9t>�ep c�D\Z���$�\n\ZKQ�۲@��T1����D6wӨ\��Q,�\��!B�8Py\�$מ\��\0\�\�5_\�_\n��<�>79�\��Nx�\�X\r$�N��j\�p���j�rOn\�\\r\�A�e�\�\�ȪK�\�JҖ\�w�z����\0O/�R8�SvaY\0\0\�dq^G/�\��k��A�\�,�5i2\�X��9���\��c\�\�mn��JV�HcE3}\��\�	\�9\�\�8b=\�-\�\�d��\'Z�\�ˣ[4ui�[Ǯ\\\�ۙ��$��\0UB��8\�y��\�Ԅ\�[��\�\�Gc\�tE}H\�.�\���tב�W�]\0֓��l?C^:Ns����{\�eA�^�\�\�q�\'�Vk�X\����u\�]sk(=B�\�Xv 5\�@�7g��\�;�>�&w˱����\0�8g#\�HzҮ2�q�\�\r�\�\�=�Π�҄(�K؞�ór1\�vc�\�\�u\�z�}�s垌\�.~�<\��[�[\�ㇺ��\0��5w\�?��\�4�+�`��T�b�ʂ\�g�z��{\�\�\���M��X��x\�P�kA3\�\�oh\�Q�\�\�He�Ѵ�[�^I��|\�v\�c��I\�ӫ\�|(���P�\��EV�aEP;�D�׼+\n\��f��\�h�\�\�=O#�\��\\xgឯ=լ��*dlJ�Kp����ڣ��?^�\�|TԤ��\\p\�\�Ϊ{|�\����E%\�M�cjb!O/k\�\�L\�~Y}�ƑN\�+k\�~�?�t�,��\�\�\�\�̍���ģ�P\�\��쟭\'�\�1�����\�aS�8�\�W�\�{��+MS��6�\�{���q*|�~s\�\��gчH[�ԡ��[N�ωu��P�n\�O�Z�\�\n\�j(\�ƥr�Zp\�7_�#\'\�\��k��O�a��q\r�\���cq67�n=�W\�}j\�\�\"�G\�m�ry6��\n{n ~$z\n\�t\�$T�]�r�7������d�:�\0��x�\�Jx�gf~�p\��\�mcA�F�	�=�\�0Ν�=��<O�x�6\�v3i�R��7\�4�2\0=���\�Z\�o�<!?�lm���u���d�dwnq�\�A\�}=;W=l9\�\�D\��ӕW4ݮ\��\0�t\�[\�Z\�YR\�\�2^y\�\\g�ץ1�$�\��Ɍ���c%s�\nF?\Z\�O\�\�\�\\��\�\�5��qV\�yv\'woN;sֻ\�#ᖃ�\�걛��y��UN3��?3S�˩%egղ��0\�:Q�:O�[%k�6�_��\�E<\�y��݈\�*7t\���\��\�j[�aR\\Xpzv��#�\�5Ӡ�\n\�nD�I\�N\�^��T��\�\�4\�\�$)}v	i�c��C\�~5�p�j�\�\�6\�\�\�\�r�\�\�R�	9I����x�\�Z5���\�N\"R\�O��i��*l\�\�\�c�s�\��\�3�W\�O�ֺ��j�\�w�f�\��\��\�G\'=�^�\��A\��\�\'\�sXipb�z�-���\�Wt�5�wkt�.�]\�a)9Gw��=\�\�{MV\�/�.V[W\�:q\��5��\'�Ԛ�\�\�D�L\�W%Kv\0�3�\�kM\n&�ko�\�O%�}�d�\�8#��d�<�\�^!�\�׺��ok�\�\\�\�7?\�s�\n����\�V՜��	��\�s���s\�uxwH��5��N>�(,G\�\0\��f��\�;��\�-\�Y�?\Z�O�7mA}Qu��&h�w2:��s\�-\��K�\��t�\�,Ր\�}\�G���\�TU9&��I��	\�0�\�vg{{\�oi�_e�\�!I󂫖\�}h\�\�i�Y=��\�\�f�7\�==�8\�_<\�\���l\�$f�\�Fs�~m�$W�[_����ݙ\�\�U�	\�8�W�9\�Ҧ8ɵ&�\Z\�\�\�\n�\Zt\�\�2��G�麆��Z5֙|����~�O~��\�8�Oṵ\�\�\�\�{�m��	�85\�h�\r�\�u+�m��i�\�8dQ������;�`5i\�\��\�\�~�	�qNX��qMj\�\�d�:��8\�\�\�1�\��.\�H`��9�֩\�z֍���J\�\�\�:\�#\0�<�\�\\\�\�\���m\�U�n�2Fq�^�����\�\����|G3L\"R{�?Ě\�ד����9](\�\�M\�{#���y�,$\�\�B��b�iG=\�J\�\�\�7).��x\�ڦ�\�\�5\r>SF\�r�\�p�a�T��_�\��h�/\���j�y\\�\�\�j�\'>\��\�\�k\�1�\��Msv\�4\'�8�`¼�M5J\n�\�Ͽ\�1r\�⥋��\�y��\�\�\��XH@��\�@�;�/\�ǶI�\�\�\�6W�\�\�\n\"���\�q\��s~&�\�\Z���\�5o-�B\\B�\��N�q\�\�\��.�\�\�;u�f\�\�p�[\���dzj�N2����͍�Z|����\�)-W��\�\\6\�ss2An�-#��5�\�;���\�:�-�o�\\�q�W\�\�v�ƾ+�E\�܋%�C\n)\�<�\�P0q����\ZxNѼ)n4\�>{�\�5Q��Y\���n\0\�\�i,UI]\�Z#��I���\�\'\�>��\�\�zMM�5�˔u9�ƹcґn�\rM����\0�\���\�O�\�2�O\�\\\�Q�\�\�\"o��$��\��ǩ�9\�,/�#��Iu	�B�<\��/a��<\�be\n��e�a�\�)N�Z���C�C��\�|�1�\�uم䀪-\�`9\�\0eG\�k�\�<?�.l\�\�\�ǧ�u3rH8#�#�}95s\�\��\�\Zv���Sr�,CoNI\�<�W�\�j\�\�N��\�\"A=\�!<\�u#�q\�\�5\�O	s.]�ڎ����*�o�\���ƻ�{�T����uC�}�Z�h\���]\�xU�v�O2HeLrz\�v�\�X�6�v�\�\n\�$�\\js#\0\0\��u��	�k��\r�ct֪���;��u���oٸ+��\��O,V�;�\�\�Ɠ��դ���+ka\0�\�o\��:�l�]SM��е\'�6��Q�\�N\��\��߉Z\�Z\�i\�k)?uj��X�\�~\'򭿄v\�?\r\�X\�I\�y�\0�\0�V8|��~\�K^�\��O\Z9o\�\�y\�x\�ڂ\�ɬ_\� D\r$q1�\� \��\��\�ڹ\"\�o�\n\�\�Kǘ��\"\�À?\Z�=F\��;\����pa�b>�5\�?��\����\�#�\��\0�ܟ\�+Җ\Z\�\ZP\��2\�uIa+\�*n���Q�}�G@p?*\ZRrI\'�I^\�VV>N\��QE1Q@\n$\�ט|b�ߪ\�\�j�-\�20�c�\0\�שۮ�����s^㛷\��u��s\�#Q\�J���k\�\�%hr��\��,[���l��6c�5�\�\Z\�F���<\�\n��7�P�\�\�ܰ\�q\�?�5\�Zu�\���+��\0�\�Ҿv֞{?^\�:4w�!W\�\��\�\�Xb�%ǡ\�d3X�½~�\�\�\�o\��\�߆s\�\��d��8� .\\ך�#��4�h-�\�\�\��D\�\�׈<A�K�\�K\�V\�\�g撚�#��px��+��?�<H.\�v(\Z)�\'i\�\�\�\�w�\�N>\��(\��a*�\"�׵�K\��nz\�\�k>�\�7z�\�n�\�,2�\�$�\��W/��\0N�\�����\'���Eso#�H\n���~?\n/5W\�\�\Z~�h�z<.\ZiX\�\'\��\'�\0\�<�oC�G�>�\�,T$Bբ�\�\��>�����QԊ\�#\�u��\�CZ^���W\�v<gᥪ\�x�\�\��ya\�9\�,�\�~/������p\���:��W��V\�@<�G$2dg�ƶ�]\�oĺ\\W��zcI�8��\�0\����\�=\Zʝ)G�\�\�9mL^aJ�k٤���\�}�\�3ݑ�\��y�f8�5��\n��ߍĬn��=��浾kϤY^[i�t\���Ԋ4TP8,݆I�\�\Zf��k�K����\�o�����T?\�$�\�t\�M\�\'6��h\��\�\�,ᱳ�b+xTA��\0\�\�Kx�\��\�\��F_�v\�\�+���׉�Yz>�g7�\'�RN\�\�\'��s\\�/�<K\r\�\�y�Y\�w\�\�nO �8<�\�V3Q�\�\�\����YW�\���h�\��\�v�\�Ӽ�ʧo\�D	�9o���\���;�=e�^��#+mo�}X�\0�\'�^3�\�=*m?\�Zd�e2wJ�\��ޜԞ�\�m2�\�.$�y7-A@\�3��9U���\�\��b(eU �\�7\�\��#_-��.\"s�\�\�z��@k��Y\��o^�b8\�؇\�h,\��+�\�Y��\����h�\�;P*O,ǦO�w~.�\�¿�Ӭ\��[�?1�\�q�r��kh�0�5�\�C/oߩ$ߒ<�F�P�\�d��}�pG%\��d��\'�]oµ���M\�\�\'��\�$�\�\�\�\�ש\�Uσ�P��\�/e�1�\nH\��$����됾�P�W��\�\�s\�\�Z& \�\����\�+�)�F�\���.�|\ZRQVg�x\����\r�J͉\'\�\�\���\�~X\�\��\r@�D\�\�\�?\����%��\�/�\�Җ\�L�uiI;\�\�d9�u�\�/j\�󬴫x\�\�\�d,wc�\�Ҷ�Z.��C\�\�e\�i\�uhE�v�\�\�s��\�}\�\�6\0\�m����\�Wy\����C�E39\���<��xߌluk-t��+Kw<k39�Tv�\�]�\0�<_���T\�t\�+_�XY&.\���\\\�\�\��Tj�]ɭL���R�JI\�j\�\��iR�g+�q�}��N���z�W�ޗ??J\�\�\rr�\�\�oz2[�C�\��+\�>\���\�\�\�����\�>�\�\�\�W�q\�]^\�M�\�a��\"S�[ �����S\���t�G�\�ߙ\Z\�P�Q�$�\�y\�3\�Sڼz�c:�\�����ʪ(�\�v��?�vw�#��/�n\'_(���\\?�\rΟ\�\�j�\�Q-|�a��Q��\�t5�\0�\Z\�I\r�Ccʌ\�\�\�^\�=q\�z�\�\�8<s\�xy�y\�.\�\\��QԊ\����\�0ueyI���\�\��\0	`I�h$p�n\��$�Dײ��P�\�\��q�k\��T��_��Ė\�Kc�\�\�}3�+��Qψ��io��\���h\�W�N�\�\\A�ⱘ�֡�l��ſ���\�\�{+R\���\�@�\��s\0\�[H\�om�\�)/u��B\��\n�3�d��7��1\�}OC�\�:̌�*��4E\�q\�=?\�\�#��7{�\��E�\�q\�p\�q\�z\�J\�s~\�JZ#����[�JӒz��\�\�zg��6�{&��\��\�j|��X�\�v\�\�}�+��$�I�e$��$W���_�~�_H��\�L��B\�\�&W*B��\�=�}�+\�|;\�-KK��\�=ƥv�DYb\�I<�$�\0�\�bcKH�\�\Z�O�ƹN�Ҕm�o/+o�\��_kR\'\�e�\�H�G#\� \���\�\�\0k��s�b�L��b\�\�>\�\0�\�z-���\0�>j�����Ť�\\�RX�\�\���\�^_\���|KR.m-�,\���\�\�L\�=H\�T�KV{J�*`\�S��\�,�m�lo��%�\��ۋ\�[Q\�L����\�\�=�\��J�?�6�����O+\�p;n ~�W�\�X�Cg6\��g�2	\��7��]�\�ã�sI��ϸvc\' �.Gz�1�t�iWw#N�7)��Vm�\��\0�;/-\��%\�6�K4��m�g��3�1�\�|�K��6(�b1]]He���Ӏ8�\�|A��n�9TJ�;�\�\0\"��+T:\�l5f�\��̙\�<�=�\�!(N���=���\�eQ�K�N�\'��+�����(\0����-\��!�\0���\0\�\��\0�\�����\0\�A�\0\�k�\�u�\�\"z\�r4N@\�\�{\�h�\�澇I�K\�%��rz�⼳�-\��!�\0���\0\�\�\�\��\0�$?����e�d\�q4���4\�%k��dv�\�\'\�\�r;U{\�/H\�\�_\�V��h\�3\�\�\�_�n\�	��&�|f��\0�?���%�Ւ\�#���)^�\���\Z\�oCp\0�R�I\��)�\�\�M��}��x�+�\0�\�s�\0@8��$�\0�4�\�=\��!�\0���\0\�\�yuF�\�\\1ӄ��\'vz���\�\��FF�~\�U\�L�\�c\�\n��\0�\\�?���\0O�\0[�(�m\�\��/\"�Zͪ�V�;P\���\��3\�S\�:04��\�Ɲ�c�K�\�\��\��=�[j��8{��L{��Rq��>�bjV\���\�/�\�\�(��>\�d�\�Q�\�½�H�J�\'�\��[<�$��7\�!��ʼ9�\\x\�\�\�k��SqrǢ�9\�����+\�\�˫>�+�\n\\��:kw���=w\�zr\�^ӡX\�.e�I)\�nny\��\�.��\�`j6p\�F@�C`�5\�\�\�#V���\�\�\�1\�)���8\�N9Ƕ*��.[��C�\0��\0�����T\�K����Nx�VRw�\�6�Zi��:u�6�\�hMC>��^݋��\�k�G\�\Z0I�\�yg�.[��C�\0��\0���	�J�����\�숢YX��NX�U$�m\�Ɖ\��[�tD\�\�\�s�f\�̏�׊�#��@v��\0,y�\0�\�@�֐�\��\0L���\��0H\'\�\�3\�ו��S��D��>a5\�۩���\��Ė�׈G�\�c�KX\�K���T`\�\"��\r��;+�_�W���4\�w�|\��#�\"H#��tX\�\�TwZ_Z}��\�+�7d$�\�\�ߓ^O�\0�\�����\0\�A�\0\�h�\0�\�s�\0@H� �\0�5\��\0gյ�t>/\�rS\�\�\���R\�\�\�m�����%��;\�M;S	���[\�4q�@H�\�yO�.[��C�\0��\0���-\��!�\0���\0\�\���R\�\�q�SsRw=|J�� �[\�\�\�v�\�U-4\�\'N���ҭm\�n���y_�.[��C�\0��\0���-\��!�\0���\0\�\���R�\0\�:�M)�OW���\�#T\�\�`�\nr�D�⤅m\�-Ž��V\��\0v5۟\�W�\�\��\0�$?���?\�r\��\0\��?�M/�{�\�&\�\�\�\�z\�iT�A\�r�y�.[�\�$?���O�\\�?���\0�\0U�\Z݌}�{��s��yw\�P\\��4@��)\�\�F�bh6a���\�_ʼ\�|e����\0�\0G�.[��E�\0��\0��ymF\�\�t\�0�\�3v=\�+�$X�c\0zqQ�U�) ��Ey\'�.[��C�\0��\0���e��\0�$?��������0�~gy=OU��\�5LKL��`8i?:u����)];N��\�R��J��\\�?���\0�\0G�.[��C�\0��\0���Ω{�}~�\'\';�\�\�q r\�\�\�A���4?\�?�&�f\��\�5\�\�Ҽ\��-\��!�\0���\0\�\��\0�\�����\0\�A�\0\�i\�.�-\�M,l\�_�M\\���Ī�F�F�U�Ǧ*�6ze�\�\�\�\�\�]�-0@�\\⼧�-\��!�\0���\0\�\��\0�\�����\0\�A�\0\�h�Ω����W����,\���p\��\�EAii�\�ʺu�6�q̆%\'\����-\��!�\0���\0\�\��\0�\�����\0\�A�\0\�i���w\�&8�F.*N\����m/텵��7P�\�ʹ\�\�\"�\�=:�e\r�7_-\0\�\�+ɿ\�r\��\0\��?�M�n\���&��}F\�\�R\�MC��\��\�WBе\�b�U\�\�\�\�\0�#\�\���j�LipA\�J#Q�\0\�\�y\'�.[��C�\0��\0���-\��!�\0���\0\�\���]\�uLt\�AS���\�4W�\�\��\0�$?���?\�r\��\0\��?�M_ԫv0��=f��o�\\�?���\0�\0G�.[��C�\0��\0��\�u���\�QE{\� QEnxKAO��\�J�v�f|\�P\�aI\�}�\'%�KdTb\�\�:+\�\"�?�u[�4�\�\r.���A=�A!^�7n�ָ9�{y\�U+$lQ�� \��\�Tӭ	���S�(\��#���\�Ǥ\�\Z��\r#jp����ہ�\�=iu\�e\�\�][�w��Th��c9\�\�(��i>���\�s�\�^�\�<O\�\��x�w�ҙl��\�[$���:�1��x\�P\�	�-c\�|Kso=źNm|��L�~�e\�K]w\�CK\��#=�\�\�\�w4E6(\�\��$��<G�\�Rp�\�\�u\�jV\�VU)�~�\��V��\�?�h�U_P\0�\�sĚ?�t9�=\�y\�\�\��\�w�\�t!H\�\�\�\0{\�����\�ާ�|E=�\�FV��\"�*�R��u\�)��w��I�kT�\�jRD]\�ũ\�2@c�q^v/\�P��m�y�?;\�\�`�I���8>�w���@�\�jzm���Ov�Wiۭ\�ya�\r\�<�Ҳ|I�.�\�+\�.9�e�}�\�@-�9 :׻\n��I=*mj̪\�>j\Z~�\�\�P�K}�\�2Hp�\�ۀy\�z\�|\�\���wok\r��\\3�\�H�}	?�N\'�Ғ�âښks�\��+��_W\Z���\�\�f�8sH��q\��\�;��1\��F\���\�M�����)9*:\0~�]T�\r�\�ׇ�]OF\�\�}> \�m�\��\�\�ұ-�9���	�u���C���rNr>��\��a\�aSqz\�ǥ�c�8\�\'Y\�z�\�_\rG�xg\\՞\�}5Q�0��ݞ�<coC���΁���j\�M\�\�S8�\��\�pA��\�zW�*��\��<\�NL䨮\�\�>\�t�\0\n��i\Z�\�\�\�s\�\r�\�\��\�<c;\�3�X���YX��[��\"�d�f w\�i¬g%�	S�ZE*+\�\'�7��X}<W\"jK\'�[C��\�)a\�$��\�.�\��\�\�]\�w2\�k� \�u9�q�\�r#�Z\�8�m^\�:3G+\�h�\���j�}��\Z\�d����\�%�\��u*{�9\�:f�\�7�\�\�^\�\�\��\�A��R��\�G98\�1\�\�,E7\�N���\�\�]�r5�~\"�C�\�v�\09\�s�:b�OÑ\�>ӵ+��[\��/�х���Ny\�w��\�n\�\�{\�\�種\�G���?�b׵�f{\�h�`\�\�t\�\rV�g���L\��\r7R��\�PU��f\�s\�O_J����/Q�Z\�\'Ez&�\��\n\�\�=Sķ0O<	0E�-\�l���\niZ\�eҴ�VYl�ݦ[�\�b�6�02H\�M�-t��$\�\�\�譏詯��\�I�f�gvS\"�$aI\��V��\��oj�[]�ka\�+�_c`d\�j�Zz���\�\�r�u���~�\�\�\��0�m$��\�*�I��\�]�\�\�ڍ�\�\�\�O.\�R.v���=N	\�{\Z%^�[M\�5NL\�(�B��O�u[=FM\'ė3Y[<\�Ԡ!A#�z1^{N�h\�o�t)\�p�\�\���\Z�Ѵ�\�_\�7�\�\�u�mK�@$dz:\�xOL\�<?�k:^�-��쬊d�f\�\'ר\"�X�mاFI\\\�讏Ğ\Z\�	o4�2j�\��3�P9\�ռ\'m�x�JЍ�\�\�ķeTm���\�9\�\�uj�\ZMu\'\�H\�h�hɢ��\�HI�E�a��A�\�Z�\�?G\�o6�\�N�\ZI��/-\�\0��V-��\�.I$\�\�\�E�EPEP]�¿�(Zw�$�\0\�\rq�\��*?�p�蘭��1?��\�#��ρ�m\�\��\�\"\�\�Y\�E �\�a��Ny�3կ��_^*\�[���)\������/|~&ۋ6�\�\��Ф\�\�\�v{c��\\�\0�\��N�-��E܁�1\�\�XP\�U}\�}�V�4\�v\Z��w�|;�Ιo%\�\�\�$�*]�l��s�oOZ�\��_\n�-ou�L�\��E*�b2# ��\�#�k����7P��r\����Пz\�<W{u��.�\�\��Oq%\�\�\�$m\�\�v$�\0\�Q(N�^\�v��)FI���\0#w\�~\��Ew�\�\�\�\�\ni�FŦD!�$�1�\�\'\�\�I�\�S\�6�\n\�\r.tp��pzuϊ�:j\�0We\�Q�Gv�|.\����l�t\�\�S,k��\�^�\�\\\�Uwz�4�/�\�\�4�^�\0E�k�:᠙�\�Y@?)\�0A\�WW�\��oS��	�_\n�bO_\�[�\�\�+hie(X(^\�dc<�F��\���~x�e\�\�\�5\�^Pt\��\�ƚ��ns��\0�\�J�\0��_�0Wu\�\�]\�^1\�\�մ��i7-�\\\�\\\�\��]�G]/�\�1\��[�(�\�o��Ih�ӛ\��l\�\�\�EENoj�]�\�nF�\�q�v�\�\�KiuE<,Qц\"�o�9�M��zD����\�\�O�-|AԌ$q�6?��f��T�\�׉u�ֻc<n\'\�c\�UVnxt\�\�\�\�<�]�\r�_��{E�\0�\��\0h׺\�\�]N\�O�d�mEX+8A��O\'�x/��xۧ�{E�\0�\�\�\��_Ue$\�4\�\n\�5.ܬ�\�_��l��\�f֙\�}W\�_�Z��	�(����\�s��\�\�Y\��\�x�\�F�o�Dnntۙżd��u#t�>������\0|g���u2I\�\�x)�Þ\��T䬬�b�\�\��Xz\��\ZMI9I�I�y\�	�d��cZӯ4σV\�\�\�[\�5&&9k\0C`\��\�\�_�t����\0C\�j��7\���\�\�%��\�l\�嘀$�\\��\�m\�?\��/�V\�o\�}\�\�\�[�6\�C�\�|%�\�\�g�M�\�~\\2�C[\'�pO�o5\��)��=��A��a\�\�%Si �A�q�9�����4\�\�\�N\�!Y\� \\01\�V~)�\�\��姛K}�9,Gos\��\�	S��S�\�+~�qS\�C>�\0	E\�\�t\�=zU�W�6�4���/:+\�\�����?@3�{և¨\�u\�J\� �Ӧ�\\�8\�{t?��x.�\��g\�ҁ\�D�\�\�0\�+\�$}2?֕Z�\��I\��\�¾�\0�:06��\�\�\�$�\�~�ה���\��\�W\�u�-�Ȋ!\�#Q�\�\�s�\�S\'�51�RR	>\�\�y��\�X�@�+3\�Ѧ��\��.�G�l^E\�?�dd�G��©B�\�ݯ�u�eD[\�ঙ��jn\�>�a�)<]�\0$\����\�o�R�\�\ZW��/�\'H\Z\�u\�\� ~X~�. �:�g�s��\0Bp�\��\���\�K�$t^<�^�\�+\�2\�M��XSO�2\�2!\�2H\�z\�a�.��\�\�Ŕ\��[M(`@a�FG\�M��,��\�!]�fE�b;��\�)ό\�9\�6s��J#\��Wv�`|�\�hlx+\��\0���\ZX_\�ZĖ\�H\�\�.�\n�8�\��N<\��\0N\���\�\�\�7\�M-ZG#\�|\�q���$&\�\�O�`$7(�2��\�˛��_�T�_//r\�\�E\Z\Z@#z\�\���\�5ll�QY�\�}w\�m7U��Y>ά�oev\�\��\�~���\�Fߌ�m��>çenM��G\�\���_iU��rҝ��w2\�N��UF$d�\���f�/\�);6��G}�\�\��/��}\Z\�\�Ϩ\�\�\�#\�\"���H�z¼��sÞ%���\�;�n&kV�������pB\�s�pq\�5\�]��\n\�\�7=�0��\�S���\0��^�\0�g�Z�\�?�G�-�\0_\�MQ��\0�E��\0\�7�-I\��#��\0����&�\��\�\�����^�\�z��\�C׌+0\��XʹL@�g\�J���H��\�.\�/�s�\�\�9{�u�F?\�2k��O���\�K�-�\�b�\�^\�B�>�( zd\�?�4x���Zd��6:�\�v\�g��9���׊|\�S�u�\�\�\�dx\��JF��\0_C�\n\����\0#ܟ�\��5�\�?\�dj|���\�V�\�\��$�\0�h���\�|T�\0\��FR�g\�p�QEz(QE\0QE\0V���\�h:�Z��\"\�DR\�r9Bk>�RJI���I�\�\�\�\�w�\����\�q@$3A\n�z��G\�s\\�s�NI\�撎*!J�P\�9Kp�+�r�\�D�\�\�d��;\��\�bI\�\�\�5�ES�m]lJmlv��R�,p\�{6X\�\"��S�&�n|W�\\x�5�p�ߠ^\n\0\�B\�\�bQY��v�庳{��\�~\"x�W\�屚h\"�a���B4���\\�\�X�&�{\�\�M5E�Ue\�0�<j΢�hӌ\\RV{�ԓwl�ky5��7А\'�U�	��2;���\�I�W\�2+m���\�`\���Bs\\MN�&\�$�(\�$�Ks4�\�#I,�Y\�\�KrI=ɯR��u\���-�B�to*�k&9�\�t�)���\�q�ˡP�(�cԼ#�#[ԼOea-�YN\�f\�*�I=��\�S^��=_D�\���\�;\0����>��(\�\�(\�9%�\�i8�\�;\rz�Mҵ\r6ِ[_�Y\�($�$�\�Mz��\�e�\�\�,�ٚ(\�@%�<�\��y�5�Ek\�\�ڑ\�+\Zrk\��xz-	�>\�\�eP�pb\'=q\�\�\�]\�a}\�	��dB\�#p ���(E&�\�\�\'&\�Z��q�jW\Z�\�)�\���Q��aڟ�귺.���9�\�2v��\�<A\��cT��Ȝym�s;ߩ\�\�\��A�i�X\�4�(�跄!�\�1\�A\�ұ\��\�\n\�FwAej\�\"\"(�\�,{�O\�Y�\�\��N)(��I=٤\�\��xut2\���\�oͿ�s\�\�\�|?�ն��h�\��lׂg�;\�\'�A�MqtR�\�.6\�#RJW=Y��\�(5��!{\'�+�#����ج@\�\�]�\���%�{\�Dե�� H���*_\'�AҼ��\�\�)\�$�E�D\�\�;T��\�T�8�ٲơ���\0�5��4\�\�2k��-\�y,V n\0\�zg�\\�V��%{D�Vo�wJծ�]VJ͕nabP��2A �\������T\�uk�R\�TK��q+\���I�\�\�ұh��8�Д\�/i:�ދ�éY����*\�7A �\���m\�mJ\�\�T��?����7rH�$��c\�C��@�$�=#^�\�\��l�]\�`�z\�\���>���j(��\�KW��ڱ��k�����kv\�\�XC\��@!x\��\�\�-ֽy�\�\�\�2}�͙\�p\�bI\�\�\�\�e��,���W���k�\�س��- [x��\�\"�\��Շ�N�-���.�\�\�Y�,�@\'�AX�R�P�VZ�Gn~+���,\�D�&\�I\�\\�7�|�_^K\�=ľo����9\0W�t����5%(�4_���aEWI�QE�\�',0,'2018-02-19 09:02:38','express_user@%',NULL,'2018-02-19 09:04:08','express_user@%',NULL),(2,'2018-01-17 21:00:00','VODAFONE УКРАИНА ПОЛНОСТЬЮ ПРЕКРАТИЛ СВОЮ РАБОТУ','11 января 2018 года вся Луганская область осталась без мобильной связи.\nVodafone (MTC – Украина) полностью прекратил свою работу на этой территории. Чуть позже работники технического персонала, обслуживающие территорию Луганской области объяснили: произошло это из-за разрыва магистрального оптоволоконного кабеля в одном из населённых пунктов Донецка – Еленовке. \n\nВследствие технических неполадок, люди потеряли канал связи со своими родными и близкими. Однако эта проблема не повлияла на деятельность Первого Коммерческого Центра. Он и дальше работает в штатном режиме. Потому как жизнь, в отличие от мобильной связи, продолжается, а нужды, в том числе и финансовые, никуда не делись, а только растут.\n\nЖители Луганской области все так же нуждаться в пенсии, снятой с украинской банковской карты; денежном переводе от родственников из России или снятии заработанных в интернете денег с электронного кошелька. Люди продолжают совершать покупки в интернете, производить обменные операции и вносить деньги на банковские счета.\n\nПервый Коммерческий Центр поможет Вам в выполнении желаемых финансовых операций. С нами, Вы и дальше будете уверенно и с комфортом распоряжаться своими денежными средствами. Никакие технические неурядицы не повлияют на нашу работу. \n\nОбращайтесь за помощью в любое отделение Первого Коммерческого Центра. Наши представительства есть в большинстве регионов Луганска. Они обеспечат Вам удобство и скорость в работе с финансовыми операциями.\n\nБольше информации по телефонам: \n+38 (099) 606 808 1(вайбер)\n+38 (072) 00-00-111','�\��\�\0JFIF\0\0`\0`\0\0�\�\0�Exif\0\0MM\0*\0\0\0\0�i\0\0\0\0\0\0\0\Z\0\0\0\0\0��\0\0\0\0z\0\0\0,\0\0\0\0UNICODE\0\0C\0R\0E\0A\0T\0O\0R\0:\0 \0g\0d\0-\0j\0p\0e\0g\0 \0v\01\0.\00\0 \0(\0u\0s\0i\0n\0g\0 \0I\0J\0G\0 \0J\0P\0E\0G\0 \0v\06\02\0)\0,\0 \0q\0u\0a\0l\0i\0t\0y\0 \0=\0 \08\02\0\n�\�\0C\0		\n\r\Z\Z $.\' \",#(7),01444\'9=82<.342�\�\0C			\r\r2!!22222222222222222222222222222222222222222222222222��\0P�\"\0�\�\0\0\0\0\0\0\0\0\0\0\0	\n�\�\0�\0\0\0}\0!1AQa\"q2���#B��R\��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz�����������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\������������\�\0\0\0\0\0\0\0\0	\n�\�\0�\0\0w\0!1AQaq\"2�B����	#3R�br\�\n$4\�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz������������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�����������\�\0\0\0?\0�\�,\�\�d\������K�����`�\�\�egz�� 1\��Uf���������\0J�K\�v}������Ғ\�\Z�Z��m�L�;�x�դ�ӵ\0e����2-\\\�\�\�A\�H~U#XY_�xYCC�(vZ\�Y�D\�;Z�\�\�e\�%���R��W��uk�uaq\��0\��ž���\�9h\�n��f=j\�q\�j�OB�d��vֶ��L\�b����	��\�C67+��\0�R�I�5\0�_�̇\�.\"=Q�tl=�\�Բxz\���u\���\�X5}{A>L\���n�N�ԏbyq�z\'�fU)�\�hU\��`yz��i\��[@��\'\�z�\�5ǆ5�~�r.-\�I\�_p\�����\�O\0�\�/OM�\04L~�EAs\��e���+��\�\\Zɹ�#�}j�KE�\�d�\�_\�\��\�I��\�d��c[\�GC�R6���>�r$��Q\�U�\0�H,�\�k�r���sl�[>��኎Oy�\�h��r��\�~�\�~������\�=|�\�\�k�a>U\�צH\��\�\���\�l0\�a8\"�Nx��k\�(\�Ɍ8�zƐ閗߼\�n��[p\��\��f�v\�oM��\�=|�\�є\�wO\�\�ø{?�zjۆ�\�\�=\�8a�T\�5ݩ1\�BdN�p\�~?\�Q�{{��\�S�v1��*���\0\�j�o�\�\�\"y��컈�tޣ���R\�8�#\�G\�*G�XǗy�\��}\�\0\'̲�\�T�7\�ީ=q\�\�\�d$A1�̞��\�ڒM\�\�\�vq\��\�R<�!\�uG���~b�G4HLL&��G\�ڝ\�\�w�\\#m&&�\���3\n\�*J7�\�\Z�,R�1I\�O��� ����F��I�8\�BF\���S\�(<`������H�?OO�������V�f\�a�9\�\�:P8\�\�ҟ��\n7�CI\�\�?�5H�XPH=��:po_Κx\��g�\�h\�\�p}	\��N��(lT\�\�W\0��Bj�8�)��:\�\Zz(�i#�\�y�4\�x��r� \����`���\�uSќ�\�\���\'ސ7�\\\�\�3�g�����\�!<\�p�\��M\'ތ\�J\�4g���);\�p�(\�\�\�rh�z.\��x��p4\�+�r�:\�`\�\�4\�hx4\�\�T`ӃU&+�)�����\�D�܌jU�\�4�\�ӱ.7,�� �@�1�\��Qp)C~JD��\�\�[\��j2\�n�p\�&�cm���oBq�\�V��\�\�\�\"\�;��\��5\"0L��\�<T\�jб\�:\�Bȯ���\�\�~\'\�(�=��[�\�@��g?�E.���\�YJ�`N?Zuݵ��Ϝ(\�c篵dG�\�j\�B\�c\�p\n�BM^\r�&gZ� ҫg\�4\�����\�\�dA\���\�\�ͥ\�`,�G!\�x?�CTm��\�����\�h\�h\��Uv7�t��f�Z\�V}Ѷ�J�r�\�%�dhwV\�%��\����\0\���,\"���Ƨj�\���?�WXuM4o!�!\�u�=*\�:\�\��:��\�ж2��=��ke%��J\n.\�\��\�J\�u��mp唜d�\0�L\�\"�\�^�\�c4\�y\nw\�\�\�NG\�\�xn\��?;M�\\\�8ݐ?���}_Ğ\Z\"9wMm\�G0ތ?\�>�CU\�H��0�M\�\��\�}|K\�\�/i\'N�<\�=~R}Z3�㚎\�\�\��Bu?_Ūگ\�$�s\�\'�\����u\"\��Cğ%�-�^<\�]\�\�O\�Zd���˪x~\��!2\�X\�I\���KM�#�Ž��\03���0kvB\�1�A�8��j&Ҭoϙ�\�\r�|��\\~�\0\n\�\�;�P\�\�Z,W\��d\n\"�䏕�Q\�\�3[Sq\�=b;�\�\�\�DS����\r;[U����\�Z�?�9����>U�_h�q�@�=:a���9��ʔ�\0\�7\���[��W\�\�6����N\n̤0ǭ@m�\�Df~\�9�\0�rp\��4]\����j׿��ZF��\�\\G\�\�8��Z�Amr�\�\�b��F8\�\�շ�\�ӿw*�Ў\�3Ǳ\�*��\�&\�oF\�O��O�D�\���\"�\�컇\�^͎:j\�糘�\�\��\0�\�w[\�!�EC\�y\�\�Qyv�D�\�/�[�}�Zz\�ӯg�27�7%n�(\�7�\�\�(�\�3\�\�\�~]�I\Zx˨|\�\�O_ΙJ;\�b���\0צ�?\�R�;��\'��\�H0v\�FA��ƥwG;n�(�\0\�Q�\�.\�bL�Y\����EU\�\�nW*�sđ����)�s�Gu=jeTs��\��\0\�c\�\�i���F��~UI�8�\"\��=(�\�\��\�R:�P\\N\�4���T�_C\�U�f\�a;㜎ǯ\�@\����\�\�\�r=_�2H\�Њ�GǸ���<zT@��\�?���\�s�Z�ɡ5r\�JS�\�z\Z���\�\�\�\�x~9�r+��V���OJ3�\�)6F\rH##�錓\�\�šԆ�4UaM{\�})s@\�-��<\�!A�\�\'_z\�L�q@4\�\�҃\�\��\�P@<\���w���\�F\r\0��I��l�FG�0\ZS�6$\��w�3q\�担�\�\rQ�\�޴\\,ZMQ\�;n�(}@�?\�W\��O\�\�\�}J\��v�q\�?�\�?\�\�bE|K�z����ũ�ĩy�\�F�F\n%|s\\�\�q]9MB\�1<x�x�i�G �pA\�]t$\�\�w<�\�M4�\�Ii\'�w��	�\�E�T\�y�\�W{c\'�a���\\���::&{�-V\�\'M�W\'㌀H\�{�ƫ\�X\�])D���?����v��\�\�QwL�\��:5է\�W\�緐K�\\�pr�\��C�\�[��Q\�\�\�ꖞt}�1�\�߈5�zqϵ6[\�khT^+4v�T8S�<��Һ�]\�I+�g\�\��XЃ�NVKt1l�5\�!��8��?�\�\\�����P��\�\nNn4��v/$�I�\0iO }@�\�7TC6�p��\�2\\��\�?�����7�\�Z776�\�.x�\�G*~�\Z\�2[\'�g\�N�ݫ���񮕭/�\�\�L��yh�9>��\rG/�\��7~\�\�\�ȃw�:�\0�I\�\�\r8x�Ú����i9\�Ώ�}r0\�\�TS�\"t_�xP[��\�1�V�\�}��j\�J\�_��*z/ā�O�\�c�;\�6CP��\�1ޡ�{7QU\��\�Ki�-ap\�\�pO��Oί\�a�Z/\�|I`��)�B$O�u���\�\�w-�^�\�\��G>��~b��\�\�F\�~˸�ִ#\�\����\�#\��\0��Ŵ\��Sm1�韯�֣���7��\�o\��o���Q�Ug�\��C�荍�\�9B��{\����\�{v{}\�3��\�M\�@y\0����\�Q��8�M�޽��\0>�m��CNe��h#aʑ��M͝\�\�w	�^�\�q�\"�}w�[�Q\�\�O\'��\�\�+\�5�q4�<�=��\�\�K��I��\��\�֭��ݚ��R�a<\�NA������%nb6��\�\��u�Rw\����z��\"�\�@6\�F%��=G\�LHՎ\�IJ��	��\0\�Փowh�\�u�\��P�\�y}�\��Zw؆�~�#v�\�[��?�\�1AIR?����x����\\@�fA4]��Ƙ���\�Y�?tc��\0תL�G��\0\�\�D��s�c\���4\�vLR\�q�\0֩ܣ��1\��\�8�E%D\��� \��j�̜{�H�\\�\�֚\�q������\�C\�>	\��i�.\�\�)?CT�c9D��?{\�QQ�EH鴍\�\�\�\�\�#�\�\�\�W{\�f\�Bg�\�\�{����<q\�I����\���1�\���\0Z�\�I ΤI\�U�Ͽ���(ls\�\�ZƣD�ܺ�\�N\�UW\�8����y��պ\�\�P�Ć�@ ��h�kd\�E���&h���N\�ߥ(\�LB���%\0Z.�E4ޔU\0��ң\0\��\�\�4	�\�4��M�ɧrl.x\�Fi��\�N\�as\�.}sM��(�X\�\�\�~�\�\��tm\�[i�2P�\�I=s]�qsi\�	x��W\�b/\�u���k�\�N��.��,$-\�H\�p�U\�P�H�F!�ԎZ\�\�\�-\'d;I\�ⱼCi/�\n\�����\0\�l<����q\�\�\�\�G�J\�?��+�\�%�4�ъ�\0_\�\\p��\��\�g��F\��+l\\oI� �\��]�:A�c4\Z�O\��F�\�T)r��Y\�y��;g#�\�t�h\�Uh\�;Xqך�;I3�O\�Q�m�eY�2��\�O�krO\�]���	�\�\�\�3\�+Ss�G���\�L]#Q\�\�/�ްl�s��\�A\��9�\��,\�N\�j�{\�OЮ\��� �}0>�ܛ�\�%���T�+\�ߐ�/�u�1kp\�\�\�g?\����Z�\'��}\"e�\�\�ݳ\�lm�\�\�g\r�W��k��\�\���\�\�ϼLp\�\�Z����\�\�ݛ��\0�\�\'٢n��\"�I-��9\�\���3�b�.�\�k�b�$_�:~��A\�5�2h�����-\�N\��n�(�U�\���^\�0\�\�ޫ��I}ګ\�\�\�+QS6�~�\�v\�?~D{\�\�ڵoM��}�L�G����ey-�\\�z\�\��Z\�C\�\�.!k�\�yB��1\� У\���fx\�bD#\�\�os�kKX\\���?N\�\�\�\�4�i��*6��鹚\�\�M\\�d\�*����ޡm\�\�\�̌J��N���;O��b\�6\�?�Bڜ\�=J\�k�\�\�N�\�MÖ\�m\� ]:h�>�s�}���k\�d�֧k��y��?��\0\n�iy>~�u�;�H\��\'\�uKO0t\��\0�M;���U��14\�\�}*\�J�J�\�}G���\�-\�;/\�2t�#~#�?*��ˉ��\�J9�\�5�ѓ�c\�ǘ\��q��է_\�ɫ//��\��m%Y\�\�B��\�v�*��4\�D�m\����\0J�����{\�d�\0�d\�{c�ᚊ[�[\�\�\�aǙ\�G\�t?�*�%�kt�\nγH�x{7_\�\�#Ec�\�R�\���\0�ZKI#S.�p&��/q��s\�Vx\�x	��I�\����>\�;n#1I�\���uuL�\�ف\�~5+$�ܢ\�\�s�\�h�\�u������\0CT�=�A�r���?\�\�Q\�d\�&6�\�c6\'C���q�Q\"8Q\�(�>\�?�Z���\\�\�\�\�ױ� �Q��\�j`���;�\�)�A\�C�T\�T�d\�FF0�=���=��.�>�>��@b\�{�t4�\��z��N\�4Ѐ\�oN\�\�O;����\�!�bpސt>\�x\��U)5��N�\�x��\����A�?_֞�\�\�\�[ƭ�\��;d\�w��NǥJn��c4\�e��})^z\�֗&���\�0isBw{S�49�z\�qXp?��)��9�L��\Zh4�⋊óA4��G\�0\'�\'z\\\�K`\�l����\0\�\�p+�ӯ\����B}G\�\�̰\\+�p8\�]���W�\�\�t<\Z�l[\�];[��wf�\�\�=��\�\��\�85��\�_g�2\�!*�>S\�<V�\�\n�44G\���{�,f���2�xu\��W-\�J\�ӳ=LU>jMJ:\�trb��;/������~�ʱ�jՄ�E�vW�3^�Ex�x:�ʼ\'ٯ\�\�\��a� (k�{��\�\�&�\�J0\�\�A\r\�Tْ۬m�n��H�\��?\�<���x�\�e����:me�#EnW\�i�\�]Z>�\0��\�@`�*{�ßΥMOJ\�\�GpT1\�m\��>���cصI�4|&*��IE;j�c��ޗ�)�\�O-��\�\�\�/\��ߚ�p\� \�\�\�o6;\�(\��q �z\��\��T�xn%\�ō̶����<�:�\0�f\�\�z\�\Zc[\�>\\\�\�B�\�\�\�\���	��>i%{�\�sͪ�\"ݬs\�ڽ���\�RM�aw�Ҧ\�`�F��\�Ps�}\�3�t\��lW�\�!A�O����P�\�2ʨ��\�E\�\�\�\�\�K$�\�\0�\�\�;�6\����s�q���\�U�Exѩ(��~��\�p�׽{�\�ڎ�\�\�.K�\�fpߑ��\�\�b�1\�xn�\�pku�\��\0�k2�\�\�)+�ެ�H\��׭uS�ϣZ�n|\�a��*猴�(2)?{�\�|Ð7`\�5�հ\�\�	�\�s�y\�!���[��7�փe\�\"h�q�k�;鿮��m��\�C���eq\�\�\�#�T\��TӔC�\�-\��O\��{0\�T>F��\�\'�)\�x\�G\��\�\�V㪸\�1�\�U�~�̥���\�\��6��\�׆\�\�� �8�\0�Ï\�\�\�}(}b\�\�0c\n\�9\��.:���U�eݧ]�\�d�\0J���������LnБ�=>�w\��\0�1qi\�k\�c-��ۇ\�n�D2��>\�\���Jd\�<,a\�\�\��6�����\�\�x�I�\�7 \�n\�\����\�\�+^\�\�*G��	*0�\"?\�a\�\�����L��\0�b��9}>\�0<�<\�\Z�F�ح\�F?����hǧ^7\�dk;��\�˟c�8��\�V\�˾�O8q\������em\n��G\�\\A\��\�\�ڣER\�\�\�Q�T~�\0\�Ո\�Gb�X�c~	�;\�r�f\�w	�O\�\��ERd���Bؕ2�:���\�ި�{H���3$\� ����#�\�*8�d�2c\�6\��\rZfN:ّ\�%IK\�z�h\\�\����T��\�2�t�\0\�RH�`̙��Rfn$ day\��7Q��\r\���ǂ>��H\�J\�\�T�E4�W��Gc�JFnx\���\�\�P\�9�\0?�;�0K��0�\�A \�ԁ\�\�;զCV\0\��\0\�OG!�*>��\�t�\0\�P8>�\0\���V�\�%�˫ a���c\�֩�\����&+ב]�}��ԘR\�\���+�;\�fՅR�M\�(4\�!A\�\�K�)���N\�\��Q\�)����\�.F)3\�-1X\r\'z8����\�\�\\<\r�\�\���ޔkÓMY��T�%$\�͋mvHH�Gq��ٵ\�\�e�\�aԌ���ǚt3Io x\�\rsO\rNZ��\�R\�kS\�N\�\']�:���?:j�{\�\��L\�.�\�\�=\�1ַ[jq]96�;�Y|\�8$\�K \�\�\�ՙ�\�\�i(%�ï��:�.Z��e\�}�}R\�KHk$v�\��#H�bF\�\�c۸柳GՇ��w>��A�\0��\�K\�\�\�`��$�aϨ=�d\��\�@n��\�5���\�y?��ZIn�\��k���+�\�\�M�m���8�l�\�`\�t��\���\��\�k�c�T���x�C\�t�c�\�୅�\�	x?B��?L\��]\�w�\�\�ıJ�FE\nG�\�滰\���\�v\�\��\�k其��95\�\��\ZL �\�\��\0�\�ƥ�@\�\0\�	�\ZG\�/\�1O+E.\�\��y���/\�^ޔQ�1^Q�aYW�\�\�u$e3�\�dt�\�Y@\�3�f�dԢfh�`+\��ˑ��+��n\�<�v�a}\�]�N�o)�l�(\�F#�E!����/:?\�\n��lm\�Mo9I\�T9\��U?\�[\����ڣ\'�B�5~�zR�!.^�Z��\�٧_�\�\�H{��U\�t\�fF?��H�:�§�\��\�\�\�\�z}O���U�b$T��W=Zу�wG��ɫb#\�Qr_n�\0q�f��\\jr[HG�\�R���\�S\�i������\�c\'%P��^A>ܚ\�\�\�c$�Bj�\�̶\�\���	�?P851\�+\�o\�\Zbx~�`\�	&\�ɕ_V\�u0\��SZ\\t76�>����T֚F����𮬷�\�eべ#\�\�n�L\Z��@�o�\��\� \�Όa���\0\�RâZ]\�.|9���r!v\� >��\�\�]I\�v_��S�\�i\��ƣ�j.c\�t\�au��E\�ў��>�iw����˨�캐�p��<��\nй\�ob\"\�\��R\�.0�\�\�.=C�\�V�J���\\xwT1\�9L\�\\�\���x�\�\n.\�C5�\�bTk9�y*O���\�0&E��=98�a�\0׫�w2��5\�4�yȻ\�{\Z�G\\ˣމGV��\�B��\�U\�Z��(\�V͔\�)甇�ƣ�\r\�/ 1�\�z�\�*ԯi;��kY�y��A>\�\��\Z\�-�\�)g\�p;���Q�?�Rd5\�\�d�c\�+q�rG�^RG�Ϊ\�T\�\�	�6��y�\�8ϰn��\�R���kD̤�GˏL\�u>�iw\n	���:pÏ^�\0�p��\�\�^�U\�ʸ�l\�\�}/\ra\�Wu!U&��9]KC�\�\�DЎ��2�G�V9�\��/}k�\�Ft��\�\�\�=�\�:\�9pQ�:�5���E�n�;=˩\��\n4�j��\'$g9\�^����:�Q\��\0�R��w=?���\�\�x�\0m9\�+�H�\Z�\���\�\��iC\�)FN\0=\� �\0�!?N�����\�\�Մ�0\0\�>���)Cb��V�2�e\�8\�9���c�����8j\�D\�e�4�\�0#��w vz�3M���XPx�\�\�\�\�;�Á4���њw\0\�#�㊶-�\\�\�T�X��ּ�\�\�Ճ��7vi��\���\�\�\�\�\�4��B�{sIޓ*:#��̠\�4D�0\��m�:W)\�vj~Y\�\�G㌏\�]ex\�\��Կs��\�0�vv\��R\�N2s�\�G�*�3�U��Cg�\�:M_6\�IawF\�H��?Z`\�Fy�?�e�=�\��\�\�+���a+u�\�x9\�;�\�\�C�\�L\�b���IÕQ\�8?2�ʼ�i\�\�!�\�Ĝ{�\�5��V\�I�[�a�7_,N�	\�\���k\�\�/u�m{�?yE�H<�V\�H\�6\���\0J\�I\�\'�s^�3\�)\"`r\�5Ҋ\�\�\��K���3C�\�>��4�Z+��Q��HcR\�ھ��.�uH.m��\n\�+�Ǒ�ֽ\�N�\���\��q\�k�����b�;9��\�W�����ό\�,S�E��ȮX�z��\�?���.\\�\0����]2\�\�ޤX�s�>�u�\�\0\0P0�`\�V\�\���\�S��r\�^�\�TWKo6��A\�=�\�#1#�\�2{\n�m\�u:��\'� �����=Ð	�\0d�z��\�\�:�3 \�R0���\�ԯ���>iB@�}i�}�گ#��9?J��a\�N<\�Wg\���\���쨶�ݕ�f΋oq,\�\�\�nL\�s�lr+B\�F\�n&\�\�\����)>��\�\��U\�\�*�P��=��6p^D\"�6\�+\�j\��\�s\�D{52$��\�Z\�� 7z���\�.�h>�ʻ��|�\�\�5M�\�5V\'}6蟺\�)�cڭ_L\�wh��#�`�8\���5^[�6�\�Z��Z\\�#�\�#�\�]*]W\��G\�\��^������B\�4\�lq\��\0�q\�z��b\�/\�5���\�\�\�H~\\�7oƮ\�gb�v�|&�����\��j���7Lb\�l\r���.}֩?\��#gk�D\�6\�y0$q\�\��\�V��\�/��ߵ�\��񑶓\�C��1Z^ڡ�K�[�;ǜ�I&�v\�/`k;��b�}\�j��\0��J:\\\�Ha\�4\�^�h�\\�S���k\n\�|�z�\�O\�,��O+U\��\0盍\�c\�\\\�\���q�OOJ\�-=�\'�+\'\�\�Q�\�����y\��Z�(�\�\��\0\\\������#\�8SJ��DW\�:u\�q\�\�\�q��\��+��R֓�����\�pk��\"\�\�aʑ�Ș\�\�\�\���E�1qD\�U \�b~\��4�v6H11\�9�\�\�IB&�\��\�\���\�?�z\nG\�J$Ev����\�~��Wh\����\0\n�(@&6=U��WcG�OPySV�f\�DF0ǦO����9Ϧ9��ԅv�=�*\�A\0�z\��5I��\�nq\�\Zz�:i\�>���\0^����?©6�D�r\�LzG�J\0a�9�� �\�\�\�=q\�[B�ٙ\�	\�\'�\�3H���\�\�S�\�\�z�錓WFN-nf���Q\"�\�\0�I�Q\�֋�\�\�x<zU�Ҝ\�\�u5\�F6G���\��҃��hj��\�On�ݑy,��\��TG�\�V�̛��9\�{\�W�����b0~�7\�Ʃ~\�?;4ɼ�F\�S�W�3\�wDf�\�hv�#��@�O6\�s���\�ּ�|vg\��]gM�1�iiZ�y�\�x�\�ߢ�+ֈHIh\���wS�\�s֙w�]\�V-=�\��N�	\�s\�\���)(����Ya\�\�MLj�7.�S,*б�@\0\�\�8���v��c$ĒKu$�\'�D5\�ƚ��WC�Y���Y\�\�|�1�\�\'\��\0\\ֵAa��\�ǌc�<�\0Z�\�ח5F\�\�2\�>\�	y\0�PiA����O@\�\��\n��=3�\��\�r=9\�[^\"��P1ʧA\�Yˀ;׵��-4~g�U�ع��N\�\�9m�\�K�4�h����P\�C�{8bmP׽O^]y�\�l�����XC���bY��I䴑LS\��;�\�?ʴ�\�\�A$�A\\\�n�ͮ�rۜo\�d�}qZaay�pg��\n<�\�Ξ�=ks\�qf\�I��a\�/\r�,\�\��W��v�\�|�Q,l/\�\�@R�@���8�-�ٙ����\�F��|z�G\�\�C>�*\"ū\��\�\�H��q\�?���\�\�*9F#�Ǌ�\'\�\�U�\�R�\"9x���\�\�?�!]�g̬\�u���a*\��\�K_+�c����\�&�\�\'�A\'��ߡ�\�-\�\�)�R�[��d`~\\*\�o\�*M��\�!\�\�\��v�\�YD�u��^�\0����\�\�x\�6W��Ȩ�����M�0J9ă����\�us�z��\�\�8�r?Uno�\�\�3[:\�z� `�ʬi-pN\�Ͱ\���@z�\�\�Q�4���\03:Q��T\�\�\�o\�-m�y<\�\Z�\�7_%\�	��5�x)\���\�^s]L\�5�\�R��\��_Ӛ\�\�t)V\�\�\�(\�0�};\�R�\�s��\��\�\�E\�\�W\�\n�Nm�>�\'��c\r�ֻ\�s�;o�\�\�T\��w𶕦��̓o6\�H�\� 0k��A$X\\�\�o0?�\��\�_�\�$�)\�\�q�\�\�9㯭cjf;\����7\'�բܧ\�a\�~5\'�E�B�\���*\�d\�ѵ�\��\0\�\���2h�Uxđ��,x?˃Z\�cv���Z\�B\�u?2�\0*�\Z\�\\��sm!?43c\�=\�\�L�FS\nY~B� \�z�h\�mC��g\'��sĩ&ۨZ\�n�Di�\�_\�5�8P\�(�>\�G\���j�%����H!�\0�o\��\r!]��6�-\�?CS.�F.>\�\��hT\�w�����L�O\�\�?\�n����9\�Gc�Cަ!B��\��\�\�j���6�ttH�\���)(.i; ���y�RW}���\�\�1\�\�?��#�jj\Z=ŀV}�\�N�\�OZ\� \�\�ߌ\�w��H\�]=���BnMtcCc�\0�SG)B0x�58\�\�=GO������#&�G;�{�\���j#�����\�s�7\"��Y=��\�ĔR�eN}�+k�3�r��zy\�7\�#ў��\�GZq\�AmbKf)s\�\"�j\�\��H��~����11I�\rm\�Kkg8pR��nG�5����Ϊ1\�5٦b�\�k�K\�\��󒄡�G�$u��\��n�����C��\�Xc#zm�=��\��wM�\�M�W�~����&�����c�:���h���Ӯ1��nQ\��\�mE�Q4pf4\�L,\�\�C���큁�>\�#5\�Q���~f�=kSA�\�\�c8\�o\�q�׳7h�~m��\�V�V�\�6`��R\n3\����갊�R]\0ҧ,8\�\�Rw���S�H69��8.i$e��\�\�J}�8���R9bı\��4��\�B=��3U�$�\���{�T\'��J�\��`�����ؘ�\���[\�\�\�{Ҏ��\��J*\�\�7rG�O\�\�)\�>�\0Ҳ\�\�mo!\"\�P�y¶G\�y�+�\�F$Fd \�\�n*��\�\�\�7�ʹ��W]&\�WG\�\�\�\�Wq{%c����\�s\�U�����\�E�g$�p·o����~w��\��C\��r\�F�GYN=�z�j\�\�S妪a*���mS;�fI\�Yc9F�\��{P0\�}�F�܇�{\Z\�H�\"���}��\��*\�#���\�h\�\Z�/\�\�R�\��\�C!�B}�V;\�$K�P-�89!�!��54�iXy�i�mn�(Oui;yz��C\'����y�\rJ\�b�\��\�$kn2d\�\�+oV����+=�c\'E\�!\'\�\��\�r�k\�\�\�Nf~��\�\�[�e\�2ɵňg\�^\\�\n\�R4�5�5\n�0\0\�Y�\r��\�Ñ�%9\'۷���>�Ō�\�>[菲\���8|:�%\�K_D-\0�r84\ZJ\�>�\�״�\�om\�ި}G��{6ݧڟ�d����!�*F=�\�M�1)䪌}3]�\�ME\�\"�_>2U���\�y�P�H���\��1\�U��U�їI��ִ\�\�ZH7�;`}֭�\�^3\�\�X�t�\�3Ϥީ\�1 g\�\�����oFx�O\�ɑ�\�\\n��\�\�\��%�=\�\�|\��\��+�@ڭ�^B~\�ݹ>�����Wn�\�V 5[8uk`�,\�Āz���U+d�\�-�joo#u��!sퟺߎ+�;\�|���У�mӮR\��\�q�@�5[8�y4۞�d\�F�\�o���W���T\�\�\�|�5�\�|t��8����\�mG�\�}~��sT��YB\���\�[�\�r�\�ʟ~8?��Sʠ2n>W\0�?Jٶ�e\�o|�\'泼\�\�>����85�{��\Z��ap�\' U�g+�)(,@瓊\�\�\��=�1c�Qǹ\�\�	n���z���Dq�`:�\�\�\�\�#\�R�s�G�Iz\�oқ��g�\�\�\�	a�-\��\�]������\�\�+�8\�5\��\����\�\����\��3���|e��V)��d�׌0���¼g�q\�}{��Sy\�>r?��a�=\�2���Ď����\�#\�%L�v\�\�����8�\nd0Gr���p	\�=x?��թ#7�+��/�$$�z���	\�jPMk\Z���5�I\�4ӏSM�W2:$\��\Zx4\�~ZF�)�\"��\�\�߼sxeA\'z�ev\��b�Vu�\�4\�ڣߵaV7i�gn�Q�_Td[~\Z�f�S?\����\0J\�wL�\������\�\�4\�ǚ\ry\Z`j�,D\'٣�\�\�JWk0�4���\��b.\�\�;Ѱ8h\�GO\�1�h��ӳ�5\"�\�\�$R�2�\�$\Z\��\�X�L\�\�(?^�f\���Z��\\��<�\0Z\�t(��)wb\�\��W���\�o\��,�\�a\��\���(��\��5KV�ƕ>\�7��y8\�߽\\\�X��\�䘰\�r+|<oQNuUS\�K\�C���\r�\0\���s�\����\��\��\0�\�{�\�z�\�\�ߡ�9\\o��\�t�s��C�͊�3<r.!io%O#�Uɰ�8p�\�#�ff�d4G]� �\Z�\��ۈY�K�����\��#�ּ�\�|v71\r\�wM�Н\���s:\�s�1� �\09A�>�\\WG4V��5Ž\�F\n��Lk��S3Nŋ0;��OS]xf\�\�<,u�yL�ث�A\�k���\�Z|W�$a�yW:\�Qể$R\�O*A\���\�C�\�up\�\'\�by\�Z|ͱAVeeW*H�e\���R��3\�^L]�Ͻ�88��\��G5\�\��ī)Eݝ�<�\�g<~��m	�\�8�V`?3V5n59\�,B9Q�\�\�qV<?�V2FB\��\��׸�%+���7[�\�\��:\�P�xUG\�Rќ�ik\�n\�S�AB*+d%�($(b\�U@\�$\���a���r�ܣ�\�\�\'鞵Jj\�\�JP��o�1��΁P\�\�G��q\�*�Էj�5->;�T��r�꿆ھ�\�ZƞSa\\Yj!\�C\�2\�9\�:���\�fV\��J��~d[\�-�ͦ\�o90\���C��c��U�Csbc\�K~	� �\03Xs\�Ks;\�!%ݲO�u\�F����3JbGA\�]ե\nP�ߡ�nx\���H�[�\�t1\�w�\����Ӿ?\n�<\�\\\�ĺm\�P\�I���9\�t�,)q�TW_\�:\�\\T�N������웢\�k��1�Y�a�D���.m\��G��z)n1\��Kpq\�>��kc+4\�X\��rF�J3d��\0�\�N�\r\�nZ\�6\�[$��>\�ڻ�8�d�>R��!\�\�\�ߡF\��?a�\0x:\�\�\�ָ1���\��\�\'\�o��\\v\��NV�O���MZO$fP��)8\���s�<\�p��\�\�m\�iw\�I����\�˺�%c�G\�OҪOn̞t�\�c�ݙ\�\�\�[ъ�G��be�\�\�\�mu�L\�\�\�\�\��?>��4��\0�\�\��\�\�\������i[\�\�q�ȿQP�Ġ�0��g\'>��n�y�7تSq\��#�\�\��\�e\��\�V���#��Cޣ+��r\�v<8��jF2�U+��z�G��i0s\��z�`�f$Xu a�\�w�$corG\�:��#	B\�c\�Z=i\�s\�&jHrVv�)ҝ�\Z�\�\����@e�\�E\�vŴ\�\�W+Úzj:�i\�٢\�2	\�?�Ղ+��{2E�$PI\� \��Ea���7%�ۗRUk�\�c�\�`�.�q��#(\�x�\�Hl�5�\��Dz��)ȕC\�\�<}E`��r禥\�\n�=�iG�=\n	<\�xe��`�8\���+;C�\�\�\��Fd?\��օxu�\�6�\�p}�\Z\��{\�\�Y��3\�8��E \�#_\�q�+���ɵ�.�P�����\�e�8\�Y?N�փ�k��NjqG��\�{<mj�ݫ|���\�\�\\�� \0�z\�+�L\'�\0�]�\�	\�k�DsK�\�\'�<\�\�*\�<:\r\�\�F\�d\�v8�@�\�%����\���яD\�9�[~a���5�Mmxm�|\�\���\�\��l�ܲ��p�:�;QK^!�a�.�1���C�ɡ]\�2ؑ\�\�r?*{#�R�2 ���ng�\�\"\�\�\�+��ml��y�\�&̍Z\�=��]�\0\�	?J�Kf���4�G\�0�\�#�OcW��i\��4I\\�oC\�Rk�w�-\�J�\��\�9>�\�|_-��>zi\�:���ʑ�#\�\�φܦ����?ҲYO�\�\�\Z\�\�	M^\�$~b�j\�M�#�\'T$��\�\�;өSK^	��C�\�\�f�?x���5w\�\���p�\�?�\0UC\�UƤ�\�tj������oR��i\����G\�\�ZK�_���7�\0�r3��\nR\�\0\�OA^RWg\�\�J1r}}\��\�Ǝc�\�\��=+�Wd`A �\�:\�f\�\0h�Y�\�\�Q\\�����jRąS�=@�_�\�S�l\�r�U\�oS�\��\�R�:>\�v5�\\V�w�MF7\'N\��\Z\��\"��t�\'u�>� \�<N�oގ��T�\���?\\�}말�2��(�\�q�.�\�(��\�\\F�\����bo# c���vp�K�\���\�}�T�|�w6}5\��$�5\�xb���P�\�\��WBy�\�\��\�\�t�\�\�\�]G��\��QE\0W�b�\����\�\�U�Uߤ])\�\��sV��L�_�fNkZm�\�m8�,▖g7�i&�6�J\�\�\nA\�Z\�\�b}k\�?�\�p=���Ҷ\�͹\�\�; \�BUR+YnQ�\�oeK]IRF#}�\��ۜ����T�8�\�\r43\�7G��\�oè��;�!�3\�\�% p1\��\�QY��+\�/�Z^��\����5��C\�L\�Wn[6\�\"�ܲ	���X�\�\�\0�rʫ��L��h\�\���,��\�Z�D0O��}.\��2Z&��~tO	`&��Y\09�M�=\��jӘ�y�PJ6)��\0,\��dCޡd\�v\\��g/>��k:�\�yz�\�_�e���9�\�F�M��\�\�\�)�\�\���jԉq\�Pd\�\�#��CޘT�����N�>Y\��e7}\rD˒\���G\�։�\�|�\�4Δ�N\�\�\"�;\�\Z\�v�s�=3W\�pcWn	Q\�T\��2�N\0\�\�\��Ӫ\�%\�ѹ��c�\'�����\�]\�\�\�\r	M8E]���O)�\�Hc\�g�r7�Ɨ��\0=Ev\��Cl��1<\�H\��1�:/\�\�ʩ�\�Z}.A㸃}�\�|�\�tgx��;��\�\�#\����\�c�YNA=\�כQ�|\�R\�<\����\�u�t7�Ї�?ҷ�Z\��97���\'\"?\�~��\�\�\�F\�/\��N�σQ{�ai3\�G4W!\�##�\��\0?����I6\�&�KI@\�\� �DB\"p�\�##h\��\�2��nylg�8\�\��\0֩\��\�p\�?:k!`Av �|\�\�\�3[\�mYv>_1JUf\�UcϏ��>\�U2p?\�?ҳeB��\n�\rX\�$��+rN�\��^\�E\�\�|~~\��5��\�\�(\�\��\�^��]\���0�\0\�\��b\�7,\�R2\�\�\�#\�\�ĉ\�B\�:�8���\�\�n\�d��\�X�\�F\��=\�O\�B\�9>�\�簓�\�\�\Z�u�X\�$/0X\���=I��\0Z��@_FDl���@�\��ӬSc\"B\��\�&G��2*\�J�=�\'\0��1 �}y����6��V*\n4yWK��	���\\щ:���tA\�\�\�=\�hh)�W��J�\�~Ǒ�nX�%\�~geܚ9��u���]ls>\'\�{\�?��~\Z���\��U�?7p�e�MZ�\��x\�=�\n���쏊\�%���_\�m�ɘ_(Χ\0�2H\�<\ndĈX���	-�c�>>�\�Bo\�Ȼ{x�/nΒ��$c\��\0\�;�[���8�+N\�役�K7P�%Tu��\�2\���F�\�\��l���E\�]���1�+���ϰ����A��\��W�[�	>»	�i�dr0{t?֖1{��=.\Z�\�p\�#D�W��j�C\�V�u\����\\cV��տ�a�~�=>(_���7�=\�0�A\�\�\�\��کh\�F�$�\�\�\�>`\�\\�\�u�=��\n��(\�Z�ztJYw\��\0u3��S\�L\�5h�ѕ	0\�Aՙ\��f#7����`l^��m3\�ܤ��:� \�m<4%OmO�\�gx�x\�\�\�\'ky\�j�\�f\�\�\��F��,\�$���(o�{RL3m8��\�\�kϊq�O�>���\\4��i�F7�O\�nG�J�n\�-�_\�i<\�\�J�� \�]@�9�\��~痐V��	\�t#\�\�r2V#�\rq�_]\�$n\�\�!Tm�Wi�\�=��s�_\�9D2>���kl\�!)5�ß\�u�4i\'��ij- X\�2\�\�\�\�>�=Z2	\r�\���\�b:\Z�H8\�pk�\�K�\�\��\�\�\��$c\�o\�On$\0�qhpG�)�^�ߨ��&_\�Z�Y�X:Km�\�\�G�$ut�\0�\�[�\�Y\�OQ�5\r�W�\�9�Z�&!\�\�\�{\nTJ<��f�&\�mq\�\���*�\�yYda�\0,nG�?��һ��*\�#�28�\�\�P�2,J9�\�\�\�x�Eo����\�I�aW\rR��d�\�\�,\\ҀA\�I�pB\�\02OA]z#\�wl�\�#<\�8\�\�+\�\�\�\�E��\��\'�9\�\\v�m,&��W\�#\\�?WH\�!�Q\�,��fH�\�k\�\��ͥt��\�=��Tvo�8\�ie\�$EC�\�5\��n\�\�\�9\�\�Ȭ\���}N��ajıĘ\�=\ri\�\�K�md�nX\�c\�=:\n+�J�PJ\�0��)ʥ\��\�\�t�*��*�@��\�\�\��\�qU�4\�L\���dH\nH\�Б[\�\�#�Qԟ1b\�o\"��\��λӍ\�t��:\0{\�w�K\�i\�99%@\'\�\\�\�\�}W�\�I�W,���Pk\�>\�	擽(�t��\n3���\�)>T\�3�H;z\�\�L~��A����\0\�\�v\�1>\�\��\�\�0�\0t\����.i7\�㵸\r��:�F\�n8#?֨�m�OC�En��I\�2�89\�s�J\��\�{T�\�M3\�1t�5\�z	�\�te���3A�\�\�I\�?\�\�+L�+ƭY�~��\�U�К\��\�V��DA\�3\�C��O�\�>㚞\�!N{�p?.j<�\�\�����\�o�X�\�\���?�oy\��\�	\�x\�\�].\�Fu=�\�?<\�\'\�)DrE�Ft�M�\0�ծ7Q#䱒Q�\�9,d\�\�a\�q,�p���?��\�}Mv�E�\�,���g���\�\�O��]\�<�\�\�ԭ�ue\�h\�\'z;׋c��3\�PE�\�C�\���k\�-�{��e?��\�>\�fp9� *\�\���1��o\��\0�\�֜m����8Z��\�v\�\�w����\0@��\�\�O���c�$��`�ny\�8\�\�^d\ZRM�k���	\�nї�[H��P\�/$0\��\�\�Uk�\�q)`��\�#��k���d�\���?���\�VB\�\�rO\�Wt%\�;�R\n0�ͅ���Ç:t�\�L�c�\0�\\�k��\��\0�t�\�@?!�\0׭�_\�e\�-}z6\�\��q�\�Ʊs�\��\�c�\�5�F�q\�H��ˁ~��=\�&W�\�t�`ۥ[�\�����s�y��a�\0�U���MM9Ŭ�\�#o\�k�z\�k\��p\�\��Ů��v�宦�\�*��>��AA�+umDv�4�\�Wa�@َ?>��r6ܸ3�^�&���\��|ϩ\�hy\�R�rcr��\�Z\�a��\���`�^B\�`�V��?�t\0nx���q	F�>\�*�\�\�\��G\0x��k���[\�ި?�p�ć\�]ś�@p	�\�_�t\�U\����\�^_���\�\�\�q���4a�\�܎�\�I\�\��\�\�>0\�I��\0ת^(\�V��/\�kWDM�L^\�֢v�4�7º�sy)��\�\�E�2j˕���\�v\0{�\��J\�ѵ�/gk{��\�&6\0�q\\��)A\�l�n�gB�\"8y�_\�ر��\�\�.�|\�\�\�x?\�~U�\��\0\�B���\�?6)\"\�J�c\�\�]cf?��\��\0֮��G�\�\�U<ʕUկ�\�\�Es\�\�\����*J���\�\�\Z^�\n\�\�zk���9^\�}\�7��\�#2I;0I c��\�����\�hU�\�J�\�r;W�R���a�]D層HAǦ*�\�\�\" ܓɩg�G��OA\�U-�F\�\�DH\�ا�>�\�\\�\�՝�j\�\�\r�Ÿ�\�m4jG�`БM��YTr\�p�\�֘�׳�\�\�c<Ζ\�\�[y\�\n�tdbT�\�s�\�9�9\�\�\�\�;E\�0��\��	\�j�ʲ\�K`3\�j��\�\�u\�H�9*94Sm�X��(\�\�a]����4ͤ�/�k�^�\�xZlI4$�@#��U�.<ԟ�\�d5��6\�\�\nZ-x�\�aHih�1N@\�▨\�7�h�\�\nI\�\� ���]8�J\�\�\�\�Tp�-`\�}S?���b\0*��X�\�X\�\\:�R4<�ZF\�\�Yo���\�t{	^\�ȼe;]\\�_Tm1��ߐP	$u\�{W+ry^i7Hwc�>��4f9YOc�W���,y[<,uER�:V\�mxn\�˹x\�8\�����WN3�\�-�6�\�:��w�H&�$S��\�&:���\�}g\��\�\r\�^���w\�\'zP9�<�\�\�^2D\�E}��A$\�#�-R\�\���IgFG\'0~Q\��]��o��$t$Et\�\�M�^\�\�\�Y\"Ŵ\�._+Ώ�;ȷV59U#\���sA\��rh\�EjΫ�;�캞\n�,5oviG\�ZJL�p2zt\�d�\�\�RJo\�\�5Fc�\\�1�\�\�\�\�\rɳT�B?L�\0J�_�\�\�e`\0\0�\��O�{�$<pO\�<׶ҝ+.\�\�\�X\�7\�\��;�ԣ�;Ұ\�I^&\�\�:J>��	�R�U\�I\�>�Nk[b&��\�\�G\0�:{�[ʑ\���,\�z\����s\�\�\�\�2a�`�6\��5݇���|eMє�\�tWa�&\�!vr���Ȫ\�`=N+���\���Ì@ש�Mm����r�j��%\�\�\�M\\���\�\��)�u�r�!\�W��\��\0\�Es`�7\�{<J�\�\��\�\��\�\�FG\�j\�(de\'\0�\�\���\�\�\�\�+U~��\�5��?S\�\�\�\�0����dُޫ�a\�@=�\�^\r�-�r�QRmrcP��#��寿\�\�a^�\��G�V5<2O\�f\�\�+��|\��\��`f�oA��\�\'\�_\\�\�\��\0\�>��\��-z��J\�S]��Νm�\�6\�v\�J;#��\�M9\�-�\����\�kMoi����\�o�\�m[��?\"Ƶ���n1��?Z\��Gݴ�\0�:\�\�?\�m�\�\�U_�<N\�R\�\���D\�\�\��&f?�\���\�-\�h.c�NXG�uz\�$\�ۆ\�Jc�99�r6���\�\�\�\�H��\�K\�]�o�\�a\���\�W;i�\�\���\�kiRy\�M��Nҧ�$,U	�g�\�=��\�Ir\�p�g�������捡\�\�ܞ\�V\��\��\��I:�\�z������\�\"l~#ֹ�\�cգL�&P�#�֢�:n]QӘfS\�\�\�R[=\�\\\�S┦@\�q�Σj@q^\�W?4�kTh\���)\�\0�\�\�b\� ���\�Ҕ�\Zy\0�a\�U�\�*\�EG�]Qn#�jA�\�lENkT�9�\�Y\�)\�Alg\�+B=%?\���ZWp��\01\\��\�y�l�ZN�X�].�\Z\�\�E�R)�9\�.�)\�8ʜ{w�Z�\�W]\�\�Ч\�]�fp5��M\�j�I\�?³H\�^1R\�Hb�����\�[MsE�,5GJ�f�4z\��EF�r+uS\�W�%gc��sS��\�-&z\�n�\�5%�X��5\�\�8�nc��\�\�[C�<UxK�g@��;\�A9�ϵtP�#l�\�Ӿ�\��SM\�1�p���S΄�Eԟ�c�5�\�[}��Uo\�\�/�:�\�A���e�Y���Z�|�\0\�\�\�c=4�\�\�\�\�$QY�����\\D���6��WP�`%Ĩ=�~D\n\�\�E̥�w\�b}�Z֍Y����B����9\0x\�u^�@\�\�~e\�g��\0?Ұ�=2}&\�ȟ�`\�:T�Okr�!\�O#\�w\�V��M�y\�~.X,J��k\��\nj+{���f��\�\'\�ԣּ9E\�ٟ�ѫ\Z�S��a\�\�)h�4\nA@��b\�Fܶ8=���3OfX��$�\n�������\�ǁ��+zT\�އ��c\nJ=Y��I��`��ǟƹ�yϥi\\�4O��\�g$�\�e\��\�G�	�bf�U\�+\\\�\�6M��@	�\�Tⰼ1s\�Z\�lO\�޿C��q[���1\�\��VS�X�$\'}mg��g�\�f�o�\�\�Yb\n�\n���$�\�]?zp$ \�\�\��N���g�Ggf��օ�3ʷS!�*�Ƿ\�]!$�M�rN\Z�MjΫ�6˲\�`�rEݽؽ���Ƭ\�\����5\���cS�m��\'\����拞|�q��\��\�xj]�\�<H�~#�\�*qq�K�4\�\��5\�\�-I@�|\0�l�O\�\\�\�\�r��8�\�R�\�Ӥx�&<d�n�ı,\�ORrk��n|\�{K\�bz=M\���.d\��g�\0�]\Z}���\�\�m~ͦ�a��\�9�\��?�h>�ǉ��V\��5K-\�\�\�q7��\�ǣ�\�]V�Ηo\��k�\�\�\�J\�v7�I\�I��\�k��$x��\�_\�\��@\��������6\�&\���\"����<\�O�o���\�M�H�g�YM_�\�;�s5\��\�x\� %7~���q�\�\�l�\�vZ\�\�\�\0	�\rr#d\�zc�WF\r�\�ǉ\�kߡ\��y�h\���H��4\�ѷĺ{x/�\"�\�G�%��\�Q\�\�F�\�5\�1��\0\�f��\0��^G�\��R}\Z�\��6\����\��\�٩\�7a �u\�^.\�+��\0L��g�Wg����\��q׭i�֛G?\�b�\�\�撜i���H�\�Z���#�E\0`\�1�;\Z�67.\�P{�֣fl�\�~��\�N�*\�R\'\�V�W�,��J]�\�\�.�B�ZH\�7|\�\0��{�Z\��\0�\��\�t\�\n\��\0b[���6DI����ŉ^\��=��ס�V�\\����N]G\�Ơ�C\�\�ra�8�\�3�⶞[\�ᢈ�i~�\�w�C��3�`��EZk�\�zoE\�\�,\�>\��\�@<:HT\�ЀH4\�n\�\�R\�y4`e�0\�\�3Ir鰯\�\�9\��VTa/5\0$\'\��rq��?\��/6\�-�\�Jtڍ�ߙ�\�T�\�N�\Z2���S%�\�g{tu$\�\'��\�!<\\0�\0���~a������b�voT��bq4\�\�F�H�\�cKY[ˑ[i�FO�Mm$f\�LU�.q&;QX\�\�\�\��\�婛\�Pf4e�\��zƮq��iu0�\�Z�x\�dL�\\�%\��Uo\�sVgNC�I�qP.>k&a\����p\�s$2Ŏ�k�?SXZ\�Con$H|�Y�\�\�|�FG\�Y�E���{׷�[Xr�퍘\�x ��A�\�<ΰ�7�3lC#\���}+����\�\�;{{��P`D�\�\�6\�\�~\����O\�T�E��>(�\�m9\n��#��<0\�\�\\C#F\�XGc^�\���\�\�\�LBPFQ\�f�*\�,\��\��\0�g\� v\�$\�F���8�\�y�\�� \�uSc)�L�X�=�v(\�\",�AC\�\"�[\�\"�Ma\�\�B��qʟ\�U�/Y���|�\'�=��b0\�\�祓\�3�\�\�\�O��;<\�Um/\�\�4R��\0t�V�q\�ח(J.\�wGJ�T�$\�\�!>�h|�I8Q\��*�\\�\�\�!\�!\�\�W<�u\�\�N0�\�\���A{\�^Ď�b�(dS\�}��j	,,�ё@\�YX~�uI�IP���ȩ�uV�\�C&c$�Q�u��)->C��\�u\'�\0�F\�+�\�\�QR \�.�\0UG\���Bov\��Gһ]\�x 3��\�q�`\�Naʲ\�3�F�\�\�؀>��\�J~�/Dxx�w�=�\�\�Sү~\�}�W8a\�Z\�T��;��\�Y\�\�DOo�W?�O��\�K�<\�t{<7�*UoIm\�nQIA�$�\����\0��c\�\�џX\��5\�W=\�8�\��@��5׃v�xG\��\�\�M>\�\��\�\�X�\��WxpNAʞA��8\r]��w��22N^?����t\�\�*K�\��\�-F��I\�4�Vsh�/s\�l`3�?)?\�Z�\�F�������tqub���\0\0\�\'zQ�\�zT\�\�Q�8\�[\�\�n�\�:\��S�\"bG\�\\\�1�\\�\�ɭ�\�G�����\�b����y3�e5\����L��/��H\���)<3&\������\0\�-\��F�xb]�SDO\�L�\�A��y��֭Oe���\�}\�\�\�b�%P2Wӯ\�Mq�`}��8\��Wz1�FA\��\�W�ٵ��!\�F\�	\�(�\�kq6�\Z\�h�f߆\�7�uʛ���i��\�f��\�m\�KzB~�T�\�1�\���?�4-q_s(��V�3~\�~\�\�\�9?��!�l��\�N��u�G�5\�\���>�x=��i:oȊh\�r2�㊈֦�����U	�\�0�fc#��>B�yfЩ���%�\�IP}\�VH5�1~\�%^�*�TH*E�1$v?gp=+gJBt�H�_�>	\���V�\0Q\'Ҷt��b\�\�z�l�޹1\���\�\�/B�y�W	�\�\Zi�l\�\�8�#4\��/�A�4\�%eRe��\\J7\���%ݕ��D$\��\0*zՋ,\�V%�2I\���j���Mٹ`Lq�\\�}k@ȇ�ꂵ���90�瓫/�g\�-�X�\�0��\������8��\0\��?E$�/)�\�\�8\�\�k$�:��\�g^��9?\�[�ml,��u<Lc\\�\��s\�\��A�O\�[\�\�\��\�\�^dt�\�\�S���\��5Qa���ޒF�\0ǱU��-\�0�����\�cU\0\�\�¢-<q���v�\�񬋒��\"\�YT\�wHq�\0�VP�r\�cj\�qV[�g\��u��}Q-c�d\"60��y\�ؒ\�\�x�GW=\�m�\0\�W?�c***�[\�\�\r�08�\0�\Z�&�#��W@���IsJ\�R�\�_R=f8\�\�H\�\�m\�%��<2I�b[:��b\�\�T1\��8�\0�\�\�g��\�Vf�+�&\�g�c\�+A�\�H�V8$gzV\�V�y͈�i>\�\�Xd}��H\��\0\�@VN�adP7�1\� �p�^}L�;�\�\���\�V=�\�M(X\�\�;T(�Q\�\�R潓��~^]U\�\�7NK���m�|�#q\�s\�Zʆ \0��������-��#[�1�I�}�\"�\r}�\�<3EI�?\"\�EҎ�2�\�?j�\n\0\�\���M\�\�@�\r)C\�6\�tƋ\�\�a\�a�\���t6#\�an\��\�$:\�5K\�fS����Ի\�yQ�Gp���\n?�\��U[y5-K��8Hy2�n\�![\�ȧ\�72o���j\�\�Ӓkf\�B���H\�J�茳ȷ\n7\�\�\0���\�\��zRjmG�RO ̶q,j!�\�%w$�b1\�`g8\��f�?�<GL��\�\�Vs�?��F�\�[��.1\'\�z��\�I����H�}&���̷�bS$L$\�r�(�Ms�O%�\�*�� סY^��\�r��R!S\��9�\�#�G��V\�\�#V����\�=\0\�κa[x\�drOkN��\�\�/i��:�@n0�=~�|��ⳗ\�vh�\�\\�AI\�?��\�;ɧ��c�\�U\����5\�R�%/ݳ\��y\�Zt\�\�F\�uL\�fT�\�\�5\�8��55\�5	�<�R<(?Q�j�H\�t���I\�ÕQ���\0Tze�G3\�n�A_�19{�դhB0i�L+guj׃�m\�\�Vtb�5�y�\�a�|΀\�ry�!%\���\�`g�\�Y�ܙ��&�Uv�\\! c�\���\�P��\�Ӛf4q8nH\�;�\�\rWOm:\�f\�\�(\�1�_֭xz�Z\��Nq�)\�\�W�\�:���\�X�m\��v���G�b�e5\�F\�i�\�{$�,&!U���\���FkG\�c��a�p�(�bp�l�m�m?�x�h\��G\�,},U%R\�v�Y�:�Q_\�\�F�٤B� s\�>��A\�b��\�	7Ժ\�U\�(�ߗFq\��\Z\�\�\�![�ΐ�\�\�a��cX�\�\�+gÇ:Y�\�+\�\�k�_#䲷l\�K\��a\�@�3\�(�F�\�\"\�[j�HNl\�x��W�\�i\� 2Y\�q��\�q.��0\�)��X4�K��q��T],�\�\�X`��s^\'?\�0��?\�[zu\�\�i�L\\\r�=\��\����\0_X��C�n��k�\r\n��c8\�G��&�mX\�Ң�t�d#Wq�N�+�\�e߮\�G�_\��+�ܑ.\�\�h��\0�\0\�W,�=\�\�ǖrO\�k\\*朦q\�P\�\�íֿq\�|\�!տQ^x\�s\�ס�\�\����Eyܼ;}M<\�Dq&����\�Ղ͵yd>jGC�팟AXҡ�V�\��$dw��\��;;4��4NTrJ�?\\\�&�V[�9!X\�uҺ)M�5m:9�����z�3�,*¯�)�\�Hӓ��\n\�\�\�j\�\�\�EO��Sy�o�\�i\�nھ���@$���\�\��\�\�\"���@�3�\n�\�w�Glr���9���!\�+_L\'�6\�F:�T\�\�Ǖ��\�⵴\�Q�B�:�\��\�ֻ�\�\��\n\�_�i�j�LDxȬ\�̘���$��Aަ�]\�F\�\�\�3Y��6\�\�\�\���Δ\�\�qX�\�6�CK\Z!�\�n����2�\�j6$1ǖ:2}�\Z�z��ъJ)\"B�\�\�\r2WSc�;�A���zI��H`�ݑBZ�r�Y�x\�\�z:ضHZ\��Y�׹��K�ͳ�s�:\�7^E�Fd1�\� �\�]�pIw<\�Q�\�\�Kw*C�;Td\�	�t�IRpZE�nI��\��\�a\\JTF\�w\0\�*z�t�[��Ы�\�Q4\��SJRU*6��\�0y�>\�\�O��\�:�\�\�\�\��͐��A�3\�NQ�b�7\0J\�;��W\�}�b?0���\�\�Yj\�j6dpC�|\��{V��\�-FY\�\�82���\\%\��@���J颮��\�\�J\�������U��$�a��eH\�tѱ\r\�\r�}\Z�q?�4q1(%`�\�\����\n��`�*Zp���H5V�� 8b��B?�4s�#\�P\n�\"�1�s�\0m?ƳGSd��F	 ��\�3O����L����,L��8�\�R�ݮ\'�<��8$�\�o�>��f���e\�\\�\�sd�/p\'�\�kH\�ȣ-#\�\�\"�\0*��*$RQ�Z,g�j�c�9�\0�\����\�ޛ�*\�rd��>@�\�;�_�\n�����X�\'-��\��my���>\�V\�k\n�{8\�9-\�9�{\�RZ�Y�r\�iv�\�c�\"(T\�98�j퓛+A�\�\�\�s��$�z�\�:VF�	�Rf\�\�\�\�Cd㓂GJ\�\����\�ӫd\�C\�ǚ*R��f?5��\�b�ҳ�G�jv�߻�\�WI��ϱ-�Eg_��S ��\�E5\�V���\��E\�!92�P9\�O\'��Kx$�\�`��ⱴȗ�ҐX�8߽h(�q\�3�\r]v��2\�\���=˅���{\�����\�\r̛DM�H�x�֛\�#R\�Gw\'��\�v\��GiW�֕�beek�\�<�껧v��9�A\�?�]{��h�b�\�Y\��99\�G>�J\�<\�3�6��3�S\��\�y2(8R� �NmJ\�\�U:iŷ�C��΋�\�\�խ�*��\\<j�*;\�s�߅e1>t\'��G\�kKK���}��U\'�{�Z֥�>F4�Yw.\�i\�m������\rt ~�iy:K���`Bpd$b�瘘d\�!O&1\�\�k\n\�+_B\\eFN2@$����&\�ͩ�v\�\�rh\�m3\�\��Mt\�+�CFN=~�-��G�D�9\�\�\�A�ٸ\\�\���F�m\�@�\�Ss�^��q?kԻ�Pݵ\�*\�E\'#	\�#�\�\�跋6\�Ì�\�\�\�+R\�\�61+�����\�5WR*\�6�*�?#WI*s\�c<]i\�z�﹏�\�\�\�\�6��2�ֺM\�8\�7/,���\�̶\�\�\Z\�^H՝�\��§i��l�w�\�5UkN\�:7�Ymw���et�Wٚ��m�H���=+�\�_�\�Ft��w�NI<�J[H| nd�8��T\�\�13\�N5v[X\�\�|v\�\�J���&q\��up\�41㑶����[}5n��<�\���\�=�\0:�:Prm\�tf؏�ӥ�t�\�\�\�+1bG���I=��2��GCYA\�F�ӖWV\��k�\�-\��\�Z{5�\�M�A\�L\"�g3�\\\���e\�h���j\�bݔ\�8A=�\�u�y+��Z\�F ��\�3G�I\n���4\�Ѭ11\�z�X��\n�\�,	č�,99<u��k{0P����\0S>\�)$\��\�G+��\�\��f��=�5�{��\0J�F}sY\�\�@1�U��\\7�ޥSkT_��ћ�wn��)7�9\��\�#�JO\�/\�Ju)\�\�����:~�\�\�g8\�:�1,�d�\�\�{\�r	ǡ���\�횸\�kS)\�\�R۶\�ʓ\�����	 ���\��Z\�v3\Z��*\�H�\0��\�b���c8Պwl�<bA\Zg6r=�l�NBS \�d&�\�z\�\�J���*a�����D\�JI&��b#\�{��S����\��\01M:�\�\�\"��/�V(ԆQ�\�Lk�[*1�\�T�5xԺ�ww�O±\�9�Y�\�!#�\��l�\���C<L���=�h\�\�\�sԮ��\���刎dߐs�b�\��\�Ú�/J�A\0��5���CM$�p��v���e\�i��M\��\�i\�59�z��\0�\rf��H��P��r\�d\�9;\�\�LqQ��\�^������\�H�U%o��!\���S�v�k\�\�\\?\�{w�\�,k(b\��}+XǕlsT��$\�\�\�X\�_\'>\��\0CR�\�)���ϛ�O`�\�,dH�\�W?�}N߬E�:{���p&a�\�k�<\�0I95\\\�F�\�>�\�Y��`Tg� V��g��J\�KFm\�M�L�;Au{�ղ�3�_�5�i6\�H\�H\0\�p�����\0\�@?\�U�\�&Ϊ5�$�\ZR:ƥʕ\�?�d\�9�bI#�\�#\�(/��<��\�8\�\�\�\�S�m\�\�W\�\�\Z\Zk*a�~���\�t���\�YVO,I \�._<�;T\�q!RF\rEH7&\�h\�Q�Lm\���|�^�\�\�P\��4\�\r\"���i�C���x#5\�\"�p\����\�0�V9�T\�m��PB��\0\0z~U\r\�\�q\�S 1�\��V7�yڵJI��$\���i7+��x��\�\�ŏ\�CW�\�j��\�c�\�\�C\�\�P~�9���T�	&<\�\�tJ\rŤqӪ�D\�sb\�|)�Ig<\�z˲o�ع\�\�j16$F�$8c\�}�;YwQ�2An\�\�QiR�4\�}͙\�ا\'�tNU*�\�$��Y�S:\�\�\�l\�\�\�C�\�݀y$q�\�U;Y\"\�UJ\�\�bŶ\r�\�\�{�j�\��p\0\�~U\\?��\'�\�\�Q�?6I<{��\�w���N)�}�\�\�\��\0�Ma��\�S��\�3U�\�\"�\�\r�1\�5fW&L\�\�g\�\\d��腵`�)?uH\�>�ɳ\�\�\�\�����r.6������K�S�\��\�Zιu\�k��\�@#٪��@�`r~C\�\�*Dq�r9��N\r\�\0\����r�\��0h4\�.OCN  $��\�\�=f\0���\�\�\�J\�H\�A�\�4\�8�!����\�<�q\�M�zR\�=)؞�\�v�d��� G��iXQI��\���c�~�\0���\�\'�\�I��\0ɢ\����k�~tr;~tXAF:67��\�\�ޣ��/zvi�[\�~t�\\��\�\r7c\��\�v?�\�@\�i2\0�d����\0Z\�21N`��ݒv�t�$\�o΋���֚>�(�ߝ ��\�g\�E�\�S\�h\n6?�7�i>�OzQ\�\�\�G�\'ր�\�;p�<\�߾\�@G��>S\�4��\��\��\�\�1@��\��\� �H�SB8\�\�Rl�\���\Z�	d��\�\�F�f���ߝF�G\�K��ڋ!\�^c�� �I\�R}�o�\��z�d�p()\'��\��\�\�\�bpdl{�g�峓ǥ3d�\��\r@\�\��&T�\�H\�\"��8$���A��t\�I�B~\�\�I\�w&3ɐs�z\n\�Y\\��\���5_l���JR$\'�Y\���)�\�w#\��4\\:�׵@<���\���\�K�<�m���Q��~4�\�\�(>��[��\�?�y\�ˊ|�9\�-�\�H\�8�O��T\�j�g�.$\�v\�\�C��KtP� \�#\\	\�\�z�U��>\�\0:�\�\�*i6�\\��8������(V#c���C\�\�@2de@�\��\Z�Q4�\�֚Us�\�N\�=?JA��(H\�\�v���*�ا~�/\�d \�(\n{P�g�*\�P����\�Q\�SE(\'�\�\�\n=S8��-\��R�\�֖�\\��*u\�4�WzRb��/D����Q��G\�KE�˰�OJ���\���\�Ӱ=1F(\���c=E���zPTz\��\Z9\�J,K��>�g\�\�d{Ӱ]p��\0x�����\0}�=\�Xw]�\0�w*�qن�!g�\�\�L挞\���\�\�k��*BPO>��>����`�\�(ǥ7vh\�qҋ2���m�����\�\�XWA�=)v��H�(\�E�^\"\����\\�4��lQ��\�\�R�\��\�\Z,>d\n8���\�!a�M�\��4�4���h\'\�N\�\�\��ix4��z\Z@A�\0\�\�`\�K�֛�J7s�X|\�\�zf�F){f��l\Zv%\�Q�td�\���7��h\�{Q`l	>�\��\�Oi_��%�A\��A֔�\�E�v��Eg�F1�\0֣i\��\�\��\�',0,'2018-02-19 09:05:36','express_user@%',NULL,'2018-02-19 09:06:47','express_user@%',NULL),(3,'2018-01-11 21:00:00','ОТКРЫТИЕ ОТДЕЛЕНИЯ В АЛЧЕВСКЕ','В нашей многодетной семье опять пополнение – родился сорок восьмой ребенок!\nА теперь без шуток. 13 января 2018 года прошло, как обычно, шумное и веселое открытие, но уже 48-го отделения «Первого Коммерческого Центра», размещенного по адресу: г. Алчевск, ул Гмыри 20 помещение 46.\n\nЗаходите в наше новое представительство и воспользуйтесь широким спектром финансовых услуг, будь то: денежные переводы в Россию и из; переводы международные на Visa и Western Union; снятие наличных денег с украинских карт и карт российских банков, электронных кошельков (Qiwi, Webmoney и др.); обмен ветхих купюр и размен крупных. А пока операция будет выполняться, Вы сможете душевно поговорить с нашим сотрудником о погоде, жизни и актуальных бонусах для наших постоянных клиентов.\n\nНа данный момент это уже третье отделение в Алчевске. Приятно осознавать, что люди нуждаться в нашей работе. «Первый Коммерческий Центр» стал на одно отделение удобнее, быстрее и качественнее в выполнении финансовых операций для своих клиентов.\n\nБольше информации по телефонам: \n+38 (099) 606 808 1\n+38 (072) 00-00-111 или переходите по ссылке\n http://pkc24.ru/otdeleniya/14-g-alchevsk/242-otdelenie-48','�\��\�\0JFIF\0\0`\0`\0\0�\�zExif\0\0MM\0*\0\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\�\0\0\0\0\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\01\0\0\0\0\0\0�2\0\0\0\0\0\0��i\0\0\0\0\0\0\�\�\0\0\0\0\0\0�\0\0\0\0\�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0Adobe Photoshop CS6 (Windows)\02018:01:12 10:48:40\0\0	�\0\0\0\0\00221�\0\0\0\0\0\0H�\0\0\0\0\0\0\\��\0\0\0\000\0\0��\0\0\0\000\0\0�\0\0\0\0��\0\0�\0\0\0\0\0\0��\0\0\0\0\0\0\�\�\0\0\0\0\0	<\0\0\0\0\�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\02018:01:12 10:47:24\02018:01:12 10:47:24\0\0\0�\�	\�http://ns.adobe.com/xap/1.0/\0<?xpacket begin=\'﻿\' id=\'W5M0MpCehiHzreSzNTczkc9d\'?>\r\n<x:xmpmeta xmlns:x=\"adobe:ns:meta/\"><rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"><rdf:Description rdf:about=\"uuid:faf5bdd5-ba3d-11da-ad31-d33d75182f1b\" xmlns:xmp=\"http://ns.adobe.com/xap/1.0/\"><xmp:CreatorTool>Adobe Photoshop CS6 (Windows)</xmp:CreatorTool><xmp:CreateDate>2018-01-12T10:47:24</xmp:CreateDate></rdf:Description></rdf:RDF></x:xmpmeta>\r\n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                                                                                                    \n                            <?xpacket end=\'w\'?>�\�\0C\0		\n\r\Z\Z $.\' \",#(7),01444\'9=82<.342�\�\0C			\r\r2!!22222222222222222222222222222222222222222222222222��\0�\"\0�\�\0\0\0\0\0\0\0\0\0\0\0	\n�\�\0�\0\0\0}\0!1AQa\"q2���#B��R\��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz�����������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\������������\�\0\0\0\0\0\0\0\0	\n�\�\0�\0\0w\0!1AQaq\"2�B����	#3R�br\�\n$4\�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz������������������������������������\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�\�����������\�\0\0\0?\0�\�^�1E�\�\�pP��\��\�@�\�ZR�\�\�\�D��?3V~������\�FGJ�Hf�Dq̻��fȩ̦𵅸�Ki�_.Ua�w\�\�[�	aؗ.#\��D��6�̏\"8��y\"�\�o|���l�q�jar�\�\�q�򦀰e$1##����\�a{\�hc���q\�*йK���\�\�?\�qn#\�y@�T̵�񮭚kG]\�PA?6OA�\�m�\�\�5��7��1P���^�z��z\�w��X\�=\�~ثt-�uX���\�\�(k8�tO+\r�\��\���\�\���\�=\�H;\�p?�Bo\r�df �QV\��}�\�\�\�#?{\"���oP�=ż��(\�@;\�$\�\��\�a�\0d@�T$�(8;��z4\Z\�\�~�	An��\�j-B\�Sq���i\�IP� ���Hw<\�T�} W�[\�	7q\�:�\�V\�\�$(\�\�\�\�Ky:\�\�֜~������\�Gtܠ��o\�\��\�z���\�$,�YH\n�	\�zu��2�/\�@%�\\s\�J�W;ٛ��\��\�\r㢹>�}�\0Z�ʨ!� \�{r)\"\�f�{\��I\�##��#�Vڶ\�\�\��BLx?O�K\\g����WF��\0<����R����\�<�=�\�g���UMM\�G\r�.!?Q\�-H��V\�1%PH\�1\�v�N�ɦM\'\�X\�\�\�I\"�;��xpU�s��٤U�vb\0(z�)�\��=\\*x�l��k	\\z�q�\0�\�\��\�3\�C�\0\�hk��\�\���q6G�\�?�Z��\03(ݓ�z���\�9�S\�X�rŷ*�\�?:�\�yQ\�K<���<\�\�7_i䝠�\���5mWw\�6�W$�\�\��\�\Z�\�w+0S\�h�\��\0���.	O�S\�\�ژ\�\0�\�\�\�֚\�c3d���P&z/\��\0�K�\��G\�6�\�:��W\�Vӯ0VpO⢺����s׊ס�z�8Ƴ	����\0BO�RO�\���\'���n�3�Dmx�\0���\0�\�W�\�\�a�.�x\0\��\�9��X�\��\��u�y���|G�|\� `z�\0\Z\�Gs+\�%�̜\��wA\���\��3ǧ��\�?s\�\' �T�;�\�\� \'<\�\n���;�\��\�\�N\�YsQ�8�\�G\�FzV�c.�\�\�\��\�ˉ�<����o�s�\�s@r�4du���J\��\"\�ܞ��sw\�\�jG��\Zm��Ʃj\�g�\�\0Ȁ�={\�\�<M!<1��\���\0\Z\�,m��M�|z���ֹ��\�\�i֯1��h]΋�F���gK\�#\����[ �}�Ox�\�[[����@�����zS@Gx\�\�s\��\��\Z��bq?/Z�[��-�\ny(�~U\�\�J�\�\r\�\�\�E\�h�\�<�=�{^\�\0@�q\�?�zѳ\�~\�i\�eb�\�3�E.���Y�{8\�?Y�r$�]+%U��\�q��z�\��u8\�\�.H�\�\�e�fb\�ŗ!\��VUͽ\�ib}�s�\��W(�krIP�V\\ڝݕ��f��]\�\�3\�t��[0�G\0u5>��\�q�J�(%H�<�V�\r1Yj�܊ng����~�8�uZg�e�kJ�\��:~y�\�G��5Pz\n\�#@\0�rN.�\0\�7qi\�{)Y!\�\'\�け��S�A�nf�xsq�o��ZU�\0A�\��T������g��w{\�F \�3\�\�RK,�a���V�SX&X�\��#V�\�j�\'˷�\"E�Y�[�X63��})WP�)�2�\�H \�SRK�f\r! =\�X�RR\�\���	�T�t�\�k�WOooZ�.CƳ#�����h��6�\�0{T��Ւ(��pZ��ݭ\�2��\�M9%�]#���ThmdFhcR\�\�i\�N[K92��\�]�ɠ#�w��`\�ҍ�\"\Z���\r5���\�iTz�9�\��\�-\�Gpθ�K/ݠG29\�F?\Z�l^I�\�\\�GPG\�H�焱�\�h\��\r ��3	�kF8\�\�})�Kk��\�UU������b��2*�C2�\�N�j�\��<`8\�9?�T\��[V18#v\�>�\��ƞ�|c~6�R1�x�\�\�\"L�;��Juq\�\�*��)\�B�-���\0\�?Ju�o2\�J(\�\�\�U�zG��i�\�z-���\����\���;�޺8<o�\�[u\�\�H\�\0\��y�`��n�4�\Z�\�\0V�8�6�g�^\�\�}\�[yW�|�Q7rF�\'�lg򭯴C�ee�Y1;NA\�ڸ�𦕥\�\�.�<�B)	�z�#8�ox\\XZ�\0h\�sh�\�iϵ#�ԩݣ\�\"�B\�\�>O��\�S�-Y�;\\cwJ�>\�\�mu\�岌\�a+8�\�Q�\�\�a�|D��i\�]\�q\"ۋ���a��7lUX����\�^�\roO�+\�G��\�� ���w������᾵���w��\�, 9h�#?)9\�\�g���Eui42�ʸ(\����f�\�AnZ1&\�\�T\��+\�\�\��\0Z�\�٠1�1��\�J\�]EK��w�\�Z\�j�MA�\�\�N?:�3h\�=�DL����,:�\�\�Ӄ��H\�f\�Nzg�0\�\�&��\�6H�)|œ�^\�\���iX\�T�ٝ׀r�_\�e|\�9\�\rvQ�l�\�O?�y��o^\���8gL�\�5\�A�F�(pA\�j�9\�%\�Y���ZqɊQ�\�®1���Q��\�lR2\�n$\�#�?�´\�F�p�2��\�p(\Zkq���(3\�/\�O�\0^�\�ĂA�ܺ�*\�;�\0Z\�n\�L�֎��g��\�q�W\�hO�\�\��X�1��E�p\�\� +H!yϭ>\�a��j0>�\�^}i\��+�;�\���Z�����\\c-��[���AE!�R8�r:Ґ��\�Q\�\�\�u9\�b\n\�F]\�(�9\��ko������ko\�Ȗ\�\�F\�\�J�wi!��#&0\0�!HL\�\�it�#\�6��~\����㐧D\�\�H3\�J\�\����\�26M��zVw�b��vO-	��Ny\�!\ZS�U$\�$\�g<�\�\\�ج�\�s�U\�$b��9�\�\�\�\�^5\\\��}+ұ�=��7ṳ\�=��\"�w~���Ү�x#���?:�FU}kZ\�\��{8R)<�Vs�,��)�z�����\�/�J\�\'\��\0�]\���,����w?Ғؗ�\���\�\��\�D%�\�\�\�=۔\r\�\0�]n�fךM\�\�˙$�\�`��\�s\�c���ޠ@\�u\'\�ٮ�\��\�ב\�V\��\�-\�h�u9u98\�\�[\Z\0Ρp1����m\�5�\�\��\0����\��F���\�O\\F��T\����F\�=��\�U�~�D2T��_X���X��f>V\�GSW4���%Fp�\�ZD\�G��P��.H\�\"�	��3	\��\�\�C�$���g=\�kAo�e1-\�O\�A\\��\�\�\n\�Ik$���E\' rqO�RB$�\�q�����$\�\�Bc�p�J�s���Pd|��h)�-��ß00\nv\�\�R��1\�cG�o� \��\�攘͉}\�/�b��[\�\�a�_\�Z\�嵊\�\�x�PW9�k	f�J����R1X��ϐ\�eh\�_|\��\0\��d[{�L�C~x\���,3ͅG@ǯ8�D�1C\�>	^�ue�vŃ4rJ�\�eZ6ʜ}8�Q\�Ml\Zk�@y \\�f��Mlo�Mln�\���\�\n�\�ў�/Zm���;\�&��<b@Ɲ&�-\�g\�I\�Hv&������\"p뷊\�\�5h\�~Y9@w�c��:�����b^J�6��s+t$�ZL´�v$󄝆{ի���\0Z��&��W�Z�\�i8��O��g\��]#�T\�?��=z�\�\�.�\�Q����L�\�\�k�_\�0\��r����1k�y]=3���\�b�U��)\�WR=�z�\�1��sp\'0\�08\�I9\���=\�6��^\�\�s\"��y`)\���܀{W�\�x\�`\�f\�\�\�R\�̅r\��\�z*��x�\�]��(�\�r\��-\�	\�OjwG|�ǗU��s\�\'��\�\Z��a�o��\�=뫶\�?�\�V1rû|�lo9Q�#�zW�\�1i�\0d��2����9;I�\�\�8�|�\�ե���ٱ\�&pN{�8�UI���c_\����\\.\�I�]>e�g\�1��\��\�kAխnu�\�\�\\\�\�In\�\�W	��Ѷ���\�Y\�c$�\�^!�iQX�ڛ2\�x�A�\���J\�M�\�w�N�hg����DcvT\�\��q�E*3j\�Z\\ٸ1jV�%\�\�C��\�i�bUb\0�\��s�\�\�)<0��b\�\�L}@\�\"[Hr���_N8���9`�x��E0�1E�sF×�\��U�uK\�N\�a\�maf��\�\�<\�n��\�o\�n=~��&\�$T���\�\�Ι\�=Ʃ%���o��>e$\�Fs�Z\��=�ӵ\�7R\�3�\0e�N\�;g�_�Wԭt�CN�L��\�\��Y\�\�D���%A\�^�>��\�+�u�e�:���6Yiܫ�A�\"q�vEx/�m�a��<�\�\�f�/�uh\�ڷ\�c\��\0*\�\r\�PI���Mu:?x�{\�{Ie�\� H�Ո�pާ\Zǉ�\'��w2Jm�\�I����\�\�.\�k�.��lyPq\�9�\�jdO�\�G$\��-�Q�b�|��\'�r>��J�\�u\Z�O���\'V�DG{�∊\�w[�\0�x\�\�\�PM{�\�.�\�yÀD�%[�;V�y�\�EF���\Z�C��2�y�OZ[q]J(��\"?\����!�\�R\�\���I�F��\�o���\�۳���s�*/2�.i-\�M�1�\�@n�q\�t1\�\�Oy\�\�̂L4L�\�H\�\�0��@;V~�\�\�m���\�\�Bʛb22Go�M\�\�\�S�*����\04�R\� �	�{f�V�ب�\�5�Q�\�\��\��,�\n\�C\�Ԩ\\4K�\�\�X\�\�;d\\7\�pX�\0\�_�Eym\'\�f�\� D\�����\0\�%\���YVj�vW*\�z\�r��혇\�$�!?\\\��\�(PѰ�S\�g��WJw\��I����X9\�U�\\�\�Q\�\�\�>h�Fт�]\�r2[y?�:QsG�׊�j]y�kD�\�X\�Ѥ\�:F#̈\n�\�>\�oė��h-\0��\�GP�U\�Ğd�koOu<\�\�\�n��1\�T�\�8�\�ZZ���\�,k=�\�c(D,�Ƕy�d�(7;��?�Э\�\�\�\�*�\�W-\�Z\�R��\�\�V \�TrJ�ݩ�\�\�Q�?\n�iS�\"t!K$�1�s^��q�ۭy\�\�ɩތ�<�1���*\'g\�B\�\�/FN\���+���:����l�\0\�\r�\�x&o-\�\�/\��\�\�\�ڒ��\�zA,�%2}��HRZ���\"٫_��\0-����t�P���y\�!�\��\0�ax\�?\�o\�P�\0\�[\��\�=ⁿ�:�3�\rǶ	\�¶�2ᵝ�\'!��?�՗y�\�t\�\�\��{5hxa�\0\�~�ة\��5��+q�⮠\�zs���=\\S�\�i�6X����\0yH�:t*�*\�Fy\�O�\�\�$H�G��Tg%�\0\��&�d�Z�L�rJ�g�8�=kH�\Z��hu\�rx4���۟Z\�E�\�\r���Q.H#+\�A��6�����VWO�<�\�!ֲ�ڗ#\�4�k�y@Ɓk}v�/\'�\��\0�J5K\�F/%\'����\�\�.F3\�^\�:\��\Z\0\�?�/�ɺ\'\��:\���P<Ș��W3���\0�g\�p\�xߧj\0\��\�1\�mjǩ;?Υ�\0��s��ќ�\0uȮU5w\�\�R\rB#�g�\�6�o*�&��\�9\�?ʄ\�,\�8?\�8�U\�\�;��\Z\r\��\0�C@�\�\�\�n���\�;�5*\\�ǘT�\��\�Y��s�M]iY��\�P�0�E\�Jƈ�{�E�\�DM\�7BG�v�\Z\�-��٣1�\"p2+\�\��\\]�j��ڂ���ѿ¹*͝t)(+!\�\�{i\r�J�i��<��G#�5\�*�p�\�2�\��cnd��6\�5�H�\�RR:��b�vMt\�\�ȰT�*�n�N\�eǤ�?�S/�h\n9��8���jW�x�s\�V��b�M^�c.���\�\�/\�J$={\�\�fO\rI��u5��\�\���\��օ��o\�\'�S%�\�n��Þ#���5\�\�zr�\0�i�~�K�;_�N�	2%�bo<)����\�&�☵[�\� ��\�`�2I\�\'<�\\hs�}�L�\��\0x��i�\�/���gJ���%\�\�H�b�.`?�\�/\�\�dXZ,���\�ny?J��p\�C\�^M/ha���gE�j��\�\�M2*\�Y�2����_\��=�1��\\]�1�w9\�6\�\�}\�ץq\����i�\�:>{|\�<ǧ�U\�ST\�-�\�\���M��Ӗd�e���8 �\�\���zRY��M�;}�\�k7���U\�%�޸s�\�~&��?\'\���i�\�r�?\�v\�\��Gٮ\�ϑ#T�!\0��??��ʫk\Z��y�\�̒X[k�0z\�種I�\�y\nG\�hm(�\'i\�\�\�R\"���%g\�\��Gt�!Җ�]�\�i�\�J��\��R&���\�\�y$1��`�bw.\���R9�\��h�\�x\�r�����WD�/sӥ7+\��M�K\�=&\�[��#V�%c�\�\�dq\�\���߅=�;5\�\�F\��ᔖ�W;A\�N�\��3�\����M?P\�X\�U���SC���������WM��Ύ$�+*H\0c�y\0\�r���o�\0d\�nqk�jp\�Gy �q^{\�\�}G�}>\�I\�]�\�h\�\�+\�\n\�J\�jsEN\�\�}?g\�.��d\�n���\�ʨۿ��k{nTq��SEP\�\�֭#�ڨ\�q��s޸+\�\�j�\����`���\�\�\�\\F�\n\�|���z��L��w\�\�Ƒ�\���:D�Ǆg;FGm\�\0~5\�[\�\�\�\�i��(\�\�}\�L��A\�>����<Xx��0K\nƓG�e+\�\'\�*V�zω�=u�\�Mqk5�WP\�\�O@==����˽5�+1�d�0%s��Z�t\�\Zс����l�\0\�����\�z�������z%�1�\0}f�byY��*Ǫߡl��t\�57�e\�\�$q�?\�Y�]\�\�%\�\�[�>ij���\nMY\���\�7��3�R���v\��\�OO�޲�\�:\rɶ��/t:\���\�q�{=Mi\�~n}kü[S�\�Q��\�\�;�\�\�U3\�`���UQ3�\�~��\�\�hΓt!<n�Wq\���jm[I�7i\�ֻ��\�\�d�3�\�n�-�S5\�P�\�\�8\\�d\�5\�\�Kr�}���:`3B��ܿ«���\�\�\�\�F1�l�@1\�)�8\�ֵ�\0�o8\"#�\��\0\�Q��}�y*}p\��)\�蹕�ӂ)=G޵Ƒ~�fn�a��j#�_I�����\�}@\��@�	\�\�A����f�\�ni�\�t<\�\�3\��Lx����\�\np*\�S�$qڮ}��;�a\�\�Jg�\Z\�3�}\�(�Q���\�E?4jGҬlV\'����ƌ�=�p x�\��*E��sWq\�Ӹ�\�\0^�\�\\,^��!�@5+e[T�!�4G�\�?�< -\���w-KHe�?j��|\�G�$������\0\�u\��F�<D(8\�=\�Ѽ{�U���s\�\�\�f?C^��\�\�hz��G�>k\�\�Z�o[)�6�I`Mܤ\��\�QCF\�3\�=iÜ�S�G�=�@9\r\�Ґ\�g�\��R+�͡7�\�g_\��o\'�_�)�oQϽ0\�8\��Ӹ�E-\�+\�7\��ǏqN������=\�RE\��\�Vc�$�\��\Z�_\�S/�ãEO�8��Аj `�\�N��8Q�����\�Cu�\�\�sc���4rndM��\�\���A>�\Z\�B�n���C\�\��^��XGԑ�J�#U\0\�\�?\Zj7vv9�Л��~U(в~\����yB8ٴ.��l-+H\�B��d`u\�ҧy\�c� ing�\�vݳ��B��ժ�f\�X\�A?�ɸ���\��\�\�\�\�]��Myk\n\�F�XV!]��u# `}Χ֠�֮簊\�\�Q\�\��\�!Yة\\ur89_Z~\�^\�k�(Ǌ��?�\�\��\�\�\��T>Tm	y�\�a!Bc*8GR}rf��DefK�,1�}\��\�<�\�\�\�qG��G�ii���2�\n�݁ӌ�¯Y��9�i\0\�\�pN+����z%��\Z3v�h\�\�ߌW�\�\�qr�\���h|����q�\��`Յk-�5\�\��\�PI1�\�*H��\��џ�*\�K\�\�>�S�S�\0�\�\�\�\�@?F�>ck\�4��\0\�,\�/H;��8\�\�\�Q���Z\�\�5CI��\�J-,����<(�s\�\n��ֽ:O\�G�&I\n\��	�9\�\'\"��\�SݷWrEw?�rZ��\�LF[#�\n\�@�}�k�\�ux5\�\�y�\n\�o�ED���J\�\�ךa���m� �\�b\�V\�\�1��Æ\�Y�\�X6\�\�3\�j\r	\�\�\��åT�ɇN~�\�]������\�-�a���}*��\�\�H���\�\�x\��\'\�\'�\�OQ\\�Dȇ\��Z��.�yte\�+9�-f�G��eˀI�8>\�����M/\�wq[�Kr\�C,!r�`\�l�\�nԒ\�W;)�mi��U���J\�TŜd��\���;\\��x�}p�L�#B>\�d����OV\�W�j-�Ivז���\�\�\�\�l\�S�\�cP\�\�b�i\'nv\�kDg-�\�@�v\�\�(�w}23]u�i�8�}\�#�1\�7z\�9�\�q\�\\���jz\�\��m���ܯ�\�J\�8\�9\�k��\�\�\"&\��;b�\�\��\��mq��\�=\�:�och\�\�*-Fg��.�\����E\�%R(\� `�$�wD�W\�T��\�2}O�b$�FK\�p`�\�#�q\�\�:V�\Z�f��< `\�m�2})X�����\�}eX�L\���\����鞀VM\�b\��h�\�Ã��Ilg\�\�y\�+�5�\��?�u Ց���F9\�Y�\Z�Ξ\��+�g?����=\��\�T�݈ P���ڋ\r2a�Bz۞���)�\��\����\�(���\�)g�\r�Q��*\�x\';y\��\�?���KI#m\�\�\�q\�;\�\\gh\�wRʵ��.\Z<}@�kYX\�\�\�m�1��L\�tc\�mmѢ�\�\0ebnv��Dq^u&\0\�8\�ɩh�a�HS�\�PJ�����$`�ǯ\�U\�L\�\�\�\'�FŤ� >�)�DRl{V��\�.\���?�H\�fBq�?�s\�SKhgF��?gν���\�;Ƨ�?\�\\p���F�\�u~,u+��3\0<�**�#�ݎMy��\'Խ\��g5\� �s��*�Jc\���\0�\�?���?x��\�}jɏ\'ژb�\�\�3 �\�֓���x更�ɧp\" Tq ����s�T\�O��\����*\�0<q�c�\�Mi4�2��\�r䈂�\�s�sIP���5n\�2Xt�`H�s\�d�U�mJ����da|Aj=VA�\0�1�F��\rJQ\�\�\'�\�\\��\�Zw�5z-�\�V���\���]7\�!Kb9��2�\�\�$C�1�1$�s�q\�\�2�8[\��b��H�b@L)�S\�\��� r��rk�����/;$q�(\�\���]M�d�\�k�ofn �_KY��,\�[��y$�5|\�̺tv�1X%�{X�\�r0s�\�kiw}M`RF\�\�A>�ں\r\'P\�t\�J6h�\0RS�~?ݤ\�\'I�r\�\�i\�,\��*h\�U�!8*r\�sѹ\�z\�ޤ�;qk(i@kW^8.P�\�|đ�\�b��*\��`�9_)ps׷�Jl�p�\0\��ܷ\�\�9\��⚕\�Q9�ƭ�غ�u7Q�\�\�\�d{sX��V\0u9��ƨ�\�mPn�\0�\���^#��;񣲎\�\�G\�M=zѓ\�\\w69�}n����lJ6\�\�:\��\"��ƴ�yz\��\0�?��\rZ���WЇ���<CkD��\��\�s\�k!u\�]\�Ie~��g��\�ҿ\"9�k��ڇ�\ZUE֙�\�Kx��\0e�q\�~*Uf7�n\�/h9��a\�]6Kح\�ܬ�6\�W��k|��\�2I�\0\�\�ݭ\�I2��#�$Dy���\���\0��i�\��Z6O\�r\�4{d�.�\���\�\�N8늭��!m#�ֹ\����\�HM\�US�\�p~��)\�,t�^\�n�4�iZ,�ڤ�|�\�$ޕZ?�f\�$�W�\\�H����3\�>�\�\\\�}u\"��Q�\�\�r3�UK]-dr%˶\��\�\��䝺\\|6\�2a�k�\0�\�\�\��iv\�#\��$\�CR3�ޕ\�Ǣ�y%�A�y�\0��Ű�$�\0	�\�\�>�(�������m\�-�U�;\�+�=}�\���\�8mUzcҖ�4���-�M3�\�ڹ\�檈o\�\�tQI���@V�P#\��!\�7�\�7vB\�m\�v�	e]�.<w��\�7_`\0\�&�1\�\�=w\Z\��]�jZ�棧^\�\�[��\��;C9>Ê�u��\�En ~tЙC\�\�A�h\�q~sE�Y\��ߜg\�^��,��Y}T\�o��柭5����ċ f\�s�9���\�WRJ}�\��3�C�*|\�b���\�\�a\�ѐqZe�x�\�Yثps\��\�C�\0>ǘk^�U\�oDrL*v�\�A�Gj`\�ͭM �J��\�]\�向ǒHeݓ���\0:�Xm&��+\��\���\�3ӭb���\�$�$�#W�\�|\�8Ҡ�W�	*ca\��⋅��\r\���S\�)D;\nn\�涞%7�I\�2퍿վ\�\�fpO\�+\�1�\\\�\�$lN?\�Sx�\�\��C\�4�eO\nG<�\�ίQ���B�o.4ʟ;0&E�1�{U������\"$M��U�\��=�\0�W�K�ޭ���\�X�\�H\�O_�j\�w\�gQT7\n�ݘw\��sJ\�=#T����Rep<�{~�&\��\�?;�zݸ֥�\\G\�\�\r�c\0�0�\��5���3�P�9�\�\Z0\�g w\�U\�3�\�8\�\�\�9 `T,��qR٥�7�ݤ[��\��\0�\Z�\�&\�Fx\�\�\�M\�:Dg�\�\Z�\�D\�z���r\'\�\Z\�B�\0x#�յ\���\0k�\�6\��\0BZ�S1�\�o��[�3n\�O�KN�\�;�ך��/��/O�O�k\���޸\�\�$��8+\n;�#�ك�FA��5a��aL�\�3�U�\�_�Zd\�\�Q2�)��\�:\�>�1SW\��\\&le$�O\�T��6O1\�\�$\�\�V�\��\0�\\q\��!X\�qޚ\�\�&m-۷��ʵ�\�dw�:b\�\�ٿ阭{t�\�RƎ�\�k�g?\�G\�\rw��A��N�H�_\�\\&�6\��\'��/�@x̚�;r>Ƣ\�%�!�Ce!�򇏡�	�,�Sg|��\�\�]�\��\\sD��ˑ�\�y�����!�.\�Ko�B	\�8\�\�Q�\�iJ\�\��WPAgmJ�\�@U\�&�uI\'��q��\0��\�f8\�1\��\�)��G\�#\�w)g�p\�\'\�z\n�s�I^`F�1�\��\��G�J\�\�\�s�\�oޙmpq�\��\�\�5���Ƽ�M\�^\�\�	�m�P�\�ʺ�\�\�m�T\�$�T�ҳ�hƶt��\�\���ӷ�\'�(\r:O\��\r\�θ�o�&}Bbӱ\0�\�b��``s\�?�w\�(�\�\�p�7\�\"�\�\0\�\��W6.Z�\�ؐ�L�F)q��Cs��\��e\�\�G�v�g�~5\�#�\�\�08�Z�*1�m\�󈮠bS�\��טʉ\�3�\�\�\0Ͻz�>q\�����©m�\\\�\��t��\���\0�v\�\�nz�8�k�\�Wn�f��\�\���ul�$s���\\�OvS�!�\�q�y⹀�jR��ێj\�\'��	\��\�0o\�G\0R	<�����\�\��e�H�`�bz`��d6(\�N	RK\�n��\�t\'���\�ߌt�O�\r1��\�r\�X\�s��\�P�+Q($L�`��\�7m��/�\Z�\0�9\�[\�hP)�ͤ(�=��[ۙ0 �\�\��9\�h�Z\��m𬖑Ȥ�	-�g��\0:e���\�*�DT$��\0\�U��.�A\�3a�R1���\�TV\�]̳�i.\�ܤ�\�\�\rU\�=3\�]���\�\�n\�\�\��\�/S�u8\��[\��\�\r�Ĳ��a|8�L�\\4�`x�\�a�<z��\�o�h�\�\n�q(�9\�y\�8�\�3\�V��z;\�M>�i��F�@��$Q\�u�\�X\r\��\�\Z�q�\�#+[�Ǵ\�@I5\���\�\�\�\�6�W��ʉ�\�\\6\��\0�,�\�\\\�L:4V\�\�y\�w��\�\�\�y7Ls4���\��\� 8\�Ȩ�\'\'r2���\� J\�ҹ\�\�^!�W^\�\�?\�\�x<42Ғ\\\�܃�OJ\��W\�7�\�)1܊\�\�g�ma��,��\�YӠ\�ȧڜL�\�w�\�!���ޫ��.�\Z�S:��\��\�\� �U�����e��8\��\��㷭+\�\�\n\��|�c�j\�\���☣:�A�\�\�q\���\�r���\�8Uh�Pv(^Cc���|J\�5�8���cv�݁�\�Ƌ\�z\�|\�k\Z��8\�;��dڈu�\"u`\��\�7L�\�:\�MR�\�\Z%�a�s��\�\�r��#,@�\�2\�\��Ұ\�[K[r�-\�\'\�\��u�\0\�޶\�\�~ϥZ���I.1Q\�8\���\�`2^�VW\�C\Z9��0y5�Ó�������:�\�\�GP	��>��5GG\���\0h{\���j\�\�~�<z\�J�\�\�\�i%��u5�<xx�^�I��O�i\�hL�\�qN�y#\\#��7�Z�S�O�=\�q�*��D\�FEq�\'�:Ԥ�J/�\�s\�\\��:�\��\�?x�sO�FR�<}G~�\�Mv�\�\�ʅq\�@\�W\�/�>���t��V�s`�zզ��\�G�0&�O��\0\����ij\�wQ@�\�[�θ\�o\�\�>qԈ��β\�PMG+��N2?\�uH����\�A>[���>�+B1\�\�\�G�6O\�?ƨhс�\�!��\0}\ZفA T=\�F��o;\�6��xϜ�1�\�f���k;ؘ\�M ZB�\�\�\0W�\r��\�vD?��\�S\�\�,\�\�ѩ(�>TL�2�)�\�\r�\n\��V�22\�h�\�\�\�I#�\�k�\�\�D\�\��#�\��D\�\�gˁ\�S\�g�Q8;Tl\���#|mt�����7*T�ֻ�b���\�,��S�\�\�?@sҸ\�-#�ѡ�5߹���\�m���X�ܽ��[��\r�W��z\�5wd\�C2}*\�m�\�B�dCd\�wl�\���\�xu���\\H%�ZG^�>�\�\�\�[\�\��\�\Z��\�\�9O1���<m�Y\������\�\0�\0ǈ�T�\�)ǌǧYI��{\�{ḏ\�\�pp� \��\'̺\��\0����}\�\���\"әD\�c2<\�w#�\0��>~c\\؝\�`�Ƥ[�+4\�&�K���`�_�\�w�\�\��j��a\�\�5\�\�\rF<x+WB\��Kl\�\�r\��h\�̵�\�KfA\�=���Pԉ�\0�O[\n2BB\��H\ry��V\r�\\��+ӡ�\�Sv\\\�X\r\0r��q\�o�\���\�\��\�\\Ɲ.\�.\�@bE�I\�~�V�`��\0\�Nլ�\"�\�\�\�	\�\�V9�&��,Oȹ�qZ���p:*\�fX��[�1�\���VLԎ\�\���\�2	*\�\�\'�[Cc&\�q#7\�1��Ԛ��\"��\�\�c�sQt-�(r��7\�p�*Dc c\�_j�\�Y	��\�0��}?�Ao<֥c`�^�\�X\�\���Y�{\�Bfc\\̮����v�ޣu��r=�\r2��оy\�1�\���L�`fyTFO����v�J��\�_�\�y\�=\�۬^4��[t2�\�+�������R�E��ER�\�\�,Ov���\0N\�%�\�2�\�d\�(8�+��u=A{���Q�\�W$q�,�\�\�Ζ�h..\ZH�]\�~L`�q׮x�����$��6\�0����\�|���\�*?*\��\�m\�\n�{y\�`��p�~ \Z����(\�ty$Rd��^�2N\��U\���8bg^-*Q�P9\'�\��J������K�i�l\�g�N��\�M�\�p�\�O}=)a�.�m�G|ц\�\�:�\���Śb)#S��b\���\�\�\�0m|5~��\�]鳭��$l�)\0�y\�8��x���]��\\�@ 2\0Ö\�R:�|Q�s\n\�wq���6pG҇�^�\�A\�.0#,3��Oj.+1�V�qe�\\\�=�A���+�prz\�U<Wuo�j0��2ǀ\�FW\�<��\�\�9�X\�\�dg\�\'\�/�4\�!o�\\1`\r�x\��\0Zc���\��H�&`w�ei`\�\�2q�ՙ�\�)�8�V�e#z�\�׊\�_خ\�.��f\�З\�z�\�وD\r=�0b\�3�\0�Qp�KY�\�\�\�Ʒ0�D\�J	\�o\�Q���\�\�|\�\'��*\�׉\��\0��\�@6q\��\�o\�bj����\�Mݖc�%�Uqߐ瞝�dR2�%��09M@\0b�\0�1S|�rKc�F\�@�� �\�\�\Z&t^�\0�[�L\�\�V�\���\�kÒ�\�%\�Q;c�Z\�X%<\�<��p{W���ŵ	\�UO\r�\"�\��\�jط�W���/\��j��\��\�D\�H\0�MTߺ	�\�\�\�\�fU\�\��\�k\n\�\�\�h潉\�̈0c;�\���\�Z2����U�\�m�=\�ʬ�\0:\� ��\nkRٙy�\�j�(+5���h�S\�}��,�kcM�k9�7�ζ�\�ч���\�\�X��9m\�ͳ�n�OoC\�+�hCF1LT2\'^*�CP�|��L��&:\�a2�p�\�Q\�\��߂�@\�<��aq\�ar?*³�8e��<&\"PW$q��Tw��a�\�]�r\�8��J�MP�<%��_\�kE�I�s�\��\��X�\�s\�g�η\"HT#��h\�8l�=H\�ϡ��;M\�\�\�\�y�rI�O8�k��@��B�f�#X\�Ʌo�\0�#�#�D�[\�E�	�i7� �\�m|�\�t<z\Z\�u2�X��S�\�zķ�|~Xf\�n~p\0(��Z�M$w�d��-\'f��O\�퓌�~�.N�Wеy\\\�[y�eK�\��Ms�zn�om\�\�w-p��� \�8\�\�c�V���\�\'�\�.T)�?\�}����\�\�T�\�F��\�iw$�	,\�L�ƪ\�1<d/\�YƝlW�7ʻu9�b\\#\�Os���\�\�\�h��\�\Z���\�\�JX#�[Җ\�Q\�\�\�\�1�gTF�\0Y�i?���z�t�3I\��\0�/\�\�mm����,6\�7\n�ˑ�\0^j��q�x�\�Ze�Ƈg�K1�\�@�J�X�)܂_C\�]԰�\�\�*N��z��ʐ\�~�m�\�\�\�\�\�:�]�� \�碏l�5f\r�zK�	�	 \�F�l`\�ɴ}1\�\�5/�5KsŚT\�\�P\��exP�m$�\�09ɭt�\'F\�<7�P�Ũl�m��vFm�\�*>���iӒ\�9-OM\�t�խ5n&2C���Y�x8\'?0L�\�T\'\0\�Z�\�V\��\ZQv,�\�\��O��b*\�U��*�,Lh\�\��\�G���C�\"bi�i3\�4�+���\�\�0\�b�&�Od�\0KX1\�l���\�y\�X^���\�pT�\�q�3�Wg�jK�\�,�`׫#\�1��\��1�z\�N��\0�$����Ss�tv�3�Z�\�(Y\�\\G�\�\�43\���l#)ш\�=\rj�\�8\�5�u\�v�M�\��\�6\Z�S\�\�x\"��\�Paq�\Z�l{d�u���EY��2�z�����j�\�R3�H�\�;f\\�2;g�Ru\'V�\nFv�;��hX�,�M,\�>}�\�+)\�ų,��w\�]%\��� T\�nc\�u\�s�L�+�P\�\�ˑ�\�B3̱��\�\��hC�\�T\\\�Xr1�j�\�C÷p�\�\�Wnmc�\�\��S��v��I�vV��0�>�i�v�sFY�8ʄbO>Útv�3\r�����⦆\�.x\�`ryf<~t\�6\�\�!b\�>$�}\�\��R��cu���\�5\�D\�a��\�)\�K�c�=�(A>q\nwg\�\�\�\�]V�@��=^3�\0�֮�8\�B\�\�!X\��\�9e8�Uε��$�\0X̡HiWs `��vs\�y\�]7�7\r\n�\�ݣ=v�e���\�q�59�@�.U\�\0�A��\0�3\�n�y\�y\�WR�`KK��(gT���\����_�dEmt�@EL�߁\�G��B4�\�g��K��\�A�#\�\�[�u �=�U\�\'\�pǯ\�\Z��O\�d�R(\�\�����\�\n��Mt��9F=M]���Ww`�@$�Z�͕�~ʐ\\��\�\�drx둃���x�O\�|C�\�Z�г��$I�]�\���I$z\n�\'�\��<E�ޔ�{K9.X\�!\r+���q�^��(�1\�\'3!67Y\�0����X�1�ϿJ���sv�\�er�F�%��Py�dg\"��5�.n\�\�\�\�\0�\�\�h�C\�\�.\�(*A\�\n\�\�UϷ\����w=�\�\�\�\�q��\� U3�c�>S�}*��3u_s\"\�\��i&Hm&��\'\�\n��\�\';�:ʚ�l�\�o,�4o*��6.7}�m�]\�Γ�ygI#x\�\�\�$���2�\�\��\�h�?\�\�R�mq�#;܂�N3�G#��_գ\�$,OvcJ��\�\�Y\r�\�\�0p����SuK\�o�Oo��\"\�G#c2�Pr?�s�5�s\�\"\�:7ٸkCI���``ztpǩ��\"�I\�t�\��g\�\"\�+o\�(�Nx\�r?K\��&����\�\�V\�O�\�?\�V\�\\\��p���=\�	\�c�9��0f\�2��\���8^(օG\'�\��tb\�u\�\"s��o�h\�V���Ms�\�\�p;HO\�]�>W�\��\rxO\�;\�ń�D0�\��\0\n\�Ӂ_�v\�����\�\�Ǹ�M`ZI�(\�\��umhs��Rbb{\n�\"��\\fя�:j�\��{�ү8�\0MN?\�տ�\Z\��32\�\�\�8dd܄l1�w�uoC�;\�$\�n�O��\�����\��z\�D;/5����q\�Ѩ�ZK��ˎ\��\�E�\�3\�\�m<�J�d����\"�e�k�֣\Z��m�F�n��q\��\0�Q\\\�\Z�2�^�\�L8�\�q\�\�x��\�LÀv��ӁV\��\��9ʠ�;�v�\n���+F5ⵎ\�2p\��Wr�o��\�W���\�C�\0L\�����}w�\�\�1<�\�\"\n�\�O�a\�g\��)��c�pq��\�ӵl^���\�����5�\�\�y�w�2Y\�\�@\�vR܊�\�v9�\�i����\�\�ȲZ\�\�j�\�G�͌w\��;�O\�\� \Zܺm��0�\�F�+�\�\�~�ZA�\�\�F�t\�<�\�=���,�W#�\Z�ϡ�۵\�u}\nH���u�\�\Z\�$�\�1°\r\�cс�\�\�2Q�<�Yզ�~h�nWm~�մ�\0\�\\\�\�X\����\�}�\�s��Ȩ\�\�oL\�u\�\�Ŷ�+�&\�X��̮.@\�\��ا�aL2�\�5�\�_���O��6��\�-Sg*���\�C�\�=��\�ǽmΌZ��?�\�w\�^\�6�\�Զ�\�%y\'�1c\�>S\�b3Վ�A.��\�ں\�iAņ���\�#c\�*�g��q�Ӧ�׋.���h�\�C�\�<.��r\�Ox�_8:#	\�)p�ch\�\�j�T�DO-n���]j��\\E|��7\�|\�\'f�K(�c\0c֯[80\�{ԏʙ/�|O{k�\�\0�X\��\�q\�TV\�\�-�e�\�j\�Z1�\��q^~%�4\�W.�җ:/^i5]��#?lp\�Ѧ?Pi�9�\��\��Er��\�uΛN&\�\�&T\�{�]V��`�ȢAÅ\�py\�\n\�v�:g�\�\��ܨ�C�}{v�r\�H�ⳕA#8\��#�],2N\�\�=\�[�5\�G�1�u\�֪]�\0\�J$�w�^jĲ�\�HR2;\�k�T���\�AǽH\�;\�E���\�30e\�=1���j*aU\�\����\"	v��8��S#H\�<�\�$c<�I�\�.��Yqҧve����d\�\�m]��銙v\�0͌�S\�@>Q\��	\�=�\�1&w\�\�#9?J�D\�\�g�R\�6\�\�\�ց\Z\��\�9��0\�\�\�Q\�\�\�u눡�-\�\�*#2\��3c?.?�qz�V���\�:�GNMw\Z�\�\�)�\0ڷ���!�+JdHȟJ�\�\�8_\�nA��78\�2=J��\0\n�\�~���G���]^v\��s�?�U����nf\��>^�A��;�^�\��\��\�\"�Q��{{\�\�$��\�H��1\��5΋(ec\�\�\"X���\�g�=k�x�\�\n�6,�(-<&���0]\�<\�\�I0�6)?{��\�^\�\���������^@_k\�Fvʠ\��\��\�p*��8=\�=m-\�\'\�\\99��\�\��-��-ǅ\�4\�ur\�d�����\0�ЀA�\�Z�����\��(KF33\�#\"�0I\n\�k��<$�!Q���[@b��\�\�\�1]\�	5�\�6�\�>\�\�\�lg�N��ss	2\�>�D�\�	4�MeMĀ��\�)�s[D\�\�	���\�+C����#\��9\0w���Hm�簩��Y\�Sy��SH\�\�\�s-m#��@pz\'z\�\�\"1څt!�s�\�W²�\0�lu�\���*\�\'ҹk\�eV6gM:J���\�\�\�m���\�qX\Z(��ʑ�\�\�\��\n��\�+ʒ��\��.�p�{1ʹ��\�/�|~�\�Fq����s̅<H�A\�\��	\�O\�\�\�4\�N���=�\�=\rh��\0LO_���\��O�j>ؾ�go�W<��<�\0\�(��\��\0A�j\�ϔ���4�4rŔ?�}���*\�B@��q\�\�pk�ixe\�W�Ү\�)A�\��?\�P+���K[�`�bHأ\�+sW�vH���#$\�fu\�	\��\�g�\�<b!i\�5x��/\�\r�\�\�\0\�\�G\�i�\��j}�\�\\�8\n\��L[��?u\'\�;l[�%�vn\r��F8\�\���T�S�+5D�\�3`�\0g�ը��0\0h\�_��\�kE��o��\�ؖݢ\�>��\0\�pַp\�\�\�Hr�\�]|$�\"\r\�I~��\�f�)\Z��-h?\�\���5�6[����\0#L���i`�\�)&�ī��\�ު\�Z��\�Am\�fh\�vJ���2p}\�\0�+����\�.\�e\r�d�H�j�\�ϩ\�MV���\�p\�gЃ�\�=��\�aunb��\�W.�*H\�05,\Z���,\"���ĶS\'�\���ׄޣ������\�c��x#\�pj@qYV:����[A6�\01L�ln}\�8\�#�C\�Q�\�`gΙ�\�k)�\0\�j�%�hi\�X\'\�ZX\�kg\�SG����m\�����mU�X\�\�\�y�Cjl\�\�v_Ɉ��\�چl59\�	�\0\\}\��{�erѴ�\�Lᘑ�^Ⱛ�*:	�C�Es\�\�\� X̤rD���f�x�\��\0\�?�+s�l�}��̍��1���Ɠ$$�\�\�dT�\�\�_W�\�\��\0��xz�\�Q�Ie�\"�+�ǜ���\0UrKz\�u\nn1�0���9!\�Ԃuy�\�\'�\0�\'�\��5X�c\n���8�.\�p\��4�88kks�o\�8+%\�\����?�Z�\��:l�q=ϛ�\\ȸ\�9��\�B�3�\�9���\�4�Q<ѫ�q���u\r\�*�#�\�Ї�6 \��*���lV@X\�\��\�H$�\�~uj\��!|�B\r�\r�:�t�\��\ZT6�\�}\�{\�\�Z\�eg�*I#\�\Zcm(\\\�������dR*W���w�0%���\�\�aGb)£\�H<�\�M\�&\�H\�`�ԧ\�W݂\n�3��\�C��y+we�K��\�\�\��\���R�O�\Zy\�1Yl\�+\�\�[$3\�r�[E\�6�V\�A]��P=��u�\0G���&H\�d�\�c��m�|�?���O���z\�\�?���h	\�\�*�H#�168H�r\�繌g�?ʬ�H1�]�����b��U\'\�s��\\\�\��ڲ4�s\���I\��\�\�#=�t��v9��@\��\�srx>�\�#|\�I+�?:zx?O@�͑��G?�J\�K�\������Pdz�.6�\�2!R&�d�EA\�\�\��21�£\�\�\�\�F�u�S�u$�q�zҮ�dDNz+��֦�\�m\�N=iŉ8g߭\0W��)U�@\�\�S���\�\�zt��\0\�~-\�\�\�z*�A��1ޓC8ĳ�\�iQ5\�}�ā\�\��U�i�\0��#k�}ˏ�ˆS�׽ u,z��\�\�6ZlA{k�i&�+�-(�5�sw\�\�PO��IcY�0 �l\�s�\�u8VM\"uP	ʰ�0?\�\�6��\�Cx\�O�\�cgC�E+\�\�{�W�*���r\�`�\\\���z���<�?.+�\�4�o�:$Rd�v&��aZ�VIor\�X\\(h\�w��\�Ȭ��[�д4�\ZI�\�n�\'\�X�˝�\�}h�[O\�(\�!Y�Ǆ,78\�6-5SI�\�(�\�\�2(\�G\\f��\�v��Z,_18<J,1֗v˨_X-\�3�\�-�=\�؂*Ռ\�Z���!YZX\�\�R\�.	 s�}=����G�4ً\�\r�\��U��XjI\��YUq�\�>�1�Іs��&ݶ�.c?Jw�����d\�����yocev�\�\�č��\��T7z*6\�ԃq\�H?Ҵ$E\�\�-�\�d\�?�g��w���t�pz��c�\0e�7�*�w\\9�To\�)�ښg(,�\�\��hjByHf�ӎ9��\�Vl5y�ԄL,����}����=�\0�f��lV=\r\�\'6\�\�kB+��e\�m��2H_䦇�\�\r:�[\�\�^\�p�3�A0x���-\���8�WN\�.e{��\�V�Z�0[���8��\�Xg�CBW\�l\�\�s�ɩe\"��{.8S\�\�V�\�ʫ\�GC�\Z�b\�\�J�tF\�\�<�\�C\��=Jc\�\�\�I\��\0YOsן\��\�\��\�[�� |���G\0�\�ʳ�~�\0N*�.4\�\�\�<\�G�\�\�n��\��h@F\\c��z�i\�0�AڈJ�SԶ	�ɩ\�88^�a`XFz��u=�j;���\�\�N@&�\��z\�Wݟ�\�8㨡-��:	!\�??\��#4\�ҠE�\0T��\�a@�[�Da\���m\��\�n\�H\�$e\\v5��\�\�ח�-\��\�x\�\�9dߜ{�j�x�\�g#�T+g������_��\�!\�;�9�+�\n�\�4&&�/\�Od\��\���\�Uν�N�v�w�?\�ʿ\�\n�\�/C�ޣi\�\�>aX�\�\�ߢ�������fǮ3��4)�\nq�\�|�qU�}\�Iƕ\�\�+��\n�߻\\s\��R[����\�d�\�9?J\�c��}�]%�؛HH��ѯa��e\"ͬ�K�\�b\�*Tr=h[�|D�r\n\�sQ�\�&\��]��b�*�dp�Q\�\�4j\�,J��r)\�\�\�\�l�\�\�\���\��\�\�5���ݖU *\�+F\�p�\�>pDI�+�l\�k�B.5�d%�\�ݿQ�ԁ��xT�J*2\�]g�\"I\r��y�b\'\r\��p\�\�[��8\��\�8�+��+/Kp~�N?\"�\�Wda\0�aӠ�2G\�N�\�E��Ԅc��5d�\�Ě4g\�\�l�\0	\��\0#Q���\�A�E�\0!^^�Q�J�Ê�-S�:rkBOEoh\nH\�\�\��������u�T���\�@�dW�\�ǝ\��jr\�E\�\�+�;W��`Gɧߕ�eE�\0٪6����<���\\��\0\�Mr_g@	t\�*\�\�v�J\�:f��t\�G�D��k�\�\�@�6\�X��(�����\�\�`c\�R\�\�c�j.3^Ox�@ \�(\��B\���Q7�5\�m\�4�>\�~��\���\����0\�\�>#�X7\�Wl\�n0��\0c�\�V.�MB\�{˅R~\�9Q�5]\�\�:�\�C\�<q�hcF��R$��s\�sɭK(�\�YvYDQ�\�V\�29�4E�\���n=z\�2]��S+�\� q�N�\0�X���P���\�`�\�]\�G*\�1�\�\09�3R�0��%�w.0r\�;\Z\�3\�\�\�F\��a�~5\�BD0O4ܰ,\�\�\��6����\�\�\�I9���+�\�	�� 6\�vј5��cH\�Ȥ)#:Uɒ\'\� -<���$8<٪\'UX@\�\0\�溯?��	u\�\�m�R\�#l\�J_�����¡�ӹ���\�	.\�4�$�\�s\�\�eZi��q\\A�H�B\�DbPs\��`���[H� \�F�w��\�&\�d��R��pg��i 6|Q�}�XK\�h�1\�B�\�c��c\�\�\n\�\Z4��c�\�\���-Ů�� ��䭲\\*G �.��#\0d9\�Y\r�$���v^����[\�M3/��}�?\�\�Ht\�h�\�\�;N{.?�cE��١�\\�\0{y�yit\�O \�t_\�b.	�@Q,\�˯_ʤ]WH��f����[G5Z<�B��D�c��\0���4y�\�w�����p)��>�\�Xݱ\'\�º��\0\"�i��5��K1�h\�x�/�q�?�Fm\�T\��\0^��;kfc��Wx;�\�\�\�I��N\�v�\�f\�8\�;�d�\�\���~\�-�\0\n�cu��\�;{�9\�X\���P�<~U5�\�Mw\0�\\\��(v�ȩ)0�	�\�V,��ձ�\"�O�����\Zˑ�6	S�\�EZ�yn,�ܣ�#\�\r��I#�\�M&\�&h�\�ia|\�7e#=NMDn\�t��5kVV�\�\��d*.�I\0>�[��g\��!�ps\�9�*\�\"\�18\�s�2s���6�\�N0x9\���\�r\�\�\\v4.c,\�\���)�Y!,\n\�P�T��1މ.\�8\�y\�\�S\�_aVc�1<�\�= �0�\�P*���NG\\�7po�,\�\�z\���\\\�r\0&���4��2���B�\���+��v IϥD[<�j�;�*<��\'UrG�5���\� Mw�8�A�\�\�1n\0\�\�j6#E\�\�x�O�eG8rB�^���BeF;��:��m$tA\0�\�N*X%\�\�\ny棒\�\�\"81>\�\�c�\�M��x9\�\'?�)1�u�ݡ��uʶнs���^��8b$L3�\�9�$\"6?6ry�q]yg%�\��\0�RI���2лpGl*\��@\���\�\�\�\\��ʟ�8o½*\�\�}\�\�:\�\�H`RE\�r�\�\�U&d\�\0�	�R�\�\�\�zf\�j<\�$\���Uܓ\�\�u\�~t�\�\�J�\�\�t\�%8�m[��U9����\�J��\�id?>��\�Z�g\�a���i\��OSң�<\�o4�3ϵ\0<恏�Fe�=}*\�֧�6�M��H\�\�j\���4������Ә\�F\��\�Z?`�G\�\��\�O�\��$�\��z�h�\�#�}\�\�\�5z2Fps���-b �\�c$�U$*A\r�+;��1JF�����\�h\�P�N%R@\�9����\���ƢF`���^9\��\�P\�\�\�+\")(\�_(�\���ʑ\�\�\�q#n���f0����5��Ik8H�\�y?zK�\�@�\0W\"��i�\0�r`��4�7\�\�MQ\\\�_ۈ\�%��l\��\�\�i_A4�DH��DG\�\�\�\�V��%6����\\Kn���(���\�9���i�\�WUn�ܙI�\�9\�#o\�U��]\"\�W�ky\�{��`.Kp~oa\�j��j��\�P8F���\��\�\�\ZriS%\�\��\���G\'֤\�\�4��d\��\�uFv�D�*�˸\�׀+�>(�K\�ŧ�p~�\�C�H�T#h��\�\�\�q\���Y� d_:�0WhO�>\�\���rkڣ�H\�`��\'�\n����,��\��.r0	\�=\�di�1�\�O�`*)D�\��c\'����3VA�\�j\��K~W��9��UF����\�NH\n\�?,V�\�t8��Yq�\�-�\�☔&\�\�\����B-2\'�)73�c�v�\�<\�ڵ!\�\�å��\�T\\\�e�6�0�֚G$ɜ~B����\�0�� d�X~dzSV\�\�\�\�W�l�\�,����\�&��a�\�#\�5V_��G�\�`,��\�~�QOP�������\�uD�{��Y$M�\�ʞ}2=�T�3j\�O��3��bz\���Y��\�6ݮ-�Q�\��?�f9&���.\�\�D�Y#$�\��kR+K��\�u(\�\r\�<�z\�ȸ�$�\�\�Y\0-kp\�F:+�\�Ω�Rx\�9��ht\�-5+W�.\�R	�0#?�\�T\�Zhdՠ,F6\�\�I�4\�\�\�\�\�Ԩ!�q�ýG,�WeGrq�U\�\Z4l\��\�n\�#���K\���:l�#*d\��&�Z�\�\�\�\������>��n[i\��\�7�l?/^ߥT�U���E���<g�F�WR�\�ӶFm\�}�ƀ-_\�\�~D�^(eq�G�I����2��\�\�4�#�$�&F=x\�n]\��\�\n\��\�֡�̮Ȭw�x�\�s�ɠ\�G�\�\�z��oqr�5�\0XڶN	\�p@\'g\�Y�H|\�`��\�\�S��O�k֤�d�J|�d;c8�U��{~Q\�\\����\ng�\0c\�Yq��\��\�I\�\�3�M*\�]퇄z�|SJ+&\�\�-�\�\�n��\�m>&�w�o\0���Qm�ۄ)$lx\�!��g\�y�SD�a6�\�\�fc�Z0\�\�\�o��#\�Dbق�NG̀s\�\0�֋i6.ٷ[\�A\�s)C�>�\�\�\�\����|\�d\��T����)e��\�\�U2�$\�4}8�&��Co&I\�\���6�\�\�ӜU���v�Dc�pq\����c6)�!h\�R\�\�9΢�K�\'\r�*w0\rk<\�7&\03�0R��\�Mh\�_�\�\�w�Z�`h�\�\�E�\0\�+�\�#�\0�Z:�{k�\�i=��\�\�)v���p9��\�DR�HB�g;Il�;MO\�\�L�DL�ݝ�#�\�\�:�yj�#B\���I�1\�\�U�lmn@�k��W?�Nq\�Mq�o4Fr8\��\0?Zs]C\n\�\�\��t\�;qڟ8#�H�\0\0\�2\�\�<��Z�e�!Y��S\�X@ɐu\�]D�lf �?\�\�~��\�zS{j��3�jh��E\�\�I��h�K��yؤ�����bH*}	?�BڙPA�g\0����\�^�r�P����\�NL��)Ȅg\��X�q~\�;�����\n`�=�`I\�d�\���t;S�O�g�\�i�\�\�%�f�H�:��s��aPz�bG���5��>`p@F)64uQ\�#�;�)�*\�<Tuϧ��[\�,\��\'�=+���+��G�q\�~�*u\0g\'��\0F\�P�\�+D\�*N\�۱�Ҳ��\�=\�1�\�d�\�wz�)c���\'����ۼn&�\��\�7\�C�\�hm׊�`NH)���<\�;�&mڭ�0\�ɟ�d��!o:\�2F\�f>R9\�\�Q��:\�Asj\�D�\\\'�~}*���\"�\"��E\r�SQEͭϛi3F\�3�[\�#��\�\�d�N�ApUP���\0֬�\�v5cL�H�\�m\�ev?�\�sǷ\�J\�\Z6�ޣy�C\�Q\�8��\�\�ze���\\\�\�n�\�|\0\� }}\�\�K\�\"�d�\�	\n��\�\���-��`�<�\\ȬUH��\�\rKCL\�\�\�\'[�.V�b+\�!p\0���~W3\�+Y�\�7\�ڄ���TH\0��\�\�ֺ(\�{��f6[�KgZo�/4�-ZF�0G$�0,H\�Ҧ�\�pɥ\��,\�^b9o���tG<�&\�\�\�Zm\�K@@�J�\0�\�2G\�*��(��H\�\�\�\�B�\03Z�6=\Z\�2L*�͑�\����Q蒞~\���\�$��V|���\�\�i��D�\�U���\�#rg��0\�@\�F��\�\�\0v��\�\�O\�P\r\�\� 4�~��\�[�\"&[\�0Q�c�\�xm�e\���q�,O?R(�{�ג-�\�2\��\�\'�ޔ\�bH��x*�ۇ\�Ŋ\�WGY7�\���Z�鷅B�Q=\�������ՎM�\�c� ���\�\��Sj7�\\&\�_1	0���x�K�\�\�w��\�\��\�\�V--�cw>j\�)�L\�#8\�R\�\�lZ�ݷp�<�b���\0��Ju�S�m�dg�Ǿ\�t����ݝ\�x\�ޣ�_.\�E��:�\�S�\�F�$��~��\�G�0f�/+i*�c8\�&ˉ8�@��0\�R�E��9\'�ˊ\�W(Nw(,p�Џ!�\�n�w5Pg��r6\��j\�[E%�g��is\"[!3:�O4�@\��\�w<��hÀ�w\�\r��q\�9�=�>�,R�Le�![\��O�y\�&\�K\�u\��l�c\���\�Cf�\�v�a\�r?\�J�t��U�ms1H\�\�)9$g��>��y\�\�\�,�H@\�s�\�g�oÚ\�&i\��Nf��3�v\�Y\�Bls�O3�+�7$�;sҘ\�	Ü(\�?�j#�\���x$\�ǧ\���\�&3�\�؎+&Ad�h�w;2��ӭ!�����caP\�2�?(\�\�\�q�&�æ\"��N\���\�KQ\�.&*�2E\�K\r��\"\� cé4@c��N\�2�\�]7�[�?CS#lO�a\�`��ӻ�{1TI	x\�\��\�;\�\�f\r�2�G\�\�q뎕2�$���3�}\�W�\�O\�\�$M�\�\��\�c\�qH��ǿ��\��\�l�\�aH\�9<\�J�\��,���`Ah��8\�U�ʷn\�$�\�\�@��yG\"�\�\�ʆX\�i��b� �3Ohwl\�U\0�\�\�=?Z\�#�N\�\���\0��-s��퐏PH-\\\�ʜ�㿭G��Iܪy��4��7s\����ۈz\�mr6��@��U\��=B\�K\�c:CԳ@�{u\�T\�K�`\�IC|\�8��*2i2\�4\�\�y\�R\�\�0\�`7d�\�Q�Ҥ�$r��\�L�~\�.<���$�u!e\n9���\�Ҹ\�P�\�\��9\�1E�\�¯|�\"�I�a�X�H\�5\\���ݐ�ӭM\�\�t�\�@U\�\�Ө�\0&�3�n��u\�S\'\�\�\�\��\���W\\;�\�s\�~��\"�٢��F#�\�*ݦ�qmp$C�^�=��4�\�VP�R�(\�\�E^�<S�\�\�L\�\�ԑp|���\�~����ğ�׵y�K5��\"�\06n>��괍qn\n�r�&8���\�F�s�\�:\�uMl�(\�ӂ0z}\r^X�/PG֚\"\�I\\{�cHfGٞ>p̀}�\��\�\���K-���Q�b2c�H\�\�{�j\���\�\�:�9B��A=x\��\0��\�u�q�ĳ��<\���sZ������2� ?x\�\��]��ar��S�~l{��sYw~���^Y(�p�\�\�9\�\�\�?^hC�j	略�>\�1\Z�1�c�s�*ͬ,�P\�-\��\��s�\�\�T-R,Em{=ʹʀlwr?ʴF�M��#/\�Y�\��\�\�7��\�f��\�\�#\�}\�$�	\�Г\�&�\�	5\�ŕ\�@�\�(�3�1\�cޒ\�KX/V\�|\�\�]�\�t\��@$��]��bq\�~z\�kI\Z=�4���c\�\�u\�W9�O�\r�ܷ%\�G�\��\0P*\�ڋ��\"\'�\�\�T�\�/w���\�kTgr\�:$L~{�\'�\����lcvi%�AІr9�+\"K\�\�N<\����(\�g�\�\�Ns�LGK�&\��G�ܜ�\0�Z��aS�U�\0�W/\��\�jF�\�E�r�\�(��sr]z6,D;��j�\�\�\��\�T\�q�N>��\��[we�+��ըtmBS��\�͎?@\�WP���#*���\�5\���� u\'<Յ\�fR\�J��#?Rj��\�c\�\\8Im��QJ\�\nv�diZy\r�g�I��Ƶ\�u�-�I�\�p�\"\n���?�U}x�m\�T�!�9�aO�\�H�� K�g�\��>\\*�r��$�1fI\�c\��*�x�ݜ\��1�?֭]h�\�#\�\�%\�?�qYW\ZD\�\�ˇ^����j\�N�\�I=\�D>T`$����o�\�\�\�7#\�\�`B)�	�\0-7<��If\��v\�q\\�R\'r�2\�\�+\�\0\�ܷ�N�%�\�\�\�*i�\�p�9<|��	Cb2S�[?��M�o$�H|b�\�0���B?P���\�R\�\�,�\�#�b�S�ˡ ��?\n,S`ޞb�\�\�\�4ɱ\"@;�\�?^��$?x!r2}zS\�&9\Z;�_.3\�-�O�\�\�\�#G�v�\0__�1de\\�\��/\�3:�F8X\�{�\0�\�\�\�AUl�\�\0t�q\�+�Zhͱ���?1ʌ\�\\dRB�1��\��\�ɓ��(3�)3�\�2O�<\� \�C�\�\�{В�Wf\�S�����QI\�LOs�?\�)�\�;�\�9 �r�̔hHb[�\�\�ҟn�\��a.�\�s%H`q�d��\0\�O\n\��qK�F�\�J�\�\�\�Nx#�\ZC�\0|\�\�Y\�\\\�㚎-�>tʍ\�w�g?\��F��Q�\�\�h\0�})�\�D��z�\�m\�=\�֝�H\�U]���n��jR]Q�6�H$�ҡ\�r�P\�{ӷ;`,����q�ҙтY�rJ�#ׂz\�N]�c�9\�X�S��\���RdQ�%�\'\�?\�NX\�#f�U\�I�#�2\�\�,\"g\0D�\�a�g\�TK\0\��\�\���q�\�&-�F�\�-ٶfPC0\�4��H\�D\r�\�T$c�~�}i�n\�\�T@�x\�9Z<9S\�IV��\0=i�\�ҫ\"�eX���T�C1\�qVc�)9�\�X\� \0�\�-۹\0~u\Z��\�#�?\�Le�U\\��dr��\0AH\n�\0�YO\��z��\0�4D�FD���&�\�*\�F#�v�N\�\�\����\��\0�s��H���(�bn�\�3\�F��\�@E\n\��`�8\�*㲣�#�@��u��\�C:g��?^�\���\�\�)E�\����Һ�\�J+�\�kˮ$A(hm\�\�j�O#\�\��*\�ڝ\�@\�I�\�|\�6�\�zu�\�LkC���Г�R��ʙ��?*�}^\�\�fI��\�c��ÓV��\�|���\n\�.��=�2A�(�\\쮧\�Pf{�e6C\�8���\�N\�SܱM&\�~S(PX~\��\�L\�&\�#�F&L��?\�kN[�v2�\�q!UQ�\�+ӿ���w\�\�4m\�k*^\�Cڪ\�i\n\�j\�(�W9֩\�^)�;\�\0�`3�\0}\n蝼\�àdpP��!�n��g\�%[�)!WU\�9�u�����\�\0��u\�y?\�W��d\0΁�9�J�ޫ$YGA\�\0Nܔ]��I��\�S˭ʾ�bJ\�0�\��\0u#\�O{K\�\0_�\�л�td\��D6z)\�_�\�J��\�4����D\�\�]\Z�\�\�\�s�Td�g.�ff\�4�4�Ġc�ֶm\�y�q	J����\�Φ\"v,c\'�a��h���\�l\"\�\"\�\���n�j�ئ\�mE\\r8 �ҤYe9Q\�8\��t�����\�񞥐eO\�9\�\0� �Ǫ��\�\�6�I\�\n���*Fx���m?ğ�~��*�%��+`�ʘD�|\�,:\�1H�$\� ї\0� \�;��\�\���+ǽ7{΄���>��\Z�ۊ�Ou8\��t�i��e��<��\�\�S\"�\Z\�\\�8�|~��\0:Y$0�0�+�?�\0\ZM�	O�\�4�G�G?�45ϗ�WW\'��+�\�R���B	86A��9W\�R\�\����\�v�i0 �\�\�\n<ؕ׏�8?�R�\�m�\�\�>�~��\�W܆\�4B;�LX\�\�p)ꑎ|\�Vs�\���m��_J\��n\'�\0�\�U\r�\�9�h\�I=/\�]!�O5cK�\�\�!1�?B8�</)\��\�zH@#\�V��\�Z\�Mۗ\'#\�%�+�WrNOҺ)l\�\\\�d��\����\0>��Ii�w�+\�\�\�${c��Rh%\�)��`s�\���Uy��8\�>l���g�j,�\�Yv}\�GޠhZN$�\n�Ε�C/\�+��!9\�9j�\�\��#\0�x\�c�5~d\�gH\�q�h��9\��\��P(��\�F@\�\�2H\�A�C&@\�2G�5��\\(/��h\�C����\�Q����\�?�!�\�\\\�L�\"\�t{#\�\�\�\�W��/9!	��F*!��o\'��1E�b�B]�t|�|\�\�\���M�\n�\�\�`H`�\�\�\��\�j�_la~\�\Z�\�\\q�\nc$��ͅ	\�\�\�8�4�l#�)c���B�,{���}�\�ʌ�|\��3��7�	\�qM6�p�`g4\�b�1cҬC:�\���\��\�&o��ͅ�s�2{*\�ٖ9898\�\�\�\�\�E�Yz��#\'\�\�OI#\\�O9H�\�#\�\�\�ފ)�(����͓\�8۞UIZ94,��4QRè\�\�&C\�RCg��\�Y���&0v/}��4QP�gi|���\�ۜ\�}iX��FQ3�֊(E��\"���[��ʡ��@V�O]\�\�E����IX\�˲#&r;V+ޠ��ٔʱ�\�(�\�&>+�0$��0\0�y\��\�V#�\��+� \�=ǡ��d�E0	�\��\�\�=)d�\�\�妒\�Y\�?rF\�>��Re�ޡ[D\�)C��\�Ǡ�\'\\�\�\��\�	\'1�O\�Ҋ*G\�\�\�\�r\�\�TabTΟ+c$u\�֊)�%I<!\�\�7\�\�ڟo(��\�\0�\�׏z(�\�5d�\�(ڡ��2q\�1��e-T\�Uݙ{�\0�B�({\0\�K\�BY�)+r�i�\\8��$f�NG\�?�Q\�e�FI`�j��N9ϭ@�\�B5W;\�\'�c�\�E6\"\�Z\�|���Z�\�Y-\�\��~a\�����+\�\���ؐnh��o\�r*ͼ0�(0�\��P:*! \��ڊ8\�\�VłO\�O�\0I\�\�`er�H\�\��\�;=�H\����\��\��QE \'1(�,\�ݙ�\�U�۔=���\�E\0U.dvFfܧ\�\��*چ�\�\�\�p?*(�{\�`� \�B��}��±���l\�Z\�/�/\�!�7c�\�g?�h��\n�.��2B\0\�9\�j)t�Gp�k�g�f�)�<*@\nJ��qI�4,\�V\�Z(��\�\�-\�&q\�1\�<��@�0\���\�R\Zl\���<���H���zQEP\�\�΄䁎�$j�~�$�\�E�\�',0,'2018-02-19 09:08:04','express_user@%',NULL,'2018-02-19 09:09:23','express_user@%',NULL);
/*!40000 ALTER TABLE `doc_news` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_news$before_ins 
BEFORE INSERT ON doc_news FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_news$before_upd 
BEFORE UPDATE ON doc_news FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_operation`
--

DROP TABLE IF EXISTS `doc_operation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_operation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `office_id` int(10) unsigned NOT NULL COMMENT 'Номер отделения. Ссылка на book_office.',
  `transaction_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на doc_transaction',
  `box_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на doc_box',
  `amount` decimal(19,2) NOT NULL COMMENT 'Сумма операции',
  `commission` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Комиссия системы за операцию',
  `state_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Ссотояние операции. Ссылка на book_state',
  `pay_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Дата и время операции',
  `pay_comment` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  KEY `fk$doc_operation$state_id$book_state$id` (`state_id`),
  KEY `fk$doc_operation$box_id$doc_box$id` (`box_id`),
  KEY `fk$doc_operation$transaction_id$doc_transaction$id` (`transaction_id`),
  KEY `fk$doc_operation$office_id$book_office$id` (`office_id`),
  KEY `fk$doc_operation$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$doc_operation$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$doc_operation$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_operation$office_id$book_office$id` FOREIGN KEY (`office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_operation$state_id$book_state$id` FOREIGN KEY (`state_id`) REFERENCES `book_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_operation$transaction_id$doc_transaction$id` FOREIGN KEY (`transaction_id`) REFERENCES `doc_transaction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_operation$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Операции';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_operation`
--

LOCK TABLES `doc_operation` WRITE;
/*!40000 ALTER TABLE `doc_operation` DISABLE KEYS */;
INSERT INTO `doc_operation` VALUES (1,34,NULL,16,550.00,0.00,1,'2018-02-28 12:43:00','орор',0,'2018-02-28 12:43:30','express_user@%',NULL,NULL,NULL,NULL),(2,34,NULL,16,777.00,0.00,1,'2018-02-28 12:43:00','ороророро',0,'2018-02-28 12:43:42','express_user@%',NULL,NULL,NULL,NULL),(3,34,NULL,16,33.00,0.00,1,'2018-02-28 12:43:00','ииии',0,'2018-02-28 12:44:01','express_user@%',NULL,NULL,NULL,NULL),(4,34,NULL,18,456.12,0.00,1,'2018-02-28 12:44:00',NULL,0,'2018-02-28 12:44:21','express_user@%',NULL,NULL,NULL,NULL),(6,34,5,18,146.00,0.00,1,'2018-02-28 12:58:00',NULL,0,'2018-02-28 12:58:34','express_user@%',NULL,NULL,NULL,NULL),(7,34,3,18,148.00,0.00,1,'2018-02-28 12:15:00',NULL,0,'2018-02-28 12:59:10','express_user@%',NULL,NULL,NULL,NULL),(8,34,7,19,190.00,0.00,1,'2018-03-01 06:29:35',NULL,0,'2018-03-01 06:29:35','express_user@%',NULL,NULL,NULL,NULL),(9,34,8,19,10338.84,0.00,1,'2018-03-01 06:30:19',NULL,0,'2018-03-01 06:30:19','express_user@%',NULL,NULL,NULL,NULL),(10,34,5,19,171.84,0.00,1,'2018-03-03 08:51:31',NULL,0,'2018-03-03 08:51:31','express_user@%',NULL,NULL,NULL,NULL),(12,34,15,18,120.00,0.00,1,'2018-03-09 12:10:00',NULL,0,'2018-03-09 11:27:40','express_user@%',NULL,NULL,NULL,NULL),(13,34,15,19,144.80,0.00,1,'2018-03-09 11:28:17',NULL,0,'2018-03-09 11:28:17','express_user@%',NULL,NULL,NULL,NULL),(14,34,12,19,150.00,0.00,1,'2018-03-23 11:44:15',NULL,0,'2018-03-23 11:44:15','express_user@%',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `doc_operation` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_operation$before_ins
BEFORE INSERT
ON doc_operation FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_operation$before_upd
BEFORE UPDATE
ON doc_operation FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_rate`
--

DROP TABLE IF EXISTS `doc_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Ссылка на book_office_group',
  `exchange_direction_id` int(10) unsigned NOT NULL COMMENT 'Направление обмена. Ссылка на book_exchange_direction',
  `is_cash` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-наличные, 0-безналичные',
  `is_retail` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-розница, 0-опт',
  `buy_minus` decimal(65,30) unsigned DEFAULT '0.000000000000000000000000000000' COMMENT 'Нижняя граница диапазона для покупки.',
  `buy` decimal(65,30) unsigned NOT NULL COMMENT 'Курс покупки',
  `buy_plus` decimal(65,30) unsigned DEFAULT '0.000000000000000000000000000000' COMMENT 'Верхняя граница диапазона для покупки.',
  `sale_minus` decimal(65,30) unsigned DEFAULT '0.000000000000000000000000000000' COMMENT 'Нижняя граница диапазона для продажи.',
  `sale` decimal(65,30) unsigned NOT NULL COMMENT 'Курс продажи',
  `sale_plus` decimal(65,30) unsigned DEFAULT '0.000000000000000000000000000000' COMMENT 'Верхняя граница диапазона для продажи.',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `create_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user_id` int(10) unsigned DEFAULT NULL,
  `create_office_id` int(10) unsigned DEFAULT NULL,
  `change_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `change_user_id` int(10) unsigned DEFAULT NULL,
  `change_office_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$doc_rate$office_id$exchange_direction_id$is_cash$is_retail` (`group_id`,`exchange_direction_id`,`is_cash`,`is_retail`),
  KEY `fk$doc_rate$exchange_direction_id$book_exchange_direction$id` (`exchange_direction_id`),
  KEY `fk$doc_rate$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_rate$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_rate$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_rate$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$doc_rate$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_rate$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_rate$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_rate$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_rate$exchange_direction_id$book_exchange_direction$id` FOREIGN KEY (`exchange_direction_id`) REFERENCES `book_exchange_direction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_rate$group_id$book_office_group$id` FOREIGN KEY (`group_id`) REFERENCES `book_office_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1850 DEFAULT CHARSET=utf8 COMMENT='Курс валют';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_rate`
--

LOCK TABLES `doc_rate` WRITE;
/*!40000 ALTER TABLE `doc_rate` DISABLE KEYS */;
INSERT INTO `doc_rate` VALUES (933,44,2,1,0,0.000000000000000000000000000000,68.500000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,71.400000000000000000000000000000,0.020000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2018-02-15 11:33:05','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(934,44,2,1,1,0.000000000000000000000000000000,68.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,72.900000000000000000000000000000,0.000000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2017-12-09 06:30:32','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(935,44,5,1,0,0.000000000000000000000000000000,31.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.300000000000000000000000000000,0.020000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2018-02-15 11:33:05','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(936,44,5,1,1,0.000000000000000000000000000000,30.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.900000000000000000000000000000,0.000000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2017-12-09 06:30:32','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(937,44,6,1,0,0.000000000000000000000000000000,1.150000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.200000000000000000000000000000,0.020000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2018-02-15 11:33:05','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(938,44,6,1,1,0.000000000000000000000000000000,1.140000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.200000000000000000000000000000,0.000000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2017-12-09 06:30:32','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(939,44,3,1,0,0.000000000000000000000000000000,2.170000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.330000000000000000000000000000,0.020000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2018-02-15 11:33:05','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(940,44,3,1,1,0.000000000000000000000000000000,2.170000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.230000000000000000000000000000,0.000000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2017-12-09 06:30:32','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(941,44,1,1,0,0.000000000000000000000000000000,58.900000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,60.820000000000000000000000000000,0.020000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2018-02-15 11:33:05','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(942,44,1,1,1,0.000000000000000000000000000000,58.080000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,61.900000000000000000000000000000,0.000000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2017-12-20 10:40:45','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(943,44,4,1,0,0.000000000000000000000000000000,26.650000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,27.600000000000000000000000000000,0.020000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2018-02-15 11:33:05','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(944,44,4,1,1,0.000000000000000000000000000000,26.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.900000000000000000000000000000,0.000000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2017-12-09 06:30:32','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(945,44,2,0,0,0.000000000000000000000000000000,68.500000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,71.400000000000000000000000000000,0.010000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2018-02-15 11:33:05','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(946,44,2,0,1,0.000000000000000000000000000000,68.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,72.900000000000000000000000000000,0.000000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2017-12-09 06:30:32','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(947,44,5,0,0,0.000000000000000000000000000000,31.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.300000000000000000000000000000,0.010000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2018-02-15 11:33:05','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(948,44,5,0,1,0.000000000000000000000000000000,30.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.900000000000000000000000000000,0.000000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2017-12-09 06:30:32','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(949,44,6,0,0,0.000000000000000000000000000000,1.150000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.200000000000000000000000000000,0.010000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2018-02-15 11:33:05','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(950,44,6,0,1,0.000000000000000000000000000000,1.140000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.200000000000000000000000000000,0.000000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2017-12-09 06:30:32','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(951,44,3,0,0,0.000000000000000000000000000000,2.170000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.330000000000000000000000000000,0.010000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2018-02-15 11:33:05','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(952,44,3,0,1,0.000000000000000000000000000000,2.170000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.330000000000000000000000000000,0.000000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2017-12-09 06:30:32','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(953,44,1,0,0,0.000000000000000000000000000000,58.900000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,60.800000000000000000000000000000,0.010000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2018-02-15 11:33:05','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(954,44,1,0,1,0.000000000000000000000000000000,58.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,61.900000000000000000000000000000,0.000000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2017-12-09 06:30:32','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(955,44,4,0,0,0.000000000000000000000000000000,26.650000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,27.600000000000000000000000000000,0.010000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2018-02-15 11:33:05','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(956,44,4,0,1,0.000000000000000000000000000000,26.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.900000000000000000000000000000,0.000000000000000000000000000000,'2017-12-09 06:29:40','express_user@%','2017-12-09 06:30:32','express_user@%','2018-07-18 07:12:45',NULL,NULL,'2018-07-18 07:12:45',NULL,NULL),(1414,49,1,1,1,0.100000000000000000000000000000,58.260000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.210000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1415,49,2,1,1,0.200000000000000000000000000000,61.212500000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,62.300000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1416,49,3,1,1,0.300000000000000000000000000000,2.050000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.130000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1417,49,4,1,1,0.000000000000000000000000000000,27.560000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.120000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1418,49,5,1,1,0.000000000000000000000000000000,32.120000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.690000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1419,49,6,1,1,0.000000000000000000000000000000,1.160000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.201000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1420,49,1,1,0,0.000000000000000000000000000000,58.102000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.200000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1421,49,2,1,0,0.000000000000000000000000000000,61.200100000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,62.150000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1422,49,3,1,0,0.000000000000000000000000000000,2.010000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.100000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1423,49,4,1,0,0.000000000000000000000000000000,27.305000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.100000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1424,49,5,1,0,0.000000000000000000000000000000,31.950000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.590000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1425,49,6,1,0,0.000000000000000000000000000000,1.150000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.190000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1426,49,1,0,1,0.000000000000000000000000000000,57.890000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.200000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1427,49,2,0,1,0.000000000000000000000000000000,61.590000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,62.360000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1428,49,3,0,1,0.000000000000000000000000000000,2.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.150000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1429,49,4,0,1,0.000000000000000000000000000000,28.230000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,29.030000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1430,49,5,0,1,0.000000000000000000000000000000,31.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,31.990000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1431,49,6,0,1,0.000000000000000000000000000000,1.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.150000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1432,49,1,0,0,0.000000000000000000000000000000,59.120000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.950000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1433,49,2,0,0,0.000000000000000000000000000000,63.230000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,64.250000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1434,49,3,0,0,0.000000000000000000000000000000,2.150000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.180000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1435,49,4,0,0,0.000000000000000000000000000000,28.360000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,29.100000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1436,49,5,0,0,0.000000000000000000000000000000,31.260000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,31.990000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1437,49,6,0,0,0.000000000000000000000000000000,1.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.240000000000000000000000000000,0.000000000000000000000000000000,'2018-07-21 08:57:38','express_user@%',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL,'2018-07-21 08:57:38',NULL,NULL),(1517,0,1,1,1,0.100000000000000000000000000000,50.260000000000000000000000000000,0.100000000000000000000000000000,0.000000000000000000000000000000,50.210000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1518,0,2,1,1,0.000000000000000000000000000000,61.212500000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,62.300000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1519,0,3,1,1,0.000000000000000000000000000000,2.050000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.130000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1520,0,4,1,1,0.000000000000000000000000000000,27.560000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.120000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1521,0,5,1,1,0.000000000000000000000000000000,32.120000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.690000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1522,0,6,1,1,0.000000000000000000000000000000,1.160000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.201000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1523,0,1,1,0,0.000000000000000000000000000000,58.102000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.200000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1524,0,2,1,0,0.000000000000000000000000000000,61.200100000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,62.150000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1525,0,3,1,0,0.000000000000000000000000000000,2.010000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.100000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1526,0,4,1,0,0.000000000000000000000000000000,27.305000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.100000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1527,0,5,1,0,0.000000000000000000000000000000,31.950000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.590000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1528,0,6,1,0,0.000000000000000000000000000000,1.150000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.190000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1529,0,1,0,1,0.000000000000000000000000000000,57.890000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.200000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1530,0,2,0,1,0.000000000000000000000000000000,61.590000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,62.360000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1531,0,3,0,1,0.000000000000000000000000000000,2.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.150000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1532,0,4,0,1,0.000000000000000000000000000000,28.230000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,29.030000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1533,0,5,0,1,0.000000000000000000000000000000,31.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,31.990000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1534,0,6,0,1,0.000000000000000000000000000000,1.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.150000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1535,0,1,0,0,0.000000000000000000000000000000,59.120000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.950000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1536,0,2,0,0,0.000000000000000000000000000000,63.230000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,64.250000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1537,0,3,0,0,0.000000000000000000000000000000,2.150000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.180000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1538,0,4,0,0,0.000000000000000000000000000000,28.360000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,29.100000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1539,0,5,0,0,0.000000000000000000000000000000,31.260000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,31.990000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1540,0,6,0,0,0.000000000000000000000000000000,1.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.240000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 07:47:41','express_user@%',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL,'2018-07-31 07:47:41',NULL,NULL),(1747,53,1,0,0,0.000000000000000000000000000000,59.120000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.950000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1748,53,1,0,1,0.000000000000000000000000000000,57.890000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.200000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1749,53,1,1,0,0.000000000000000000000000000000,58.102000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.200000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1750,53,1,1,1,0.000000000000000000000000000000,50.260000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,50.210000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1751,53,2,0,0,0.000000000000000000000000000000,63.230000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,64.250000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1752,53,2,0,1,0.000000000000000000000000000000,61.590000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,62.360000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1753,53,2,1,0,0.000000000000000000000000000000,61.200100000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,62.150000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1754,53,2,1,1,0.000000000000000000000000000000,61.212500000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,62.300000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1755,53,3,0,0,0.000000000000000000000000000000,2.150000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.180000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1756,53,3,0,1,0.000000000000000000000000000000,2.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.150000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1757,53,3,1,0,0.000000000000000000000000000000,2.010000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.100000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1758,53,3,1,1,0.000000000000000000000000000000,2.050000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.130000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1759,53,4,0,0,0.000000000000000000000000000000,28.360000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,29.100000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1760,53,4,0,1,0.000000000000000000000000000000,28.230000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,29.030000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1761,53,4,1,0,0.000000000000000000000000000000,27.305000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.100000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1762,53,4,1,1,0.000000000000000000000000000000,27.560000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.120000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1763,53,5,0,0,0.000000000000000000000000000000,31.260000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,31.990000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1764,53,5,0,1,0.000000000000000000000000000000,31.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,31.990000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1765,53,5,1,0,0.000000000000000000000000000000,31.950000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.590000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1766,53,5,1,1,0.000000000000000000000000000000,32.120000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.690000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1767,53,6,0,0,0.000000000000000000000000000000,1.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.240000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1768,53,6,0,1,0.000000000000000000000000000000,1.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.150000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1769,53,6,1,0,0.000000000000000000000000000000,1.150000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.190000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1770,53,6,1,1,0.000000000000000000000000000000,1.160000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.201000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:15:14','express_user@%',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL,'2018-07-31 12:15:14',NULL,NULL),(1778,45,1,1,1,0.000000000000000000000000000000,123.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,452.000000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1779,45,2,1,1,0.000000000000000000000000000000,68.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,72.900000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1780,45,3,1,1,0.000000000000000000000000000000,2.170000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.230000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1781,45,4,1,1,0.000000000000000000000000000000,26.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.900000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1782,45,5,1,1,0.000000000000000000000000000000,30.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.900000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1783,45,6,1,1,0.020000000000000000000000000000,1.140000000000000000000000000000,0.020000000000000000000000000000,0.020000000000000000000000000000,1.200000000000000000000000000000,0.020000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1784,45,1,1,0,0.000000000000000000000000000000,58.900000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,60.830000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1785,45,2,1,0,0.000000000000000000000000000000,68.500000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,71.400000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1786,45,3,1,0,0.000000000000000000000000000000,2.170000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.330000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1787,45,4,1,0,0.000000000000000000000000000000,26.650000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,27.600000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1788,45,5,1,0,0.000000000000000000000000000000,31.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.300000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1789,45,6,1,0,0.020000000000000000000000000000,1.150000000000000000000000000000,0.020000000000000000000000000000,0.020000000000000000000000000000,1.200000000000000000000000000000,0.020000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1790,45,1,0,1,0.000000000000000000000000000000,58.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,61.900000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1791,45,2,0,1,0.000000000000000000000000000000,68.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,72.900000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1792,45,3,0,1,0.000000000000000000000000000000,2.170000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.330000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1793,45,4,0,1,0.000000000000000000000000000000,26.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.900000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1794,45,5,0,1,0.000000000000000000000000000000,30.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.900000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1795,45,6,0,1,0.010000000000000000000000000000,1.140000000000000000000000000000,0.010000000000000000000000000000,0.010000000000000000000000000000,1.200000000000000000000000000000,0.010000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1796,45,1,0,0,0.000000000000000000000000000000,58.900000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,60.800000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1797,45,2,0,0,0.000000000000000000000000000000,68.500000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,71.400000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1798,45,3,0,0,0.000000000000000000000000000000,2.170000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.330000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1799,45,4,0,0,0.000000000000000000000000000000,26.650000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,27.600000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1800,45,5,0,0,0.000000000000000000000000000000,31.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.300000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1801,45,6,0,0,0.010000000000000000000000000000,1.150000000000000000000000000000,0.010000000000000000000000000000,0.010000000000000000000000000000,1.200000000000000000000000000000,0.010000000000000000000000000000,'2018-07-31 12:23:17','express_user@%',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL,'2018-07-31 12:23:17',NULL,NULL),(1802,54,1,0,0,0.000000000000000000000000000000,59.120000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.950000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1803,54,1,0,1,0.000000000000000000000000000000,57.890000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.200000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1804,54,1,1,0,0.000000000000000000000000000000,58.102000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,59.200000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1805,54,1,1,1,0.000000000000000000000000000000,50.260000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,50.210000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1806,54,2,0,0,0.000000000000000000000000000000,63.230000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,64.250000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1807,54,2,0,1,0.000000000000000000000000000000,61.590000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,62.360000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1808,54,2,1,0,0.000000000000000000000000000000,61.200100000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,62.150000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1809,54,2,1,1,0.000000000000000000000000000000,61.212500000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,62.300000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1810,54,3,0,0,0.000000000000000000000000000000,2.150000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.180000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1811,54,3,0,1,0.000000000000000000000000000000,2.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.150000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1812,54,3,1,0,0.000000000000000000000000000000,2.010000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.100000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1813,54,3,1,1,0.000000000000000000000000000000,2.050000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.130000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1814,54,4,0,0,0.000000000000000000000000000000,28.360000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,29.100000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1815,54,4,0,1,0.000000000000000000000000000000,28.230000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,29.030000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1816,54,4,1,0,0.000000000000000000000000000000,27.305000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.100000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1817,54,4,1,1,0.000000000000000000000000000000,27.560000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.120000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1818,54,5,0,0,0.000000000000000000000000000000,31.260000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,31.990000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1819,54,5,0,1,0.000000000000000000000000000000,31.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,31.990000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1820,54,5,1,0,0.000000000000000000000000000000,31.950000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.590000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1821,54,5,1,1,0.000000000000000000000000000000,32.120000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.690000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1822,54,6,0,0,0.000000000000000000000000000000,1.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.240000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1823,54,6,0,1,0.000000000000000000000000000000,1.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.150000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1824,54,6,1,0,0.000000000000000000000000000000,1.150000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.190000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1825,54,6,1,1,0.000000000000000000000000000000,1.160000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.201000000000000000000000000000,0.000000000000000000000000000000,'2018-07-31 12:23:50','express_user@%',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL,'2018-07-31 12:23:50',NULL,NULL),(1826,47,1,1,1,0.000000000000000000000000000000,58.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,65.000000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1827,47,2,1,1,0.000000000000000000000000000000,68.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,72.900000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1828,47,3,1,1,0.000000000000000000000000000000,2.170000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.330000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1829,47,4,1,1,0.000000000000000000000000000000,26.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.900000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1830,47,5,1,1,0.000000000000000000000000000000,30.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.900000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1831,47,6,1,1,0.000000000000000000000000000000,1.140000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.200000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1832,47,1,1,0,0.000000000000000000000000000000,58.900000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,60.800000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1833,47,2,1,0,0.000000000000000000000000000000,68.500000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,71.400000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1834,47,3,1,0,0.000000000000000000000000000000,2.170000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.330000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1835,47,4,1,0,0.000000000000000000000000000000,26.650000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,27.600000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1836,47,5,1,0,0.000000000000000000000000000000,31.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.300000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1837,47,6,1,0,0.000000000000000000000000000000,1.150000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.200000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1838,47,1,0,1,0.000000000000000000000000000000,54.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,51.900000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1839,47,2,0,1,0.000000000000000000000000000000,68.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,72.900000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1840,47,3,0,1,0.000000000000000000000000000000,2.170000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.330000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1841,47,4,0,1,0.000000000000000000000000000000,26.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,28.900000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1842,47,5,0,1,0.000000000000000000000000000000,30.200000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.900000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1843,47,6,0,1,0.000000000000000000000000000000,1.140000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.200000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1844,47,1,0,0,0.000000000000000000000000000000,58.900000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,60.800000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1845,47,2,0,0,0.000000000000000000000000000000,68.500000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,71.400000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1846,47,3,0,0,0.000000000000000000000000000000,2.170000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,2.330000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1847,47,4,0,0,0.000000000000000000000000000000,26.650000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,27.600000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1848,47,5,0,0,0.000000000000000000000000000000,31.100000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,32.300000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL),(1849,47,6,0,0,0.000000000000000000000000000000,1.150000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,1.200000000000000000000000000000,0.000000000000000000000000000000,'2018-08-01 09:27:23','express_user@%',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL,'2018-08-01 09:27:23',NULL,NULL);
/*!40000 ALTER TABLE `doc_rate` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`doc_rate$before_ins` 
BEFORE INSERT ON express_db.doc_rate
FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`doc_rate$before_upd` 
BEFORE UPDATE ON express_db.doc_rate FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_send`
--

DROP TABLE IF EXISTS `doc_send`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_send` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `cross_service_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на таблицу cross_service_type',
  `currency_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на таблицу book_currency',
  `status_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_status',
  `phone` varchar(12) DEFAULT NULL COMMENT 'Номер телефона клиента',
  `last_name` varchar(60) DEFAULT NULL COMMENT 'Фамилия',
  `first_name` varchar(60) DEFAULT NULL COMMENT 'Имя',
  `middle_name` varchar(60) DEFAULT NULL COMMENT 'Отчество',
  `marketing_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на book_marketing',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$doc_send$cross_service_id$cross_service_type$id` (`cross_service_id`),
  KEY `fk$doc_send$status_id$book_status$id` (`status_id`),
  KEY `fk$doc_send$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_send$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_send$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_send$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$doc_send$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_send$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_send$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_send$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_send$cross_service_id$cross_service_type$id` FOREIGN KEY (`cross_service_id`) REFERENCES `cross_service_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_send$status_id$book_status$id` FOREIGN KEY (`status_id`) REFERENCES `book_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Отправка';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_send`
--

LOCK TABLES `doc_send` WRITE;
/*!40000 ALTER TABLE `doc_send` DISABLE KEYS */;
INSERT INTO `doc_send` VALUES (1,1,2,1,'000000','фамилия','имя','отчество',89,NULL,1,1,'2018-05-29 13:45:08',1,1,'2018-05-31 12:51:00','2018-05-29 13:45:07','express_user@%','2018-05-31 12:50:57','express_user@%'),(2,1,2,3,'4343','4343','4343','4343',89,NULL,1,1,'2018-05-31 11:43:47',1,1,'2018-05-31 13:16:00','2018-05-31 11:43:44','express_user@%','2018-06-05 11:35:03','express_user@%');
/*!40000 ALTER TABLE `doc_send` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_send$before_ins
BEFORE INSERT
ON doc_send FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_send$before_upd
BEFORE UPDATE
ON doc_send FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_send_info`
--

DROP TABLE IF EXISTS `doc_send_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_send_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `deal_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на таблицу doc_deal',
  `card_number` varchar(30) NOT NULL COMMENT 'Номер карты клиента',
  `amount` decimal(19,2) unsigned NOT NULL COMMENT 'Сумма',
  `paid` decimal(19,2) DEFAULT '0.00' COMMENT 'Оплаченная сумма',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$doc_send_info$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_send_info$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_send_info$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_send_info$change_office_id$book_office$id` (`change_office_id`),
  KEY `fk$doc_send_info$deal_id$doc_send$id` (`deal_id`),
  CONSTRAINT `fk$doc_send_info$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_send_info$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_send_info$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_send_info$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_send_info$deal_id$doc_deal$id` FOREIGN KEY (`deal_id`) REFERENCES `doc_deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8 COMMENT='Отправка (карты)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_send_info`
--

LOCK TABLES `doc_send_info` WRITE;
/*!40000 ALTER TABLE `doc_send_info` DISABLE KEYS */;
INSERT INTO `doc_send_info` VALUES (153,32,'23213213',500.00,300.00,'refdsfdsf',1,1,'2018-08-13 12:50:52',1,1,'2018-08-13 12:53:13','2018-08-13 12:50:52','express_user@%','2018-08-13 12:53:13','express_user@%'),(154,32,'123',123.00,123.00,'fdgdfgdf',1,1,'2018-08-13 12:50:52',1,1,'2018-08-13 12:52:02','2018-08-13 12:50:52','express_user@%','2018-08-13 12:52:02','express_user@%'),(156,34,'3242343',2433.00,2433.00,'fdgdfgdf',1,1,'2018-08-16 11:11:11',1,1,'2018-08-16 11:11:47','2018-08-16 11:11:11','express_user@%','2018-08-16 11:11:47','express_user@%');
/*!40000 ALTER TABLE `doc_send_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_send_info$before_ins
BEFORE INSERT
ON doc_send_info FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_send_info$before_upd
BEFORE UPDATE
ON doc_send_info FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_tariff`
--

DROP TABLE IF EXISTS `doc_tariff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_tariff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(120) DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL COMMENT 'Категория. Ссылка на book_tariff',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на product_id',
  `city_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_city',
  `interval_begin` decimal(19,2) unsigned NOT NULL COMMENT 'Начало интервала',
  `interval_end` decimal(19,2) unsigned NOT NULL COMMENT 'Конец интервала',
  `sys_percent` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Комиссия системы в процентах',
  `sys_unit` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Комиссия системы в денежных единицах',
  `sys_percent_add` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Дополнительная комиссия системы в процентах',
  `sys_unit_add` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Дополнительная комиссия системы в денежных единицах',
  `fcc_percent` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Комиссия ПКЦ в процентах',
  `fcc_unit` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Комиссия ПКЦ в денежных единицах',
  `fcc_percent_add` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Дополнительная комиссия ПКЦ в процентах',
  `fcc_unit_add` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Дополнительная комиссия ПКЦ в денежных единицах',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  KEY `fk$doc_tariff$product_id$book_product$id` (`product_id`),
  KEY `fk$doc_tariff$city_id$book_city$id` (`city_id`),
  KEY `fk$doc_tariff$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$doc_tariff$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$doc_tariff$city_id$book_city$id` FOREIGN KEY (`city_id`) REFERENCES `book_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_tariff$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_tariff$product_id$book_product$id` FOREIGN KEY (`product_id`) REFERENCES `book_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_tariff$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='Тарифы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_tariff`
--

LOCK TABLES `doc_tariff` WRITE;
/*!40000 ALTER TABLE `doc_tariff` DISABLE KEYS */;
INSERT INTO `doc_tariff` VALUES (2,NULL,NULL,69,1,0.00,150.00,4.99,10.00,2.30,10.00,1.00,10.00,3.00,10.00,NULL,0,'2017-12-19 07:37:35','express_user@%',NULL,NULL,NULL,NULL),(3,NULL,NULL,87,4,0.00,800.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,NULL,0,'2017-12-26 09:23:08','express_user@%',NULL,'2017-12-26 09:23:22','express_user@%',NULL),(4,NULL,NULL,97,1,0.00,5000.00,5.00,50.00,5.00,50.00,5.00,50.00,5.00,50.00,NULL,0,'2018-01-12 11:31:53','express_user@%',NULL,NULL,NULL,NULL),(5,NULL,NULL,97,1,5001.00,10000.00,4.00,40.00,4.00,40.00,4.00,40.00,4.00,40.00,NULL,0,'2018-01-12 11:32:55','express_user@%',NULL,NULL,NULL,NULL),(6,NULL,NULL,97,1,10001.00,20000.00,3.00,30.00,3.00,30.00,3.00,30.00,3.00,30.00,NULL,0,'2018-01-12 11:34:05','express_user@%',NULL,NULL,NULL,NULL),(7,NULL,NULL,97,1,20001.00,30000.00,2.00,20.00,2.00,20.00,2.00,20.00,2.00,20.00,NULL,0,'2018-01-12 11:35:47','express_user@%',NULL,NULL,NULL,NULL),(8,NULL,NULL,97,1,300001.00,999000.00,1.00,10.00,1.00,10.00,1.00,10.00,1.00,10.00,NULL,0,'2018-01-12 11:36:54','express_user@%',NULL,NULL,NULL,NULL),(9,NULL,NULL,99,1,0.00,25000.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,NULL,0,'2018-01-22 11:08:17','express_user@%',NULL,NULL,NULL,NULL),(10,NULL,NULL,99,1,25001.00,55000.00,3.00,3.00,3.00,3.00,3.00,3.00,3.00,3.00,NULL,0,'2018-01-22 11:09:01','express_user@%',NULL,NULL,NULL,NULL),(11,NULL,NULL,99,1,55001.00,12500.00,2.00,2.00,2.00,2.00,2.00,2.00,2.00,2.00,NULL,0,'2018-01-22 11:10:01','express_user@%',NULL,NULL,NULL,NULL),(12,NULL,NULL,99,1,125001.00,999000.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,NULL,0,'2018-01-22 11:10:51','express_user@%',NULL,NULL,NULL,NULL),(17,NULL,NULL,119,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,30.00,0.00,0.00,NULL,0,'2018-02-05 12:17:23','express_user@%',NULL,NULL,NULL,NULL),(19,NULL,NULL,120,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,3800.00,0.00,0.00,NULL,0,'2018-02-05 12:39:13','express_user@%',NULL,NULL,NULL,NULL),(20,NULL,NULL,121,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,10.00,0.00,0.00,NULL,0,'2018-02-05 12:40:43','express_user@%',NULL,NULL,NULL,NULL),(21,NULL,NULL,122,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,50.00,0.00,0.00,NULL,0,'2018-02-05 12:43:30','express_user@%',NULL,NULL,NULL,NULL),(22,NULL,NULL,124,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,100.00,0.00,0.00,NULL,0,'2018-02-05 12:46:13','express_user@%',NULL,NULL,NULL,NULL),(23,NULL,NULL,125,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,300.00,0.00,0.00,NULL,0,'2018-02-05 12:47:00','express_user@%',NULL,NULL,NULL,NULL),(24,NULL,NULL,126,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,70.00,0.00,0.00,NULL,0,'2018-02-05 12:48:01','express_user@%',NULL,NULL,NULL,NULL),(25,NULL,NULL,127,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,40.00,0.00,0.00,NULL,0,'2018-02-05 12:48:30','express_user@%',NULL,NULL,NULL,NULL),(26,NULL,NULL,128,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1200.00,0.00,0.00,NULL,0,'2018-02-05 12:49:42','express_user@%',NULL,NULL,NULL,NULL),(27,NULL,NULL,129,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,50.00,0.00,0.00,NULL,0,'2018-02-05 12:50:57','express_user@%',NULL,NULL,NULL,NULL),(28,NULL,NULL,130,14,0.00,0.00,0.00,100.00,0.00,0.00,0.00,20.00,0.00,0.00,NULL,0,'2018-02-05 12:54:38','express_user@%',NULL,NULL,NULL,NULL),(29,NULL,NULL,131,14,0.00,0.00,0.00,110.00,0.00,0.00,0.00,20.00,0.00,0.00,NULL,0,'2018-02-05 12:57:01','express_user@%',NULL,NULL,NULL,NULL),(30,NULL,NULL,132,14,0.00,0.00,0.00,90.00,0.00,0.00,0.00,30.00,0.00,0.00,NULL,0,'2018-02-05 12:59:17','express_user@%',NULL,NULL,NULL,NULL),(31,NULL,NULL,133,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,200.00,0.00,0.00,NULL,0,'2018-02-05 13:00:51','express_user@%',NULL,'2018-02-05 13:02:48','express_user@%',NULL),(32,NULL,NULL,134,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,100.00,0.00,0.00,NULL,0,'2018-02-05 13:01:56','express_user@%',NULL,NULL,NULL,NULL),(33,NULL,NULL,135,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,60.00,0.00,0.00,NULL,0,'2018-02-05 13:04:40','express_user@%',NULL,NULL,NULL,NULL),(34,NULL,NULL,136,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,20.00,0.00,0.00,NULL,0,'2018-02-05 13:05:20','express_user@%',NULL,NULL,NULL,NULL),(35,NULL,NULL,137,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,150.00,0.00,0.00,NULL,0,'2018-02-05 13:06:12','express_user@%',NULL,NULL,NULL,NULL),(36,NULL,NULL,138,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,90.00,0.00,0.00,NULL,0,'2018-02-05 13:07:12','express_user@%',NULL,NULL,NULL,NULL),(37,NULL,NULL,139,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,45.00,0.00,0.00,NULL,0,'2018-02-05 13:07:43','express_user@%',NULL,NULL,NULL,NULL),(38,NULL,NULL,140,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,30.00,0.00,0.00,NULL,0,'2018-02-05 13:08:43','express_user@%',NULL,NULL,NULL,NULL),(39,NULL,NULL,141,14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,75.00,0.00,0.00,NULL,0,'2018-02-05 13:09:35','express_user@%',NULL,NULL,NULL,NULL),(40,'Мелкая грн (1,2,5) 50%',1,69,14,0.00,0.00,0.00,0.00,0.00,0.00,50.00,0.00,0.00,0.00,NULL,0,'2018-03-21 06:39:43','express_user@%',NULL,NULL,NULL,NULL),(41,'Мелкая грн (10,20) 20%',1,69,14,0.00,0.00,0.00,0.00,0.00,0.00,20.00,0.00,0.00,0.00,NULL,0,'2018-03-21 06:39:46','express_user@%',NULL,NULL,NULL,NULL),(42,'Ветхий доллар 10%',2,69,14,0.00,0.00,0.00,0.00,0.00,0.00,10.00,0.00,0.00,0.00,NULL,0,'2018-03-21 06:40:57','express_user@%',NULL,NULL,NULL,NULL),(43,'Ветхий доллар 20%',2,69,14,0.00,0.00,0.00,0.00,0.00,0.00,20.00,0.00,0.00,0.00,NULL,0,'2018-03-21 06:41:17','express_user@%',NULL,NULL,NULL,NULL),(44,'Ветхий доллар 30%',2,69,14,0.00,0.00,0.00,0.00,0.00,0.00,30.00,0.00,0.00,0.00,NULL,0,'2018-03-21 06:41:31','express_user@%',NULL,NULL,NULL,NULL),(45,'Ветхий доллар 40%',2,69,14,0.00,0.00,0.00,0.00,0.00,0.00,40.00,0.00,0.00,0.00,NULL,0,'2018-03-21 06:41:44','express_user@%',NULL,NULL,NULL,NULL),(46,'Ветхий доллар 50%',2,69,14,0.00,0.00,0.00,0.00,0.00,0.00,50.00,0.00,0.00,0.00,NULL,0,'2018-03-21 06:42:09','express_user@%',NULL,NULL,NULL,NULL),(47,'Ветхие евро 20%',3,69,14,0.00,0.00,0.00,0.00,0.00,0.00,20.00,0.00,0.00,0.00,NULL,0,'2018-03-21 06:42:23','express_user@%',NULL,NULL,NULL,NULL),(48,'Ветхие евро 30%',3,69,14,0.00,0.00,0.00,0.00,0.00,0.00,30.00,0.00,0.00,0.00,NULL,0,'2018-03-21 06:42:37','express_user@%',NULL,NULL,NULL,NULL),(49,'Ветхие евро 40%',3,69,14,0.00,0.00,0.00,0.00,0.00,0.00,40.00,0.00,0.00,0.00,NULL,0,'2018-03-21 06:42:48','express_user@%',NULL,NULL,NULL,NULL),(50,'Ветхие евро 50%',3,69,14,0.00,0.00,0.00,0.00,0.00,0.00,50.00,0.00,0.00,0.00,NULL,0,'2018-03-21 06:43:11','express_user@%',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `doc_tariff` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_tariff$before_ins
BEFORE INSERT
ON doc_tariff FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_tariff$before_upd
BEFORE UPDATE
ON doc_tariff FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_ticket`
--

DROP TABLE IF EXISTS `doc_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_ticket` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `cross_service_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на таблицу cross_service_type',
  `currency_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на таблицу book_currency',
  `status_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_status',
  `phone` varchar(12) DEFAULT NULL COMMENT 'Номер телефона клиента',
  `last_name` varchar(60) DEFAULT NULL COMMENT 'Фамилия',
  `first_name` varchar(60) DEFAULT NULL COMMENT 'Имя',
  `middle_name` varchar(60) DEFAULT NULL COMMENT 'Отчество',
  `marketing_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на book_marketing',
  `amount` decimal(16,2) unsigned DEFAULT NULL COMMENT 'Сумма по заявке',
  `commission` decimal(16,2) unsigned DEFAULT NULL COMMENT 'Сумма комиссии',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$doc_ticket$cross_service_id$cross_service_type$id` (`cross_service_id`),
  KEY `fk$doc_ticket$status_id$book_status$id` (`status_id`),
  KEY `fk$doc_ticket$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_ticket$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_ticket$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_ticket$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$doc_ticket$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_ticket$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_ticket$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_ticket$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_ticket$cross_service_id$cross_service_type$id` FOREIGN KEY (`cross_service_id`) REFERENCES `cross_service_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_ticket$status_id$book_status$id` FOREIGN KEY (`status_id`) REFERENCES `book_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Покупка билетов и OLX';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_ticket`
--

LOCK TABLES `doc_ticket` WRITE;
/*!40000 ALTER TABLE `doc_ticket` DISABLE KEYS */;
INSERT INTO `doc_ticket` VALUES (1,153,2,6,'323424','fanmoodsj','rerere','ewewew',90,400.00,50.00,NULL,1,1,'2018-08-07 07:59:37',1,1,'2018-08-07 07:59:37','2018-08-06 11:28:53','express_user@%','2018-08-07 07:59:37','express_user@%'),(2,153,2,6,NULL,'person','person','person',90,1500.00,100.00,NULL,4,11,'2018-08-07 10:51:29',2,11,'2018-08-07 10:51:29','2018-08-07 10:45:14','express_user@%','2018-08-07 10:51:29','express_user@%');
/*!40000 ALTER TABLE `doc_ticket` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_ticket$before_ins
BEFORE INSERT
ON doc_ticket FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_ticket$before_upd
BEFORE UPDATE
ON doc_ticket FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_transaction`
--

DROP TABLE IF EXISTS `doc_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `office_id` int(10) unsigned NOT NULL COMMENT 'Где создали. Ссылка на book_office',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Для продукта. Ссылка на book_product',
  `client_id` int(10) unsigned DEFAULT NULL COMMENT 'Клиент. Ссылка на doc_client',
  `contractor` varchar(300) DEFAULT NULL COMMENT 'Контрагент',
  `declare_sum` decimal(19,2) unsigned NOT NULL COMMENT 'Объявленная сумма',
  `declare_currency` int(10) unsigned NOT NULL COMMENT 'Объявленная валюта. Ссылка на book_currency',
  `real_sum` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Реальная сумма',
  `real_currency` int(10) unsigned DEFAULT NULL COMMENT 'Реальная валюта. Ссылка на book_currency',
  `finish_declare` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Остаток заявки',
  `finish_real` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Остаток реальной суммы',
  `wallet_fcc` int(10) unsigned DEFAULT NULL COMMENT 'Номер карты/кошелька ПКЦ',
  `wallet_client` varchar(32) DEFAULT NULL COMMENT 'Номер карты/кошелька Клиента',
  `sys_percent` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Комиссия системы в процентах',
  `sys_unit` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Комиссия системы в денежных единицах',
  `sys_percent_add` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Дополнительная комиссия системы в процентах',
  `sys_unit_add` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Дополнительная комиссия системы в денежных единицах',
  `fcc_percent` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Комиссия ПКЦ в процентах',
  `fcc_unit` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Комиссия ПКЦ в денежных единицах',
  `fcc_percent_add` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Дополнительная комиссия ПКЦ в процентах',
  `fcc_unit_add` decimal(19,2) unsigned DEFAULT NULL COMMENT 'Дополнительная комиссия ПКЦ в денежных единицах',
  `rate` decimal(65,30) unsigned DEFAULT NULL COMMENT 'Курс обмена',
  `quantity` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Количество ДопУслуг',
  `speed_id` int(10) unsigned DEFAULT NULL COMMENT 'Скорость выполнения заявки. Ссылка на book_speed',
  `stock_id` int(10) unsigned DEFAULT NULL COMMENT 'Скидка по акции. Ссылка на doc_stock',
  `status_id` int(10) unsigned DEFAULT NULL COMMENT 'Статус декларации. Ссылка на book_status',
  `cash` enum('empty','gave','put','non-cash') NOT NULL DEFAULT 'empty' COMMENT 'empty - нет денег, gave - выдали клиенту, put - положили в кассу, non-cash - безналичная декларация',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  KEY `fk$doc_transaction$client_id$doc_client$id` (`client_id`),
  KEY `fk$doc_transaction$declare_currency$book_currency$id` (`declare_currency`),
  KEY `fk$doc_transaction$office_id$book_office$id` (`office_id`),
  KEY `fk$doc_transaction$product_id$book_product$id` (`product_id`),
  KEY `fk$doc_transaction$real_currency$book_currency$id` (`real_currency`),
  KEY `fk$doc_transaction$speed_id$book_speed$id` (`speed_id`),
  KEY `fk$doc_transaction$status_id$book_status$id` (`status_id`),
  KEY `fk$doc_transaction$stock_id$book_stock$id` (`stock_id`),
  KEY `fk$doc_transaction$wallet_fcc$doc_box$id` (`wallet_fcc`),
  KEY `fk$doc_transaction$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$doc_transaction$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$doc_transaction$client_id$doc_client$id` FOREIGN KEY (`client_id`) REFERENCES `doc_client` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_transaction$declare_currency$book_currency$id` FOREIGN KEY (`declare_currency`) REFERENCES `book_currency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_transaction$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_transaction$office_id$book_office$id` FOREIGN KEY (`office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_transaction$product_id$book_product$id` FOREIGN KEY (`product_id`) REFERENCES `book_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_transaction$real_currency$book_currency$id` FOREIGN KEY (`real_currency`) REFERENCES `book_currency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_transaction$speed_id$book_speed$id` FOREIGN KEY (`speed_id`) REFERENCES `book_speed` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_transaction$stock_id$book_stock$id` FOREIGN KEY (`stock_id`) REFERENCES `book_stock` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_transaction$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='Декларации';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_transaction`
--

LOCK TABLES `doc_transaction` WRITE;
/*!40000 ALTER TABLE `doc_transaction` DISABLE KEYS */;
INSERT INTO `doc_transaction` VALUES (1,34,69,NULL,NULL,150.00,1,176.00,NULL,150.00,176.00,18,NULL,NULL,NULL,NULL,NULL,1.00,10.00,3.00,10.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-02-28 11:50:14','express_user@%',NULL,NULL,NULL,NULL),(2,34,69,NULL,NULL,149.00,1,174.96,NULL,149.00,174.96,18,NULL,NULL,NULL,NULL,NULL,1.00,10.00,3.00,10.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-02-28 11:51:39','express_user@%',NULL,NULL,NULL,NULL),(3,34,69,NULL,NULL,148.00,1,173.92,NULL,0.00,173.92,18,NULL,NULL,NULL,NULL,NULL,1.00,10.00,3.00,10.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-02-28 11:52:01','express_user@%',NULL,'2018-02-28 12:59:10','express_user@%',NULL),(4,34,69,NULL,NULL,147.00,1,172.88,NULL,147.00,172.88,18,NULL,NULL,NULL,NULL,NULL,1.00,10.00,3.00,10.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-02-28 11:52:28','express_user@%',NULL,NULL,NULL,NULL),(5,34,69,NULL,NULL,146.00,1,171.84,NULL,0.00,0.00,18,NULL,NULL,NULL,NULL,NULL,1.00,10.00,3.00,10.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-02-28 11:52:47','express_user@%',NULL,'2018-03-03 08:51:31','express_user@%',NULL),(6,34,69,NULL,NULL,145.00,1,170.80,NULL,145.00,170.80,18,NULL,NULL,NULL,NULL,NULL,1.00,10.00,3.00,10.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-02-28 11:53:31','express_user@%',NULL,NULL,NULL,NULL),(7,34,99,NULL,NULL,150.00,1,190.00,NULL,150.00,0.00,NULL,'47348738478',4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-02-28 13:33:58','express_user@%',NULL,'2018-03-01 06:29:35','express_user@%',NULL),(8,34,99,NULL,NULL,8899.00,1,10338.84,NULL,8899.00,0.00,NULL,NULL,4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-02-28 13:34:23','express_user@%',NULL,'2018-03-01 06:30:19','express_user@%',NULL),(9,34,99,NULL,NULL,478.00,1,570.48,NULL,478.00,570.48,NULL,NULL,4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-03-01 06:28:11','express_user@%',NULL,NULL,NULL,NULL),(10,34,99,NULL,NULL,980.00,1,1152.80,NULL,980.00,1152.80,NULL,NULL,4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-03-01 06:28:35','express_user@%',NULL,NULL,NULL,NULL),(11,34,99,NULL,NULL,980.00,1,1152.80,NULL,980.00,1152.80,NULL,'4738473894',4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-03-01 06:29:00','express_user@%',NULL,NULL,NULL,NULL),(12,34,119,NULL,NULL,1.00,1,150.00,1,NULL,0.00,NULL,NULL,NULL,0.00,NULL,NULL,NULL,30.00,NULL,NULL,NULL,5,NULL,NULL,NULL,'empty',NULL,0,'2018-03-01 07:38:14','express_user@%',NULL,'2018-03-23 11:44:15','express_user@%',NULL),(13,34,69,NULL,NULL,127.00,4,152.08,NULL,127.00,152.08,18,NULL,NULL,NULL,NULL,NULL,1.00,10.00,3.00,10.00,NULL,1,NULL,NULL,NULL,'empty','яяяяя',0,'2018-03-06 12:32:46','express_user@%',NULL,NULL,NULL,NULL),(14,34,99,NULL,NULL,5000.00,1,5816.00,NULL,5000.00,5816.00,NULL,'5555444466667777',4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-03-06 13:34:21','express_user@%',NULL,NULL,NULL,NULL),(15,34,69,NULL,NULL,120.00,1,144.80,NULL,0.00,0.00,18,NULL,NULL,NULL,NULL,NULL,1.00,10.00,3.00,10.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-03-09 11:24:59','express_user@%',NULL,'2018-03-09 11:28:17','express_user@%',NULL),(16,34,119,NULL,NULL,1.00,1,30.00,1,NULL,30.00,NULL,NULL,NULL,0.00,NULL,NULL,NULL,30.00,NULL,NULL,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-03-23 11:42:18','express_user@%',NULL,NULL,NULL,NULL),(17,34,69,NULL,NULL,150.00,1,176.00,NULL,150.00,176.00,18,NULL,NULL,NULL,NULL,NULL,1.00,10.00,3.00,10.00,NULL,1,NULL,NULL,NULL,'empty',NULL,0,'2018-04-24 12:06:04','express_user@%',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `doc_transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`doc_transaction$before_ins` BEFORE INSERT ON express_db.doc_transaction FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`doc_transaction$before_upd` BEFORE UPDATE ON express_db.doc_transaction FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_unfinished`
--

DROP TABLE IF EXISTS `doc_unfinished`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_unfinished` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `coming_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Дата поступления',
  `note` varchar(1200) NOT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$doc_unfinished$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_unfinished$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_unfinished$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_unfinished$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$doc_unfinished$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_unfinished$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_unfinished$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_unfinished$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='Незавершенка';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_unfinished`
--

LOCK TABLES `doc_unfinished` WRITE;
/*!40000 ALTER TABLE `doc_unfinished` DISABLE KEYS */;
INSERT INTO `doc_unfinished` VALUES (32,'2018-08-07 02:05:00','Первая 1234 5678',1,1,'2018-08-07 10:37:46',NULL,NULL,'2018-08-07 10:37:46','2018-08-07 10:37:46','express_user@%',NULL,NULL),(33,'2018-08-07 12:15:00','Вторая 1234 5678',1,1,'2018-08-07 10:38:08',NULL,NULL,'2018-08-07 10:38:08','2018-08-07 10:38:08','express_user@%',NULL,NULL),(34,'2018-08-07 12:15:00','Третья 1234 5678',1,1,'2018-08-07 10:38:36',NULL,NULL,'2018-08-07 10:38:36','2018-08-07 10:38:36','express_user@%',NULL,NULL),(35,'2018-08-07 10:13:00','Четвертая 1234 5678',1,1,'2018-08-07 10:39:29',NULL,NULL,'2018-08-07 10:39:29','2018-08-07 10:39:29','express_user@%',NULL,NULL),(36,'2018-08-07 12:15:00','к5-000',1,1,'2018-08-07 10:40:08',NULL,NULL,'2018-08-07 10:40:08','2018-08-07 10:40:08','express_user@%',NULL,NULL),(37,'2018-08-07 02:05:00','к6-000',1,1,'2018-08-07 10:40:44',NULL,NULL,'2018-08-07 10:40:44','2018-08-07 10:40:44','express_user@%',NULL,NULL),(38,'2018-08-09 12:15:00','ав',1,1,'2018-08-09 12:30:34',NULL,NULL,'2018-08-09 12:30:34','2018-08-09 12:30:34','express_user@%',NULL,NULL),(39,'2018-08-09 12:15:00','пап',1,1,'2018-08-09 12:30:48',NULL,NULL,'2018-08-09 12:30:48','2018-08-09 12:30:48','express_user@%',NULL,NULL),(40,'2018-08-09 12:15:00','пап',1,1,'2018-08-09 12:31:08',NULL,NULL,'2018-08-09 12:31:08','2018-08-09 12:31:08','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `doc_unfinished` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_unfinished$before_ins
BEFORE INSERT
ON doc_unfinished FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_unfinished$before_upd
BEFORE UPDATE
ON doc_unfinished FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_withdrawal`
--

DROP TABLE IF EXISTS `doc_withdrawal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_withdrawal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `cross_service_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на таблицу cross_service_type',
  `currency_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_currency',
  `status_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на book_status',
  `phone` varchar(12) DEFAULT NULL COMMENT 'Номер телефона клиента',
  `last_name` varchar(60) DEFAULT NULL COMMENT 'Фамилия',
  `first_name` varchar(60) DEFAULT NULL COMMENT 'Имя',
  `middle_name` varchar(60) DEFAULT NULL COMMENT 'Отчество',
  `marketing_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на book_marketing',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$doc_withdrawal$cross_service_id$cross_service_type$id` (`cross_service_id`),
  KEY `fk$doc_withdrawal$status_id$book_status$id` (`status_id`),
  KEY `fk$doc_withdrawal$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_withdrawal$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_withdrawal$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_withdrawal$change_office_id$book_office$id` (`change_office_id`),
  KEY `fk$doc_withdrawal$currency_id$book_currency$id` (`currency_id`),
  CONSTRAINT `fk$doc_withdrawal$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_withdrawal$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_withdrawal$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_withdrawal$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_withdrawal$cross_service_id$cross_service_type$id` FOREIGN KEY (`cross_service_id`) REFERENCES `cross_service_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_withdrawal$currency_id$book_currency$id` FOREIGN KEY (`currency_id`) REFERENCES `book_currency` (`id`),
  CONSTRAINT `fk$doc_withdrawal$status_id$book_status$id` FOREIGN KEY (`status_id`) REFERENCES `book_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='Снятие';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_withdrawal`
--

LOCK TABLES `doc_withdrawal` WRITE;
/*!40000 ALTER TABLE `doc_withdrawal` DISABLE KEYS */;
INSERT INTO `doc_withdrawal` VALUES (15,35,2,1,NULL,'kjjk','вывы','lk',90,'l;kl;;lk;k',1,1,'2018-06-01 05:48:08',NULL,NULL,NULL,'2018-06-01 05:48:08','express_user@%','2018-06-04 13:09:37','express_user@%'),(18,35,2,1,NULL,'Иванов','Иван','Иванович',99,'фыыыфыф',1,1,'2018-06-07 13:22:47',NULL,NULL,NULL,'2018-06-07 13:22:47','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `doc_withdrawal` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_withdrawal$before_ins
BEFORE INSERT
ON doc_withdrawal FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_withdrawal$before_upd
BEFORE UPDATE
ON doc_withdrawal FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `doc_withdrawal_info`
--

DROP TABLE IF EXISTS `doc_withdrawal_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_withdrawal_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `deal_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на doc_deal',
  `box_id` int(10) unsigned NOT NULL COMMENT 'Ссылка doc_box',
  `card_number` varchar(30) NOT NULL COMMENT 'Номер карты клиента',
  `card_time` timestamp NULL DEFAULT NULL COMMENT 'Время операции перевода на нашу карту',
  `amount` decimal(19,2) unsigned NOT NULL COMMENT 'Сумма перевода',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  KEY `fk$doc_withdrawal_info$box_id$doc_box$id` (`box_id`),
  KEY `fk$doc_withdrawal_info$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$doc_withdrawal_info$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$doc_withdrawal_info$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$doc_withdrawal_info$change_office_id$book_office$id` (`change_office_id`),
  KEY `fk$doc_withdrawal_info$deal_id$doc_withdrawal$id` (`deal_id`),
  CONSTRAINT `fk$doc_withdrawal_info$box_id$doc_box$id` FOREIGN KEY (`box_id`) REFERENCES `doc_box` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_withdrawal_info$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_withdrawal_info$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_withdrawal_info$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$doc_withdrawal_info$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COMMENT='Снятие (карты)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_withdrawal_info`
--

LOCK TABLES `doc_withdrawal_info` WRITE;
/*!40000 ALTER TABLE `doc_withdrawal_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_withdrawal_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_withdrawal_info$before_ins
BEFORE INSERT
ON doc_withdrawal_info FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER doc_withdrawal_info$before_upd
BEFORE UPDATE
ON doc_withdrawal_info FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `teh_callback`
--

DROP TABLE IF EXISTS `teh_callback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teh_callback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `phone` varchar(12) NOT NULL COMMENT 'Телефон',
  `callback` tinyint(1) DEFAULT NULL COMMENT '0 - не звонили, 1 - перезвонили',
  `message` varchar(120) DEFAULT NULL COMMENT 'Комментарий',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  KEY `fk$teh_callback$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$teh_callback$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$teh_callback$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_callback$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='Заказы обратных звонков';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teh_callback`
--

LOCK TABLES `teh_callback` WRITE;
/*!40000 ALTER TABLE `teh_callback` DISABLE KEYS */;
INSERT INTO `teh_callback` VALUES (1,'380661080718',NULL,'тест','2018-03-16 10:29:38','client',NULL,NULL,NULL,NULL),(3,'380661080718',NULL,'тест3','2018-03-16 13:35:49','client',NULL,NULL,NULL,NULL),(4,'380668050639',NULL,'тест4 ','2018-03-21 12:49:18','client',NULL,NULL,NULL,NULL),(5,'380668050639',NULL,'тест4 ','2018-03-21 12:49:50','client',NULL,NULL,NULL,NULL),(6,'380668050639',NULL,'тест4 ','2018-03-21 12:50:01','client',NULL,NULL,NULL,NULL),(7,'380632525258',NULL,' ','2018-03-21 12:54:25','client',NULL,NULL,NULL,NULL),(8,'380632525258',NULL,' ','2018-03-21 12:55:12','client',NULL,NULL,NULL,NULL),(9,'380666666666',NULL,' ','2018-03-21 13:01:19','client',NULL,NULL,NULL,NULL),(10,'380666666666',NULL,' ','2018-03-21 13:01:33','client',NULL,NULL,NULL,NULL),(11,'380222222222',NULL,'тест ','2018-03-21 13:03:27','client',NULL,NULL,NULL,NULL),(12,'380222222222',NULL,'тест ','2018-03-21 13:03:32','client',NULL,NULL,NULL,NULL),(13,'380661080718',NULL,'тест ','2018-03-23 06:36:57','client',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `teh_callback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teh_children`
--

DROP TABLE IF EXISTS `teh_children`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teh_children` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `user_id` int(10) unsigned NOT NULL COMMENT 'Родитель. Ссылка на teh_user.id',
  `last_name` varchar(60) NOT NULL COMMENT 'Фамилия',
  `first_name` varchar(60) NOT NULL COMMENT 'Имя',
  `middle_name` varchar(60) NOT NULL COMMENT 'Отчество',
  `birthday` date NOT NULL COMMENT 'День рождения',
  `gender` enum('m','f') NOT NULL COMMENT 'Пол. m - мужской, f - женский.',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  KEY `fk$teh_children$user_id$teh_user$id` (`user_id`),
  KEY `fk$teh_children$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$teh_children$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$teh_children$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_children$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_children$user_id$teh_user$id` FOREIGN KEY (`user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Дети сотрудника';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teh_children`
--

LOCK TABLES `teh_children` WRITE;
/*!40000 ALTER TABLE `teh_children` DISABLE KEYS */;
/*!40000 ALTER TABLE `teh_children` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 trigger teh_children$before_ins
before insert
on teh_children for each row
begin
  set new.ins_user = current_user();
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 trigger teh_children$before_upd
before update
on teh_children for each row
begin
  set new.upd_user = current_user();
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `teh_move`
--

DROP TABLE IF EXISTS `teh_move`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teh_move` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `user_id` int(10) unsigned NOT NULL COMMENT 'Сотрудник. Ссылка на teh_user',
  `positions_id` int(10) unsigned NOT NULL COMMENT 'Должность. Ссылка на teh_positions',
  `office_id` int(10) unsigned NOT NULL COMMENT 'Отделение. Ссылка на book_office',
  `date_hire` date NOT NULL COMMENT 'Дата принятия на работу',
  `date_fired` date DEFAULT NULL COMMENT 'Дата увольнения',
  `is_move` tinyint(1) DEFAULT NULL COMMENT 'Было перемещение? 1 - да, 0 - нет, увольнение',
  `command_id` int(10) unsigned DEFAULT NULL COMMENT 'Приказ id',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве? 0-нет, 1-да',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  KEY `fk$teh_move$positions_id$teh_positions$id` (`positions_id`),
  KEY `fk$teh_move$office_id$book_office$id` (`office_id`),
  KEY `fk$teh_move$user_id$teh_user$id` (`user_id`),
  KEY `fk$teh_move$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$teh_move$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$teh_move$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_move$office_id$book_office$id` FOREIGN KEY (`office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_move$positions_id$teh_positions$id` FOREIGN KEY (`positions_id`) REFERENCES `teh_positions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_move$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_move$user_id$teh_user$id` FOREIGN KEY (`user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Кадровый учет';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teh_move`
--

LOCK TABLES `teh_move` WRITE;
/*!40000 ALTER TABLE `teh_move` DISABLE KEYS */;
INSERT INTO `teh_move` VALUES (1,1,6,1,'2018-02-23',NULL,NULL,NULL,NULL,0,'2018-02-23 11:19:41','express_user@%',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `teh_move` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`teh_move$before_ins` 
BEFORE INSERT ON express_db.teh_move FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`teh_move$before_upd` 
BEFORE UPDATE ON express_db.teh_move FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `teh_node`
--

DROP TABLE IF EXISTS `teh_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teh_node` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(80) NOT NULL COMMENT 'Название узла',
  `menu_descr` varchar(80) DEFAULT NULL,
  `icon` varchar(80) DEFAULT NULL,
  `show` tinyint(1) unsigned DEFAULT '0' COMMENT 'Признак - выводить в меню или нет ',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на родительский элемент',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$teh_node$descr$parent_id` (`descr`,`parent_id`),
  KEY `fk$teh_node$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$teh_node$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$teh_node$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$teh_node$change_office_id$book_office$id` (`change_office_id`),
  KEY `fk$teh_node$parent_id$teh_node$id` (`parent_id`),
  CONSTRAINT `fk$teh_node$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_node$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_node$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_node$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_node$parent_id$teh_node$id` FOREIGN KEY (`parent_id`) REFERENCES `teh_node` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8 COMMENT='Узлы дерева элементов системы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teh_node`
--

LOCK TABLES `teh_node` WRITE;
/*!40000 ALTER TABLE `teh_node` DISABLE KEYS */;
INSERT INTO `teh_node` VALUES (81,'send','Отправка','fa fa-cc-discover fa-fw',1,123,'Отправка Карты/Кошельки/Счета',NULL,NULL,'2018-07-06 06:20:19',NULL,NULL,'2018-07-06 06:20:19','2018-06-21 12:44:16','express_user@%','2018-07-06 06:20:19','express_user@%'),(82,'withdrawal','Снятие','fa fa-bell fa-fw',1,123,'Снятие карты/кошельки/счет',NULL,NULL,'2018-07-06 06:00:58',NULL,NULL,'2018-07-06 06:00:58','2018-06-21 12:44:16','express_user@%','2018-07-06 06:00:58','express_user@%'),(83,'additional','Доп. услуги','fa fa-book fa-fw',1,123,'ДопУслуги',NULL,NULL,'2018-08-08 12:31:26',1,1,'2018-08-08 12:31:26','2018-06-21 12:44:16','express_user@%','2018-08-08 12:31:26','express_user@%'),(84,'global','Международные переводы','fa fa-desktop fa-fw',1,123,'МП Отправка/Снятие',NULL,NULL,'2018-07-31 13:02:41',1,1,'2018-07-31 13:02:41','2018-06-21 12:44:16','express_user@%','2018-07-31 13:02:41','express_user@%'),(85,'panda','Панда','fa fa-mobile fa-fw',1,123,'Панда',NULL,NULL,'2018-07-07 07:33:24',NULL,NULL,'2018-07-07 07:33:24','2018-06-21 12:44:16','express_user@%','2018-07-07 07:33:24','express_user@%'),(86,'incoming','Приход/Расход','fa fa-columns fa-fw',1,123,'Приход/Расход',NULL,NULL,'2018-07-09 05:52:02',NULL,NULL,'2018-07-09 05:52:02','2018-06-21 12:44:16','express_user@%','2018-07-09 05:52:02','express_user@%'),(87,'bum','БУМ','fa fa-certificate fa-fw',1,123,'БУМ',NULL,NULL,'2018-07-06 06:23:35',NULL,NULL,'2018-07-06 06:23:35','2018-06-21 12:44:16','express_user@%','2018-07-06 06:23:35','express_user@%'),(88,'partner','Партнёры','fa fa-users fa-fw',1,123,'Партнеры',NULL,NULL,'2018-07-06 06:17:19',NULL,NULL,'2018-07-06 06:17:19','2018-06-21 12:44:16','express_user@%','2018-07-06 06:17:19','express_user@%'),(89,'encashment','Инкассация','fa fa-dashboard fa-fw',1,123,'Инкассация',NULL,NULL,'2018-07-31 07:44:34',1,1,'2018-07-31 07:44:34','2018-06-21 12:44:16','express_user@%','2018-07-31 07:44:34','express_user@%'),(90,'ticket','Билеты/Olx','fa fa-map fa-fw',1,123,'Билеты/OLX',NULL,NULL,'2018-07-05 13:50:50',NULL,NULL,'2018-07-05 13:50:50','2018-06-21 12:44:16','express_user@%','2018-07-05 13:50:50','express_user@%'),(91,'exchange','Валютообмен','fa fa-dot-circle-o fa-fw',1,123,'Наличный валютообмен',NULL,NULL,'2018-07-09 05:43:02',NULL,NULL,'2018-07-09 05:43:02','2018-06-21 12:44:16','express_user@%','2018-07-09 05:43:02','express_user@%'),(92,'rate','Курсы','fa fa-dollar fa-fw',1,123,'Курсы валют',NULL,NULL,'2018-07-06 05:28:53',NULL,NULL,'2018-07-06 05:28:53','2018-06-21 12:44:16','express_user@%','2018-07-06 05:28:53','express_user@%'),(93,'tariff','Тарифы','fa fa-credit-card fa-fw',1,94,'Тарифы',NULL,NULL,'2018-07-04 06:43:16',NULL,NULL,NULL,'2018-06-21 12:44:16','express_user@%','2018-07-04 06:43:16','express_user@%'),(94,'book','Справочники','fa fa-archive fa-fw',1,123,'Справочники',NULL,NULL,'2018-07-05 13:53:11',NULL,NULL,'2018-07-05 13:53:11','2018-06-21 12:44:16','express_user@%','2018-07-05 13:53:11','express_user@%'),(95,'box','Боксы','fa fa-briefcase fa-fw',1,94,'Боксы',NULL,NULL,'2018-07-06 06:19:39',NULL,NULL,'2018-07-06 06:19:39','2018-06-21 12:44:16','express_user@%','2018-07-06 06:19:39','express_user@%'),(96,'cash_register','Свод кассы','fa fa-dashcube fa-fw',1,123,'Свод кассы',NULL,NULL,'2018-07-07 09:57:47',NULL,NULL,'2018-07-07 09:57:47','2018-06-21 12:44:16','express_user@%','2018-07-07 09:57:47','express_user@%'),(97,'send_request',NULL,NULL,0,81,'Заявка',NULL,NULL,'2018-08-03 08:37:54',1,1,'2018-08-03 08:37:54','2018-06-21 12:56:24','express_user@%','2018-08-03 08:37:54','express_user@%'),(98,'send_payment',NULL,NULL,0,81,'Оплата',NULL,NULL,'2018-08-03 08:38:01',1,1,'2018-08-03 08:38:01','2018-06-21 12:56:24','express_user@%','2018-08-03 08:38:01','express_user@%'),(104,'unfinished','Незавершёнка','fa fa-arrow-circle-down fa-fw',1,123,'Незавершенка',NULL,NULL,'2018-07-23 06:16:44',1,1,'2018-07-23 06:16:44','2018-06-21 12:59:09','express_user@%','2018-07-23 06:16:44','express_user@%'),(113,'nbu','Курс НБУ','fa fa-eur fa-fw',1,94,'Курсы НБУ',NULL,NULL,'2018-07-05 13:47:54',NULL,NULL,'2018-07-05 13:47:54','2018-06-21 13:04:15','express_user@%','2018-07-05 13:47:54','express_user@%'),(114,'tariff_group','Группы тарифов','fa fa-dedent fa-fw',1,94,'Группы для тарифов',NULL,NULL,'2018-07-07 08:34:54',NULL,NULL,'2018-07-07 08:34:54','2018-06-21 13:06:22','express_user@%','2018-07-07 08:34:54','express_user@%'),(115,'tariff_type','Типы тарифов','fa fa-barcode fa-fw',1,94,'Типы тарифов',NULL,NULL,'2018-07-05 13:55:15',NULL,NULL,'2018-07-05 13:55:15','2018-06-21 13:06:22','express_user@%','2018-07-05 13:55:15','express_user@%'),(123,'root',NULL,NULL,0,NULL,'Корень',NULL,NULL,'2018-07-06 06:12:02',NULL,NULL,'2018-07-06 06:12:02','2018-07-04 05:44:15','express_user@%','2018-07-06 06:12:02','express_user@%'),(124,'node','Элементы приложения','fa fa-desktop fa-fw',1,143,'Узловые элементы',NULL,NULL,'2018-07-14 09:37:06',NULL,NULL,'2018-07-14 09:37:06','2018-07-04 12:57:49','express_user@%','2018-07-14 09:37:06','express_user@%'),(126,'marketing','Источники привлечения','fa fa-asl-interpreting fa-fw',1,94,'Маркетинг',NULL,NULL,'2018-07-07 08:35:52',NULL,NULL,'2018-07-07 08:35:52','2018-07-07 08:35:52','express_user@%',NULL,NULL),(127,'send_repayment',NULL,NULL,0,81,'Погашение',NULL,NULL,'2018-08-03 08:38:08',1,1,'2018-08-03 08:38:08','2018-07-07 09:00:11','express_user@%','2018-08-03 08:38:08','express_user@%'),(128,'send_refund',NULL,NULL,0,81,'Возврат',NULL,NULL,'2018-08-03 08:38:19',1,1,'2018-08-03 08:38:19','2018-07-07 09:21:54','express_user@%','2018-08-03 08:38:19','express_user@%'),(134,'role','Роли пользователей','fa fa-registered fa-fw',1,143,'Роли',NULL,NULL,'2018-07-16 06:33:53',NULL,NULL,'2018-07-16 06:33:53','2018-07-09 07:36:46','express_user@%','2018-07-16 06:33:53','express_user@%'),(135,'system','Системы','fa fa-bitcoin fa-fw',1,94,'Системы',NULL,NULL,'2018-07-13 05:19:19',NULL,NULL,'2018-07-13 05:19:19','2018-07-13 05:19:19','express_user@%',NULL,NULL),(136,'office','Отделения','fa fa-bank fa-fw',1,94,'Отделения',NULL,NULL,'2018-07-13 05:20:57',NULL,NULL,'2018-07-13 05:20:57','2018-07-13 05:20:57','express_user@%',NULL,NULL),(137,'bush','\"Кусты\" отделений','fa fa-image fa-fw',1,94,'Кусты',NULL,NULL,'2018-07-13 05:52:15',NULL,NULL,'2018-07-13 05:52:15','2018-07-13 05:52:15','express_user@%',NULL,NULL),(138,'city','Города','fa fa-area-chart fa-fw',1,94,'Города',NULL,NULL,'2018-07-13 05:52:53',NULL,NULL,'2018-07-13 05:52:53','2018-07-13 05:52:47','express_user@%','2018-07-13 05:52:53','express_user@%'),(139,'country','Страны','fa fa-dedent fa-fw',1,94,'Страны',NULL,NULL,'2018-07-13 05:53:20',NULL,NULL,'2018-07-13 05:53:20','2018-07-13 05:53:20','express_user@%',NULL,NULL),(140,'category','Категории','fa fa-columns fa-fw',1,94,'Категории',NULL,NULL,'2018-07-13 12:42:58',NULL,NULL,'2018-07-13 12:42:58','2018-07-13 12:35:12','express_user@%','2018-07-13 12:42:58','express_user@%'),(141,'user','Пользователи','fa fa-user-md fa-fw',1,143,'Пользователи',NULL,NULL,'2018-07-14 09:36:59',NULL,NULL,'2018-07-14 09:36:59','2018-07-14 07:45:18','express_user@%','2018-07-14 09:36:59','express_user@%'),(142,'service','Услуги','fa fa-cc-visa fa-fw',1,94,'Сервисы',NULL,NULL,'2018-07-17 07:13:58',NULL,NULL,'2018-07-17 07:13:58','2018-07-14 07:46:01','express_user@%','2018-07-17 07:13:58','express_user@%'),(143,'admin','Администрирование','fa fa-support fa-fw',1,123,'Администрирование',NULL,NULL,'2018-07-14 09:36:28',NULL,NULL,'2018-07-14 09:36:28','2018-07-14 09:36:28','express_user@%',NULL,NULL),(144,'office_group','Группы для отделений','fa fa-users fa-fw',1,94,'Группы для отделений',NULL,NULL,'2018-07-14 10:17:00',NULL,NULL,'2018-07-14 10:17:00','2018-07-14 10:17:00','express_user@%',NULL,NULL),(145,'position','Должности','fa fa-black-tie fa-fw',1,94,'Должности',NULL,NULL,'2018-07-16 06:28:31',NULL,NULL,'2018-07-16 06:28:31','2018-07-16 06:28:24','express_user@%','2018-07-16 06:28:31','express_user@%'),(146,'send_bank_refund',NULL,NULL,0,81,'Возврат банком',1,1,'2018-08-03 08:38:53',NULL,NULL,'2018-08-03 08:38:53','2018-08-03 08:38:53','express_user@%',NULL,NULL),(147,'withdrawal_request',NULL,NULL,0,82,'Заявка',1,1,'2018-08-06 09:52:37',NULL,NULL,'2018-08-06 09:52:37','2018-08-06 09:52:37','express_user@%',NULL,NULL),(148,'withdrawal_repayment',NULL,NULL,0,82,'Подтверждение',1,1,'2018-08-10 06:44:30',1,1,'2018-08-10 06:44:30','2018-08-06 09:53:59','express_user@%','2018-08-10 06:44:30','express_user@%'),(149,'withdrawal_payment',NULL,NULL,0,82,'Оплата',1,1,'2018-08-06 09:54:28',NULL,NULL,'2018-08-06 09:54:28','2018-08-06 09:54:28','express_user@%',NULL,NULL),(150,'ticket_request',NULL,NULL,0,90,'Заявка',1,1,'2018-08-07 10:32:53',NULL,NULL,'2018-08-07 10:32:53','2018-08-07 10:32:53','express_user@%',NULL,NULL),(151,'ticket_payment',NULL,NULL,0,90,'Оплата',1,1,'2018-08-07 10:33:13',NULL,NULL,'2018-08-07 10:33:13','2018-08-07 10:33:13','express_user@%',NULL,NULL),(152,'ticket_repayment',NULL,NULL,0,90,'Погашение',1,1,'2018-08-07 10:33:44',NULL,NULL,'2018-08-07 10:33:44','2018-08-07 10:33:44','express_user@%',NULL,NULL),(153,'ticket_bank_refund',NULL,NULL,0,90,'Возврат банком',1,1,'2018-08-07 10:34:44',1,1,'2018-08-07 10:34:44','2018-08-07 10:34:30','express_user@%','2018-08-07 10:34:44','express_user@%'),(154,'ticket_refund',NULL,NULL,0,90,'Возврат',1,1,'2018-08-07 10:35:06',NULL,NULL,'2018-08-07 10:35:06','2018-08-07 10:35:06','express_user@%',NULL,NULL),(155,'additional_request',NULL,NULL,0,83,'Заявка',1,1,'2018-08-07 12:18:39',NULL,NULL,'2018-08-07 12:18:39','2018-08-07 12:18:39','express_user@%',NULL,NULL),(156,'additional_payment',NULL,NULL,0,83,'Оплата',1,1,'2018-08-07 12:19:11',1,1,'2018-08-07 12:19:11','2018-08-07 12:19:00','express_user@%','2018-08-07 12:19:11','express_user@%'),(157,'kolibri','Колибри','fa fa-rebel fa-fw',1,123,'Колибри',1,1,'2018-08-08 12:32:30',NULL,NULL,'2018-08-08 12:32:30','2018-08-08 12:32:30','express_user@%',NULL,NULL),(158,'kolibri_request',NULL,NULL,0,157,'Заявка',1,1,'2018-08-08 12:33:13',NULL,NULL,'2018-08-08 12:33:13','2018-08-08 12:33:13','express_user@%',NULL,NULL),(159,'kolibri_payment',NULL,NULL,0,157,'Оплата',1,1,'2018-08-08 12:34:40',1,1,'2018-08-08 12:34:40','2018-08-08 12:33:32','express_user@%','2018-08-08 12:34:40','express_user@%'),(160,'kolibri_repayment',NULL,NULL,0,157,'Погашение',1,1,'2018-08-08 12:34:35',1,1,'2018-08-08 12:34:35','2018-08-08 12:33:45','express_user@%','2018-08-08 12:34:35','express_user@%'),(161,'kolibri_refund',NULL,NULL,0,157,'Возврат',1,1,'2018-08-08 12:34:51',1,1,'2018-08-08 12:34:51','2018-08-08 12:34:16','express_user@%','2018-08-08 12:34:51','express_user@%'),(162,'bum_request',NULL,NULL,0,87,'Заявка',1,1,'2018-08-10 09:00:14',1,1,'2018-08-10 09:00:14','2018-08-10 08:59:30','express_user@%','2018-08-10 09:00:14','express_user@%'),(163,'bum_payment',NULL,NULL,0,87,'Оплата',1,1,'2018-08-10 09:00:19',1,1,'2018-08-10 09:00:19','2018-08-10 08:59:52','express_user@%','2018-08-10 09:00:19','express_user@%'),(164,'bum_refund',NULL,NULL,0,87,'Выплаты',1,1,'2018-08-10 09:02:56',1,1,'2018-08-10 09:02:56','2018-08-10 09:00:06','express_user@%','2018-08-10 09:02:56','express_user@%');
/*!40000 ALTER TABLE `teh_node` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER teh_node$before_ins
BEFORE INSERT
ON teh_node FOR EACH ROW
BEGIN 
  SET NEW.ins_user = current_user();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER teh_node$before_upd
BEFORE UPDATE
ON teh_node FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `teh_permission`
--

DROP TABLE IF EXISTS `teh_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teh_permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `role_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на teh_role',
  `node_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на teh_node',
  `access_type` enum('read','write','delete') NOT NULL COMMENT 'Вид доступа',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$teh_permission$role_id$node_id` (`role_id`,`node_id`),
  KEY `fk$teh_permission$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$teh_permission$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$teh_permission$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$teh_permission$change_office_id$book_office$id` (`change_office_id`),
  KEY `fk$teh_permission$node_id$teh_node$id` (`node_id`),
  CONSTRAINT `fk$teh_permission$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_permission$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_permission$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_permission$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_permission$node_id$teh_node$id` FOREIGN KEY (`node_id`) REFERENCES `teh_node` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk$teh_permission$role_id$teh_role$id` FOREIGN KEY (`role_id`) REFERENCES `teh_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8 COMMENT='Узлы дерева элементов системы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teh_permission`
--

LOCK TABLES `teh_permission` WRITE;
/*!40000 ALTER TABLE `teh_permission` DISABLE KEYS */;
INSERT INTO `teh_permission` VALUES (10,1,81,'delete',NULL,NULL,NULL,'2018-07-31 06:19:51',1,1,'2018-07-31 06:19:51','2018-07-10 09:31:03','express_user@%','2018-07-31 06:19:51','express_user@%'),(11,1,90,'delete',NULL,NULL,NULL,'2018-07-31 06:19:51',1,1,'2018-07-31 06:19:51','2018-07-10 10:46:06','express_user@%','2018-07-31 06:19:51','express_user@%'),(12,1,91,'delete',NULL,NULL,NULL,'2018-07-31 06:19:51',1,1,'2018-07-31 06:19:51','2018-07-10 10:46:06','express_user@%','2018-07-31 06:19:51','express_user@%'),(13,1,92,'delete',NULL,NULL,NULL,'2018-07-31 06:19:51',1,1,'2018-07-31 06:19:51','2018-07-10 10:46:06','express_user@%','2018-07-31 06:19:51','express_user@%'),(14,1,96,'delete',NULL,NULL,NULL,'2018-07-31 06:19:51',1,1,'2018-07-31 06:19:51','2018-07-10 10:46:06','express_user@%','2018-07-31 06:19:51','express_user@%'),(20,1,88,'delete',NULL,NULL,NULL,'2018-07-31 06:19:51',1,1,'2018-07-31 06:19:51','2018-07-10 11:08:51','express_user@%','2018-07-31 06:19:51','express_user@%'),(21,1,93,'delete',NULL,NULL,NULL,'2018-07-31 07:46:22',1,1,'2018-07-31 07:46:22','2018-07-10 11:11:54','express_user@%','2018-07-31 07:46:22','express_user@%'),(22,1,95,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-10 11:11:54','express_user@%','2018-07-31 06:19:52','express_user@%'),(31,1,94,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-10 11:51:21','express_user@%','2018-07-31 06:19:52','express_user@%'),(33,1,114,'delete',NULL,NULL,NULL,'2018-07-31 06:19:53',1,1,'2018-07-31 06:19:53','2018-07-10 11:51:21','express_user@%','2018-07-31 06:19:53','express_user@%'),(35,1,124,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-10 11:51:21','express_user@%','2018-07-31 06:19:52','express_user@%'),(37,1,134,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-10 11:51:21','express_user@%','2018-07-31 06:19:52','express_user@%'),(43,2,84,'write',NULL,NULL,NULL,'2018-07-10 13:16:50',NULL,NULL,'2018-07-10 13:16:50','2018-07-10 13:16:50','express_user@%',NULL,NULL),(44,2,85,'write',NULL,NULL,NULL,'2018-07-10 13:16:50',NULL,NULL,'2018-07-10 13:16:50','2018-07-10 13:16:50','express_user@%',NULL,NULL),(45,2,86,'write',NULL,NULL,NULL,'2018-07-10 13:16:50',NULL,NULL,'2018-07-10 13:16:50','2018-07-10 13:16:50','express_user@%',NULL,NULL),(46,2,87,'write',NULL,NULL,NULL,'2018-07-10 13:16:50',NULL,NULL,'2018-07-10 13:16:50','2018-07-10 13:16:50','express_user@%',NULL,NULL),(47,2,88,'write',NULL,NULL,NULL,'2018-07-10 13:16:50',NULL,NULL,'2018-07-10 13:16:50','2018-07-10 13:16:50','express_user@%',NULL,NULL),(48,2,89,'write',NULL,NULL,NULL,'2018-07-10 13:16:50',NULL,NULL,'2018-07-10 13:16:50','2018-07-10 13:16:50','express_user@%',NULL,NULL),(49,2,91,'write',NULL,NULL,NULL,'2018-07-10 13:16:50',NULL,NULL,'2018-07-10 13:16:50','2018-07-10 13:16:50','express_user@%',NULL,NULL),(50,2,96,'write',NULL,NULL,NULL,'2018-07-10 13:16:50',NULL,NULL,'2018-07-10 13:16:50','2018-07-10 13:16:50','express_user@%',NULL,NULL),(51,2,90,'write',NULL,NULL,NULL,'2018-07-10 13:17:27',NULL,NULL,'2018-07-10 13:17:27','2018-07-10 13:17:27','express_user@%',NULL,NULL),(52,12,81,'delete',NULL,NULL,NULL,'2018-07-12 10:31:56',NULL,NULL,'2018-07-12 10:31:56','2018-07-12 10:31:56','express_user@%',NULL,NULL),(53,12,82,'delete',NULL,NULL,NULL,'2018-07-12 10:31:56',NULL,NULL,'2018-07-12 10:31:56','2018-07-12 10:31:56','express_user@%',NULL,NULL),(57,1,87,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-12 13:07:34','express_user@%','2018-07-31 06:19:52','express_user@%'),(58,1,126,'delete',NULL,NULL,NULL,'2018-07-31 06:21:56',1,1,'2018-07-31 06:21:56','2018-07-12 13:07:34','express_user@%','2018-07-31 06:21:56','express_user@%'),(59,1,139,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-13 07:36:06','express_user@%','2018-07-31 06:19:52','express_user@%'),(60,1,138,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-13 07:36:06','express_user@%','2018-07-31 06:19:52','express_user@%'),(61,1,137,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-13 07:36:06','express_user@%','2018-07-31 06:19:52','express_user@%'),(62,1,136,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-13 07:36:06','express_user@%','2018-07-31 06:19:52','express_user@%'),(63,1,135,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-13 07:36:06','express_user@%','2018-07-31 06:19:52','express_user@%'),(64,1,115,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-13 09:02:26','express_user@%','2018-07-31 06:19:52','express_user@%'),(65,1,113,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-13 11:14:37','express_user@%','2018-07-31 06:19:52','express_user@%'),(66,1,140,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-13 12:39:30','express_user@%','2018-07-31 06:19:52','express_user@%'),(67,1,141,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-14 07:45:32','express_user@%','2018-07-31 06:19:52','express_user@%'),(68,1,142,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-14 07:46:23','express_user@%','2018-07-31 06:19:52','express_user@%'),(69,1,143,'delete',NULL,NULL,NULL,'2018-07-31 06:19:52',1,1,'2018-07-31 06:19:52','2018-07-14 09:41:16','express_user@%','2018-07-31 06:19:52','express_user@%'),(70,1,144,'delete',NULL,NULL,NULL,'2018-07-31 06:19:53',1,1,'2018-07-31 06:19:53','2018-07-14 10:17:25','express_user@%','2018-07-31 06:19:53','express_user@%'),(81,1,86,'delete',NULL,1,1,'2018-07-31 07:45:19',1,1,'2018-07-31 07:45:19','2018-07-31 06:21:56','express_user@%','2018-07-31 07:45:19','express_user@%'),(82,1,89,'delete',NULL,1,1,'2018-07-31 07:45:19',1,1,'2018-07-31 07:45:19','2018-07-31 06:21:56','express_user@%','2018-07-31 07:45:19','express_user@%'),(83,1,145,'delete',NULL,1,1,'2018-07-31 07:45:20',1,1,'2018-07-31 07:45:20','2018-07-31 06:21:56','express_user@%','2018-07-31 07:45:20','express_user@%'),(84,1,83,'delete',NULL,1,1,'2018-08-01 11:45:56',1,1,'2018-08-01 11:45:56','2018-07-31 07:46:22','express_user@%','2018-08-01 11:45:56','express_user@%'),(85,1,84,'delete',NULL,1,1,'2018-08-01 11:45:56',1,1,'2018-08-01 11:45:56','2018-07-31 07:46:22','express_user@%','2018-08-01 11:45:56','express_user@%'),(86,1,85,'delete',NULL,1,1,'2018-08-01 11:45:56',1,1,'2018-08-01 11:45:56','2018-07-31 07:46:22','express_user@%','2018-08-01 11:45:56','express_user@%'),(87,1,97,'delete',NULL,1,1,'2018-08-03 12:17:40',1,1,'2018-08-03 12:17:40','2018-08-03 10:48:35','express_user@%','2018-08-03 12:17:40','express_user@%'),(88,1,98,'delete',NULL,1,1,'2018-08-03 12:17:40',1,1,'2018-08-03 12:17:40','2018-08-03 11:11:48','express_user@%','2018-08-03 12:17:40','express_user@%'),(89,1,127,'delete',NULL,1,1,'2018-08-06 09:55:58',1,1,'2018-08-06 09:55:58','2018-08-03 12:17:40','express_user@%','2018-08-06 09:55:58','express_user@%'),(90,1,128,'delete',NULL,1,1,'2018-08-06 09:55:58',1,1,'2018-08-06 09:55:58','2018-08-03 12:17:40','express_user@%','2018-08-06 09:55:58','express_user@%'),(91,1,146,'delete',NULL,1,1,'2018-08-06 09:55:58',1,1,'2018-08-06 09:55:58','2018-08-03 12:17:40','express_user@%','2018-08-06 09:55:58','express_user@%'),(92,8,98,'delete',NULL,1,1,'2018-08-03 12:48:13',1,1,'2018-08-03 12:48:13','2018-08-03 12:45:43','express_user@%','2018-08-03 12:48:13','express_user@%'),(93,8,97,'delete',NULL,1,1,'2018-08-03 12:48:13',1,1,'2018-08-03 12:48:13','2018-08-03 12:45:43','express_user@%','2018-08-03 12:48:13','express_user@%'),(94,8,128,'delete',NULL,1,1,'2018-08-03 12:48:13',1,1,'2018-08-03 12:48:13','2018-08-03 12:45:43','express_user@%','2018-08-03 12:48:13','express_user@%'),(95,8,146,'read',NULL,1,1,'2018-08-03 12:48:13',1,1,'2018-08-03 12:48:13','2018-08-03 12:45:43','express_user@%','2018-08-03 12:48:13','express_user@%'),(96,7,97,'delete',NULL,1,1,'2018-08-03 12:47:55',1,1,'2018-08-03 12:47:55','2018-08-03 12:45:55','express_user@%','2018-08-03 12:47:55','express_user@%'),(97,10,97,'read',NULL,1,1,'2018-08-03 12:48:24',1,1,'2018-08-03 12:48:24','2018-08-03 12:46:25','express_user@%','2018-08-03 12:48:24','express_user@%'),(98,10,98,'read',NULL,1,1,'2018-08-03 12:48:24',1,1,'2018-08-03 12:48:24','2018-08-03 12:46:25','express_user@%','2018-08-03 12:48:24','express_user@%'),(99,10,127,'delete',NULL,1,1,'2018-08-03 12:48:24',1,1,'2018-08-03 12:48:24','2018-08-03 12:46:25','express_user@%','2018-08-03 12:48:24','express_user@%'),(100,10,146,'delete',NULL,1,1,'2018-08-03 12:48:24',1,1,'2018-08-03 12:48:24','2018-08-03 12:46:25','express_user@%','2018-08-03 12:48:24','express_user@%'),(101,10,128,'read',NULL,1,1,'2018-08-03 12:48:24',1,1,'2018-08-03 12:48:24','2018-08-03 12:46:25','express_user@%','2018-08-03 12:48:24','express_user@%'),(102,7,81,'read',NULL,1,1,'2018-08-03 13:13:05',1,1,'2018-08-03 13:13:05','2018-08-03 12:47:55','express_user@%','2018-08-03 13:13:05','express_user@%'),(103,8,81,'read',NULL,1,1,'2018-08-03 13:13:21',1,1,'2018-08-03 13:13:21','2018-08-03 12:48:13','express_user@%','2018-08-03 13:13:21','express_user@%'),(104,10,81,'delete',NULL,1,1,'2018-08-07 10:41:16',1,1,'2018-08-07 10:41:16','2018-08-03 12:48:25','express_user@%','2018-08-07 10:41:16','express_user@%'),(105,7,93,'read',NULL,1,1,'2018-08-07 10:40:41',1,1,'2018-08-07 10:40:41','2018-08-03 13:13:06','express_user@%','2018-08-07 10:40:41','express_user@%'),(106,1,147,'delete',NULL,1,1,'2018-08-06 09:58:11',1,1,'2018-08-06 09:58:11','2018-08-06 09:55:58','express_user@%','2018-08-06 09:58:11','express_user@%'),(107,1,148,'delete',NULL,1,1,'2018-08-06 09:57:40',1,1,'2018-08-06 09:57:40','2018-08-06 09:55:58','express_user@%','2018-08-06 09:57:40','express_user@%'),(108,1,149,'delete',NULL,1,1,'2018-08-06 09:57:19',1,1,'2018-08-06 09:57:19','2018-08-06 09:55:59','express_user@%','2018-08-06 09:57:19','express_user@%'),(109,1,150,'delete',NULL,1,1,'2018-08-07 12:25:08',1,1,'2018-08-07 12:25:08','2018-08-07 10:35:47','express_user@%','2018-08-07 12:25:08','express_user@%'),(110,1,151,'delete',NULL,1,1,'2018-08-07 12:25:08',1,1,'2018-08-07 12:25:08','2018-08-07 10:35:47','express_user@%','2018-08-07 12:25:08','express_user@%'),(111,1,152,'delete',NULL,1,1,'2018-08-07 12:25:08',1,1,'2018-08-07 12:25:08','2018-08-07 10:35:47','express_user@%','2018-08-07 12:25:08','express_user@%'),(112,1,153,'delete',NULL,1,1,'2018-08-07 12:25:08',1,1,'2018-08-07 12:25:08','2018-08-07 10:35:47','express_user@%','2018-08-07 12:25:08','express_user@%'),(113,1,154,'delete',NULL,1,1,'2018-08-07 12:25:08',1,1,'2018-08-07 12:25:08','2018-08-07 10:35:47','express_user@%','2018-08-07 12:25:08','express_user@%'),(114,8,150,'delete',NULL,1,1,'2018-08-07 10:40:21',1,1,'2018-08-07 10:40:21','2018-08-07 10:40:12','express_user@%','2018-08-07 10:40:21','express_user@%'),(115,8,151,'delete',NULL,1,1,'2018-08-07 10:40:21',1,1,'2018-08-07 10:40:21','2018-08-07 10:40:12','express_user@%','2018-08-07 10:40:21','express_user@%'),(116,8,154,'delete',NULL,1,1,'2018-08-07 10:40:21',1,1,'2018-08-07 10:40:21','2018-08-07 10:40:12','express_user@%','2018-08-07 10:40:21','express_user@%'),(117,8,90,'delete',NULL,1,1,'2018-08-10 07:29:27',1,1,'2018-08-10 07:29:27','2018-08-07 10:40:21','express_user@%','2018-08-10 07:29:27','express_user@%'),(118,7,150,'delete',NULL,1,1,'2018-08-10 07:29:52',1,1,'2018-08-10 07:29:52','2018-08-07 10:40:41','express_user@%','2018-08-10 07:29:52','express_user@%'),(119,7,90,'delete',NULL,1,1,'2018-08-10 07:29:52',1,1,'2018-08-10 07:29:52','2018-08-07 10:40:41','express_user@%','2018-08-10 07:29:52','express_user@%'),(120,10,150,'read',NULL,1,1,'2018-08-07 10:41:16',NULL,NULL,'2018-08-07 10:41:16','2018-08-07 10:41:16','express_user@%',NULL,NULL),(121,10,90,'delete',NULL,1,1,'2018-08-07 10:41:16',NULL,NULL,'2018-08-07 10:41:16','2018-08-07 10:41:16','express_user@%',NULL,NULL),(122,10,151,'read',NULL,1,1,'2018-08-07 10:41:16',NULL,NULL,'2018-08-07 10:41:16','2018-08-07 10:41:16','express_user@%',NULL,NULL),(123,10,152,'delete',NULL,1,1,'2018-08-07 10:41:16',NULL,NULL,'2018-08-07 10:41:16','2018-08-07 10:41:16','express_user@%',NULL,NULL),(124,10,153,'delete',NULL,1,1,'2018-08-07 10:41:16',NULL,NULL,'2018-08-07 10:41:16','2018-08-07 10:41:16','express_user@%',NULL,NULL),(125,10,154,'read',NULL,1,1,'2018-08-07 10:41:16',NULL,NULL,'2018-08-07 10:41:16','2018-08-07 10:41:16','express_user@%',NULL,NULL),(126,1,155,'delete',NULL,1,1,'2018-08-08 12:35:13',1,1,'2018-08-08 12:35:13','2018-08-07 12:25:08','express_user@%','2018-08-08 12:35:13','express_user@%'),(127,1,156,'delete',NULL,1,1,'2018-08-08 12:35:13',1,1,'2018-08-08 12:35:13','2018-08-07 12:25:08','express_user@%','2018-08-08 12:35:13','express_user@%'),(128,1,157,'delete',NULL,1,1,'2018-08-10 09:00:47',1,1,'2018-08-10 09:00:47','2018-08-08 12:35:13','express_user@%','2018-08-10 09:00:47','express_user@%'),(129,1,158,'delete',NULL,1,1,'2018-08-10 09:00:47',1,1,'2018-08-10 09:00:47','2018-08-08 12:35:13','express_user@%','2018-08-10 09:00:47','express_user@%'),(130,1,159,'delete',NULL,1,1,'2018-08-10 09:00:47',1,1,'2018-08-10 09:00:47','2018-08-08 12:35:13','express_user@%','2018-08-10 09:00:47','express_user@%'),(131,1,160,'delete',NULL,1,1,'2018-08-10 09:00:47',1,1,'2018-08-10 09:00:47','2018-08-08 12:35:13','express_user@%','2018-08-10 09:00:47','express_user@%'),(132,1,161,'delete',NULL,1,1,'2018-08-10 09:00:47',1,1,'2018-08-10 09:00:47','2018-08-08 12:35:13','express_user@%','2018-08-10 09:00:47','express_user@%'),(133,8,157,'delete',NULL,1,1,'2018-08-10 12:38:53',1,1,'2018-08-10 12:38:53','2018-08-10 07:29:27','express_user@%','2018-08-10 12:38:53','express_user@%'),(134,8,158,'delete',NULL,1,1,'2018-08-10 12:38:53',1,1,'2018-08-10 12:38:53','2018-08-10 07:29:27','express_user@%','2018-08-10 12:38:53','express_user@%'),(135,8,159,'delete',NULL,1,1,'2018-08-10 12:38:53',1,1,'2018-08-10 12:38:53','2018-08-10 07:29:27','express_user@%','2018-08-10 12:38:53','express_user@%'),(136,8,160,'delete',NULL,1,1,'2018-08-10 12:38:53',1,1,'2018-08-10 12:38:53','2018-08-10 07:29:27','express_user@%','2018-08-10 12:38:53','express_user@%'),(137,8,161,'delete',NULL,1,1,'2018-08-10 12:38:53',1,1,'2018-08-10 12:38:53','2018-08-10 07:29:27','express_user@%','2018-08-10 12:38:53','express_user@%'),(138,7,157,'delete',NULL,1,1,'2018-08-10 07:29:52',NULL,NULL,'2018-08-10 07:29:52','2018-08-10 07:29:52','express_user@%',NULL,NULL),(139,7,158,'delete',NULL,1,1,'2018-08-10 07:29:52',NULL,NULL,'2018-08-10 07:29:52','2018-08-10 07:29:52','express_user@%',NULL,NULL),(140,7,160,'write',NULL,1,1,'2018-08-10 07:29:52',NULL,NULL,'2018-08-10 07:29:52','2018-08-10 07:29:52','express_user@%',NULL,NULL),(141,1,162,'delete',NULL,1,1,'2018-08-16 11:09:24',1,1,'2018-08-16 11:09:24','2018-08-10 09:00:47','express_user@%','2018-08-16 11:09:24','express_user@%'),(142,1,163,'delete',NULL,1,1,'2018-08-16 11:09:24',1,1,'2018-08-16 11:09:24','2018-08-10 09:00:47','express_user@%','2018-08-16 11:09:24','express_user@%'),(143,1,164,'delete',NULL,1,1,'2018-08-16 11:09:25',1,1,'2018-08-16 11:09:25','2018-08-10 09:00:47','express_user@%','2018-08-16 11:09:25','express_user@%'),(144,8,162,'delete',NULL,1,1,'2018-08-10 12:38:53',NULL,NULL,'2018-08-10 12:38:53','2018-08-10 12:38:53','express_user@%',NULL,NULL),(145,8,87,'delete',NULL,1,1,'2018-08-10 12:38:53',NULL,NULL,'2018-08-10 12:38:53','2018-08-10 12:38:53','express_user@%',NULL,NULL),(146,8,164,'delete',NULL,1,1,'2018-08-10 12:38:53',NULL,NULL,'2018-08-10 12:38:53','2018-08-10 12:38:53','express_user@%',NULL,NULL),(147,8,163,'delete',NULL,1,1,'2018-08-10 12:38:53',NULL,NULL,'2018-08-10 12:38:53','2018-08-10 12:38:53','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `teh_permission` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER teh_permission$before_ins
BEFORE INSERT
ON teh_permission FOR EACH ROW
BEGIN 
  SET NEW.ins_user = current_user();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER teh_permission$before_upd
BEFORE UPDATE
ON teh_permission FOR EACH ROW 
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `teh_positions`
--

DROP TABLE IF EXISTS `teh_positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teh_positions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(80) NOT NULL COMMENT 'Наименование должности',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `create_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_date_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_user_id` int(11) unsigned DEFAULT NULL,
  `create_office_id` int(11) unsigned DEFAULT NULL,
  `change_user_id` int(11) unsigned DEFAULT NULL,
  `change_office_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$teh_positions$descr` (`descr`),
  KEY `fk$teh_positions$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$teh_positions$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$teh_positions$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$teh_positions$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$teh_positions$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_positions$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_positions$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_positions$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Должности';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teh_positions`
--

LOCK TABLES `teh_positions` WRITE;
/*!40000 ALTER TABLE `teh_positions` DISABLE KEYS */;
INSERT INTO `teh_positions` VALUES (1,'Менеджер',0,'2017-09-29 13:06:26','express_user@%',NULL,NULL,'2018-07-16 06:14:45',NULL,NULL,NULL,NULL,NULL),(2,'Кассир',0,'2017-09-29 13:06:26','express_user@%',NULL,NULL,'2018-07-16 06:14:45',NULL,NULL,NULL,NULL,NULL),(3,'Контроллер',0,'2017-09-29 13:06:26','express_user@%',NULL,NULL,'2018-07-16 06:14:45',NULL,NULL,NULL,NULL,NULL),(4,'Руководитель отделения',0,'2017-09-29 13:06:26','express_user@%',NULL,NULL,'2018-07-16 06:14:45',NULL,NULL,NULL,NULL,NULL),(5,'Разработчик',0,'2017-09-29 13:06:26','express_user@%',NULL,NULL,'2018-07-16 06:14:45',NULL,NULL,NULL,NULL,NULL),(6,'Программист',0,'2018-02-23 10:28:27','express_user@%',NULL,NULL,'2018-07-16 06:14:45',NULL,NULL,NULL,NULL,NULL),(7,'Бухгалтер',1,'2018-07-16 06:31:07','express_user@%','2018-07-16 06:31:13','express_user@%','2018-07-16 06:31:07','2018-07-16 06:31:13',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `teh_positions` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER teh_positions$before_ins
BEFORE INSERT
ON teh_positions FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER teh_positions$before_upd
BEFORE UPDATE
ON teh_positions FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `teh_role`
--

DROP TABLE IF EXISTS `teh_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teh_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `descr` varchar(80) NOT NULL COMMENT 'Название роли',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$teh_role$descr` (`descr`),
  KEY `fk$teh_role$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$teh_role$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$teh_role$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$teh_role$change_office_id$book_office$id` (`change_office_id`),
  CONSTRAINT `fk$teh_role$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_role$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_role$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_role$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='Роли';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teh_role`
--

LOCK TABLES `teh_role` WRITE;
/*!40000 ALTER TABLE `teh_role` DISABLE KEYS */;
INSERT INTO `teh_role` VALUES (1,'admin','Администратор',NULL,NULL,'2018-07-31 06:15:02',1,1,'2018-07-31 06:15:02','2018-06-21 11:28:16','express_user@%','2018-07-31 06:15:02','express_user@%'),(2,'director_general','Генеральный директор',NULL,NULL,'2018-06-21 11:28:16',NULL,NULL,NULL,'2018-06-21 11:28:16','express_user@%',NULL,NULL),(3,'director_executive','Исполнительный директор',NULL,NULL,'2018-06-21 11:28:16',NULL,NULL,NULL,'2018-06-21 11:28:16','express_user@%',NULL,NULL),(4,'director_financial','Финансовый директор',NULL,NULL,'2018-06-21 11:28:16',NULL,NULL,NULL,'2018-06-21 11:28:16','express_user@%',NULL,NULL),(5,'director_сommercial','Коммерческий директор',NULL,NULL,'2018-06-21 11:28:16',NULL,NULL,NULL,'2018-06-21 11:28:16','express_user@%',NULL,NULL),(6,'director_network','РСО',NULL,NULL,'2018-06-21 11:28:16',NULL,NULL,NULL,'2018-06-21 11:28:16','express_user@%',NULL,NULL),(7,'manager','Менеджер',NULL,NULL,'2018-08-03 12:45:55',1,1,'2018-08-03 12:45:55','2018-06-21 11:28:16','express_user@%','2018-08-03 12:45:55','express_user@%'),(8,'cashier','Кассир',NULL,NULL,'2018-08-03 12:45:43',1,1,'2018-08-03 12:45:43','2018-06-21 11:28:16','express_user@%','2018-08-03 12:45:43','express_user@%'),(9,'director_department','Руководитель отделения',NULL,NULL,'2018-06-21 11:28:16',NULL,NULL,NULL,'2018-06-21 11:28:16','express_user@%',NULL,NULL),(10,'operator','Контролер',NULL,NULL,'2018-08-03 12:46:25',1,1,'2018-08-03 12:46:25','2018-06-21 11:28:16','express_user@%','2018-08-03 12:46:25','express_user@%'),(11,'operator_senior','Старший контролер',NULL,NULL,'2018-06-21 11:28:16',NULL,NULL,NULL,'2018-06-21 11:28:16','express_user@%',NULL,NULL),(12,'operator_main','Главный контролер',NULL,NULL,'2018-06-21 11:28:16',NULL,NULL,NULL,'2018-06-21 11:28:16','express_user@%',NULL,NULL);
/*!40000 ALTER TABLE `teh_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER teh_role$before_ins
BEFORE INSERT
ON teh_role FOR EACH ROW
BEGIN 
  SET NEW.ins_user = current_user();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER teh_role$before_upd
BEFORE UPDATE
ON teh_role FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `teh_timesheet`
--

DROP TABLE IF EXISTS `teh_timesheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teh_timesheet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `boss_id` int(10) unsigned NOT NULL COMMENT 'Руководитель. Ссылка на teh_user',
  `office_id` int(10) unsigned NOT NULL COMMENT 'Отделение. Ссылка на book_office',
  `working_day` date NOT NULL COMMENT 'Рабочий день',
  `user_id` int(10) unsigned NOT NULL COMMENT 'Подчиненный сотрудник. Ссылка на teh_user',
  `positions_id` int(10) unsigned NOT NULL COMMENT 'Должность. Ссалка на teh_positions',
  `clock` tinyint(1) unsigned DEFAULT NULL COMMENT 'Количество отработанных часов за день',
  `time_open` timestamp NULL DEFAULT NULL COMMENT 'Время открытия отделения',
  `time_close` timestamp NULL DEFAULT NULL COMMENT 'Время закрытия отделения',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'В архиве? 0-нет, 1-да',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `ins_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  `upd_php` int(10) unsigned DEFAULT NULL COMMENT 'PHP добавил запись. teh_user.id',
  PRIMARY KEY (`id`),
  KEY `fk$teh_timesheet$boss_id$teh_user$id` (`boss_id`),
  KEY `fk$teh_timesheet$office_id$book_office$id` (`office_id`),
  KEY `fk$teh_timesheet$user_id$teh_user$id` (`user_id`),
  KEY `fk$teh_timesheet$positions_id$teh_positions$id` (`positions_id`),
  KEY `fk$teh_timesheet$ins_php$teh_user$id` (`ins_php`),
  KEY `fk$teh_timesheet$upd_php$teh_user$id` (`upd_php`),
  CONSTRAINT `fk$teh_timesheet$boss_id$teh_user$id` FOREIGN KEY (`boss_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_timesheet$ins_php$teh_user$id` FOREIGN KEY (`ins_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_timesheet$office_id$book_office$id` FOREIGN KEY (`office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_timesheet$positions_id$teh_positions$id` FOREIGN KEY (`positions_id`) REFERENCES `teh_positions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_timesheet$upd_php$teh_user$id` FOREIGN KEY (`upd_php`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_timesheet$user_id$teh_user$id` FOREIGN KEY (`user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Табель учета';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teh_timesheet`
--

LOCK TABLES `teh_timesheet` WRITE;
/*!40000 ALTER TABLE `teh_timesheet` DISABLE KEYS */;
/*!40000 ALTER TABLE `teh_timesheet` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 trigger teh_timesheet$before_ins
before insert on teh_timesheet for each row
begin
  set new.ins_user = current_user();
  
  if isnull(new.working_day) then
    set new.working_day = curdate();
  end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 trigger teh_timesheet$before_upd
before update on teh_timesheet for each row
begin
  set new.upd_user = current_user();
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `teh_user`
--

DROP TABLE IF EXISTS `teh_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teh_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `login_phone` varchar(12) DEFAULT NULL COMMENT 'Логин - телефон',
  `pass_hash` varchar(64) DEFAULT NULL COMMENT 'Хэш пароля',
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `works` enum('no','yes') NOT NULL DEFAULT 'yes' COMMENT 'yes - работает, no - не работате',
  `role_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на teh_role',
  `phone` varchar(12) DEFAULT NULL COMMENT 'Телефон',
  `email` varchar(320) DEFAULT NULL COMMENT 'E-mail',
  `last_name` varchar(60) NOT NULL COMMENT 'Фамилия',
  `first_name` varchar(60) NOT NULL COMMENT 'Имя',
  `middle_name` varchar(60) NOT NULL COMMENT 'Отчество',
  `gender` enum('m','f') NOT NULL COMMENT 'Пол. m - мужской, f - женский.',
  `birthday` date NOT NULL COMMENT 'День рождения',
  `office_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на book_office',
  `position_id` int(10) unsigned DEFAULT NULL COMMENT 'Ссылка на teh_positions',
  `passport_number` char(8) DEFAULT NULL COMMENT 'Серия и номер паспорта',
  `passport_by` varchar(200) DEFAULT NULL COMMENT 'Кем выдан паспорт',
  `passport_when` date DEFAULT NULL COMMENT 'Когда выдан паспорт',
  `tin` char(12) DEFAULT NULL COMMENT 'ИНН',
  `formal_country_id` int(10) unsigned DEFAULT NULL COMMENT 'Прописка. Страна. Ссылка на book_country',
  `formal_city_id` int(10) unsigned DEFAULT NULL COMMENT 'Прописка. Город. Ссылка на book_city',
  `formal_address` varchar(160) DEFAULT NULL COMMENT 'Прописка. Адрес: кв/ул дом квартира',
  `fact_country_id` int(10) unsigned DEFAULT NULL COMMENT 'Проживание. Страна. Ссылка на book_country',
  `fact_city_id` int(10) unsigned DEFAULT NULL COMMENT 'Проживание. Город. Ссылка на book_city',
  `fact_address` varchar(160) DEFAULT NULL COMMENT 'Проживание. Адрес: кв/ул дом квартира',
  `avatar` varchar(255) DEFAULT NULL COMMENT 'Фото или аватар пользователя',
  `note` varchar(600) DEFAULT NULL COMMENT 'Комментарий',
  `create_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем создана?',
  `create_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где создана?',
  `create_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда создана?',
  `change_user_id` int(10) unsigned DEFAULT NULL COMMENT 'Кем изменена?',
  `change_office_id` int(10) unsigned DEFAULT NULL COMMENT 'Где изменена?',
  `change_date_time` timestamp NULL DEFAULT NULL COMMENT 'Когда изменена?',
  `ins_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Когда добавили запись',
  `ins_user` varchar(93) NOT NULL COMMENT 'Кто добавил запись',
  `upd_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Когда изменили запись',
  `upd_user` varchar(93) DEFAULT NULL COMMENT 'Кто изменил запись',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk$teh_user$email` (`email`),
  UNIQUE KEY `uk$teh_user$phone` (`phone`),
  UNIQUE KEY `uk$teh_worker$login_phone` (`login_phone`),
  UNIQUE KEY `uk$teh_user$passport_number` (`passport_number`),
  UNIQUE KEY `uk$teh_user$tin` (`tin`),
  KEY `fk$teh_user$formal_country_id$book_country$id` (`formal_country_id`),
  KEY `fk$teh_user$formal_city_id$book_city$id` (`formal_city_id`),
  KEY `fk$teh_user$fact_country_id$book_country$id` (`fact_country_id`),
  KEY `fk$teh_user$fact_city_id$book_city$id` (`fact_city_id`),
  KEY `fk$teh_user$create_user_id$teh_user$id` (`create_user_id`),
  KEY `fk$teh_user$create_office_id$book_office$id` (`create_office_id`),
  KEY `fk$teh_user$change_user_id$teh_user$id` (`change_user_id`),
  KEY `fk$teh_user$change_office_id$book_office$id` (`change_office_id`),
  KEY `fk$teh_user$position_id$teh_positions$id` (`position_id`),
  KEY `fk$teh_user$office_id$book_office$id` (`office_id`),
  KEY `teh_user$role_id$teh_role$id` (`role_id`),
  CONSTRAINT `fk$teh_user$change_office_id$book_office$id` FOREIGN KEY (`change_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_user$change_user_id$teh_user$id` FOREIGN KEY (`change_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_user$create_office_id$book_office$id` FOREIGN KEY (`create_office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_user$create_user_id$teh_user$id` FOREIGN KEY (`create_user_id`) REFERENCES `teh_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_user$fact_city_id$book_city$id` FOREIGN KEY (`fact_city_id`) REFERENCES `book_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_user$fact_country_id$book_country$id` FOREIGN KEY (`fact_country_id`) REFERENCES `book_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_user$formal_city_id$book_city$id` FOREIGN KEY (`formal_city_id`) REFERENCES `book_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_user$formal_country_id$book_country$id` FOREIGN KEY (`formal_country_id`) REFERENCES `book_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_user$office_id$book_office$id` FOREIGN KEY (`office_id`) REFERENCES `book_office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk$teh_user$position_id$teh_positions$id` FOREIGN KEY (`position_id`) REFERENCES `teh_positions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `teh_user$role_id$teh_role$id` FOREIGN KEY (`role_id`) REFERENCES `teh_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Должности';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teh_user`
--

LOCK TABLES `teh_user` WRITE;
/*!40000 ALTER TABLE `teh_user` DISABLE KEYS */;
INSERT INTO `teh_user` VALUES (1,'380663794572','f53c6169c9fb99d59322255ac124dbec','$2b$10$WSv3AvAEAWErJKuDont0oep.mVkDSNhB0bH4av6TAnKwVmCy9j15m','IaS250cWNwPiLu7BLcKJUig3Z0VU8f7JtN3z9xSLSh4aT3r33HP8Q5r736oh','yes',1,'380663794572','jscodepkc@gmail.com','Черных','Александр','Викторович','m','1983-03-22',1,NULL,NULL,NULL,'2018-06-22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'2018-07-03 11:43:28','2018-02-23 10:23:07','express_user@%','2018-08-12 10:57:47','express_user@%'),(2,'380660011100','d1e8427f465e981ab968e03596b121ae','$2y$10$MIqF9HpF0Bg.psvyNCMRcuaJTbLAFfVQyvBOVX34h92wSU7052qXO','mmZ2bwaBIpinCaFomMq8MivlJtNTeCUJVMoxaO9rAxYCAEtrdkKztAroBA4R','yes',8,'380660011100','simple@mail.ru','Сидоров','Сидор','Сидорович','m','2000-02-02',11,2,'ЕН123123','by','2014-12-12','02020208',NULL,NULL,'where',1,1,'address',NULL,NULL,1,1,'2018-06-21 13:49:19',1,1,'2018-08-04 05:59:36','2018-06-21 13:49:19','express_user@%','2018-08-10 13:07:11','express_user@%'),(3,'380660000000',NULL,'$2y$10$Gmh94H4JGV29he0JeMegk.d6MDTRmQtAGrUwXI9BOCsAaVOntGa4.','6jAU9waDSz6KQjFnnmqAO9Cj7z7lcGith7CLMUsSxWrSUfvwDGrGzt2UzL7K','yes',10,'380660000000',NULL,'Фёдорова','Анастасия','Андреевна','f','1999-01-01',1,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-14 09:47:25',NULL,NULL,'2018-08-07 10:48:42','2018-07-14 09:47:25','express_user@%','2018-08-07 10:50:29','express_user@%'),(4,'380661111111',NULL,'$2y$10$mrlRDFdQVO0PWG3/zfpd0.D2cChNLDJiKp7l2yOZBmfh2mQovEMWy','5JvcuesSGIyjaNRNpyyih7EbOVziLRVDvPjrt1mMUFR6QqLMbPM2G3jgqNzq','yes',7,'380661111111',NULL,'Филиппов','Карл','Олегович','m','1996-11-12',11,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-03 12:35:33',NULL,NULL,'2018-08-03 12:35:44','2018-08-03 12:35:33','express_user@%','2018-08-10 07:32:08','express_user@%');
/*!40000 ALTER TABLE `teh_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER `express_db`.`teh_user$before_ins` BEFORE INSERT
    ON express_db.teh_user FOR EACH ROW
BEGIN 
  SET NEW.ins_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`express_user`@`%`*/ /*!50003 TRIGGER teh_user$before_upd
BEFORE UPDATE
ON teh_user FOR EACH ROW
BEGIN 
  SET NEW.upd_user = CURRENT_USER();  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `test_book`
--

DROP TABLE IF EXISTS `test_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_book` (
  `id` int(10) unsigned DEFAULT NULL,
  `book` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_book`
--

LOCK TABLES `test_book` WRITE;
/*!40000 ALTER TABLE `test_book` DISABLE KEYS */;
INSERT INTO `test_book` VALUES (1,'Советская'),(2,'Оборонная'),(3,'Коцюбинскоо'),(3,'Коцюбинскоо2'),(4,'Шевченко'),(5,NULL);
/*!40000 ALTER TABLE `test_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_categories`
--

DROP TABLE IF EXISTS `test_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `number` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=963;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_categories`
--

LOCK TABLES `test_categories` WRITE;
/*!40000 ALTER TABLE `test_categories` DISABLE KEYS */;
INSERT INTO `test_categories` VALUES (1,'Ноутбуки',0,1),(2,'Acer',1,1),(3,'Lenovo',1,2),(4,'Apple',1,3),(5,'Macbook Air',4,1),(6,'Macbook Pro',4,2),(7,'Sony Vaio',1,4),(8,'Смартфоны',0,2),(9,'iPhone',8,1),(10,'Samsung',8,2),(11,'LG',8,3),(12,'Vertu',8,4),(13,'Комплектующие',0,3),(14,'Процессоры',13,1),(15,'Память',13,2),(16,'Видеокарты',13,3),(17,'Жесткие диски',13,4);
/*!40000 ALTER TABLE `test_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_doc`
--

DROP TABLE IF EXISTS `test_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_doc` (
  `id` int(10) unsigned DEFAULT NULL,
  `document` varchar(30) DEFAULT NULL,
  `book_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Документы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_doc`
--

LOCK TABLES `test_doc` WRITE;
/*!40000 ALTER TABLE `test_doc` DISABLE KEYS */;
INSERT INTO `test_doc` VALUES (1,'10',1),(2,'20',2),(3,'30',3),(4,'40',3),(5,'50',NULL);
/*!40000 ALTER TABLE `test_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_box_crossrate`
--

DROP TABLE IF EXISTS `v_box_crossrate`;
/*!50001 DROP VIEW IF EXISTS `v_box_crossrate`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_box_crossrate` AS SELECT 
 1 AS `office_id`,
 1 AS `box_id`,
 1 AS `currency`,
 1 AS `ret_rate_buy`,
 1 AS `ret_rate_buy_rub`,
 1 AS `ret_rate_sale`,
 1 AS `ret_rate_sale_rub`,
 1 AS `wh_rate_buy`,
 1 AS `wh_rate_buy_rub`,
 1 AS `wh_rate_sale`,
 1 AS `wh_rate_sale_rub`,
 1 AS `from_currency`,
 1 AS `to_currency`,
 1 AS `from_currency_id`,
 1 AS `to_currency_id`,
 1 AS `border`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_box_rate`
--

DROP TABLE IF EXISTS `v_box_rate`;
/*!50001 DROP VIEW IF EXISTS `v_box_rate`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_box_rate` AS SELECT 
 1 AS `box_id`,
 1 AS `currency`,
 1 AS `ret_rate_buy`,
 1 AS `ret_rate_buy_rub`,
 1 AS `ret_rate_sale`,
 1 AS `ret_rate_sale_rub`,
 1 AS `wh_rate_buy`,
 1 AS `wh_rate_buy_rub`,
 1 AS `wh_rate_sale`,
 1 AS `wh_rate_sale_rub`,
 1 AS `from_currency_id`,
 1 AS `ed_to_currency_id`,
 1 AS `ed_from_currency_id`,
 1 AS `direction`,
 1 AS `border`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_doc_send_amount`
--

DROP TABLE IF EXISTS `v_doc_send_amount`;
