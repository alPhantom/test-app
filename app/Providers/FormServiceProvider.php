<?php

namespace App\Providers;

use Collective\Html\FormBuilder;
use Illuminate\Support\ServiceProvider;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        FormBuilder::component('textInput', 'components.form.text', [
            'id', 'name', 'value', 'label', 'class', 'attributes'
        ]);
        FormBuilder::component('hiddenInput', 'components.form.hidden', [
            'id', 'name', 'value'
        ]);
        FormBuilder::component('isArchive', 'components.form.is_archive', [
            'isArchive', 'disabled'
        ]);
        FormBuilder::component('saveCancel', 'components.form.save_cancel', [
            'route', 'disabled'
        ]);
        FormBuilder::component('saveCancelDelete', 'components.form.save_cancel_delete', [
            'saveDisabled','cancelRoute', 'id' ,'deleteDisabled', 'deleteRoute'
        ]);
        FormBuilder::component('selectList', 'components.form.select',[
            'id', 'name', 'attributes' , 'collection', 'need', 'value', 'text'
        ]);
        FormBuilder::component('dropdownCheckbox', 'components.form.dropdown_checkbox',[
            'name', 'label', 'attributes', 'collection', 'savedValues', 'savedIds', 'id', 'value'
        ]);


    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
