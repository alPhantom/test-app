<?php

namespace App\Providers;

use App\Models\BaseModel;
use App\Models\Menu;
use App\Models\Operations\OperationModel;
use App\Policies\BookPolicy;
use App\Policies\MenuPolicy;
use App\Policies\WithdrawalPolicy;
use App\Policies\UnfinishedPolicy;
use App\Policies\OperationPolicy;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [

        OperationModel::class => OperationPolicy::class,
        BaseModel::class => BookPolicy::class,
        Menu::class => MenuPolicy::class,
        \App\Models\Unfinished::class => UnfinishedPolicy::class,
        User::class => BookPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
