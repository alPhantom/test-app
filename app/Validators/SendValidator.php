<?php


namespace App\Validators;


class SendValidator extends OperationValidator {

    protected function setOpenValidation() {

        $this->rules = ['cross_service_id' => 'required',
                        'currency_id' => 'required'];

        $this->messages = [ 'cross_service_id.required' => 'service_type_id',
                            'currency_id.required' => 'currency_id'];
    }

    protected function setPayValidation() {
        $this->setOpenValidation();
    }

    protected function setRepayValidation() {

    }

    protected function setPaybackValidation() {

    }

    protected function setCloseValidation() {

    }

    protected function setClosePaybackValidation() {

    }

    protected function setArchiveValidation() {

    }
}