<?php

namespace App\Validators;


use App\Constants\OperationStatus;
use Illuminate\Support\Facades\Validator;

abstract class OperationValidator {

    protected $rules = [];
    protected $messages = [];

    protected $errors;

    public function __construct($status) {

        switch ($status){

            case OperationStatus::OPEN :
                $this->setOpenValidation();
                break;
            case OperationStatus::PAY :
                $this->setPayValidation();
                break;
            case OperationStatus::REPAY :
                $this->setRepayValidation();
                break;
            case OperationStatus::PAYBACK :
                $this->setPaybackValidation();
                break;
            case OperationStatus::CLOSE :
                $this->setCloseValidation();
                break;
            case OperationStatus::CLOSE_WITH_PAYBACK :
                $this->setClosePaybackValidation();
                break;
            case OperationStatus::ARCHIVE :
                $this->setArchiveValidation();
                break;
            default:
                break;

        }

    }

    public function isValid($inputs){

        $validator = Validator::make($inputs, $this->rules, $this->messages);

        if ($validator->fails()) {
            $this->errors = $validator;
            return false;
        }

        return true;
    }

    public function getErrors(){

        return $this->errors;
    }

    protected abstract function setOpenValidation();

    protected abstract function setPayValidation();

    protected abstract function setRepayValidation();

    protected abstract function setPaybackValidation();

    protected abstract function setCloseValidation();

    protected abstract function setClosePaybackValidation();

    protected abstract function setArchiveValidation();



}