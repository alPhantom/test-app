<?php


namespace App\Validators;


class WithdrawalValidator extends OperationValidator {

    protected function setOpenValidation() {
        $this->rules = [
            'cross_service_id' => 'required',
            'currency_id' => 'required'
        ];

        $this->messages = [
            'cross_service_id.required' => 'service_type_id',
            'currency_id.required' => 'currency_id'
        ];
    }

    protected function setPayValidation() {
        // TODO: Implement setPayValidation() method.
    }

    protected function setRepayValidation() {
        // TODO: Implement setRepayValidation() method.
    }

    protected function setPaybackValidation() {
        // TODO: Implement setPaybackValidation() method.
    }

    protected function setCloseValidation() {
        // TODO: Implement setCloseValidation() method.
    }

    protected function setClosePaybackValidation() {
        // TODO: Implement setClosePaybackValidation() method.
    }

    protected function setArchiveValidation() {
        // TODO: Implement setArchiveValidation() method.
    }
}