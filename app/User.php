<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const CREATED_AT = 'create_date_time';
    const UPDATED_AT = 'change_date_time';

//    /**
//     * The attributes that are mass assignable.
//     *
//     * @var array
//     */
//    protected $fillable = [
//        'phone', 'password',
//    ];
    protected $appends = ['fullName'];

    protected $table = 'teh_user';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    public function getFullNameAttribute()
    {
        return $this->last_name . ' ' . $this->first_name . ' ' . $this->middle_name;
    }

    public function office()
    {

        return $this->belongsTo('App\Models\Books\Office', 'office_id');
    }

    public function position()
    {

        return $this->belongsTo('App\Models\Books\Position', 'position_id');
    }

}
