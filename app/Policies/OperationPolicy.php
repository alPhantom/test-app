<?php

namespace App\Policies;


use App\Constants\OperationStatus;
use App\Models\Operations\OperationModel;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Session;

class OperationPolicy {

    use HandlesAuthorization;

    private $access;
    private $path;

    function __construct() {

        $permissions = Session::get('permissions');
        $this->path = explode('/',request()->path())[0];
        $this->access[$this->path] = $permissions[$this->path] ?? null;
        $this->access['request'] = $permissions[$this->path . '_request'] ?? null;
        $this->access['payment'] = $permissions[$this->path . '_payment'] ?? null;
        $this->access['repayment'] = $permissions[$this->path . '_repayment'] ?? null;
        $this->access['refund'] = $permissions[$this->path . '_refund'] ?? null;

    }

    public function viewList(){
        if ($this->access[$this->path] !== null){
            return true;
        }
        else return false;
    }

    public function addRequest(){
        return ($this->access['request'] === 'write'
            || $this->access['request'] === 'delete');
    }

    public function viewItem($user, OperationModel $model){
        return ($this->access['request'] !== null
            && (($user->office_id === $model->create_office_id || $user->office_id === 1)));
    }

    /*
     *
     */

    public function readRequest($user, OperationModel $model){
        return $this->checkReadAccess('request', $user, $model);
    }

    public function readPayment($user, OperationModel $model){
        return $this->checkReadAccess('payment', $user, $model);
    }

    public function readRepayment($user, OperationModel $model){
        return $this->checkReadAccess('repayment', $user, $model);
    }

    public function readBankRefund($user, OperationModel $model){
        return $this->checkReadAccess('repayment', $user, $model);
    }

    public function readRefund($user, OperationModel $model){
        return $this->checkReadAccess('refund', $user, $model);
    }

    /*
     *
     */

    public function editRequest($user, OperationModel $model){
        return $this->checkEdidAccess('request', $user, $model, [OperationStatus::OPEN] );
    }

    public function editPayment($user, OperationModel $model){
        return $this->checkEdidAccess('payment', $user, $model, [OperationStatus::PAY]);
    }

    public function editRepayment($user, OperationModel $model){
        return $this->checkEdidAccess('repayment', $user, $model, [OperationStatus::REPAY]);
    }

    public function editBankRefund($user, OperationModel $model){
        return $this->checkEdidAccess('repayment', $user, $model, [OperationStatus::CLOSE, OperationStatus::PAYBACK]);
    }

    public function editRefund($user, OperationModel $model){
        return $this->checkEdidAccess('refund', $user, $model, [OperationStatus::PAYBACK]);
    }

    private function checkReadAccess($path, $user, $model){
        if ($this->access[$path] !== null
            && ($user->office_id === $model->create_office_id || $user->office_id === 1)){
            return true;
        }
        else return false;
    }

    private function checkEdidAccess($path, $user, $model, array $status){
        if (($this->access[$path] === 'write' || $this->access[$path] === 'delete')
            && in_array($model->status_id .'', $status)) {
            return true;
        }
        else return false;
    }

}