<?php

namespace App\Policies;

use App\Models\Menu;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Session;

class MenuPolicy {
    use HandlesAuthorization;

    public function view($user, Menu $menu){

        if(array_key_exists($menu->descr, Session::get('permissions')))
            return true;
        else return false;
    }

    public function write(){
        if (array_key_exists('node', Session::get('permissions'))
            && (Session::get('permissions')['node'] === 'write'
                || Session::get('permissions')['node'] === 'delete'))
            return true;
        else return false;
    }

    public function delete(){
        if (array_key_exists('node', Session::get('permissions'))
            && Session::get('permissions')['node'] === 'delete')
            return true;
        else return false;
    }
}
