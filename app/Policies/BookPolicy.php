<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Session;

class BookPolicy
{
    use HandlesAuthorization;

    private $access;

    function __construct() {

        $permissions = Session::get('permissions');
        $path = explode('/',request()->path())[0];
        if ( array_key_exists( $path, $permissions ) ) {
            $this->access = $permissions[$path];
        }

    }

    public function read(){
        if ( ! empty($this->access) )
            return true;
        else return false;
    }

    public function write() {
        if ( ! empty($this->access) && ($this->access === 'write' || $this->access === 'delete') )
            return true;
        else return false;
    }

    public function delete() {
        if ( ! empty($this->access) && $this->access === 'delete' )
            return true;
        else return false;
    }

}
