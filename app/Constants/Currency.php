<?php

namespace App\Constants;


class Currency
{
    const RUB = 1;
    const UAH = 2;
    const USD = 3;
    const EUR = 4;
}