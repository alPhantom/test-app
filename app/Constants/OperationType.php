<?php

namespace App\Constants;


class OperationType
{
    const SEND = 1;
    const WITHDRAWAL = 2;
    const ADDITIONAL = 3;
    const KOLIBRI = 4;
    const TICKET = 5;
    const PANDA = 6;
    const BUM = 7;
    const GLOBAL = 8;
}
