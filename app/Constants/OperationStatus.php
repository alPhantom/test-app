<?php

namespace App\Constants;

class OperationStatus
{
    const OPEN = '1';
    const PAY = '2';
    const REPAY = '3';
    const PAYBACK = '4';
    const CLOSE = '5';
    const CLOSE_WITH_PAYBACK = '6';
    const ARCHIVE = '7';

}