<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class Bush extends BaseModel
{
    //
    protected $table = 'book_bush';
    protected $fillable = ['descr', 'is_archive'];
}
