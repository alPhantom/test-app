<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class Marketing extends BaseModel
{
    protected $table = 'book_marketing';
    protected $fillable = ['attraction', 'sorting', 'is_archive'];
}