<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class Category extends BaseModel
{
    protected $table = 'book_category';
    protected $fillable = ['descr'];

    public function boxes()
    {
        return $this->hasManyThrough(
            'App\Models\Box',
            'App\Models\Books\System',
            'category_id', // Foreign key on users table...
            'system_id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'id' // Local key on users table...
        );
    }
}