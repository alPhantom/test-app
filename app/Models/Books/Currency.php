<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class Currency extends BaseModel {
    protected $table = 'book_currency';
}
