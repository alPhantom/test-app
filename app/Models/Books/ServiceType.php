<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class ServiceType extends BaseModel
{
    protected $table = 'book_service_type';

}
