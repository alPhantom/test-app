<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class TariffType extends BaseModel
{
    //
    protected $table = 'book_tariff_type';
    protected $fillable = ['descr', 'percent_unit', 'plus_minus', 'is_archive'];

}
