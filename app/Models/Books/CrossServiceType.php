<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class CrossServiceType extends BaseModel
{
    protected $table = 'cross_service_type';


    protected $fillable = ['type_id', 'service_id'];
    protected $appends = ['descr', 'serviceDescr'];

    public function getDescrAttribute(){
        return $this->service->descr. ' ' .$this->type->descr;
    }

    public function getServiceDescrAttribute(){
        return $this->service->descr;
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Books\ServiceType', 'type_id');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Books\Service', 'service_id');
    }
}
