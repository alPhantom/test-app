<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class System extends BaseModel {

    protected $table = "book_system";

    public function category()
    {
        return $this->belongsTo('App\Models\Books\Category', 'category_id');
    }
}
