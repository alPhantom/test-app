<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class City extends BaseModel
{
    //
    protected $table = 'book_city';
    protected $fillable = ['descr', 'is_archive'];
}
