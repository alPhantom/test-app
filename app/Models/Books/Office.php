<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class Office extends BaseModel {

    protected $table = 'book_office';


    public function city()
    {
        return $this->belongsTo('App\Models\Books\City', 'city_id', 'id');
    }

    public function officeGroup()
    {
        return $this->belongsTo('App\Models\Books\OfficeGroup', 'group_id', 'id');
    }

    public function tariffGroup()
    {
        return $this->belongsTo('App\Models\Books\TariffGroup', 'tariff_group_id', 'id');
    }

}
