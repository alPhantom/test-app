<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class Nbu extends BaseModel
{
    //
    protected $table = 'book_nbu';
    protected $fillable = ['rate_uah', 'rate_nbu', 'rate_eur','is_archive'];
}
