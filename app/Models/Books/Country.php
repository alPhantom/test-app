<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class Country extends BaseModel
{
    //
    protected $table = 'book_country';
    protected $fillable = ['descr', 'is_archive'];
}
