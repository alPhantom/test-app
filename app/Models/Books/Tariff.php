<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class Tariff extends BaseModel {

    protected $table = "book_tariff";

    protected $appends = ['sign', 'unit', 'descr'];

    public function currency(){

        return $this->belongsTo('App\Models\Books\Currency', 'currency_id');
    }

    public function serviceType(){

        return $this->belongsTo('App\Models\Books\CrossServiceType', 'service_type_id');
    }

    public function tariffType(){

        return $this->belongsTo('App\Models\Books\TariffType', 'tariff_type_id');
    }

    public function groups(){

        return $this->belongsToMany('App\Models\Books\TariffGroup', 'cross_tariff_group',
            'tariff_id', 'group_id');
    }

    public function serializeCommission() {

        $this->visible = ['id', 'tariff', 'sign', 'unit', 'descr'];
    }

    public function getSignAttribute(){
        return $this->tariffType->plus_minus;
    }

    public function getUnitAttribute(){
        return $this->tariffType->percent_unit;
    }

    public function getDescrAttribute(){
        return $this->tariffType->descr;
    }

}