<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class TariffGroup extends BaseModel
{
    //
    protected $table = 'book_tariff_group';
    protected $fillable = ['descr', 'is_archive'];
}