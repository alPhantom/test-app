<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class Status extends BaseModel
{
    protected $table = 'book_status';
}
