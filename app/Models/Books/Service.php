<?php

namespace App\Models\Books;

use App\Models\BaseModel;
use Illuminate\Support\Facades\Auth;

class Service extends BaseModel {
    protected $table = 'book_service';
    protected $fillable = ['category_id', 'descr', 'is_archive'];

    public function save(array $options = []) {

        if($this->exists) {
            $this->attributes['change_office_id'] = Auth::user()->office_id;
            $this->attributes['change_user_id'] = Auth::user()->id;
        } else {
            $this->attributes['create_office_id'] = Auth::user()->office_id;
            $this->attributes['create_user_id'] = Auth::user()->id;
        }

        return parent::save($options);
    }

    public function category() {
        return $this->belongsTo('App\Models\Books\Category', 'category_id', 'id');
    }

    public function types(){

        return $this->belongsToMany('App\Models\Books\ServiceType', 'cross_service_type',
            'service_id', 'type_id');
    }
}
