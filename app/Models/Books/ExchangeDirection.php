<?php

namespace App\Models\Books;

use App\Models\BaseModel;

class ExchangeDirection extends BaseModel{

    protected $table = "book_exchange_direction";

}
