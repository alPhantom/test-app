<?php

namespace App\Models\Books;


use App\Models\BaseModel;

class OfficeGroup extends BaseModel
{
    //
    protected $table = 'book_office_group';
    protected $fillable = ['descr', 'is_archive'];

    public function setVisibleCols($array){

        $this->visible = $array;

    }
}
