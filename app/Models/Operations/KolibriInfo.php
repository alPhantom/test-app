<?php

namespace App\Models\Operations;


use App\Models\BaseModel;

class KolibriInfo extends BaseModel
{
    protected $table = 'doc_kolibri_info';
    protected $fillable = ['*'];

}