<?php

namespace App\Models\Operations;


use App\Models\BaseModel;

class OperationModel extends BaseModel
{
    protected $table = 'doc_deal';

    public function cashflow()
    {
        return $this->hasMany('App\Models\Cashflow', 'deal_id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Books\Currency', 'currency_id');
    }

    public function serviceType()
    {
        return $this->belongsTo('App\Models\Books\CrossServiceType', 'cross_service_id');
    }

    public function office()
    {
        return $this->belongsTo('App\Models\Books\Office', 'create_office_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'create_user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Books\Status', 'status_id');
    }
}