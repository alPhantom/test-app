<?php

namespace App\Models\Operations;

class Send extends OperationModel
{
    public function sendInfo()
    {
        return $this->hasMany('App\Models\SendInfo', 'deal_id');
    }

}
