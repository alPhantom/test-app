<?php

namespace App\Models\Operations;


use App\Models\BaseModel;

class SendInfo extends BaseModel
{
    protected $table = 'doc_send_info';
    protected $fillable = ['*'];
}
