<?php

namespace App\Models\Operations;


class Kolibri extends OperationModel
{
    public function info(){
        return $this->hasOne('App\Models\Operations\KolibriInfo', 'head_id');
    }
}