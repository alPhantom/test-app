<?php

namespace App\Models;

class Rate extends BaseModel {

    protected $table = "doc_rate";

    protected $hidden = ['create_user_id', 'create_date_time', 'create_office_id',
                        'change_user_id', 'change_date_time', 'change_office_id',
                        'ins_time', 'ins_user', 'upd_time', 'upd_user','is_retail',
                        'is_cash', 'group_id'];

    public function exchangeDirection(){

        return $this->belongsTo('App\Models\Books\ExchangeDirection', 'exchange_direction_id');
    }

    public function group(){

        return $this->belongsTo('App\Models\Books\OfficeGroup', 'group_id');
    }
}
