<?php

namespace App\Models;

class Box extends BaseModel
{
    protected $table = 'doc_box';
    protected $fillable = [
        'system_id',
        'currency_id',
        'office_id',
        'unique_number',
        'balance',
        'rate',
        'note',
        'is_archive'
    ];

    public function system()
    {
        return $this->belongsTo('App\Models\Books\System', 'system_id', 'id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Books\Currency', 'currency_id', 'id');
    }

    public function office()
    {
        return $this->belongsTo('App\Models\Books\Office', 'office_id', 'id');
    }
}