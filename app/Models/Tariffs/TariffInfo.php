<?php
/**
 * Created by PhpStorm.
 * User: albina
 * Date: 23.07.18
 * Time: 20:42
 */

namespace App\Models\Tariffs;


use Illuminate\Database\Eloquent\Model;

class TariffInfo extends Model {

    protected $table = 'v_tariff_interface';
    public $timestamps = false;

    protected $visible = ['id', 'tariff', 'percent_unit', 'plus_minus', 'tariff_type_descr'];

}