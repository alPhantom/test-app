<?php
/**
 * Created by PhpStorm.
 * User: albina
 * Date: 23.07.18
 * Time: 20:14
 */

namespace App\Models\Tariffs;


use App\Models\BaseModel;

class CrossTariffModel extends BaseModel {

    public function tariff(){
        return $this->belongsTo('App\Models\Books\Tariff', 'tariff_id');
    }

}