<?php

namespace App\Models;


class Cashflow extends BaseModel
{

    protected $table = 'doc_cashflow';

    protected $fillable = ['*'];

    protected $appends = ['currency'];

    public function box()
    {
        return $this->belongsTo('App\Models\Box', 'box_id', 'id');
    }

    public function getCurrencyAttribute()
    {
        return $this->box->currency->descr;
    }

    public function unfinished()
    {
        return $this->belongsTo('App\Models\Unfinished', 'unfinished_id', 'id');
    }

    public function sendInfo()
    {
        return $this->belongsTo('App\Models\Operations\SendInfo', 'send_info_id');
    }

    public function currency()
    {
        return $this->hasManyThrough('App\Models\Books\Currency', 'App\Models\Box', 'box_id', 'currency_id');
    }

}
