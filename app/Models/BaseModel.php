<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BaseModel extends Model {
    const CREATED_AT = 'create_date_time';
    const UPDATED_AT = 'change_date_time';

    public function save(array $options = []) {

        if($this->exists) {
            $this->attributes['change_office_id'] = Auth::user()->office_id;
            $this->attributes['change_user_id'] = Auth::user()->id;
        } else {
            $this->attributes['create_office_id'] = Auth::user()->office_id;
            $this->attributes['create_user_id'] = Auth::user()->id;
        }

        return parent::save($options);
    }

    public function setVisibleCols($array){

        $this->visible = $array;

    }

}