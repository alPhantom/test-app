<?php


namespace App\Models;


class IncomeExpense extends BaseModel
{
    protected $table = 'doc_income_expense';

    public function box(){

        return $this->hasManyThrough('App\Models\Box', 'App\Models\Cashflow', 'income_expense_id', 'box_id');
    }

    public function cashflow(){

        return $this->hasOne(Cashflow::class, 'income_expense_id');
    }

    public function section(){

        return $this->belongsTo('App\Models\Books\SectionCashflow', 'section_id');
    }

    public function responsibleUser(){

        return $this->belongsTo('App\User', 'responsible_user_id');
    }

    public function responsibleOffice(){

        return $this->belongsTo('App\Models\Books\Office', 'responsible_office_id');
    }

}