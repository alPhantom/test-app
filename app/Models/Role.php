<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends BaseModel {
    //
    protected $table = "teh_role";

    public function permissions(){
        return $this->hasMany('App\Models\Permission', 'role_id');
    }

}
