<?php

namespace App\Models;


class Menu extends BaseModel {

    protected $table = "teh_node";

    protected $hidden = ['create_user_id', 'create_date_time', 'create_office_id',
                        'change_user_id', 'change_date_time', 'change_office_id',
                        'ins_time', 'ins_user', 'upd_time', 'upd_user',
                        'note', 'descr', 'menu_descr', 'show', 'parent_id'];

    protected $appends = ['li_attr', 'text', 'parent', 'state'];

    public function childNodes() {
        return $this->hasMany('App\Models\Menu', 'parent_id');
    }

//

    public function getLiAttrAttribute()     {
        return ['show' => $this->attributes['show'],
                'path' => $this->attributes['descr'],
                'menu_descr' => $this->attributes['menu_descr']];
    }

    public function getTextAttribute(){

        return $this->attributes['note'];

    }

    public function getParentAttribute(){

        return $this->attributes['parent_id']===null ? '#' : $this->attributes['parent_id'];
    }

    public function getStateAttribute(){
        return $this->attributes['descr']==='root' ? ['disabled' => true] : [];
    }

}
