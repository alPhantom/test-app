<?php

namespace App\Http\Controllers;

use App\Models\Books\ExchangeDirection;
use App\Models\Books\OfficeGroup;
use App\Models\Rate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class RateController extends Controller
{
    /**
     * Отображение курсов для отделений группы "Базовая группа" - по умолчанию.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('read', Rate::class)) {
            return redirect('access_denied');
        }

        $rates = $this->getRateData(0);

        return view('rate_form')
            ->withGroups(OfficeGroup::all())
            ->withGroupId(0)
            ->withDirections(ExchangeDirection::all())
            ->withCashRetail($rates['cashRetail'])
            ->withCashWholesale($rates['cashWholesale'])
            ->withNoncashRetail($rates['noncashRetail'])
            ->withNoncashWholesale($rates['noncashWholesale']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('write', Rate::class)) {
            return redirect('access_denied');
        }

        try{

            if(!empty($request->rates)) {

                Rate::where('group_id', $request->group_id)->delete();
                DB::table('doc_rate')->insert($request->rates);

                return json_encode(array(
                    'code' => 'success',
                    'message' => 'Данные были обновлены!',
                ));

            } else throw new \Exception(); //todo: make own exceptions!!!!

        }catch (\Exception $e){
            return json_encode(array(
                'code' => 'error',
                'message' => $e->getMessage(),
            ));
        }

    }

    /**
     * Возвращает json-строку с курсами для выбранной группы отделений.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try {
            if (Gate::denies('read', Rate::class))
                return json_encode(array(
                    'code' => 'error',
                    'message' => 'Нет доступа для просмотра разрешений',
                ));

            $rates = $this->getRateData($request->id);

            return json_encode(array(
                'code' => 'success',
                'cashRetail' => $rates['cashRetail']->toJson(),
                'cashWholesale' => $rates['cashWholesale']->toJson(),
                'noncashRetail' => $rates['noncashRetail']->toJson(),
                'noncashWholesale' => $rates['noncashWholesale']->toJson()

               ));

        } catch (  \Exception $e ) {
            // Возвращаем клиенту ответ с ошибкой
            return json_encode(array(
                'code' => 'error',
                'message' => $e->getMessage(),
            ));
        }

    }

    private function getRateData($groupId){
        return [
            'cashRetail' => Rate::where('is_retail', 1)
                ->where('is_cash', 1)
                ->where('group_id', $groupId)->get(),
            'cashWholesale' => Rate::where('is_retail', 0)
                ->where('is_cash', 1)
                ->where('group_id', $groupId)->get(),
            'noncashRetail' => Rate::where('is_retail', 1)
                ->where('is_cash', 0)
                ->where('group_id', $groupId)->get(),
            'noncashWholesale' => Rate::where('is_retail', 0)
                ->where('is_cash', 0)
                ->where('group_id', $groupId)->get()
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
