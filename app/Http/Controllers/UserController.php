<?php

namespace App\Http\Controllers;

use App\Models\Books\City;
use App\Models\Books\Country;
use App\Models\Books\Office;
use App\Models\Books\Position;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        if (Gate::denies('read', User::class))
            return redirect('access_denied');

        return view('user_list')->withUsers(User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        if (Gate::denies('write', User::class))
            return redirect('access_denied');
        return view('user_form')
            ->withCities(City::all())
            ->withCountries(Country::all())
            ->withRoles(Role::all())
            ->withPositions(Position::all())
            ->withOffices(Office::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        if (Gate::denies('write', User::class))
            return redirect('access_denied');

        // validate
        $rules = array(
            'first_name' => 'required|max:80',
            'last_name' => 'required|max:80',
            'middle_name' => 'required|max:80',
            'birthday' => 'required',
            'gender' => 'required',
            'works' => 'required',
            'phone' => 'required|max:12',
        );

        $messages = [

            'first_name.required' => 'Не заполнено имя',
            'last_name.required' => 'Не заполнена фамилия',
            'middle_name.required' => 'Не заполнена фамилия',
            'birthday.required' => 'Не выбрана дата рождения',
            'gender.required' => 'Не выбран пол',
            'works.required' => 'Необходима пометка работает/не работает',
            'phone.required' => 'Не заполнен телефон',
            'phone.max' => 'В телефоне должно быть не более 12 цифр',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            return back()
                ->withInput()
                ->withErrors($validator);
        } else {

            try {
                // store
                if (empty($request->id)) {
                    $user = new User();
                } else {
                    $user = User::find($request->id);
                }

                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->middle_name = $request->middle_name;
                $user->gender = $request->gender;
                $user->birthday = $request->birthday;
                $user->works = $request->works;
                $user->position_id = $request->position_id;
                $user->office_id = $request->office_id;
                $user->role_id = $request->role_id;
                $user->login_phone = $request->login_phone;
                $user->phone = $request->phone;
                $user->email = $request->email;
                $user->fact_country_id = $request->country_id;
                $user->fact_city_id = $request->city_id;
                $user->fact_address = $request->fact_address;
                $user->tin = $request->tin;
                $user->passport_number = $request->passport_number;
                $user->passport_by = $request->passport_by;
                $user->passport_when = $request->passport_when;
                $user->formal_address = $request->formal_address;

                if (!empty($request->password)) {
                    $user->password = password_hash($request->password, PASSWORD_BCRYPT);
                }

                $user->save();

                return redirect('/user');

            } catch (\Exception $e){
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        if (Gate::denies('read', User::class))
            return redirect('access_denied');

        return view('user_form')
            ->withUser(User::find($id))
            ->withCities(City::all())
            ->withCountries(Country::all())
            ->withRoles(Role::all())
            ->withPositions(Position::all())
            ->withOffices(Office::all());

    }
}
