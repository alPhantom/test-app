<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\Bush;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class BushController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if ( Gate::denies('read', Bush::class ) ) {
            return redirect('access_denied');
        }
        $bushes = Bush::all();
        return view('books.bush_list')->withBushes($bushes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        if ( Gate::denies('write', Bush::class) ) {
            return redirect('access_denied');
        }
        return view('books.bush_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if ( Gate::denies('write', Bush::class) ) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(
            'descr' => 'required|max:80',
        );
        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {
            try {
                // store
                if (empty($request->id)) {
                    $bush = new Bush;
                } else {
                    $bush = Bush::find($request->id);
                }
                $bush->descr = $request->descr;
                $bush->is_archive = ($request->is_archive) ? 1 : 0;
                $bush->save();

                return redirect('/bush');
            } catch (\Exception $e){
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Books\Bush  $bush
     * @return \Illuminate\Http\Response
     */
    public function edit(Bush $bush) {
        if ( Gate::denies('read', Bush::class ) ) {
            return redirect('access_denied');
        }
        return view('books.bush_form')->with( 'bush', $bush );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Books\Bush  $bush
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bush $bush) {
        if ( Gate::denies('delete', Bush::class ) ) {
            return redirect('access_denied');
        }
        Bush::find($bush->id)->delete();
        return redirect('/bush');
    }
}