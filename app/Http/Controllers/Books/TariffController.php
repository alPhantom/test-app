<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\CrossServiceType;
use App\Models\Books\Currency;
use App\Models\Books\ServiceType;
use App\Models\Books\Tariff;
use App\Models\Books\TariffGroup;
use App\Models\Books\TariffType;
use App\Models\Tariffs\TariffInfo;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class TariffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Gate::denies('read', Tariff::class) ) {
            return redirect('access_denied');
        }

        return view('books.tariff_list')->with( 'tariffs', Tariff::all() );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Gate::denies('write', Tariff::class) ) {
            return redirect('access_denied');
        }
        return view('books.tariff_form')
            ->withServices(CrossServiceType::all())
            ->withCurrencies(Currency::all())
            ->withTariffTypes(TariffType::all())
            ->withGroups(TariffGroup::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Gate::denies('write', Tariff::class) ) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(

            'service_type_id' => 'required',
            'currency_id' => 'required',
            'tariff' => 'required',
            'tariff_type_id' => 'required',
            'sum_min' => 'required',
            'sum_max' => 'required',
            'date_begin' => 'required',

        );

        $messages = [
            'service_type_id.required' => 'service_type_id',
            'currency_id.required' => 'currency_id',
            'tariff.required' => 'tariff',
            'tariff_type_id.required' => 'tariff_type_id',
            'sum_min.required' => 'sum_min',
            'sum_max.required' => 'sum_min',
            'date_begin.required' => 'date_begin',
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {
            // store
            try {
                if ( empty($request->id) ) {
                    $tariff = new Tariff;
                } else {
                    $tariff = Tariff::find($request->id);
                }
                $tariff->service_type_id = $request->service_type_id;
                $tariff->currency_id = $request->currency_id;
                $tariff->tariff = $request->tariff;
                $tariff->tariff_type_id = $request->tariff_type_id;
                $tariff->note = $request->note;
                $tariff->sum_min = $request->sum_min;
                $tariff->sum_max = $request->sum_max;
                $tariff->date_begin = $request->date_begin;
                $tariff->date_end = $request->date_end;
                $tariff->save();

                $this->saveTariffGroups($tariff, $request->group);

                return redirect('/tariff');

            } catch (\Exception $e){

                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

    private function saveTariffGroups($tariff, $groups){

        if ($groups !== null) {

            $oldGroups = $tariff->groups()->pluck('group_id')->all();

            foreach($oldGroups as $oldGroup){

                $position = array_search($oldGroup, $groups);

                if ($position !== false){
                    unset($groups[$position]);
                } else {
                    $tariff->groups()->detach($oldGroup);
                }
            }

            $tariff->groups()->attach($groups);
        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Gate::denies('read', Tariff::class)) {
            return redirect('access_denied');
        }

        $tariff = Tariff::find($id);
        $groups = $tariff->groups;
        $groupString = '';
        $groupIds ='';

        foreach ($groups as $group){
            $groupString .= $group->descr . ',';
            $groupIds .= $group->id . ' ';
        }

        return view('books.tariff_form')
            ->withTariff($tariff)
            ->withServices(CrossServiceType::all())
            ->withCurrencies(Currency::all())
            ->withTariffTypes(TariffType::all())
            ->withGroups(TariffGroup::all())
            ->withSelectedGroups(rtrim($groupString,','))
            ->withSelectedIds($groupIds);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Gate::denies('delete', Tariff::class) ) {
            return redirect('access_denied');
        }
        Tariff::find($id)->delete();
        return redirect('/tariff');
    }

    public function getCommission(Request $request){

        if ( Gate::denies('read', Tariff::class) ) {
            return json_encode(array(
                'code' => 'error',
                'message' => 'Нет прав для просмотра тарифов!',
            ));
        }

        try {

            $time = date('Y-m-d h:i:s', time());

            $tariffs = TariffInfo::where('currency_id', $request->currency_id)
                ->where('service_type_id', $request->service_type_id)
                ->where('plus_minus', 'plus')
                ->where('office_id', Auth::user()->office_id)
                ->where('sum_min', '<', $request->amount)
                ->where('sum_max', '>=', $request->amount)
                ->where('date_begin', '<=', date('Y-m-d H:i:s', time()))
                ->where(function($query) {
                    $query->whereNull('date_end')
                        ->orWhere('date_end', '>=', date('Y-m-d H:i:s', time()));
                })->get();

            $discounts = TariffInfo::where('currency_id', $request->currency_id)
                ->where('service_type_id', $request->service_type_id)
                ->where('plus_minus', 'minus')
                ->where('office_id', Auth::user()->office_id)
                ->where('sum_min', '<', $request->amount)
                ->where('sum_max', '>=', $request->amount)
                ->where('date_begin', '<=', date('Y-m-d H:i:s', time()))
                ->where(function($query) {
                    $query->where('date_end', '=', 'null')
                        ->orWhere('date_end', '>=', date('Y-m-d H:i:s', time()));
                })->get();

            return json_encode(array(
                'code' => 'success',
                'commission' => $tariffs->toArray(),
                'discounts' => $discounts->toArray(),));

        } catch (\Exception $e){
            return json_encode(array(
                'code' => 'error',
                'message' => $e->getMessage(),
            ));
        }

    }
}
