<?php

namespace App\Http\Controllers\Books;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use Illuminate\Support\Facades\Gate;

class NodeController extends Controller {


    public function showTree(Request $request) {

        if (Gate::denies('write', Menu::class))
            return redirect('access_denied');
        return view('books.node');
    }

    public function getJsonTree(){
        try {

            return json_encode(array(
                'code' => 'success',
                'result' => Menu::all()->toArray()));

        } catch (  \Exception $e ) {
            // Возвращаем клиенту ответ с ошибкой
            return json_encode(array(
                'code' => 'error',
                'message' => $e->getMessage(),
            ));
        }

    }

    public function update(Request $request){

        if (Gate::denies('write', Menu::class))
            return json_encode(array(
                'code' => 'error',
                'message' => 'Нет доступа!',
            ));

        $node = Menu::find($request->input('id'));
        return $this->setToDataBase($node, $request);

    }

    public function create(Request $request){

        if (Gate::denies('write', Menu::class))
            return json_encode(array(
                'code' => 'error',
                'message' => 'Нет доступа!',
            ));

        $node = new Menu();

        return $this->setToDataBase($node, $request);
    }

    private function setToDataBase(Menu $node, Request $request) {

        try {
            $node->note = $request->input('note');
            $node->parent_id = $request->input('parent_id');
            $node->descr = $request->input('descr');
            $node->menu_descr = $request->input('menu_descr');
            $node->show = $request->input('show');
            $node->icon = $request->input('icon');

            $node->save();

            return $this->getJsonTree();


        } catch (\Exception $e) {

            return json_encode(array(
                'code' => 'error',
                'message' => $e->getMessage(),
            ));
        }
    }

    public function delete(Request $request){

        if (Gate::denies('delete', Menu::class))
            return json_encode(array(
            'code' => 'error',
            'message' => 'Нет доступа!',
        ));

        Menu::destroy($request->input('id'));

        return $this->getJsonTree();

    }
}
