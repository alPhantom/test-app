<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        if ( Gate::denies('read', City::class) ) {
            return redirect('access_denied');
        }
        $cities = City::all();
        return view('books.city_list')->withCities($cities);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        if ( Gate::denies('write', City::class) ) {
            return redirect('access_denied');
        }
        return view('books.city_form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        if ( Gate::denies('write', City::class ) ) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(
            'descr' => 'required|max:80',
        );
        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {
            // store
            try {
                if (empty($request->id)) {
                    $city = new City;
                } else {
                    $city = City::find($request->id);
                }
                $city->descr = $request->descr;
                $city->is_archive = ($request->is_archive) ? 1 : 0;
                $city->save();

                return redirect('/city');

            } catch (\Exception $e){
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
        }
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Books\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city) {

        if ( Gate::denies('read', City::class) ) {
            return redirect('access_denied');
        }
        return view('books.city_form')->with( 'city', $city );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Books\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city) {

        if ( Gate::denies('delete', City::class) ) {
            return redirect('access_denied');
        }
        City::find($city->id)->delete();
        return redirect('/city');

    }
}