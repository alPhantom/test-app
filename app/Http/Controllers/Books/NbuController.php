<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\Nbu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class NbuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Gate::denies('read', Nbu::class ) ) {
            return redirect('access_denied');
        }
        $nbu = Nbu::find(3);
        $nbu->rate_uah = round( $nbu->rate_uah,2 );
        $nbu->rate_usd = round( $nbu->rate_usd,2 );
        $nbu->rate_eur = round( $nbu->rate_eur,2 );
        return view('books.nbu_form')->with( 'nbu', $nbu );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        if ( Gate::denies('write', Nbu::class) ) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(
            'rate_uah' => 'required|numeric|min:0',
            'rate_usd' => 'required|numeric|min:0',
            'rate_eur' => 'required|numeric|min:0',
        );
        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {
            try {
                // store
                $nbu = Nbu::find($request->id);
                $nbu->rate_uah = $request->rate_uah;
                $nbu->rate_usd = $request->rate_usd;
                $nbu->rate_eur = $request->rate_eur;
                $nbu->save();

                return redirect('/nbu');

            } catch (\Exception $e){
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

}