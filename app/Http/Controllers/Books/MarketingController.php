<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\Marketing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class MarketingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if ( Gate::denies('read', Marketing::class) ) {
            return redirect('access_denied');
        }
        $marketings = Marketing::all();
        return view('books.marketing_list')->with( 'marketings', $marketings );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        if ( Gate::denies('write', Marketing::class) ) {
            return redirect('access_denied');
        }

        return view('books.marketing_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {
        if ( Gate::denies('write', Marketing::class) ) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(
            'attraction' => 'required|max:80',
        );
        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {
            try {
                // store
                if (empty($request->id)) {
                    $marketing = new Marketing;
                } else {
                    $marketing = Marketing::find($request->id);
                }
                $marketing->attraction = $request->attraction;
                $marketing->is_archive = ($request->is_archive) ? 1 : 0;
                $marketing->save();

                return redirect('/marketing');

            } catch (\Exception $e){
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Books\Marketing  $marketing
     * @return \Illuminate\Http\Response
     */
    public function edit(Marketing $marketing) {
        if ( Gate::denies('read', Marketing::class ) ) {
            return redirect('access_denied');
        }
        return view('books.marketing_form')->with( 'marketing', $marketing );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Books\Marketing  $marketing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marketing $marketing) {
        if ( Gate::denies('delete', Marketing::class) ) {
            return redirect('access_denied');
        }
        Marketing::find( $marketing->id )->delete();
        return redirect('/marketing');
    }
}