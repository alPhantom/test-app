<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\TariffGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class TariffGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Gate::denies('read', TariffGroup::class) ) {
            return redirect('access_denied');
        }

        $tariffGroups = TariffGroup::all();
        return view('books.tariff_group_list')->withtariffGroups($tariffGroups);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Gate::denies('write', TariffGroup::class ) ) {
            return redirect('access_denied');
        }

        return view('books.tariff_group_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Gate::denies('write', TariffGroup::class) ) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(
            'descr' => 'required|max:80',
        );
        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {
            try {
                // store
                if (empty($request->id)) {
                    $tariffGroup = new TariffGroup();
                } else {
                    $tariffGroup = TariffGroup::find($request->id);
                }
                $tariffGroup->descr = $request->descr;
                $tariffGroup->is_archive = ($request->is_archive) ? 1 : 0;
                $tariffGroup->save();

                return redirect('/tariff_group');

            } catch (\Exception $e){
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Books\TariffGroup  $tariffGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(TariffGroup $tariffGroup)
    {
        if ( Gate::denies('read', TariffGroup::class ) ) {
            return redirect('access_denied');
        }
        return view('books.tariff_group_form')->with( 'tariffGroup', $tariffGroup );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Books\TariffGroup  $tariffGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(TariffGroup $tariffGroup)
    {
        if ( Gate::denies('delete', TariffGroup::class) ) {
            return redirect('access_denied');
        }
        TariffGroup::find($tariffGroup->id)->delete();
        return redirect('/tariff_group');
    }
}