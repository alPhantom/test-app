<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\SectionCashflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class SectionCashflowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Gate::denies('read', SectionCashflow::class ) ) {
            return redirect('access_denied');
        }

        return view('books.sectioncf_list')->withSections(SectionCashflow::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Gate::denies('write', SectionCashflow::class ) ) {
            return redirect('access_denied');
        }
        return view('books.sectioncf_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Gate::denies('write', SectionCashflow::class) ) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(
            'note' => 'required|max:120',
            'section_code' => 'required',
            'descr' => 'required|max:120',
        );
        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {
            try {
                DB::beginTransaction();
                // store
                if (empty($request->id)) {
                    $section = new SectionCashflow;
                } else {
                    $section = SectionCashflow::find($request->id);
                }
                $section->section_code = $request->section_code;
                $section->descr = $request->descr;
                $section->note = $request->note;
                $section->transit = ($request->transit) ? 1 : 0;
                $section->is_archive = ($request->is_archive) ? 1 : 0;
                $section->save();

                DB::commit();

                return redirect('/sectioncf');
            } catch (\Exception $e){
                DB::rollBack();
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Gate::denies('read', SectionCashflow::class ) ) {
            return redirect('access_denied');
        }
        return view('books.sectioncf_form')->withSection(SectionCashflow::find($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Gate::denies('delete', SectionCashflow::class ) ) {
            return redirect('access_denied');
        }
        SectionCashflow::find($id)->delete();
        return redirect('/sectioncf');
    }
}