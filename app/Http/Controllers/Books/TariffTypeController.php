<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\TariffType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class TariffTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Gate::denies('read', TariffType::class) ) {
            return redirect('access_denied');
        }

        $tariffTypes = TariffType::all();
        return view('books.tariff_type_list')->withtariffTypes( $tariffTypes );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Gate::denies('write', TariffType::class) ) {
            return redirect('access_denied');
        }

        $percentUnit = [1 => 'Процент', 2 => 'Ден. ед.'];
        $plusMinus = [1 => 'Плюс', 2 => 'Минус'];
        $selectedPercentUnit = 0;
        $selectedPlusMinus = 0;
        return view('books.tariff_type_form')
                ->withPercentUnit( $percentUnit )
                ->withPlusMinus( $plusMinus )
                ->withSelectedPercentUnit( $selectedPercentUnit )
                ->withSelectedPlusMinus( $selectedPlusMinus );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Gate::denies('write', TariffType::class) ) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(
            'descr' => 'required|max:80',
        );
        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {

            try {
                // store
                if (empty($request->id)) {
                    $tariffType = new TariffType();
                } else {
                    $tariffType = TariffType::find($request->id);
                }
                $tariffType->descr = $request->descr;
                $tariffType->percent_unit = $request->percent_unit;
                $tariffType->plus_minus = $request->plus_minus;
                $tariffType->is_archive = ($request->is_archive) ? 1 : 0;
                $tariffType->save();

                return redirect('/tariff_type');

            } catch (\Exception $e){
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Books\TariffType  $tariffType
     * @return \Illuminate\Http\Response
     */
    public function edit(TariffType $tariffType)
    {
        if ( Gate::denies('read', TariffType::class) ) {
            return redirect('access_denied');
        }

        $percentUnit = [1 => 'Процент', 2 => 'Ден. ед.'];
        $plusMinus = [1 => 'Плюс', 2 => 'Минус'];
        $selectedPercentUnit = ( $tariffType->percent_unit == 'percent' ? 1 : 2 );
        $selectedPlusMinus = ( $tariffType->plus_minus == 'plus' ? 1 : 2 );
        return view('books.tariff_type_form')->with( 'tariffType', $tariffType )
                ->withPercentUnit( $percentUnit )
                ->withPlusMinus( $plusMinus )
                ->withSelectedPercentUnit( $selectedPercentUnit )
                ->withSelectedPlusMinus( $selectedPlusMinus );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Books\TariffType  $tariffType
     * @return \Illuminate\Http\Response
     */
    public function destroy(TariffType $tariffType)
    {
        if ( Gate::denies('delete', TariffType::class) ) {
            return redirect('access_denied');
        }

        TariffType::find($tariffType->id)->delete();
        return redirect('/tariff_type');
    }
}