<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\Position;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Gate::denies('read', Position::class) ) {
            return redirect('access_denied');
        }
        $positions = Position::all();
        return view('books.position_list')->with( 'positions', $positions );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Gate::denies('write', Position::class) ) {
            return redirect('access_denied');
        }
        return view('books.position_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Gate::denies('write', Position::class) ) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(
            'descr' => 'required|max:80',
        );
        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {
            try {
                // store
                if (empty($request->id)) {
                    $position = new Position;
                } else {
                    $position = Position::find($request->id);
                }
                $position->descr = $request->descr;
                $position->is_archive = ($request->is_archive) ? 1 : 0;
                $position->save();

                return redirect('/position');

            } catch (\Exception $e){
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Gate::denies('read', Position::class ) ) {
            return redirect('access_denied');
        }
//        return view('books.service_form')->with( 'service', $service );
        return view('books.position_form')
            ->withPosition(Position::find($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Gate::denies('delete', Position::class) ) {
            return redirect('access_denied');
        }
        Position::find($id)->delete();
        return redirect('/position');
    }
}
