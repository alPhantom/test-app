<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\Category;
use App\Models\Books\System;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class SystemController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Gate::denies('read', System::class) ) {
            return redirect('access_denied');
        }

        return view('books.system_list')->withSystems(System::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Gate::denies('write', System::class) ) {
            return redirect('access_denied');
        }
        return view('books.system_form')->withCategories(Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Gate::denies('write', System::class) ) {
            return redirect('access_denied');
        }

        // validate
        $rules = array(
            'descr' => 'required|max:80',
            'category_id' => 'required'
        );

        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {
            try {
                // store
                if (empty($request->id)) {
                    $system = new System();
                } else {
                    $system = System::find($request->id);
                }
                $system->descr = $request->descr;
                $system->category_id = $request->category_id;
                $system->is_archive = ($request->is_archive) ? 1 : 0;
                $system->save();

                return redirect('/system');
            } catch (\Exception $e){
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Gate::denies('read', System::class) ) {
            return redirect('access_denied');
        }

        return view('books.system_form')
            ->withSystem(System::find($id))
            ->withCategories(Category::all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Gate::denies('delete', System::class ) ) {
            return redirect('access_denied');
        }

        System::find($id)->delete();
        return redirect('/system');
    }
}
