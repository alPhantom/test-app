<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\Office;
use App\Models\Books\OfficeGroup;
use App\Models\Books\TariffGroup;
use App\Models\Books\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class OfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Gate::denies('read', Office::class ) ) {
            return redirect('access_denied');
        }

        $offices = Office::all();
        return view('books.office_list')->withOffices( $offices );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Gate::denies('write', Office::class ) ) {
            return redirect('access_denied');
        }

        return view('books.office_form')
            ->withCities( City::all() )
            ->withTariffGroups( TariffGroup::all() )
            ->withOfficeGroups( OfficeGroup::all() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Gate::denies('write', Office::class ) ) {
            return redirect('access_denied');
        }

        $rules = [
            'item' => 'required',
            'descr' => 'max:80',
            'city_id' => 'required',
            'tariff_group_id' => 'required',
            'group_id' => 'required',
            'border_uah' => 'required',
            'border_usd' => 'required',
            'border_eur' => 'required',
            'address' => 'max:160',
            'guide' => 'max:160',
            'vodafone' => 'max:12',
            'lugacom' => 'max:12',
            'note' => 'max:600',
        ];

        $messages = [
            'item' => 'Поле (Номер отделения) должно быть заполнено',
            'descr' => 'Поле (Наименование отделения) не должно превышать 60 символов',
            'city_id' => 'Поле (Город) должно быть заполнено',
            'tariff_group_id' => 'Поле (Группа тарифа) должно быть заполнено',
            'group_id' => 'Поле (Группа отделения) должно быть заполнено',
            'border_uah' => 'Поле (Лимит гривна) должно быть заполнено',
            'border_usd' => 'Поле (Лимит доллар) должно быть заполнено',
            'border_eur' => 'Поле (Лимит евро) должно быть заполнено',
            'address' => 'Поле (Адрес) не должно превышать 60 символов',
            'guide' => 'Поле (Ориентир) не должно превышать 160 символов',
            'vodafone' => 'Поле (Телефон водафон) не должно превышать 12 символов',
            'lugacom' => 'Поле (Телефон лугаком) не должно превышать 12 символов',
            'note' => 'Поле (Телефон коссентарий) не должно превышать 600 символов',
        ];

        $validator = Validator::make( $request->all(), $rules, $messages );

        if ( $validator->fails() ) {
            return redirect('/office/create')
                ->withInput()
                ->withErrors($validator);
        } else {
            // store
            if ( empty( $request->id ) ) {
                $office = new Office();
            } else {
                $office = Office::find( $request->id );
            }
            $office->item = $request->item;
            $office->descr = $request->descr;
            $office->city_id = $request->city_id;
            $office->tariff_group_id = $request->tariff_group_id;
            $office->group_id = $request->group_id;
            $office->border_uah = $request->border_uah;
            $office->border_usd = $request->border_usd;
            $office->border_eur = $request->border_eur;
            $office->address = $request->address;
            $office->guide = $request->guide;
            $office->vodafone = $request->vodafone;
            $office->lugacom = $request->lugacom;
            $office->note = $request->note;
            $office->save();

            return redirect('/office');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Books\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function edit(Office $office)
    {
        if ( Gate::denies('read', Office::class) ) {
            return redirect('access_denied');
        }

        return view('books.office_form')
            ->withOffice( $office )
            ->withCities( City::all() )
            ->withTariffGroups( TariffGroup::all() )
            ->withOfficeGroups( OfficeGroup::all() );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Books\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function destroy(Office $office)
    {
        if ( Gate::denies('delete', Office::class ) ) {
            return redirect('access_denied');
        }

        Office::find( $office->id )->delete();
        return redirect('/office');
    }
}