<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if ( Gate::denies('read', Category::class) ) {
            return redirect('access_denied');
        }
        $categories = Category::all();
        return view('books.category_list')->with( 'categories', $categories );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        if ( Gate::denies('write', Category::class) ) {
            return redirect('access_denied');
        }
        return view('books.category_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {
        if ( Gate::denies('write', Category::class) ) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(
            'descr' => 'required|max:80',
        );
        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {
            // store
            try {
                if (empty($request->id)) {
                    $category = new Category;
                } else {
                    $category = Category::find($request->id);
                }
                $category->descr = $request->descr;
                $category->save();

                return redirect('/category');

            } catch (\Exception $e){
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Books\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category) {
        if ( Gate::denies('read', Category::class ) ) {
            return redirect('access_denied');
        }
        return view('books.category_form')->with( 'category', $category );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Books\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category) {
        if ( Gate::denies('delete', Category::class) ) {
            return redirect('access_denied');
        }
        Category::find( $category->id )->delete();
        return redirect('/category');
    }
}