<?php

namespace App\Http\Controllers\Books;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class RoleController extends Controller {

    public function edit($id=null) {
        if (Gate::denies('read', Role::class))
            return redirect('access_denied');

        return view('books.role')->withRole(Role::find($id));

    }

    public function index() {
        if (Gate::denies('read', Role::class))
            return redirect('access_denied');

        return view('books.role_list')->withRoles(Role::all());
    }

    public function create(Request $request) {

        if (Gate::denies('write', Role::class))
            return redirect('access_denied');

        $role = new Role();

        $id = ($this->save($role, $request));
        $this->setPermissions($request->input('data'), $id);

        return json_encode(array(
            'code' => 'success',
            'location' => route('roles')
        ));

    }

    public function update(Request $request) {

        if (Gate::denies('write', Role::class))
            return redirect('access_denied');

        $role = Role::find($request->input('id'));

        $id = ($this->save($role, $request));
        $this->setPermissions($request->input('data'), $id);

        return json_encode(array(
            'code' => 'success',
            'location' => route('roles')
        ));

    }

    public function destroy($id=null) {

        if (Gate::denies('delete', Role::class))
            return redirect('access_denied');

        Permission::where('role_id', $id)->delete();
        Role::destroy($id);

        return redirect(route('roles'));

    }
    //

    public function getPermissions(Request $request){
        try {
            if (Gate::denies('read', Role::class))
                return json_encode(array(
                    'code' => 'error',
                    'message' => 'Нет доступа для просмотра разрешений',
                ));

            return json_encode(array(
                'code' => 'success',
                'result' => Role::find($request->input('id'))->permissions->map(function ($obj){
                    $obj->setVisibleCols(['node_id', 'access_type']);
                    return $obj;
                })->toArray()));

        } catch (  \Exception $e ) {
            // Возвращаем клиенту ответ с ошибкой
            return json_encode(array(
                'code' => 'error',
                'message' => $e->getMessage(),
            ));
        }
    }

    private function save(Role $role, Request $request) {

        $role->descr = $request->input('descr');
        $role->note = $request->input('note');
        $role->save();

        return $role->id;
    }

    private function setPermissions($data, $id) {

        if ($data !== null) {
            $ids = [];
            foreach ($data as $item) {
                $permission = Permission::updateOrCreate(['role_id' => $id, 'node_id' => $item['node_id']],
                    ['access_type' => $item['access_type']]);
                array_push($ids, $permission->id);
            }

            $oldPermissions = Permission::whereNotIn('id', $ids)->where('role_id', $id)->get();

            foreach ($oldPermissions as $item) {
                $item->delete();
            }
        }
    }
}
