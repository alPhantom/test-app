<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Framework\Constraint\Count;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        if ( Gate::denies('read', Country::class) ) {
            return redirect('access_denied');
        }
        $countries = Country::all();
        return view('books.country_list')->with( 'countries', $countries );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        if ( Gate::denies('write', Country::class) ) {
            return redirect('access_denied');
        }
        return view('books.country_form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        if ( Gate::denies('write', Country::class) ) {
            return redirect('access_denied');
        }
        
        // validate
        $rules = array(
            'descr' => 'required|max:80',
        );
        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {

            try {
                // store
                if (empty($request->id)) {
                    $country = new Country;
                } else {
                    $country = Country::find($request->id);
                }
                $country->descr = $request->descr;
                $country->is_archive = ($request->is_archive) ? 1 : 0;
                $country->save();

                return redirect('/country');

            } catch (\Exception $e){
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Books\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country) {

        if ( Gate::denies('read', Country::class) ) {
            return redirect('access_denied');
        }

        return view('books.country_form')->with( 'country', $country );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Books\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country) {

        if ( Gate::denies('delete', Country::class) ) {
            return redirect('access_denied');
        }

//        $country->delete();
        Country::find( $country->id )->delete();
        return redirect('/country');

    }
}
