<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\CrossServiceType;
use App\Models\Books\Service;
use App\Models\Books\Category;
use App\Models\Books\ServiceType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Gate::denies('read', Service::class) ) {
            return redirect('access_denied');
        }
        $services = Service::all();
        return view('books.service_list')->with( 'services', $services );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Gate::denies('write', Service::class) ) {
            return redirect('access_denied');
        }
        return view('books.service_form')
            ->withCategories(Category::all())
            ->withTypes(ServiceType::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('write', Service::class)) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(
            'category_id' => 'required',
            'descr' => 'required|max:60',
        );
        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {
            // store
            try {
                if (empty($request->id)) {
                    $service = new Service;
                } else {
                    $service = Service::find($request->id);
                }

                $service->category_id = $request->category_id;
                $service->descr = $request->descr;
                $service->is_archive = ($request->is_archive) ? 1 : 0;
                $service->save();

                $this->storeServiceType($service->id, $request->type);

                return redirect('/service');
            } catch (\Exception $e) {
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

    private function storeServiceType($id, $types){

        if ($types !== null) {

            foreach ($types as $type) {
                $serviceType = CrossServiceType::firstOrCreate(['service_id' => $id, 'type_id' => $type]);
            }

            $oldTypes = CrossServiceType::whereNotIn('type_id', $types)->where('service_id', $id)->delete();
//
//            foreach ($oldTypes as $item) {
//                $item->delete();
//            }

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Books\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        if ( Gate::denies('read', Service::class ) ) {
            return redirect('access_denied');
        }

        $types = $service->types;
        $typeString = '';
        $typeIds ='';
        foreach ($types as $type){
            $typeString .= $type->descr . ',';
            $typeIds .= $type->id . ' ';
        }

        return view('books.service_form')
            ->withService( $service )
            ->withCategories(Category::all())
            ->withTypes(ServiceType::all())
            ->withSelectedTypes(rtrim($typeString,','))
            ->withSelectedIds($typeIds);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Books\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        if ( Gate::denies('delete', Service::class) ) {
            return redirect('access_denied');
        }
        Service::find( $service->id )->delete();
        return redirect('/service');
    }
}