<?php

namespace App\Http\Controllers\Books;

use App\Models\Books\OfficeGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class OfficeGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        if ( Gate::denies('read', OfficeGroup::class) ) {
            return redirect('access_denied');
        }

        $officeGroups = OfficeGroup::all();
        return view('books.office_group_list')->withofficeGroups($officeGroups);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        if ( Gate::denies('write', OfficeGroup::class) ) {
            return redirect('access_denied');
        }

        return view('books.office_group_form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        if ( Gate::denies('write', OfficeGroup::class) ) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(
            'descr' => 'required|max:80',
        );
        $validator = Validator::make( $request->all(), $rules );

        if ( $validator->fails() ) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {

            try {
                // store
                if (empty($request->id)) {
                    $officeGroup = new OfficeGroup();
                } else {
                    $officeGroup = OfficeGroup::find($request->id);
                }
                $officeGroup->descr = $request->descr;
                $officeGroup->is_archive = ($request->is_archive) ? 1 : 0;
                $officeGroup->save();

                return redirect('/office_group');

            } catch (\Exception $e){
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Books\OfficeGroup  $officeGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(OfficeGroup $officeGroup) {

        if ( Gate::denies('read', OfficeGroup::class) ) {
            return redirect('Нет прав!');
        }

        return view('books.office_group_form')->with( 'officeGroup', $officeGroup );

    }

    public function update(Request $request){

        if ( Gate::denies('write', OfficeGroup::class) ) {
            return json_encode(array(
                'code' => 'error',
                'message' => 'Нет прав!',
            ));
        }
        try {

            if (empty($request->id)) {
                $officeGroup = new OfficeGroup();
            } else {
                $officeGroup = OfficeGroup::find($request->id);
            }
            $officeGroup->descr = $request->descr;
            $officeGroup->is_archive = $request->is_archive;
            $officeGroup->save();

            return json_encode(array(
                'code' => 'success',
                'result' => [
                    'descr' => $officeGroup->descr,
                    'id' => $officeGroup->id,
                    'is_archive' => $officeGroup->is_archive
                ],
                'message' => 'Данные были обновлены!',
            ));

        } catch (\Exception $e){
            return json_encode(array(
                'code' => 'error',
                'message' => $e->getMessage()
            ));
        }
    }

    public function delete(Request $request){
        if ( Gate::denies('delete', OfficeGroup::class) ) {
            return json_encode(array(
                'code' => 'error',
                'message' => 'Нет прав!',
            ));
        }
        try {

            OfficeGroup::find($request->id)->delete();
            return json_encode(array(
                'code' => 'success',
                'message' => 'Группа успешно удалена!',
            ));

        } catch (\Exception $e){
            return json_encode(array(
                    'code' => 'error',
                    'message' => $e->getMessage()
                ));
        }
    }

    public function list(){

        if ( Gate::denies('read', OfficeGroup::class) ) {
            return json_encode(array(
                'code' => 'error',
                'message' => 'Нет прав!',
            ));
        }

        try{

            return json_encode(array(
                'code' => 'success',
                'result' => OfficeGroup::all()->map(function ($obj){
                    $obj->setVisibleCols(['id', 'descr']);
                    return $obj;
                })->toArray(),
            ));

        } catch (\Exception $e){

            return json_encode(array(
                'code' => 'error',
                'message' => 'Произошла ошибка. Перезагрузите страницу',
            ));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Books\OfficeGroup  $officeGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(OfficeGroup $officeGroup) {

        if ( Gate::denies('delete', OfficeGroup::class) ) {
            return redirect('access_denied');
        }

        OfficeGroup::find($officeGroup->id)->delete();
        return redirect('/office_group');

    }
}