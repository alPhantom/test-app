<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    protected function attemptLogin(Request $request) {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    protected function credentials(Request $request) {
        return $request->only($this->username(), 'password') + ['works' => 'yes'];
    }

    public function username() {
        return 'phone';
    }

    protected function authenticated(Request $request, $user) {

        $permissions =[];
        $permissionsQ = DB::select('select 
                                      n.descr, 
                                      p.access_type 
                                      from teh_permission as p
                                      inner join teh_node as n
                                      on p.node_id = n.id
                                      inner join teh_role as r
                                      on p.role_id = r.id
                                      inner join teh_user as u
                                      on r.id = u.role_id where u.id=?',[$user->id]);
        foreach ($permissionsQ as $permission){
            $permissions[$permission->descr] = $permission->access_type;

        }

        $request->session()->put('permissions', $permissions);

    }


}
