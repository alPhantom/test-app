<?php

namespace App\Http\Controllers;

use App\Models\Books\Office;
use App\Models\Books\SectionCashflow;
use App\Models\Box;
use App\Models\Cashflow;
use App\Models\IncomeExpense;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class IncomeExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('read', IncomeExpense::class)) {
            return redirect('access_denied');
        }

        return view('income_expense.income_expense_list')->withMovements(IncomeExpense::with('cashflow.section.responsibleOffice.responsibleUser')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('write', IncomeExpense::class)) {
            return redirect('access_denied');
        }
        return view('income_expense.income_expense_form')
            ->withBoxes(Box::where('office_id', Auth::user()->office_id)->whereIn('system_id', [18, 19])->get())
            ->withSections(SectionCashflow::all())
            ->withOffices(Office::all())
            ->withUsers(User::where('works', 'yes')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('write', IncomeExpense::class)) {
            return redirect('access_denied');
        }
        // validate
        $rules = array(
            'note' => 'required|max:120',
            'section_code' => 'required',
            'descr' => 'required|max:120',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } else {
            try {
                DB::beginTransaction();
                // store
                if (empty($request->id)) {
                    $ie = new IncomeExpense;
                } else {
                    $ie = IncomeExpense::find($request->id);
                }
                $ie->section_id = $request->section_code;
                $ie->responsible_user = $request->user;
                $ie->responsible_office = $request->cfr;
                $ie->cause = $request->cause;
                $ie->note = $request->note;
                $ie->is_income = ($request->transit) ? 1 : 0;
                $ie->save();

                Cashflow::where('income_expense', $ie->id)->delete();

                $cf = new Cashflow();
                $cf->box_id = $request->box_id;
                $cf->amount = $request->amount;
                $cf->rate = $request->rate;
                $cf->rub_rate = Calculator::getNbuRate($cf->currency->id);
                $cf->income_expense_id = $ie->id;

                DB::commit();

                return redirect('/income_expense');
            } catch (\Exception $e) {
                DB::rollBack();
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('read', IncomeExpense::class)) {
            return redirect('access_denied');
        }
        return view('income_expense.income_expense_form')
            ->withBoxes(Box::where('office_id', Auth::user()->office_id)->whereIn('system_id', [18, 19])->get())
            ->withSections(SectionCashflow::all())
            ->withOffices(Office::all())
            ->withUsers(User::where('works', 'yes')->get())
            ->withMovement(IncomeExpense::find($id));
    }

}
