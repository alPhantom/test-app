<?php

namespace App\Http\Controllers;


use App\Constants\Currency;
use App\Models\Box;
use App\Models\BoxRate;
use App\Models\Cashflow;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

trait Payment {

    /**
     *
     * @param $totalAmount int Сумма по заявке
     * @param $rates Collection of BoxRate
     * @param $currency int id валюты
     * @return array Суммы к оплате во всех валютах
     */
    private function getDebtAmount($totalAmount, Collection $rates, $currency) {

        $amounts = [];
        $border = $rates->first(function ($value, $key) use ($currency) {
            return $value->to_currency === $currency;
        });
        $retail = $totalAmount < $border->border;

        foreach ($rates as $rate) {

            $current = $retail ? $rate->retail : $rate->wholesale;
            $amounts += [$rate->currency => $totalAmount / $current];
        }
        return $amounts;
    }

    /**
     * @return array Набор полей для курсов покупки валюты
     */
    private function buyFields() {
        return ['ret_rate_buy as retail',
            'wh_rate_buy as wholesale',
            'wh_rate_buy_rub as wholesale_rub',
            'ret_rate_buy_rub as retail_rub',
            'box_id',
            'currency',
            'to_currency',
            'from_currency',
            'border'
        ];
    }

    /**
     * @return array Набор полей для курсов продажи валюты
     */
    private function saleFields() {
        return ['ret_rate_sale as retail',
            'wh_rate_sale as wholesale',
            'wh_rate_sale_rub as wholesale_rub',
            'ret_rate_sale_rub as retail_rub',
            'box_id',
            'currency',
            'to_currency',
            'from_currency',
            'border'
        ];
    }

    /**
     * Сохранение данных по оплате/выплатам в Cashflow
     * @param $inputs array Поля ввода
     * @param string $sign plus|minus знак для сохранения сумм в таблицу
     */
    private function saveCashflow($inputs, $sign = 'plus') {

        for ($i = 0; $i < count($inputs['box_id']); $i++) {
            if ($inputs['payment'][$i] !== null) {
                $cf = new Cashflow();
                $cf->box_id = $inputs['box_id'][$i];
                $cf->amount = ($sign === 'minus') ? (-1 * $inputs['payment'][$i]) : $inputs['payment'][$i];
                $cf->rate = $inputs['rate'][$i];
                $cf->rub_rate = $inputs['rub_rate'][$i];
                $cf->deal_id = $inputs['id'];
                $cf->save();
            }
        }
    }

    /**
     * @param $currency_id int id валюты операции
     * @param bool $buy
     * @return Collection данные из BoxRate для операции
     */
    protected function getPaymentRate($currency_id, $buy = false){
        $boxes = Box::where('office_id', Auth::user()->office_id)->where('system_id', 18)->get()->pluck('id');
        $rates = BoxRate::whereIn('box_id', $boxes)->where('from_currency', $currency_id)
            ->get($buy ? $this->buyFields() : $this->saleFields());
        return $rates;
    }

    /**
     * @param $amount
     * @param $currency_id
     * @param $office_id
     * @return array массив для отображения формы возврата в Руб
     */
    protected function getRefundInfo($amount, $currency_id, $office_id){

        $rubAmount = $amount * Calculator::getRubRate($currency_id, $amount, 'buy', $office_id);

        $boxes = Box::where('office_id', $office_id)->where('system_id', 18)->get()->pluck('id');
        $rates = BoxRate::whereIn('box_id', $boxes)->where('from_currency', Currency::RUB)
            ->get($this->saleFields());

        return ['paybackAmount' => $rubAmount,
                'paybackData' => $rates,
                'refundDebts' => $this->getDebtAmount($rubAmount, $rates, Currency::RUB),
                ];
    }
}