<?php

namespace App\Http\Controllers;

use App\Models\Box;
use App\Models\Books\System;
use App\Models\Books\Currency;
use App\Models\Books\Office;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class BoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Gate::denies('read', Box::class ) ) {
            return redirect('access_denied');
        }

        $boxes = Box::all();
        return view('box_list')->withBoxes( $boxes );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Gate::denies('write', Box::class) ) {
            return redirect('access_denied');
        }

        return view('box_form')
            ->withSystems( System::all() )
            ->withCurrencies( Currency::all() )
            ->withOffices( Office::all() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Gate::denies('write', Box::class) ) {
            return redirect('access_denied');
        }

        $rules = [
            'system' => 'required',
            'currency' => 'required',
            'office' => 'required',
            'unique_number' => 'required|max:30',
            'balance' => 'required',
        ];

        $messages = [
            'system' => 'Поле системы должно быть заполнено',
            'currency' => 'Поле валюты',
            'office' => 'Поле отделение должно быть заполнено',
            'unique_number' => 'Уникальный номер должен быть заполнен и не повторяться',
            'balance' => 'Баланс должен быть заполнен',
        ];

        $validator = Validator::make( $request->all(), $rules, $messages );

        if ( $validator->fails() ) {
            return redirect('/box/create')
                ->withInput()
                ->withErrors($validator);
        } else {
            // store
            if ( empty( $request->id ) ) {
                $box = new Box();
            } else {
                $box = Box::find( $request->id );
            }
            $box->system_id = $request->system;
            $box->currency_id = $request->currency;
            $box->office_id = $request->office;
            $box->unique_number = $request->unique_number;
            $box->balance = $request->balance;
            $box->note = $request->note;
            $box->is_archive = ( $request->is_archive ) ? 1 : 0;
            $box->save();

            return redirect('/box');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function edit(Box $box)
    {
        if ( Gate::denies('read', Box::class) ) {
            return redirect('access_denied');
        }

        return view('box_form')
            ->withBox($box)
            ->withSystems( System::all() )
            ->withCurrencies( Currency::all() )
            ->withOffices( Office::all() );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function destroy(Box $box)
    {
        if ( Gate::denies('delete', Box::class) ) {
            return redirect('access_denied');
        }

        Box::find($box->id)->delete();
        return redirect('/box');
    }

    public function getRemoteBoxesJson(Request $request){

        try {
            return json_encode(array(
                'code' => 'success',
                'result' => $this->getRemoteBoxes($request->currency_id)->map(function ($obj){
                        $obj->setVisibleCols(['id', 'unique_number']);
                        return $obj;
                    })->toArray()
            ));
        } catch (\Exception $e) {

            return json_encode(array(
                'code' => 'error',
                'message' => $e->getMessage(),
            ));
        }

    }

    public static function getRemoteBoxes($currency_id){

        return Box::with('system')
            ->whereHas('system', function ($query) {
                $query->whereIn('category_id', [1,2,7]);})
            ->where('currency_id', $currency_id)
            ->get();
    }

    public static function getBoxesUnfinished()
    {
        return Box::with('system')
            ->whereHas('system', function ($query) {
                $query->whereIn('category_id', [1,2,7]);})
            ->get();
    }

}