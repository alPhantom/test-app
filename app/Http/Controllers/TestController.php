<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tariffs\TariffInfo;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $tariffs = TariffInfo::all();

        $tariffs = TariffInfo::where('currency_id', 1)
            ->where('service_type_id', 39)
            ->where('plus_minus', 'plus')
            ->where('office_id', Auth::user()->office_id)
            ->where('sum_min', '<', 500)
            ->where('sum_max', '>=', 500)
            ->where('date_begin', '<=', date('Y-m-d h:i:s', time()))
            ->where(function($query) {
                $query->where('date_end', '=', 'null')
                    ->orWhere('date_end', '>=', date('Y-m-d h:i:s', time()));
            })->get();

        return dd( $tariffs );
//        return Auth::user()->office_id;
    }
}
