<?php

namespace App\Http\Controllers;

use App\Constants\Currency;
use App\Models\Books\Nbu;
use App\Models\Books\Office;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Calculator {

    public static function getRubRate($currency_id, $amount, $type, $office_id){

        if ($currency_id === 1) {
            return 1;
        } else {
            $office = Office::find($office_id);
            $isRetail = 0;
            switch ($currency_id) {

                case 2:
                    $isRetail = $amount >= $office->border_uah ? 0 : 1;
                    break;
                case 3:
                    $isRetail = $amount >= $office->border_usd ? 0 : 1;
                    break;
                case 4:
                    $isRetail = $amount >= $office->border_eur ? 0 : 1;
                    break;
                default:
                    break;
            };

            $rates = DB::select('SELECT
                                        if (:type = "buy", dr.buy, dr.sale) as rate
                                    FROM
                                        doc_rate dr
                                        INNER JOIN book_exchange_direction dir 
                                          ON dr.exchange_direction_id = dir.id
                                    WHERE
                                        dir.to_currency_id = 1
                                            AND dir.from_currency_id = :currency_id
                                            AND dr.is_cash = 0
                                            AND dr.is_retail = :retail
                                            AND dr.group_id = :group_id;',
                ['currency_id' => $currency_id,
                    'group_id' => $office->group_id,
                    'retail' => $isRetail,
                    'type' => $type]);

            return empty($rates) ? 1 : $rates[0]->rate;
        }
    }

    public static function getNbuRate($currency_id){

        $nbu = Nbu::all()[0];
        switch ($currency_id){
            case Currency::EUR :
                return $nbu->rate_eur;
            case Currency::UAH:
                return $nbu->rate_uah;
            case Currency::USD:
                return $nbu->rate_usd;
            default:
                return 1;
        }

    }


}