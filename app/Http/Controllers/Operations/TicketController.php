<?php

namespace App\Http\Controllers\Operations;

use App\Constants\OperationType;
use App\Http\Controllers\BoxController;
use App\Http\Controllers\Calculator;
use App\Http\Controllers\Operations\OperationManagers\TicketOperation;
use App\Http\Controllers\Payment;
use App\Models\Cashflow;
use App\Models\Operations\Ticket;
use App\Validators\TicketValidator;
use Illuminate\Support\Facades\DB;

class TicketController extends OperationController
{
    protected $editView = 'ticket.ticket_form';
    protected $redirectUrl = '/ticket';
    protected $listView = 'ticket.ticket_list';
    protected $operation = OperationType::TICKET;

    protected function getValidator($status)
    {
        return new TicketValidator($status);
    }

    protected function getOpenData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new TicketOperation($model, $this->operation);
        return $data->getOpenData();
    }

    protected function getPayData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new TicketOperation($model, $this->operation);
        return $data->getOpenData() + $data->getPayData();
    }

    protected function getRepayData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new TicketOperation($model, $this->operation);
        return $data->getOpenData() + $data->getCashIn();
    }

    protected function getPaybackData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new TicketOperation($model, $this->operation);
        $refunds =  $data->getRefundBank()['refunds'];
        return $data->getOpenData() + $data->getCashIn() + $data->getRefundBank()
            + $data->getPaybackData(empty($refunds) ? $model->amount : $refunds->sum('amount'));
    }

    protected function getCloseData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new TicketOperation($model, $this->operation);
        return $data->getOpenData() + $data->getCashIn() + $data->getRepayData();
    }

    protected function getClosePaybackData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new TicketOperation($model, $this->operation);
        return $data->getOpenData() + $data->getCashIn() + $data->getRepayData()
            + $data->getRefundBank() + $data->getCashOut();
    }

    protected function getArchiveData($id)
    {
        return $this->getOpenData($id);
    }

    protected function setOpenData($inputs)
    {
        if (empty($inputs['id'])){
            $model = new Ticket();
        } else {
            $model = Ticket::find($inputs['id']);
        }
        $data = new TicketOperation($model, $this->operation);
        $data->saveRequestData($inputs);

        if (!empty($inputs['tariff'])) {
            $data->saveTariff($inputs);
        }
    }

    protected function setPayData($inputs)
    {
        $this->setOpenData($inputs);
    }

    protected function setRepayData($inputs)
    {
        $data = new TicketOperation(Ticket::find($inputs['id']), $this->operation);
        $data->savePayData($inputs);
        $data->updateStatus($inputs['status']);
    }

    protected function setPaybackData($inputs)
    {
//            К возврату после возврата средств на карту банком
        $data = new TicketOperation(Ticket::find($inputs['id']), $this->operation);
        $data->saveRefundBank($inputs);
        $data->updateStatus($inputs['status']);

    }

    protected function setCloseData($inputs)
    {
        $data = new TicketOperation(Ticket::find($inputs['id']), $this->operation);
        $data->saveRepayData($inputs);
        $data->updateStatus($inputs['status']);
    }

    protected function setClosePaybackData($inputs)
    {

        $data = new TicketOperation(Ticket::find($inputs['id']), $this->operation);
        $data->savePaybackData($inputs);
        $data->updateStatus($inputs['status']);
    }

}
