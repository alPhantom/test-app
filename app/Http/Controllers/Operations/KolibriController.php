<?php

namespace App\Http\Controllers\Operations;

use App\Constants\OperationStatus;
use App\Constants\OperationType;
use App\Http\Controllers\Operations\OperationManagers\KolibriOperation;
use App\Models\Cashflow;
use App\Models\Operations\Kolibri;
use App\Validators\KolibriValidator;

class KolibriController extends OperationController
{

    protected $editView = 'kolibri.kolibri_form';
    protected $redirectUrl = '/kolibri';
    protected $listView = 'kolibri.kolibri_list';
    protected $operation = OperationType::KOLIBRI;

    protected function getValidator($status)
    {
        return new KolibriValidator($status);
    }

    protected function getOpenData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new KolibriOperation($model, $this->operation);
        return $data->getOpenData() + $data->getAdditionalInfo();
    }

    protected function getPayData($id)
    {
        $model = $this->getVerifyModel($id);
        $data = new KolibriOperation($model, $this->operation);

        return $data->getOpenData() + $data->getAdditionalInfo()
                + $data->getPayData() + $data->getRepayData();
    }

    protected function getRepayData($id)
    {
        $model = $this->getVerifyModel($id);
        $data = new KolibriOperation($model, $this->operation);

        $repayment = Cashflow::where('deal_id', $id)->where('amount', '>', 0)->get();
        if ($repayment->count() > 0) {
            return $data->getOpenData() + $data->getAdditionalInfo()
                + $data->getCashIn() + $data->getRepayData() + $data->getPaybackData($model->amount);
        } else {
            return $data->getOpenData() + $data->getAdditionalInfo()
                + $data->getRepayData() ;
        }
    }

    protected function getPaybackData($id)
    {

    }

    protected function getCloseData($id)
    {
        $model = $this->getVerifyModel($id);
        $data = new KolibriOperation($model, $this->operation);

        return $data->getOpenData() + $data->getAdditionalInfo()
            + $data->getCashIn() + $data->getRepayData() + $data->getPaybackData($model->amount);
    }

    protected function getClosePaybackData($id)
    {
        $model = $this->getVerifyModel($id);
        $data = new KolibriOperation($model, $this->operation);

        return $data->getOpenData() + $data->getAdditionalInfo() + $data->getRepayData()
            + $data->getCashIn() + $data->getCashOut();
    }

    protected function getArchiveData($id)
    {
        return $this->getOpenData($id);
    }

    protected function setOpenData($inputs)
    {
        if (empty($inputs['id'])){
            $model = new Kolibri();
        } else {
            $model = Kolibri::find($inputs['id']);
        }
        $data = new KolibriOperation($model, $this->operation);
        $data->saveRequestData($inputs);
        if (!empty($inputs['number'])) {
            $data->saveAdditionalInfo($inputs);
        }
        if (!empty($inputs['tariff'])) {
            $data->saveTariff($inputs);
        }
    }

    protected function setPayData($inputs)
    {
        $model = Kolibri::find($inputs['id']);
        if (empty($model) || $model->status_id .'' === OperationStatus::OPEN){
            $this->setOpenData($inputs);
        } else {
            $data = new KolibriOperation($model,$this->operation);
            $data->updateStatus($inputs['status']);
            $data->saveRepayData($inputs);
        }
    }

    protected function setRepayData($inputs)
    {
        $model = Kolibri::find($inputs['id']);
        if (empty($model) || $model->status_id .'' === OperationStatus::OPEN){
            $this->setOpenData($inputs);
        } else {
            $data = new KolibriOperation($model, $this->operation);
            $data->updateStatus($inputs['status']);
            $data->savePayData($inputs);
        }
    }

    protected function setPaybackData($inputs)
    {

    }

    protected function setCloseData($input)
    {
        $model = Kolibri::find($input['id']);
        $data = new KolibriOperation($model,$this->operation);
        $cf = Cashflow::where('deal_id', $model->id)->get();

        if ($model->status_id .'' === OperationStatus::REPAY) {
            $data->saveRepayData($input);
            $data->updateStatus($input['status']);

        } elseif ($model->status_id .'' === OperationStatus::PAY){
            $data->savePayData($input);
            $data->updateStatus($input['status']);

        } elseif($cf->count() === 0) {
            throw new \Exception('Заявка не может считаться выполненной, так как по ней не было движений денежных средств');
        } else {
            throw new \Exception('Что-то новое! Обратитесь в службу поддержки');
        }
    }

    protected function setClosePaybackData($inputs)
    {
        $data = new KolibriOperation(Kolibri::find($inputs['id']),$this->operation);
        $data->savePaybackData($inputs);
        $data->updateStatus($inputs['status']);
    }
}