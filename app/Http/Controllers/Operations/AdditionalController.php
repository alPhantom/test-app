<?php

namespace App\Http\Controllers\Operations;


use App\Constants\OperationType;
use App\Http\Controllers\Operations\OperationManagers\BaseOperation;
use App\Http\Controllers\Payment;
use App\Models\Cashflow;
use App\Models\Operations\Additional;
use App\Validators\AdditionalValidator;
use Illuminate\Support\Facades\DB;

class AdditionalController extends OperationController
{
    use Payment;

    protected $editView = 'additional.additional_form';
    protected $redirectUrl = '/additional';
    protected $listView = 'additional.additional_list';
    protected $operation = OperationType::ADDITIONAL;


    protected function getValidator($status)
    {
        return new AdditionalValidator($status);
    }

    protected function getOpenData($id)
    {
        $data = new BaseOperation(Additional::find($id), $this->operation);
        return $data->getOpenData();
    }

    protected function getPayData($id)
    {
        $data = new BaseOperation(Additional::find($id), $this->operation);
        return $data->getOpenData() + $data->getPayData();
    }

    protected function getRepayData($id)
    {
        //
    }

    protected function getPaybackData($id)
    {
        //
    }

    protected function getCloseData($id)
    {
        $data = new BaseOperation(Additional::find($id), $this->operation);
        return $data->getOpenData() + $data->getCashIn();
    }

    protected function getClosePaybackData($id)
    {
//
    }

    protected function getArchiveData($id)
    {
        $this->getOpenData($id);
    }

    protected function setOpenData($inputs)
    {
        if (empty($inputs['id'])){
            $model = new Additional();
        } else {
            $model = Additional::find($inputs['id']);
        }

        $data = new BaseOperation($model, $this->operation);
        $data->saveRequestData($inputs);

        if (!empty($inputs['tariff'])) {
            $data->saveTariff($inputs);
        }

    }

    protected function setPayData($inputs)
    {
        $this->setOpenData($inputs);
    }

    protected function setRepayData($input)
    {
        //
    }

    protected function setPaybackData($input)
    {
        //
    }

    protected function setCloseData($inputs)
    {
        $data = new BaseOperation(Additional::find($inputs['id']), $this->operation);
        $data->savePayData($inputs);
        $data->updateStatus($inputs['status']);
    }

    protected function setClosePaybackData($input)
    {
        //
    }

    protected function saveAdditionalInfo($input, $model)
    {
        //
    }

}