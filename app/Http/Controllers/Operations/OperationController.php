<?php

namespace App\Http\Controllers\Operations;

use App\Constants\OperationStatus;
use App\Http\Controllers\Operations\OperationManagers\BaseOperation;
use App\Http\Controllers\Payment;
use App\Http\Controllers\TariffSave;
use App\Models\Books\CrossServiceType;
use App\Models\Books\Currency;
use App\Models\Books\Marketing;
use App\Models\Cashflow;
use App\Models\Operations\OperationModel;
use App\Models\Tariffs\CrossTariffDeal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

/** Общий контроллер - регулирует поведение, исходя из текущего статуса операции.
 *  Наследники его реализуют
 * Class OperationController
 * @package App\Http\Controllers\Operations
 */
abstract class OperationController
{
    use TariffSave, Payment;

    protected $editView = '/';
    protected $redirectUrl = '/';
    protected $status;
    protected $listView;
    protected $operation;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->listView)->withModels(OperationModel::with('serviceType')
            ->whereHas('serviceType', function ($q) {
                return $q->where('type_id', $this->operation);
            })->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->editView, BaseOperation::getCreateData($this->operation));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $status = (int)$request->status;
        $input = $request->all();

        $validator = $this->getValidator($status);
        if (!$validator->isValid($input)) {
            return back()
                ->withInput()
                ->withErrors($validator->getErrors());
        } else {
            // store
            try {
                DB::beginTransaction();
                switch ($status) {

                    case OperationStatus::OPEN :
                        $this->setOpenData($input);
                        break;
                    case OperationStatus::PAY :
                        $this->setPayData($input);
                        break;
                    case OperationStatus::REPAY :
                        $this->setRepayData($input);
                        break;
                    case OperationStatus::PAYBACK :
                        $this->setPaybackData($input);
                        break;
                    case OperationStatus::CLOSE :
                        $this->setCloseData($input);
                        break;
                    case OperationStatus::CLOSE_WITH_PAYBACK :
                        $this->setClosePaybackData($input);
                        break;
                    case OperationStatus::ARCHIVE :
                        $this->setArchiveData($input);
                        break;
                    default:
                        return back()
                            ->withInput()->withErrors('undefined status');

                }
                DB::commit();
                return redirect($this->redirectUrl);

            } catch (\PDOException $e) {

                DB::rollBack();
                throw $e;

            } catch (\Exception $e) {

                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

    public function edit($id, $status)
    {
        $this->status = $status;
        try {

            switch ($status) {

                case OperationStatus::OPEN :
                    $data = $this->getOpenData($id);
                    break;
                case OperationStatus::PAY :
                    $data = $this->getPayData($id);
                    break;
                case OperationStatus::REPAY :
                    $data = $this->getRepayData($id);
                    break;
                case OperationStatus::PAYBACK :
                    $data = $this->getPaybackData($id);
                    break;
                case OperationStatus::CLOSE :
                    $data = $this->getCloseData($id);
                    break;
                case OperationStatus::CLOSE_WITH_PAYBACK :
                    $data = $this->getClosePaybackData($id);
                    break;
                case OperationStatus::ARCHIVE :
                    $data = $this->getArchiveData($id);
                    break;
                default:
                    throw new \Exception('Документа в таком статусе не существует!');

            }

            return view($this->editView, $data);

        } catch (\Exception $e) {

            if ($e->getCode() === 403) {

                return redirect('access_denied');
            } else {
                return back()
                    ->withInput()
                    ->withErrors($e->getMessage());
            }
        }
    }

    protected function getVerifyModel($id)
    {
        $model = OperationModel::find($id);

        if (Gate::denies('viewItem', $model)){
            throw new \Exception('Нет доступа!', 403);
        } elseif ($model->status_id . '' !== $this->status) {
            throw new \Exception('Документ находится в другом статусе');
        } else {
            return $model;
        }
    }

    protected abstract function getValidator($status);

//  edit

    protected abstract function getOpenData($id);

    protected abstract function getPayData($id);

    protected abstract function getRepayData($id);

    protected abstract function getPaybackData($id);

    protected abstract function getCloseData($id);

    protected abstract function getClosePaybackData($id);

    protected abstract function getArchiveData($id);

//  store

    protected abstract function setOpenData($inputs);

    protected abstract function setPayData($input);

    protected abstract function setRepayData($input);

    protected abstract function setPaybackData($input);

    protected abstract function setCloseData($input);

    protected abstract function setClosePaybackData($input);

    /**
     * Сохраняет сделку в статусе "В архив"
     * @param $inputs array Данные формы
     * @throws \Exception
     */
    protected function setArchiveData($inputs)
    {
        $payment = Cashflow::where('deal_id', $inputs['id'])->get();
        if ($payment->count() > 0) {
            throw new \Exception('Нельзя поместить в архив заявку, по которой были совершены денежные операции');
        }
        else {
            $data = new BaseOperation(OperationModel::find($inputs['id']),$this->operation);
            $data->updateStatus($inputs['status']);
        }
    }

}