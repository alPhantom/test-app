<?php

namespace App\Http\Controllers\Operations\OperationManagers;


use App\Http\Controllers\BoxController;
use App\Http\Controllers\Calculator;
use App\Models\Cashflow;
use App\Models\Operations\SendInfo;

class SendOperation extends BaseOperation implements IAdditionalInfo, IRemoteOperation, IBankRefund
{
    public function getRepayData()
    {
        return ['repayment' => Cashflow::whereIn('send_info_id',
            SendInfo::where('deal_id', $this->model->id)->get()->pluck('id')->all())->where('amount', '<', 0)->get(),
            'boxes' => BoxController::getRemoteBoxes($this->model->currency_id)];
    }

    public function getRefundBank(){
        return ['refunds' => Cashflow::whereHas('sendInfo', function ($query){
            return $query->where('deal_id', $this->model->id);
        })->where('amount', '>', 0)->get()];
    }

    public function saveAdditionalInfo($inputs)
    {
        $deleted = SendInfo::where('deal_id', $this->model->id)->delete();

        for ($i = 0; $i < count($inputs['amount']); $i++) {
            $si = new SendInfo();
            $si->deal_id = $this->model->id;
            $si->amount = $inputs['amount'][$i];
            $si->card_number = $inputs['card_number'][$i];
            $si->note = $inputs['comment'][$i];
            $si->save();
        }
    }

    public function saveRepayData($inputs)
    {
        for ($i = 0; $i < count($inputs['box_id']); $i++) {

            $cf = new Cashflow();
            $cf->box_id = $inputs['box_id'][$i];
            $cf->amount = -1 * ($inputs['repayment_amount'][$i] + $inputs['commission_amount'][$i]);
            $cf->rate = 1;
            $cf->rub_rate = Calculator::getRubRate($inputs['currency_id'],
                $inputs['repayment_amount'][$i] + $inputs['commission_amount'][$i],
                'sale', $this->model->create_office_id);
            $cf->send_info_id = $inputs['send_info_id'][$i];
            $cf->save();

            $si = SendInfo::find($inputs['send_info_id'][$i]);
            $si->paid += $inputs['repayment_amount'][$i];
            $si->save();

        }
    }

    public function saveRefundBank($inputs){

        for ($i = 0; $i < count($inputs['box_id']); $i++) {

            $cf = new Cashflow();
            $cf->box_id = $inputs['box_id'][$i];
            $cf->amount = $inputs['refund'][$i];
            $cf->rate = 1;
            $cf->rub_rate = Calculator::getRubRate($inputs['currency_id'],
                $inputs['refund'][$i],
                'buy', $this->model->create_office_id);
            $cf->send_info_id = $inputs['send_info'][$i];
            $cf->save();

            $si = SendInfo::find($inputs['send_info'][$i]);
            $si->paid -= $inputs['refund'][$i];
            $si->save();
        }
    }

    public function getPaybackData($amount)
    {
        return parent::getPaybackData($amount) + ['refunds' => Cashflow::whereHas('sendInfo', function ($query) {
                return $query->where('deal_id', $this->model->id);
            })->where('amount', '>', 0)->get()];
    }

    public function getAdditionalInfo()
    {
        return ['cardsInfo' => SendInfo::where('deal_id', $this->model->id)->get()];
    }
}