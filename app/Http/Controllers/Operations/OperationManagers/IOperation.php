<?php

namespace App\Http\Controllers\Operations\OperationManagers;

/**
 * Задаёт общее поведение для всех операций
 * Interface IOperation
 * @package App\Http\Controllers\Operations\OperationManagers
 */
interface IOperation
{
//    open models
    /** Возвращает данные по заявке (doc_deal)
     * @return mixed
     */
    public function getOpenData();

    /** Возвращает данные, необходимые для заявки в статусе К оплате (нал)
     * @return mixed
     */
    public function getPayData();

    /** Возвращает информацию о поступлениях наличных дс в кассу по заявке
     * @return mixed
     */
    public function getCashIn();

    /**
     * Возвращает данные, необходимые для заявки в статусе К возврату (нал)
     * @param float $amount сумма к возврату
     * @return mixed
     */
    public function getPaybackData($amount);

    /**
     * Возвращает информацию о выдачи дс из кассы
     * @return mixed
     */
    public function getCashOut();


//  store models

    /**
     * Сохраняет заявку
     * @param array $inputs данные формы
     * @return mixed
     */
    public function saveRequestData(array $inputs);

    /**
     * Сохраняет тарифы
     * @param array $inputs
     * @return mixed
     */
    public function saveTariff(array $inputs);

    /** Сохраняет данные по оплате в кассу
     * @param array $inputs
     * @return mixed
     */
    public function savePayData(array $inputs);

    /** Сохраняет даные по возврату дс из кассы
     * @param array $inputs
     * @return mixed
     */
    public function savePaybackData(array $inputs);

    /** Добавляет заявку в архив
     * @param array $inputs
     * @return mixed
     */
    public function saveArchiveData(array $inputs);

}