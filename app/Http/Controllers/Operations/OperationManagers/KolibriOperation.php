<?php

namespace App\Http\Controllers\Operations\OperationManagers;


use App\Http\Controllers\Calculator;
use App\Models\Box;
use App\Models\Cashflow;
use App\Models\Operations\KolibriInfo;

class KolibriOperation extends BaseOperation implements IAdditionalInfo, IRemoteOperation
{

    public function getAdditionalInfo()
    {
        return ['info' => KolibriInfo::where('head_id', $this->model->id)->first()];
    }

    public function saveAdditionalInfo($inputs)
    {
        $ki = new KolibriInfo();
        $ki->k_number = $inputs['number'];
        $ki->head_id = $this->model->id;
        $ki->save();
    }

    public function getRepayData()
    {
        return ['box' => Box::where('system_id', 20)->where('currency_id', $this->model->currency_id)->first(),
                'amount' => Cashflow::with('box')->whereHas('box', function ($q){
                    return $q->where('system_id', 20);
                })->where('deal_id', $this->model->id)->where('amount', '<', 0)->sum('amount')];
    }

    public function saveRepayData($inputs)
    {
        $cf = new Cashflow();
        $cf->amount = -$inputs['repayment_amount'];
        $cf->box_id = $inputs['box_id'];
        $cf->deal_id= $inputs['id'];
        $cf->rub_rate = 1;
        $cf->rate = Calculator::getRubRate($this->model->currency_id, $inputs['repayment_amount'], 'sale', $this->model->create_office_id);
        $cf->save();
    }
}