<?php


namespace App\Http\Controllers\Operations\OperationManagers;

/** Задаёт поведение для операций, для которых предусмотрено использование удалённых сервисов
 * Interface IRemoteOperation
 * @package App\Http\Controllers\Operations\OperationManagers
 */
interface IRemoteOperation
{
    public function getRepayData();

    public function saveRepayData($inputs);
}