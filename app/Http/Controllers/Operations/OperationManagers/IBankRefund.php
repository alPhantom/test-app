<?php

namespace App\Http\Controllers\Operations\OperationManagers;

/**
 * Задаёт поведение для операций, по которым возможен возврат дс банком
 *
 * Interface IBankRefund
 * @package App\Http\Controllers\Operations\OperationManagers
 */
interface IBankRefund
{
    public function getRefundBank();
    public function saveRefundBank($inputs);

}