<?php

namespace App\Http\Controllers\Operations\OperationManagers;


use App\Http\Controllers\BoxController;
use App\Http\Controllers\Calculator;
use App\Models\Cashflow;

class TicketOperation extends BaseOperation implements IRemoteOperation, IBankRefund
{

    public function getRepayData()
    {
        return ['repayment' => Cashflow::with('box')->whereHas('box', function ($query) {
                                            return $query->whereNotIn('system_id', [18, 19]);
                                        })->where('deal_id', $this->model->id)
                                        ->where('amount', '<', 0)->get(),
                'boxes' => BoxController::getRemoteBoxes($this->model->currency_id)];
    }

    public function saveRepayData($inputs)
    {
        for ($i = 0; $i < count($inputs['box_id']); $i++) {

            $cf = new Cashflow();
            $cf->box_id = $inputs['box_id'][$i];
            $cf->amount = -$inputs['repayment_amount'][$i];
            $cf->rate = 1;
            $cf->rub_rate = Calculator::getRubRate($inputs['currency_id'],
                $inputs['repayment_amount'][$i],
                'sale', $this->model->create_office_id);
            $cf->deal_id = $this->model->id;
            $cf->save();

        }
    }

    public function getRefundBank()
    {
        return ['refunds' => Cashflow::with('box')->whereHas('box', function ($query) {
                                                        return $query->whereNotIn('system_id', [18, 19]);
                                                    })->where('deal_id', $this->model->id)
                                                    ->where('amount', '>', 0)->get()];
    }

    public function saveRefundBank($inputs)
    {
        for ($i = 0; $i < count($inputs['box_id']); $i++) {

            $cf = new Cashflow();
            $cf->box_id = $inputs['box_id'][$i];
            $cf->amount = $inputs['repayment_amount'][$i];
            $cf->rate = 1;
            $cf->rub_rate = Calculator::getRubRate($inputs['currency_id'],
                $inputs['repayment_amount'][$i],
                'buy', $this->model->create_office_id);
            $cf->deal_id = $this->model->id;
            $cf->save();
        }
    }
}