<?php

namespace App\Http\Controllers\Operations\OperationManagers;

use App\Models\Operations\BumInfo;


class BumOperation extends BaseOperation implements IAdditionalInfo
{

    public function getAdditionalInfo()
    {
        return ['unique_key' => BumInfo::where('head_id', $this->model->id)->first()];
    }

    public function saveAdditionalInfo($inputs)
    {
        BumInfo::where('head_id', $this->model->id)->delete();
        $bi = new BumInfo();
        $bi->unique_key = preg_replace('/[ -]+/i', '', $this->model->id . '/' . $this->model->create_office_id . date('Y-m-d h-m-s'));
        $bi->head_id = $this->model->id;
        $bi->save();

    }
}