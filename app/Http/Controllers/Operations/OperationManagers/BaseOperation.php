<?php


namespace App\Http\Controllers\Operations\OperationManagers;


use App\Constants\OperationStatus;
use App\Http\Controllers\Payment;
use App\Models\Books\CrossServiceType;
use App\Models\Books\Currency;
use App\Models\Books\Marketing;
use App\Models\Cashflow;
use App\Models\Tariffs\CrossTariffDeal;

/** Базовая операция
 * Class BaseOperation
 * @package App\Http\Controllers\Operations\OperationManagers
 */
class BaseOperation implements IOperation
{
    use Payment;

    protected $model;
    protected $operation;

    public function __construct($model, $operation)
    {
        $this->model = $model;
        $this->operation = $operation;
    }

    public static function getCreateData($operation){
        return [
            'currencies' => Currency::all(),
            'serviceTypes' => CrossServiceType::where('type_id', $operation)->get(),
            'marketings' => Marketing::all(),
        ];
    }

    public function getOpenData()
    {
        return [
            'tariffs' => CrossTariffDeal::where('deal_id', $this->model->id)->get(),
            'currencies' => Currency::all(),
            'serviceTypes' => CrossServiceType::where('type_id', $this->operation)->get(),
            'marketings' => Marketing::all(),
            'model' => $this->model
        ];
    }

    public function getPayData()
    {
        $rates = $this->getPaymentRate($this->model->currency_id, false);
        $totalAmount = (int)$this->model->amount + (int)$this->model->commission;

        return ['amountData' => $rates,
            'debts' => $this->getDebtAmount($totalAmount, $rates, $this->model->currency_id)];
    }


    public function getCashIn()
    {
        return ['amountData' => Cashflow::with('box')
                                        ->whereHas('box', function ($query) {
                                            return $query->whereIn('system_id', [18, 19]);
                                        })->where('deal_id', $this->model->id)
                                        ->where('amount', '>', 0)
                                        ->get()];
    }

    public function getCashOut()
    {
        return ['paybackData' => Cashflow::with('box')
                                        ->whereHas('box', function ($query) {
                                            return $query->whereIn('system_id', [18, 19]);
                                        })->where('deal_id', $this->model->id)
                                        ->where('amount', '<', 0)
                                        ->get()];
    }

    public function getPaybackData($amount)
    {
        return $this->getRefundInfo($amount, $this->model->currency_id, $this->model->create_office_id);
    }

    public function getPaybackDataCur($amount)
    {
        $rates = $this->getPaymentRate($this->model->currency_id, false);

        return ['paybackAmount' => $amount,
            'paybackData' => $rates,
            'refundDebts' => $this->getDebtAmount($amount, $rates, $this->model->currency_id),
        ];
    }

    /**
     * @param $inputs
     */
    public function saveRequestData($inputs)
    {
        $this->model->cross_service_id = $inputs['cross_service_id'];
        $this->model->currency_id = $inputs['currency_id'];
        $this->model->first_name = $inputs['first_name'];
        $this->model->last_name = $inputs['last_name'];
        $this->model->note = $inputs['note'];
        $this->model->middle_name = $inputs['middle_name'];
        $this->model->phone = $inputs['phone'];
        $this->model->marketing_id = $inputs['marketing_id'];
        $this->model->amount = $inputs['total_amount'];
        $this->model->commission = empty($inputs['commission_amount']) ? null : (int)$inputs['commission_amount'] - (int)$inputs['total_amount'];
        $this->model->status_id = $inputs['status'];
        $this->model->save();
    }

    public function savePayData($inputs)
    {
        $this->saveCashflow($inputs);
    }

    public function savePaybackData($inputs)
    {
        $this->saveCashflow($inputs, 'minus');
    }


    /**
     * Сохраняет сделку в статусе "В архив"
     * @param $inputs array Данные формы
     * @throws \Exception
     */
    public function saveArchiveData($inputs)
    {
        $payment = Cashflow::where('deal_id', $inputs['id'])->get();
        if ($payment->count() > 0)
            throw new \Exception('Нельзя поместить в архив заявку, по которой были совершены денежные операции');
        else
            $this->updateStatus(OperationStatus::ARCHIVE);
    }

    public function saveTariff($inputs) {

        $deleted = CrossTariffDeal::where('deal_id', $this->model->id)->delete();

        for ($i = 0; $i < count($inputs['tariff']); $i++){
            $row = new CrossTariffDeal();
            $row->deal_id = $this->model->id;
            $row->changed_tariff = $inputs['tariff'][$i];
            $row->tariff_id = $inputs['tariff_id'][$i];
            $row->explanation = $inputs['tariff_comment'][$i] ?? null;
            $row->save();
        }
    }

    public function updateStatus($status)
    {
        $this->model->status_id = $status;
        $this->model->save();
    }
}