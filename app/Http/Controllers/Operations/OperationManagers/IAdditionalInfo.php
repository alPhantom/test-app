<?php


namespace App\Http\Controllers\Operations\OperationManagers;


/**
 * Задаёт поведение для операций, у которых есть дочерние таблицы в бд (н-р, Send - SendInfo)
 *
 * Interface IAdditionalInfo
 * @package App\Http\Controllers\Operations\OperationManagers
 *
 */
interface IAdditionalInfo
{
    public function getAdditionalInfo();
    public function saveAdditionalInfo($inputs);
}