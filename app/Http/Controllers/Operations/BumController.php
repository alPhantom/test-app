<?php

namespace App\Http\Controllers\Operations;


use App\Constants\OperationType;
use App\Http\Controllers\Operations\OperationManagers\BumOperation;
use App\Models\Operations\Bum;
use App\Validators\BumValidator;

class BumController extends OperationController
{
    protected $editView = 'bum.bum_form';
    protected $redirectUrl = '/bum';
    protected $listView = 'bum.bum_list';
    protected $operation = OperationType::BUM;

    public function index()
    {
        return view($this->listView)->withModels(Bum::with('serviceType')
            ->whereHas('serviceType', function ($q) {
                return $q->where('type_id', $this->operation);
            })->get());
    }

    protected function getValidator($status)
    {
        return new BumValidator($status);
    }

    protected function getOpenData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new BumOperation($model, $this->operation);
        return $data->getOpenData() + $data->getAdditionalInfo();
    }

    protected function getPayData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new BumOperation($model, $this->operation);
        return $data->getOpenData() + $data->getAdditionalInfo() + $data->getPayData();
    }

    protected function getRepayData($id)
    {
        // TODO: Implement getRepayData() method.
    }

    protected function getPaybackData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new BumOperation($model, $this->operation);
        return $data->getOpenData() + $data->getAdditionalInfo()
            + $data->getCashIn() + $data->getPaybackDataCur($model->amount);

    }

    protected function getCloseData($id)
    {
        $model = $this->getVerifyModel($id);
        $data = new BumOperation($model, $this->operation);

        return $data->getOpenData() + $data->getAdditionalInfo() + $data->getCashIn() + $data->getCashOut();
    }

    protected function getClosePaybackData($id)
    {
        // TODO: Implement getClosePaybackData() method.
    }

    protected function getArchiveData($id)
    {
        return $this->getOpenData($id);
    }

    protected function setOpenData($inputs)
    {
        if (empty($inputs['id'])){
            $model = new Bum();
        } else {
            $model = Bum::find($inputs['id']);
        }
        $data = new BumOperation($model, $this->operation);
        $data->saveRequestData($inputs);

        if (!empty($inputs['tariff'])) {
            $data->saveTariff($inputs);
        }
    }

    protected function setPayData($inputs)
    {
        $this->setOpenData($inputs);
    }

    protected function setRepayData($input)
    {
        // TODO: Implement setRepayData() method.
    }

    protected function setPaybackData($inputs)
    {
        $data = new BumOperation(Bum::find($inputs['id']), $this->operation);
        $data->savePayData($inputs);
        $data->saveAdditionalInfo($inputs);
        $data->updateStatus($inputs['status']);
    }

    protected function setCloseData($inputs)
    {
        $data = new BumOperation(Bum::find($inputs['id']), $this->operation);
        $data->savePaybackData($inputs);
        $data->updateStatus($inputs['status']);
    }

    protected function setClosePaybackData($input)
    {
        // TODO: Implement setClosePaybackData() method.
    }
}