<?php

namespace App\Http\Controllers\Operations;

use App\Constants\OperationType;
use App\Http\Controllers\Operations\OperationManagers\SendOperation;
use App\Models\Operations\Send;
use App\Models\Operations\SendInfo;
use App\Validators\SendValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SendController extends OperationController
{

    protected $editView = 'send.send_form';
    protected $redirectUrl = '/send';
    protected $listView = 'send.send_list';
    protected $operation = OperationType::SEND;

    /*
     * Получение данных для открытия формы в разных статусах
     *
     */

    protected function getOpenData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new SendOperation($model, $this->operation);
        return $data->getOpenData() + $data->getAdditionalInfo();
    }

    protected function getPayData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new SendOperation($model, $this->operation);
        return $data->getOpenData() + $data->getAdditionalInfo()
            + $data->getPayData((int)$model->amount + (int)$model->commission);
    }

    protected function getRepayData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new SendOperation($model, $this->operation);
        return $data->getOpenData() + $data->getAdditionalInfo() + $data->getCashIn();
    }

    protected function getPaybackData($id)
    {
        $model = $this->getVerifyModel($id);

        $data = new SendOperation($model, $this->operation);
        $amount = DB::select('select 
                                          sum(amount - paid) as refund 
                                    from doc_send_info 
                                    where deal_id = ?', [$id])[0];

        return $data->getOpenData() + $data->getAdditionalInfo() + $data->getCashIn() + $data->getRepayData() + $data->getPaybackData($amount->refund);
    }

    protected function getCloseData($id)
    {
        $model = $this->getVerifyModel($id);
        $data = new SendOperation($model, $this->operation);

        return $data->getOpenData() + $data->getAdditionalInfo() + $data->getCashIn() + $data->getRepayData();

    }

    protected function getClosePaybackData($id)
    {
        $model = $this->getVerifyModel($id);
        $data = new SendOperation($model, $this->operation);
        return $data->getOpenData() + $data->getAdditionalInfo() + $data->getCashIn() + $data->getRepayData() + $data->getRefundBank() + $data->getCashOut();

    }

    protected function getArchiveData($id)
    {
        return $this->getOpenData($id);
    }

    /*
     * Сохранение данных модели в разных статусах
     *
     */

    protected function setOpenData($inputs)
    {
        if (empty($inputs['id'])){
            $model = new Send();
        } else {
            $model = Send::find($inputs['id']);
        }
        $data = new SendOperation($model, $this->operation);
        $data->saveRequestData($inputs);
        if (!empty($inputs['amount'])) {
            $data->saveAdditionalInfo($inputs);
        }
        if (!empty($inputs['tariff'])) {
            $data->saveTariff($inputs);
        }
    }

    protected function setPayData($inputs)
    {
        $this->setOpenData($inputs);
    }

    protected function setRepayData($inputs)
    {
        $data = new SendOperation(Send::find($inputs['id']), $this->operation);
        $data->savePayData($inputs);
        $data->updateStatus($inputs['status']);

    }

    protected function setPaybackData($inputs)
    {

        if (empty($inputs['refund'])) {
//            К возврату после частичной оплаты
            $this->setCloseData($inputs);
        } else {
//            К возврату после возврата средств на карту банком
            $data = new SendOperation(Send::find($inputs['id']), $this->operation);
            $data->saveRefundBank($inputs);
            $data->updateStatus($inputs['status']);
        }
    }

    protected function setCloseData($inputs)
    {
        $data = new SendOperation(Send::find($inputs['id']), $this->operation);
        $data->saveRepayData($inputs);
        $data->updateStatus($inputs['status']);
    }

    protected function setClosePaybackData($inputs)
    {
        $data = new SendOperation(Send::find($inputs['id']), $this->operation);
        $data->savePaybackData($inputs);
        $data->updateStatus($inputs['status']);
    }

    protected function getValidator($status)
    {
        return new SendValidator($status);
    }


    /**
     * @param Request $request
     * @return string json
     */
    public function getOperationCardsJson(Request $request)
    {
        try {

            $clientCards = SendInfo::where('deal_id', $request->id)->get();
            $boxes = DB::select('select 
                                          db.id, db.unique_number 
                                       from doc_box as db inner join doc_cashflow as cf on db.id = cf.box_id 
                                       where cf.send_info_id in (' . implode(',', $clientCards->pluck('id')->all()) . ')');

            return json_encode(array(
                    'code' => 'success',
                    'boxes' => $boxes,
                    'cards' => $clientCards->toArray(),
                )
            );

        } catch (\Exception $e) {

            return json_encode(array(
                'code' => 'error',
                'message' => $e->getMessage(),
            ));

        }
    }

}