<?php

namespace App\Http\Controllers;


trait TariffSave {

    private function saveTariff($type ,$model_id, $tariffs, $tariff_ids, $comments ) {

        try{

            $cross = 'App\Models\Tariffs\CrossTariff' . $type;
            $id = strtolower($type) . '_id';

            $deleted = $cross::where($id, $model_id)->delete();

//            $data = [];

            for ($i = 0; $i < count($tariffs); $i++){
                $row = new $cross();
                $row->$id = $model_id;
                $row->changed_tariff = $tariffs[$i];
                $row->tariff_id = $tariff_ids[$i];
                $row->explanation = $comments[$i] ?? null;
                $row->save();
//                array_push($data, [ $id => $model_id,
//                    'changed_tariff' => $tariffs[$i],
//                    'tariff_id' => $tariff_ids[$i],
//                    'explanation' => $comments[$i] ?? null ]);
            }

//            $cross::insert($data);
        } catch(\Exception $e) {

        }

    }
}