<?php


namespace App\Http\Controllers;
use Illuminate\Support\Facades\View;
use App\Models\Menu;

class MenuController extends Controller {

        public function getMenu(){

//          корневой узел
            $root = Menu::where('descr', 'root')->first();
//          все узлы
            $nodes = Menu::where('show', 1)->get();

            $parentItems = $nodes->filter(function ($value, $key) use ($root) {
                return $value->parent_id === $root->id;
            });

            $childItems = $nodes->reject(function ($value, $key) use ($root){
                return $value->parent_id === $root->id;
            });
//          массив элементов, которые являются родителями
            $groupItems = $nodes->pluck('parent_id')->all();


            return View::make('includes.navigation.sidebar',
                                ['parents' => $parentItems, 'children' => $childItems, 'groups' => $groupItems]) ;
        }

}