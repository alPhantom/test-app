<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/access_denied', function (){
        return view('access_denied');
    })->name('access.denied');

//Route::resource('/city', 'Books\CityController');

/*
 * BEGIN: book
 */

// book_city
Route::get('/city', ['uses' => 'Books\CityController@index', 'as' => 'city.index']);
Route::post('/city', ['uses' => 'Books\CityController@store', 'as' => 'city.store']);
Route::get('/city/create', ['uses' => 'Books\CityController@create', 'as' => 'city.create']);
Route::get('/city/{city}/edit', ['uses' => 'Books\CityController@edit', 'as' => 'city.edit']);
Route::post('/city/{city}', ['uses' => 'Books\CityController@destroy', 'as' => 'city.destroy']);


// book_country
Route::get('/country', ['uses' => 'Books\CountryController@index', 'as' => 'country.index']);
Route::post('/country', ['uses' => 'Books\CountryController@store', 'as' => 'country.store']);
Route::get('/country/create', ['uses' => 'Books\CountryController@create', 'as' => 'country.create']);
Route::get('/country/{country}/edit', ['uses' => 'Books\CountryController@edit', 'as' => 'country.edit']);
Route::delete('/country/{country}', ['uses' => 'Books\CountryController@destroy', 'as' => 'country.destroy']);

// book_marketing
Route::get('/marketing', ['uses' => 'Books\MarketingController@index', 'as' => 'marketing.index']);
Route::post('/marketing', ['uses' => 'Books\MarketingController@store', 'as' => 'marketing.store']);
Route::get('/marketing/create', ['uses' => 'Books\MarketingController@create', 'as' => 'marketing.create']);
Route::get('/marketing/{marketing}/edit', ['uses' => 'Books\MarketingController@edit', 'as' => 'marketing.edit']);
Route::delete('/marketing/{marketing}', ['uses' => 'Books\MarketingController@destroy', 'as' => 'marketing.destroy']);

// book_bush
Route::get('/bush', ['uses' => 'Books\BushController@index', 'as' => 'bush.index']);
Route::post('/bush', ['uses' => 'Books\BushController@store', 'as' => 'bush.store']);
Route::get('/bush/create', ['uses' => 'Books\BushController@create', 'as' => 'bush.create']);
Route::get('/bush/{bush}/edit', ['uses' => 'Books\BushController@edit', 'as' => 'bush.edit']);
Route::delete('/bush/{bush}', ['uses' => 'Books\BushController@destroy', 'as' => 'bush.destroy']);

// book_office_group
Route::get('/office_group', ['uses' => 'Books\OfficeGroupController@index', 'as' => 'office_group.index']);
Route::post('/office_group', ['uses' => 'Books\OfficeGroupController@store', 'as' => 'office_group.store']);
Route::get('/office_group/create', ['uses' => 'Books\OfficeGroupController@create', 'as' => 'office_group.create']);
Route::get('/office_group/{office_group}/edit', ['uses' => 'Books\OfficeGroupController@edit', 'as' => 'office_group.edit']);
Route::delete('/office_group/{office_group}', ['uses' => 'Books\OfficeGroupController@destroy', 'as' => 'office_group.destroy']);
Route::post('/office_group/update', ['uses' => 'Books\OfficeGroupController@update', 'as' => 'office_group.update']);
Route::post('/office_group/delete', ['uses' => 'Books\OfficeGroupController@delete', 'as' => 'office_group.delete']);
Route::post('/office_group/list', ['uses' => 'Books\OfficeGroupController@list', 'as' => 'office_group.list']);

// book_office
Route::get('/office', ['uses' => 'Books\OfficeController@index', 'as' => 'office.index']);
Route::post('/office', ['uses' => 'Books\OfficeController@store', 'as' => 'office.store']);
Route::get('/office/create', ['uses' => 'Books\OfficeController@create', 'as' => 'office.create']);
Route::get('/office/{office}/edit', ['uses' => 'Books\OfficeController@edit', 'as' => 'office.edit']);
Route::delete('/office/{office}', ['uses' => 'Books\OfficeController@destroy', 'as' => 'office.destroy']);

// book_tariff_group
Route::get('/tariff_group', ['uses' => 'Books\TariffGroupController@index', 'as' => 'tariff_group.index']);
Route::post('/tariff_group', ['uses' => 'Books\TariffGroupController@store', 'as' => 'tariff_group.store']);
Route::get('/tariff_group/create', ['uses' => 'Books\TariffGroupController@create', 'as' => 'tariff_group.create']);
Route::get('/tariff_group/{tariff_group}/edit', ['uses' => 'Books\TariffGroupController@edit', 'as' => 'tariff_group.edit']);
Route::delete('/tariff_group/{tariff_group}', ['uses' => 'Books\TariffGroupController@destroy', 'as' => 'tariff_group.destroy']);

// book_nbu
Route::get('/nbu', ['uses' => 'Books\NbuController@index', 'as' => 'nbu.index']);
Route::post('/nbu', ['uses' => 'Books\NbuController@store', 'as' => 'nbu.store']);

// book_tariff_type
Route::get('/tariff_type', ['uses' => 'Books\TariffTypeController@index', 'as' => 'tariff_type.index']);
Route::post('/tariff_type', ['uses' => 'Books\TariffTypeController@store', 'as' => 'tariff_type.store']);
Route::get('/tariff_type/create', ['uses' => 'Books\TariffTypeController@create', 'as' => 'tariff_type.create']);
Route::get('/tariff_type/{tariff_type}/edit', ['uses' => 'Books\TariffTypeController@edit', 'as' => 'tariff_type.edit']);
Route::delete('/tariff_type/{tariff_type}', ['uses' => 'Books\TariffTypeController@destroy', 'as' => 'tariff_type.destroy']);

// book_system
Route::get('/system', ['uses' => 'Books\SystemController@index', 'as' => 'system.index']);
Route::post('/system', ['uses' => 'Books\SystemController@store', 'as' => 'system.store']);
Route::get('/system/create', ['uses' => 'Books\SystemController@create', 'as' => 'system.create']);
Route::get('/system/{system}/edit', ['uses' => 'Books\SystemController@edit', 'as' => 'system.edit']);
Route::post('/system/{system}', ['uses' => 'Books\SystemController@destroy', 'as' => 'system.destroy']);


// book_category
Route::get('/category', ['uses' => 'Books\CategoryController@index', 'as' => 'category.index']);
Route::post('/category', ['uses' => 'Books\CategoryController@store', 'as' => 'category.store']);
Route::get('/category/create', ['uses' => 'Books\CategoryController@create', 'as' => 'category.create']);
Route::get('/category/{category}/edit', ['uses' => 'Books\CategoryController@edit', 'as' => 'category.edit']);
Route::delete('/category/{category}', ['uses' => 'Books\CategoryController@destroy', 'as' => 'category.destroy']);

// book_service
Route::get('/service', ['uses' => 'Books\ServiceController@index', 'as' => 'service.index']);
Route::post('/service', ['uses' => 'Books\ServiceController@store', 'as' => 'service.store']);
Route::get('/service/create', ['uses' => 'Books\ServiceController@create', 'as' => 'service.create']);
Route::get('/service/{service}/edit', ['uses' => 'Books\ServiceController@edit', 'as' => 'service.edit']);
Route::post('/service/{service}', ['uses' => 'Books\ServiceController@destroy', 'as' => 'service.destroy']);

// teh_position
Route::get('/position', ['uses' => 'Books\PositionController@index', 'as' => 'position.index']);
Route::post('/position', ['uses' => 'Books\PositionController@store', 'as' => 'position.store']);
Route::get('/position/create', ['uses' => 'Books\PositionController@create', 'as' => 'position.create']);
Route::get('/position/{position}/edit', ['uses' => 'Books\PositionController@edit', 'as' => 'position.edit']);
Route::delete('/position/{position}', ['uses' => 'Books\PositionController@destroy', 'as' => 'position.destroy']);

// book_section_cashflow
Route::get('/sectioncf', ['uses' => 'Books\SectionCashflowController@index', 'as' => 'sectioncf.index']);
Route::post('/sectioncf', ['uses' => 'Books\SectionCashflowController@store', 'as' => 'sectioncf.store']);
Route::get('/sectioncf/create', ['uses' => 'Books\SectionCashflowController@create', 'as' => 'sectioncf.create']);
Route::get('/sectioncf/{sectioncf}/edit', ['uses' => 'Books\SectionCashflowController@edit', 'as' => 'sectioncf.edit']);
Route::delete('/sectioncf/{sectioncf}', ['uses' => 'Books\SectionCashflowController@destroy', 'as' => 'sectioncf.destroy']);

// book_tariff
Route::get('/tariff', ['uses' => 'Books\TariffController@index', 'as' => 'tariff.index']);
Route::post('/tariff', ['uses' => 'Books\TariffController@store', 'as' => 'tariff.store']);
Route::get('/tariff/create', ['uses' => 'Books\TariffController@create', 'as' => 'tariff.create']);
Route::get('/tariff/{tariff}/edit', ['uses' => 'Books\TariffController@edit', 'as' => 'tariff.edit']);
Route::post('/tariff/{tariff}/delete', ['uses' => 'Books\TariffController@destroy', 'as' => 'tariff.destroy']);
Route::post('/tariff/commission', ['uses' => 'Books\TariffController@getCommission', 'as' => 'tariff.commission']);

// teh_user
Route::get('/user', ['uses' => 'UserController@index', 'as' => 'user.index']);
Route::post('/user', ['uses' => 'UserController@store', 'as' => 'user.store']);
Route::get('/user/create', ['uses' => 'UserController@create', 'as' => 'user.create']);
Route::get('/user/{user}/edit', ['uses' => 'UserController@edit', 'as' => 'user.edit']);

/*
 * END: book
 */

// doc_box
Route::get('/box', ['uses' => 'BoxController@index', 'as' => 'box.index']);
Route::post('/box', ['uses' => 'BoxController@store', 'as' => 'box.store']);
Route::get('/box/create', ['uses' => 'BoxController@create', 'as' => 'box.create']);
Route::get('/box/{box}/edit', ['uses' => 'BoxController@edit', 'as' => 'box.edit']);
Route::delete('/box/{box}', ['uses' => 'BoxController@destroy', 'as' => 'box.destroy']);
Route::post('/box/boxlist', ['uses' => 'BoxController@getRemoteBoxesJson', 'as' => 'box.boxlist']);

// doc_income_expense
Route::get('/income_expense', ['uses' => 'IncomeExpenseController@index', 'as' => 'income_expense.index']);
Route::post('/income_expense', ['uses' => 'IncomeExpenseController@store', 'as' => 'income_expense.store']);
Route::get('/income_expense/create', ['uses' => 'IncomeExpenseController@create', 'as' => 'income_expense.create']);
Route::get('/income_expense/{income_expense}/edit', ['uses' => 'IncomeExpenseController@edit', 'as' => 'income_expense.edit']);
Route::delete('/income_expense/{income_expense}', ['uses' => 'IncomeExpenseController@destroy', 'as' => 'income_expense.destroy']);

// send
Route::get('/send', ['uses' => 'Operations\SendController@index', 'as' => 'send.index']);
Route::post('/send', ['uses' => 'Operations\SendController@store', 'as' => 'send.store']);
Route::get('/send/create', ['uses' => 'Operations\SendController@create', 'as' => 'send.create']);
Route::get('/send/{send}/{status}/edit', ['uses' => 'Operations\SendController@edit', 'as' => 'send.edit']);
Route::post('/send/bank_refund', ['uses' => 'Operations\SendController@getOperationCardsJson', 'as' => 'send.bank_refund']);

// ticket
Route::get('/ticket', ['uses' => 'Operations\TicketController@index', 'as' => 'ticket.index']);
Route::post('/ticket', ['uses' => 'Operations\TicketController@store', 'as' => 'ticket.store']);
Route::get('/ticket/create', ['uses' => 'Operations\TicketController@create', 'as' => 'ticket.create']);
Route::get('/ticket/{id}/{status}/edit', ['uses' => 'Operations\TicketController@edit', 'as' => 'ticket.edit']);
Route::post('/ticket/bank_refund', ['uses' => 'Operations\TicketController@getOperationCardsJson', 'as' => 'ticket.bank_refund']);

// additional
Route::get('/additional', ['uses' => 'Operations\AdditionalController@index', 'as' => 'additional.index']);
Route::post('/additional', ['uses' => 'Operations\AdditionalController@store', 'as' => 'additional.store']);
Route::get('/additional/create', ['uses' => 'Operations\AdditionalController@create', 'as' => 'additional.create']);
Route::get('/additional/{additional}/{status}/edit', ['uses' => 'Operations\AdditionalController@edit', 'as' => 'additional.edit']);

// kolibri
Route::get('/kolibri', ['uses' => 'Operations\KolibriController@index', 'as' => 'kolibri.index']);
Route::post('/kolibri', ['uses' => 'Operations\KolibriController@store', 'as' => 'kolibri.store']);
Route::get('/kolibri/create', ['uses' => 'Operations\KolibriController@create', 'as' => 'kolibri.create']);
Route::get('/kolibri/{kolibri}/{status}/edit', ['uses' => 'Operations\KolibriController@edit', 'as' => 'kolibri.edit']);

// bum
Route::get('/bum', ['uses' => 'Operations\BumController@index', 'as' => 'bum.index']);
Route::post('/bum', ['uses' => 'Operations\BumController@store', 'as' => 'bum.store']);
Route::get('/bum/create', ['uses' => 'Operations\BumController@create', 'as' => 'bum.create']);
Route::get('/bum/{bum}/{status}/edit', ['uses' => 'Operations\BumController@edit', 'as' => 'bum.edit']);

/*
|          POST      | city                 | city.store       | App\Http\Controllers\Books\CityController@store         | web          |
|        | GET|HEAD  | city                 | city.index       | App\Http\Controllers\Books\CityController@index         | web          |
|        | GET|HEAD  | city/create          | city.create      | App\Http\Controllers\Books\CityController@create        | web          |
|        | PUT|PATCH | city/{city}          | city.update      | App\Http\Controllers\Books\CityController@update        | web          |
|        | GET|HEAD  | city/{city}          | city.show        | App\Http\Controllers\Books\CityController@show          | web          |
|        | DELETE    | city/{city}          | city.destroy     | App\Http\Controllers\Books\CityController@destroy       | web          |
|        | GET|HEAD  | city/{city}/edit     | city.edit        | App\Http\Controllers\Books\CityController@edit          | web          |
*/

// doc_withdrawal
Route::get('/withdrawal', ['uses' => 'Operations\WithdrawalController@index', 'as' => 'withdrawal.index']);
Route::post('/withdrawal', ['uses' => 'Operations\WithdrawalController@store', 'as' => 'withdrawal.store']);
Route::get('/withdrawal/create', ['uses' => 'Operations\WithdrawalController@create', 'as' => 'withdrawal.create']);
Route::get('/withdrawal/{withdrawal}/{status}/edit', ['uses' => 'Operations\WithdrawalController@edit', 'as' => 'withdrawal.edit']);
//Route::post('/withdrawal/{id}', ['uses' => 'Operations\WithdrawalController@destroy', 'as' => 'withdrawal.destroy'])->where('id', '[0-9]+');
Route::post('/withdrawal/listbox', ['uses' => 'Operations\WithdrawalController@listBox', 'as' => 'withdrawal.listbox']);

// doc_unfinished
Route::get('/unfinished', ['uses' => 'Operations\UnfinishedController@index', 'as' => 'unfinished.index']);
Route::post('/unfinished', ['uses' => 'Operations\UnfinishedController@store', 'as' => 'unfinished.store']);
Route::get('/unfinished/create', ['uses' => 'Operations\UnfinishedController@create', 'as' => 'unfinished.create']);
Route::get('/unfinished/{id}/edit', ['uses' => 'Operations\UnfinishedController@edit', 'as' => 'unfinished.edit']);
Route::post('/unfinished/{id}/delete', ['uses' => 'Operations\UnfinishedController@destroy', 'as' => 'unfinished.destroy']);

Route::get('/node', 'Books\NodeController@showTree');
Route::post('/node/get_tree', 'Books\NodeController@getJsonTree');
Route::post('/node/update_node', 'Books\NodeController@update');
Route::post('/node/create_node', 'Books\NodeController@create');
Route::post('/node/delete_node', 'Books\NodeController@delete');

Route::get('/role', 'Books\RoleController@index')->name('roles');
Route::get('/role/{id}/edit', 'Books\RoleController@edit')->where('id', '[0-9]+')->name('role.edit');
Route::post('/role/{id}/edit', 'Books\RoleController@update')->where('id', '[0-9]+');
Route::get('/role/create', 'Books\RoleController@edit')->name('role.create');
Route::post('/role/create', 'Books\RoleController@create');
Route::delete('/role/{id}/delete', 'Books\RoleController@destroy')->where('id', '[0-9]+')->name('role.delete');
Route::post('/role/get_permissions', 'Books\RoleController@getPermissions');

// doc_rate
Route::get('/rate', ['uses' => 'RateController@index', 'as' => 'rate.index']);
Route::post('/rate/store', ['uses' => 'RateController@store', 'as' => 'rate.store']);
Route::post('/rate/show', ['uses' => 'RateController@show', 'as' => 'rate.show']);

// test
Route::get('/test', ['uses' => 'TestController@index', 'as' => 'test.index']);

