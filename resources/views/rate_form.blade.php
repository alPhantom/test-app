@extends('layouts.app')

@section('js')
    <script src="{{ asset('js/rate.js') }}" defer></script>
    <script src="{{ asset('js/office_group.js') }}" defer></script>
@endsection

@section('title', 'Курсы валют')

@section('content')


        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group input-group-sm">
                            <div class="col-lg-2 col-md-2">
                                <label class="control-label" for="group">
                                    Группа отделений:
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                {{Form::selectList('group',
                                                    'group_id',
                                                    '',
                                                    $groups,
                                                    $groupId ?? 0,
                                                    'id',
                                                    'descr'
                                )}}
                            </div>
                            <div class="col-lg-1" align="left">
                                <button data-toggle="modal" data-target="#group_modal" class="btn" id="group_edit">
                                    <i class="fa fa-cogs"></i>
                                </button>
                                @include('components.rate.group_modal')

                            </div>
                            <div id="edit_block" class="col-lg-offset-4 col-lg-2" >
                                <button type="button" id="edit_button" class="btn btn-success">
                                    Изменить
                                </button>
                            </div>
                            <div id="save_cancel_block" class="col-lg-offset-3 col-lg-3" hidden>
                                <button id="save_button" type="button" class="btn btn-primary">
                                    Сохранить
                                </button>
                                <button id="cancel_button" type="button" class="btn btn-primary">
                                    Отмена
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#cash" data-toggle="tab" aria-expanded="true">Наличный</a>
                            </li>
                            <li class=""><a href="#noncash" data-toggle="tab" aria-expanded="false">Безналичный</a>
                            </li>
                        </ul>
                        <fieldset id="content" disabled>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="cash">
                                <br>
                                @include('components.rate.rate_tables', ['retails' => $cashRetail,
                                                                         'wholesales' => $cashWholesale,
                                                                         'typeRetail' => 'cash_retail',
                                                                         'typeWholesale' => 'cash_wholesale',
                                                                         'directions' => $directions])
                            </div>
                            <div class="tab-pane fade" id="noncash">
                                <br>
                                @include('components.rate.rate_tables', ['retails' => $noncashRetail,
                                                                         'wholesales' => $noncashWholesale,
                                                                         'typeRetail' => 'noncash_retail',
                                                                         'typeWholesale' => 'noncash_wholesale',
                                                                         'directions' => $directions])
                            </div>
                        </div>
                        </fieldset>
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

@endsection