@extends('layouts.app')

@section('title', 'Незавершенка')

@section('content')

<form role="form" method="post" id="unfinished">
    {{ csrf_field() }}
    {{--<fieldset disabled="disabled">'--}}
    <div class="col-lg-6 col-md-6">
        {{ Form::hiddenInput('id', 'id', old('id') ?? $unfinished->id ?? '') }}

        <div class="form-group input-group-sm required">
            <label class="control-label" for="box">Боксы:</label>
            {{ Form::selectList('box',
                               'box_id',
                               'required' ,
                               $boxes,
                               old('box_id') ?? $unfinished->cashflow->box_id ?? '',
                               'id',
                               'unique_number') }}
        </div>

        <div class="form-group input-group-sm required">
            <label class="control-label" for="amount">Сумма:</label>
                <input
                        type="number"
                        class="form-control"
                        name="amount"
                        id="amount"
                        min="0"
                        autocomplete="off"
                        value="{{ old('amount') ?? $unfinished->cashflow->amount ?? '' }}"
                />
        </div>

        <div class="form-group input-group-sm required">
            <label class="control-label" for="coming_date">Дата и время зачисления:</label>
            <input class="form-control"
                   type="datetime-local"
                   id="coming_date"
                   name="coming_date"
                   autocomplete="off"
                   value=@if( !empty( $unfinished->coming_date ) )
                           "{{ date( 'Y-m-d\TH:i', strtotime( $unfinished->coming_date ) ) }}"
                    @endif
            >
        </div>

        <div class="form-group input-group-sm">
            <label class="control-label" for="rub_rate">Курс к рублю:</label>
            <input
                    type="number"
                    class="form-control"
                    name="rub_rate"
                    id="rub_rate"
                    min="0"
                    disabled
                    value="{{ old('rub_rate') ?? $unfinished->cashflow->rub_rate ?? '' }}"
            />
        </div>

        <div class="form-group required">
            <label for="note">Комментарий:</label>
            <textarea
                    class="form-control"
                    rows="2" name="note" id="note">{{ old('note') ?? $unfinished->note ?? '' }}</textarea>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12 panel panel-default panel-body">
            <div class="div-left">
                <button type="submit"
                        name="status"
                        formaction="{{ route('unfinished.store') }}"
                        id="to_save"
                        value="1"
                        class="btn btn-primary">Сохранить</button>
                <button type="submit"
                        name="status"
                        formaction="{{ route('unfinished.destroy', ['id' => $unfinished->id ?? '']) }}"
                        id="to_delete"
                        value="save"
                        @if ( empty( $unfinished->id ) )
                            disabled
                        @endif
                        class="btn btn-primary">Удалить</button>
            </div>
            <div class="div-right">
                <a href="{{ route('unfinished.index') }}" class="btn btn-primary" role="button">Отмена</a>
            </div>
        </div>
    </div>
    {{--</fieldset>--}}
</form>

@endsection