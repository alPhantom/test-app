@extends('layouts.app')

@section('title', 'Незавершенка')

@section('content')

    @can('write', \App\Models\Unfinished::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('unfinished.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-12">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Платежнаф система</th>
                    <th>Номер бокса</th>
                    <th>Сумма</th>
                    <th>Валюта</th>
                    <th>Курс к рублю</th>
                    <th>Дата время</th>
                    <th>Комментарий</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($unfinisheds as $unfinished)
                    <tr id="row{{ $unfinished->id }}">
                        <td><a href="{{ route('unfinished.edit', ['id' => $unfinished->id] ) }}">Открыть</a></td>
                        <td>{{ $unfinished->id }}</td>
                        <td>{{ $unfinished->cashflow->box->system->descr }}</td>
                        <td>{{ $unfinished->cashflow->box->unique_number }}</td>
                        <td>{{ $unfinished->cashflow->amount }}</td>
                        <td>{{ $unfinished->cashflow->box->currency->descr }}</td>
                        <td>{{ $unfinished->cashflow->rub_rate }}</td>
                        <td>{{ $unfinished->coming_date }}</td>
                        <td>{{ $unfinished->note }}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection