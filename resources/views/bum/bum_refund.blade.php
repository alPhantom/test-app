<form role="form" action="{{ route('bum.store') }}" method="post" id="send_cash_payment">
    {{ csrf_field() }}
    {{ Form::hiddenInput('id', 'id', old('id') ?? $model->id ?? '') }}

    @cannot('editRefund', $model)
        <fieldset disabled>
            @endcannot
            @include('components.payment', ['totalAmount' => ($model->amount ?? 0),
                                            'currency' =>  $model->currency->descr ?? '',
                                            'amountData' => $paybackData ?? '',
                                            'debts' => $refundDebts ?? '',
                                            'sign' => 'refund'])

            @cannot('editRefund', $model)
        </fieldset>
    @endcannot
    <div class="col-lg-12 panel panel-default panel-body">
        <div class="div-left">
            <button type="submit" name="status" value="{{ \App\Constants\OperationStatus::CLOSE }}" class="btn btn-primary"
                    @cannot('editRefund', $model) disabled @endcannot>Завершить</button>
        </div>
        <div class="div-right">
            <a href="{{ route('bum.index') }}" class="btn btn-primary" role="button">Отмена</a>
        </div>
    </div>

</form>

