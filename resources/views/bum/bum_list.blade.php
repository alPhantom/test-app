@extends('layouts.app')

@section('title', 'Бум')

@section('content')

    @can('addRequest', \App\Models\Operations\Bum::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('bum.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    @can('viewList', \App\Models\Operations\Bum::class)
        <div class="row">
            <div class="col-lg-12">
                <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                    <thead>
                    <tr>
                        <th>Открыть</th>
                        <th>Код</th>
                        <th>Идентификатор</th>
                        <th>Дата</th>
                        <th>Вид услуги</th>
                        <th>Сумма</th>
                        <th>Валюта</th>
                        <th>Статус</th>
                        <th>Отделение</th>
                        <th>Автор</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($models as $bum)
                        {{--@can('viewItem', $bum)--}}
                            <tr id="row{{ $bum->id }}">
                                <td><a href="{{ route('bum.edit', ['id' => $bum->id, 'status' => $bum->status->id]) }}">Открыть</a></td>
                                <td>{{ $bum->id }}</td>
                                <td>{{ $bum->info->unique_key ?? '--' }}</td>
                                <td>{{ $bum->create_date_time }}</td>
                                <td>{{ $bum->serviceType->serviceDescr }}</td>
                                <td>{{ ($bum->amount ?? 0) + ($bum->commission ?? 0) }}</td>
                                <td>{{ $bum->currency->descr }}</td>
                                <td>{{ $bum->status->descr }}</td>
                                <td>{{ $bum->office->item }}</td>
                                <td>{{ $bum->user->last_name }}</td>
                            </tr>
                        {{--@endcan--}}
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    @endcan
@endsection