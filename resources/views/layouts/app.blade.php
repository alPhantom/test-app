<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'PKC24Express') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('include/jquery/jquery.min.js') }}" defer></script>
    <script src="{{ asset('include/bootstrap/js/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('include/metisMenu/metisMenu.min.js') }}" defer></script>
    <script src="{{ asset('include/sb-admin/js/sb-admin-2.js') }}" defer></script>
    <script src="{{ asset('include/datatables/js/jquery.dataTables.min.js') }}" defer></script>
    <script src="{{ asset('include/datatables-plugins/dataTables.bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('include/datatables-responsive/dataTables.responsive.js') }}" defer></script>
    <script src="{{ asset('js/datatables_settings.js') }}" defer></script>

    @yield('js')

    <!-- Fonts -->
    {{--<link rel="dns-prefetch" href="https://fonts.gstatic.com">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">--}}

    <link href="{{ asset('images/favicon.ico') }}" rel="shortcut icon" type="image/x-icon">

    <!-- Styles -->
    <link href="{{ asset('include/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('include/metisMenu/metisMenu.min.css') }}" rel="stylesheet">
    <link href="{{ asset('include/datatables-plugins/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('include/datatables-responsive/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('include/sb-admin/css/sb-admin-2.css') }}" rel="stylesheet">
    <link href="{{ asset('include/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    @yield('css')


</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            @include('includes.header')
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Begin: alert_error -->
                    <div class="alert alert-danger" role="alert" id="error-message-box" hidden>
                        <strong>Ошибка:</strong>
                        <span id="error-text"></span>
                        <br>
                        <strong>Время:</strong>
                        <span id="error-time">2018-07-05 12:07:42</span>
                    </div>
                    <!-- End: alert_error -->

                    <!-- Begin: alert_success -->
                    <div class="alert alert-success" role="alert" id="success-message-box" hidden>
                        <strong>Сообщение:</strong>
                        <span id="success-text"></span>
                        <br>
                        <strong>Время:</strong>
                        <span id="success-time">2018-07-05 12:07:42</span>
                    </div>
                    <!-- End: alert_success -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

{{--            @include('_messages')--}}

{{--            @include('messages')--}}

            <!-- Отображение ошибок проверки ввода -->
            @include('includes.errors')

            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">@yield('title')</h2>
                </div>
            </div>

            @yield('content')

        </div>
        <!-- /.page-wrapper -->
    </div>
    <!-- /.wrapper -->
</body>
</html>
