<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ПКЦ24 Экспресс</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('include/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('include/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Custom Login -->
    <link href="{{ asset('css/login.css')}}" rel="stylesheet" type="text/css">


    <link href="{{asset('css/replacer.css')}}" rel="stylesheet" type="text/css">

    <link href="{{ asset('images/favicon.ico') }}" rel="shortcut icon" type="image/x-icon">

    <!-- jQuery -->
    <script src="{{ asset('include/jquery/jquery.min.js') }}" defer></script>
    <script src="{{ asset('include/jquery-maskedinput-master/jquery.maskedinput.min.js') }}" defer></script>

    <!-- Validate Phone -->
    <script src="{{ asset('js/validate_phone.js') }}"></script>

    <script src="{{ asset('include/bootstrap/js/bootstrap.min.js') }}"></script>

</head>

<body>

<div class="container">
    <h1 class="logo">ПКЦ24 Экспресс</h1>
    <div class="row">

        @yield('content')

    </div>
</div>

</body>

</html>
