@extends('layouts.app')

@section('title', 'Колибри')

@section('content')

    @can('addRequest', \App\Models\Operations\Kolibri::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('kolibri.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    @can('viewList', \App\Models\Operations\Kolibri::class)
        <div class="row">
            <div class="col-lg-12">
                <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                    <thead>
                    <tr>
                        <th>Открыть</th>
                        <th>Код</th>
                        <th>Дата</th>
                        <th>Вид услуги</th>
                        <th>Сумма</th>
                        <th>Валюта</th>
                        <th>Статус</th>
                        <th>Отделение</th>
                        <th>Автор</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($models as $model)
                        @can('viewItem', $model)
                            <tr id="row{{ $model->id }}">
                                <td><a href="{{ route('kolibri.edit', ['id' => $model->id, 'status' => $model->status->id]) }}">Открыть</a></td>
                                <td>{{ $model->id }}</td>
                                <td>{{ $model->create_date_time }}</td>
                                <td>{{ $model->serviceType->descr }}</td>
                                <td>{{ ($model->amount ?? 0) + ($model->commission ?? 0) }}</td>
                                <td>{{ $model->currency->descr }}</td>
                                <td>{{ $model->status->descr }}</td>
                                <td>{{ $model->office->item }}</td>
                                <td>{{ $model->user->last_name }}</td>
                            </tr>
                        @endcan
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    @endcan
@endsection