<form role="form" action="{{ route('kolibri.store') }}" method="post" id="kolibri_cash_payment">
    {{ csrf_field() }}
    {{ Form::hiddenInput('id', 'id', old('id') ?? $model->id ?? '') }}

    @cannot('editPayment', $model)
        <fieldset disabled>
            @endcannot

            @include('components.payment', ['totalAmount' => ($model->amount ?? 0) + ($model->commission ?? 0),
                                            'currency' =>  $model->currency->descr ?? '',
                                            'amountData' => $amountData ?? '',
                                            'debts' => $debts ?? ''])

            @cannot('editPayment', $model)
        </fieldset>
    @endcannot
    <div class="col-lg-12 panel panel-default panel-body">
        <div class="div-left">
            @if (empty($amount))
                <button type="submit" name="status" id="to_execute" value="3"
                        @cannot('editPayment', $model) disabled @endcannot
                        class="btn btn-primary">К выполнению</button>
            @else
                <button type="submit" name="status" id="to_execute" value="5"
                        @cannot('editPayment', $model) disabled @endcannot
                        class="btn btn-primary">Выполнено</button>
            @endif
        </div>
        <div class="div-right">
            <a href="{{ route('kolibri.index') }}" class="btn btn-primary" role="button">Отмена</a>
        </div>
    </div>
</form>