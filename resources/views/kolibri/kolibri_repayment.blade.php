<form role="form" action="{{ route('kolibri.store') }}" method="post" id="kolibri_info_repayment">
    {{ csrf_field() }}

    {{ Form::hiddenInput('id', 'id', old('id') ?? $model->id ?? '') }}
    {{ Form::hiddenInput('currency_id', 'currency_id', old('currency_id') ?? $model->currency_id ?? '') }}
    <br>
    @cannot('editRepayment', $model)
        <fieldset disabled>
            @endcannot
            <div class="col-lg-8 col-md-8 col-sm-8">

                <div id="repayment_block">
                    <div class="form-group row repayment-row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group input-group-sm">
                                <label class="control-label">Карта ПКЦ:</label>
                                <input class="form-control" id="box_descr"
                                       type="text" required="required"
                                       value="{{ $box->unique_number ?? ''}}" disabled/>
                                {{ Form::hiddenInput('box_id', 'box_id', old('box_id') ?? $box->id ?? '') }}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-group input-group-sm">
                                <label class="control-label">Сумма:</label>
                                <input class="form-control" name="repayment_amount"
                                       type="number" required="required"
                                       placeholder="{{ $model->amount ?? '' }}"
                                       value="{{ old('repayment_amount') ?? $amount === 0 ? '' : -$amount }}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

                <div class="form-group row">
                    <textarea class="form-control" rows="3" name="note" id="note" placeholder="Оставьте комментарий..."
                    >{{ old('note') ?? $model->note ?? '' }}</textarea>
                </div>

            </div>
            @cannot('editRepayment', $model)
        </fieldset>
    @endcannot

    <div class="row">
        <div class="col-lg-12 panel panel-default panel-body">
            <div class="div-left">
                @cannot('editRepayment', $model)
                    <fieldset disabled>
                        @endcannot
                        @if (empty($amountData))
                            <button type="submit" name="status" formaction="{{ route('kolibri.store') }}" id="to_pay" value="2" class="btn btn-primary">К оплате</button>
                        @else
                            <button type="submit" name="status" formaction="{{ route('kolibri.store') }}" id="close" value="5" class="btn btn-primary">Выполнено</button>
                        @endif
                        @cannot('editRepayment', $model)
                    </fieldset>
                @endcannot
            </div>
            <div class="div-right">
                <a href="{{ route('kolibri.index') }}" class="btn btn-primary" role="button">Отмена</a>
            </div>
        </div>
    </div>

</form>
