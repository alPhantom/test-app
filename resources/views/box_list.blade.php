@extends('layouts.app')

@section('title', 'Боксы')

@section('content')

    @can('write', \App\Models\Box::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('box.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-12">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Система</th>
                    <th>Валюта</th>
                    <th>Отделение</th>
                    <th>Уникальный номер</th>
                    <th>Баланс</th>
                    <th>Курс</th>
                    {{--<th>Комментарий</th>--}}
                    <th>В архиве?</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($boxes as $box)
                    <tr id="row{{ $box->id }}">
                        <td><a href="{{ route('box.edit', ['id' => $box->id] ) }}">Открыть</a></td>
                        <td>{{ $box->id }}</td>
                        <td>{{ $box->system->descr }}</td>
                        <td>{{ $box->currency->descr }}</td>
                        <td>{{ $box->office->item }}</td>
                        <td>{{ $box->unique_number }}</td>
                        <td>{{ $box->balance }}</td>
                        <td>{{ round( $box->rate, 2 ) }}</td>
                        {{--<td>{{ $box->note }}</td>--}}
                        <td>{{ ( $box->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ url('box/' . $box->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Box::class)
                                        disabled
                                        @endcannot
                                >
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection