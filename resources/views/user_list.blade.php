@extends('layouts.app')

@section('title', 'Пользователи')

@section('content')

    @can('write', \App\User::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('user.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-10">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Пользователь</th>
                    <th>Телефон</th>
                    <th>Работает</th>
                    <th>Должность</th>
                    <th>Отделение</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr id="row{{ $user->id }}">
                        <td><a href="{{ route('user.edit', ['id' => $user->id] ) }}">Открыть</a></td>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->last_name . ' ' . $user->first_name }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ ($user->works == 'yes' ) ? 'Да' : 'Нет' }}</td>
                        <td>{{ $user->position->descr or '' }}</td>
                        <td>{{ $user->office->item or ''}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection