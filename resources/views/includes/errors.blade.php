@if ( count( $errors ) > 0 )
    <!-- Список ошибок формы -->
    <div class="alert alert-danger" role="alert">
        <ul>
            @foreach ( $errors->all() as $error )
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


@if ( Session::has( 'success' ) )
    <div class="alert alert-success" role="alert">
        <strong>Success: </strong> {{ Session::get('success') }}
    </div>
@endif