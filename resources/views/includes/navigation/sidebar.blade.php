

<ul class="nav" id="side-menu">

    @foreach($parents as $parent)
        @can('view', $parent)
            <li>
                <a href="{{ '/' . $parent->descr }}">
                    <i class="{{ $parent->icon }}"> </i>
                    <b>{{ $parent->menu_descr }}</b>
                    @if( in_array($parent->id, $groups))
                        <span class="fa arrow"></span> </a>
                         <ul class="nav nav-second-level collapse">
                            @foreach($children as $child)
                                @if($child->parent_id === $parent->id)
                                    @can('view', $child)
                                        <li>
                                            <a href="{{ '/' . $child->descr }}">
                                                <i class="{{ $child->icon }}"></i>
                                                <b>{{ $child->menu_descr }} </b>
                                            </a>
                                        </li>
                                    @endcan
                                @endif
                            @endforeach
                        </ul>
                    @else
                        </a>
                    @endif
            </li>
        @endcan

    @endforeach

</ul>