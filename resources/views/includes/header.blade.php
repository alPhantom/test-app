<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/">{{ config('app.name', 'PKC24Express') }}</a>
</div>

@include('includes.navigation.navright')

<div class="navbar-default sidebar" role="navigation">

    <div class="sidebar-nav navbar-collapse">

        {!! include_action('App\Http\Controllers\MenuController', 'getMenu') !!}
        {{--@include('includes.navigation.sidebar')--}}

    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
