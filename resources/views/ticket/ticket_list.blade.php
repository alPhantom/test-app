@extends('layouts.app')

@section('title', 'Билеты/Olx')

@section('content')

    @can('addRequest', \App\Models\Operations\Ticket::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('ticket.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    @can('viewList', \App\Models\Operations\Ticket::class)
        <div class="row">
            <div class="col-lg-12">
                <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                    <thead>
                    <tr>
                        <th>Открыть</th>
                        <th>Код</th>
                        <th>Дата</th>
                        <th>Вид услуги</th>
                        <th>Сумма</th>
                        <th>Валюта</th>
                        <th>Статус</th>
                        <th>Отделение</th>
                        <th>Автор</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($models as $ticket)
                        @can('viewItem', $ticket)
                            <tr id="row{{ $ticket->id }}">
                                <td><a href="{{ route('ticket.edit', ['id' => $ticket->id, 'status' => $ticket->status->id]) }}">Открыть</a></td>
                                <td>{{ $ticket->id }}</td>
                                <td>{{ $ticket->create_date_time }}</td>
                                <td>{{ $ticket->serviceType->descr }}</td>
                                <td>{{ ($ticket->amount ?? 0) + ($ticket->commission ?? 0) }}</td>
                                <td>{{ $ticket->currency->descr }}</td>
                                <td>{{ $ticket->status->descr }}</td>
                                <td>{{ $ticket->office->item }}</td>
                                <td>{{ $ticket->user->last_name }}</td>
                            </tr>
                        @endcan
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    @endcan
@endsection