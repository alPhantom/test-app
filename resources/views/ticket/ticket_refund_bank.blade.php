<form role="form" action="{{ route('ticket.store') }}" method="post" id="ticket_refund_bank">
    {{ csrf_field() }}

    {{ Form::hiddenInput('id', 'id', old('id') ?? $model->id ?? '') }}
    {{ Form::hiddenInput('currency_id', 'currency_id', old('currency_id') ?? $model->currency_id ?? '') }}

    @cannot('editBankRefund', $model)
        <fieldset disabled>
            @endcannot

            <div class="col-lg-8 col-md-8 col-sm-10">
                <div class="form-group input-group-sm repayment-button">
                    <button role="button" class="btn" id="add_box_refund">Добавить операцию</button>
                </div>

                <br>
                <div class="form-group">
                    <div id="repayment_block_refund">

                        @if(!empty($refunds))
                            @foreach($refunds as $row)
                                <div class="form-group row required repayment-row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group input-group-sm">
                                            <label class="control-label">Карта ПКЦ:</label>
                                            {{ Form::selectList('box_' . $row->box_id,
                                                                   'box_id[]',
                                                                   'disabled' ,
                                                                   $boxes,
                                                                   $row->box_id ?? '',
                                                                   'id',
                                                                   'unique_number') }}
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-group input-group-sm">
                                            <label class="control-label">Сумма:</label>
                                            <input class="form-control" name="refund[]" type="number" disabled value="{{ $row->amount }}"></div>
                                    </div>

                                </div>
                            @endforeach

                        @endif

                    </div>
                </div>
                <br>
                <div class="form-group row">
                    <textarea class="form-control" rows="3" name="note" id="note" placeholder="Оставьте комментарий...">{{ old('note') ?? $model->note ?? '' }}</textarea>
                </div>
            </div>
            @cannot('editBankRefund', $model)
        </fieldset>
    @endcannot

    <div class="row">
        <div class="col-lg-12 panel panel-default panel-body">
            <div class="div-left">
                <button type="submit" name="status" formaction="{{ route('ticket.store') }}" id="payback" value="4"
                        class="btn btn-primary"
                        @cannot('editBankRefund', $model) disabled @endcannot
                        >Возврат</button>
            </div>
            <div class="div-right">
                <a href="{{ route('ticket.index') }}" class="btn btn-primary" role="button">Отмена</a>
            </div>
        </div>
    </div>

</form>
