@extends('layouts.app')

@section('js')

    <script src="{{ asset('js/ticket/ticket_repayment.js') }}" defer></script>
    <script src="{{ asset('js/tariff.js') }}" defer></script>
    <script src="{{ asset('js/payment.js') }}" defer></script>
    {{--<script src="{{ asset('js/send/send_repayment.js') }}" defer></script>--}}
    {{--<script src="{{ asset('js/send/send_bank_refund.js') }}" defer></script>--}}
@endsection

@section('css')
    <link href="{{ asset('css/send.css') }}" rel="stylesheet">
@endsection

@section('title', 'Билеты/Olx')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                @if(!empty($model))
                    @can('readRequest', $model)
                        <li class="active">
                            <a href="#request" data-toggle="tab" aria-expanded="true">Заявка</a>
                        </li>
                    @endcan
                    @can('readPayment', $model)
                        <li class="">
                            <a href="#payment" data-toggle="tab" aria-expanded="false">Оплата</a>
                        </li>
                    @endcan
                    @can('readRepayment', $model)
                        <li class="">
                            <a href="#repayment" data-toggle="tab" aria-expanded="false">Погашение</a>
                        </li>
                    @endcan
                    @can('readBankRefund', $model)
                        <li class="">
                            <a href="#refundBank" data-toggle="tab" aria-expanded="false">Возврат банком</a>
                        </li>
                    @endcan
                    @can('readRefund', $model)
                        <li class="">
                            <a href="#refund" data-toggle="tab" aria-expanded="false">Возврат</a>
                        </li>
                    @endcan
                @else
                    <li class="active">
                        <a href="#request" data-toggle="tab" aria-expanded="true">Заявка</a>
                    </li>
                @endif
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                @if(!empty($model))
                    @can('readRequest', $model)
                        <div class="tab-pane fade active in" id="request">
                            <br>
                            @include('ticket.ticket_request')
                        </div>
                    @endcan
                    @can('readPayment', $model)
                        <div class="tab-pane fade" id="payment">
                            <br>
                            @include('ticket.ticket_payment', [])
                        </div>
                    @endcan
                    @can('readRepayment', $model)
                        <div class="tab-pane fade" id="repayment">
                            <br>
                            @include('ticket.ticket_repayment', [])
                        </div>
                    @endcan
                    @can('readBankRefund', $model)
                        <div class="tab-pane fade" id="refundBank">
                            <br>
                            @include('ticket.ticket_refund_bank', [])
                        </div>
                    @endcan
                    @can('readRefund', $model)
                        <div class="tab-pane fade" id="refund">
                            <br>
                            @include('ticket.ticket_refund', [])
                        </div>
                    @endcan
                @else
                    <div class="tab-pane fade active in" id="request">
                        <br>
                        @include('ticket.ticket_request')
                    </div>
                @endif
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@endsection