<form role="form" action="{{ route('ticket.store') }}" method="post" id="ticket_info_repayment">
    {{ csrf_field() }}

    {{ Form::hiddenInput('id', 'id', old('id') ?? $model->id ?? '') }}
    {{ Form::hiddenInput('currency_id', 'currency_id', old('currency_id') ?? $model->currency_id ?? '') }}

    @cannot('editRepayment', $model)
        <fieldset disabled>
            @endcannot
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="form-group input-group-sm repayment-button">
                    <button role="button" class="btn" id="add_box">Добавить операцию</button>
                </div>

                <br>
                <div id="repayment_block">
                    @if (!empty($repayment))
                        @foreach($repayment as $row)
                            <div class="form-group row repayment-row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group input-group-sm">
                                        <label class="control-label">Карта ПКЦ:</label>
                                        {{ Form::selectList('box',
                                                               'box_id[]',
                                                               'disabled' ,
                                                               $boxes,
                                                               $row->box_id ?? '',
                                                               'id',
                                                               'unique_number') }}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group input-group-sm">
                                        <label class="control-label">Сумма:</label>
                                        <input class="form-control" name="repayment_amount[]"
                                               type="number" required="required"
                                               value="{{ -$row->amount }}" disabled/>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endIf
                </div>
                <br>

                <div class="form-group row">
                    <textarea class="form-control" rows="3" name="note" id="note" placeholder="Оставьте комментарий...">{{ old('note') ?? $model->note ?? '' }}</textarea>
                </div>

            </div>
            @cannot('editRepayment', $model)
        </fieldset>
    @endcannot

    <div class="row">
        <div class="col-lg-12 panel panel-default panel-body">
            <div class="div-left">
                @cannot('editRepayment', $model)
                    <fieldset disabled>
                        @endcannot
                        <button type="submit" name="status" formaction="{{ route('ticket.store') }}" id="close" value="5" class="btn btn-primary">Выполнено</button>
                        @cannot('editRepayment', $model)
                    </fieldset>
                @endcannot
            </div>
            <div class="div-right">
                <a href="{{ route('ticket.index') }}" class="btn btn-primary" role="button">Отмена</a>
            </div>
        </div>
    </div>

</form>
