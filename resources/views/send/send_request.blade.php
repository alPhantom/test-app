<form role="form" method="post" id="send_request">
    {{ csrf_field() }}
    @if(!empty($model))
        @cannot('editRequest', $model)
            <fieldset disabled>
        @endcannot
    @endif

    <div class="col-lg-6 col-md-6">
        {{ Form::hiddenInput('id', 'id', old('id') ?? $model->id ?? '') }}

        @include('components.client', ['model' => $model ?? null, 'marketings'=> $marketings])

        <br>
        <div class="form-group input-group-sm required row request">
            <div class="col-lg-4">
                <label class="control-label" for="last_name">Вид операции:</label>
            </div>
            <div class="col-lg-8">
                {{ Form::selectList('cross_service',
                                   'cross_service_id',
                                   'required' ,
                                   $serviceTypes,
                                   old('cross_service_id') ?? $model->cross_service_id?? '',
                                   'id',
                                   'descr') }}
            </div>
        </div>

        <div class="form-group input-group-sm required row request">
            <div class="col-lg-4">
                <label class="control-label">Валюта:</label>
            </div>
            <div class="col-lg-8">
                {{ Form::selectList('currency',
                                   'currency_id',
                                   'required' ,
                                   $currencies,
                                   old('currency_id') ?? $model->currency_id?? '',
                                   'id',
                                   'descr') }}
            </div>
        </div>

        <div class="form-group input-group-sm required row request">
            <div class="col-lg-4">
                <label class="control-label" for="total_amount">Сумма:</label>
            </div>
            <div class="col-lg-8">
                <input
                        type="number"
                        class="form-control readonly"
                        name="total_amount"
                        id="total_amount"
                        readonly
                        value="{{ $model->amount ?? 0 }}"
                />
            </div>
        </div>


        @include('components.tariff', ['tariffs' => $tariffs ?? null,
                                       'commission_amount' => ($model->amount ?? 0) + ($model->commission ?? 0)])

    </div>

    <!--------------------------------- Блок добавления карт ------------------------->

    <div class="col-lg-6 col-md-6">

        <div class="row">
            <div class="col-lg-4">
                <button role="button" class="btn" id="add_card">Добавить операцию</button>
            </div>
        </div>
        <div class="form-group" id="card_info_block">
            <br>
            @if (!empty($cardsInfo))  {{-- todo show old() values --}}
                @foreach ($cardsInfo as $info)
                    <div class="form-group row card-block required">
                        <div class="row">
                            <div class="col-lg-5 col-md-4">
                                <label class="control-label" for="card_number">Карта/кошелек клиента:</label>
                            </div>
                            <div class="col-lg-6 col-md-5">
                                <input class="form-control"
                                       type="text"
                                       id="card_number"
                                       name="card_number[]"
                                       required
                                       maxlength="30"
                                       autocomplete="off"
                                       value="{{ $info->card_number ?? '' }}"
                                >
                            </div>
                            @can('editRequest', $model)
                            <div class="col-lg-1 col-md-2 col-sm-1 col-xs-1">
                                <div class="delete-card-btn pointer">
                                    <i class="fa fa-trash fa-2x"></i>
                                </div>
                            </div>
                            @endcan
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-md-5">
                                <label class="control-label" for="amount">Сумма:</label>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <input class="form-control"
                                       type="number"
                                       id="amount"
                                       name="amount[]"
                                       min="0"
                                       required="required"
                                       autocomplete="off"
                                       value="{{ $info->amount }}"
                                >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-md-5">
                                <label class="control-label">Комментарий:</label>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <input class="form-control" type="text"
                                       name="comment[]" required="required"
                                       value="{{ $info->note }}"
                                >
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>

    <div class="col-lg-12">
        <div class="form-group row">
            <textarea
                    class="form-control"
                    rows="2" name="note" id="note"
                    placeholder="Комментарий...">{{ old('note') ?? $model->note ?? '' }}</textarea>
        </div>
    </div>
    @if(!empty($model))
        @cannot('editRequest', $model)
            </fieldset>
        @endcannot
    @endif
    <div class="col-lg-12 panel panel-default panel-body">
        <div class="div-left">
            @if(!empty($model))
                @cannot('editRequest', $model)
                    <fieldset disabled>
                @endcannot
            @endif
            <button type="submit" name="status" formaction="{{ route('send.store') }}" id="to_save" value="1" class="btn btn-primary">Сохранить</button>
            <button type="submit" name="status" formaction="{{ route('send.store') }}" id="to_pay" value="2" class="btn btn-primary">К оплате</button>
            <button type="submit" name="status" formaction="{{ route('send.store') }}" id="to_delete" value="7" class="btn btn-primary">Архив</button>
            @if(!empty($model))
                @cannot('editRequest', $model)
                    </fieldset>
                @endcannot
            @endif
        </div>
        <div class="div-right">
            <a href="{{ route('send.index') }}" class="btn btn-primary" role="button">Отмена</a>
        </div>
    </div>
</form>
