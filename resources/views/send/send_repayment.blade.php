<form role="form" action="{{ route('send.store') }}" method="post" id="send_info_repayment">
    {{ csrf_field() }}

    {{ Form::hiddenInput('id', 'id', old('id') ?? $model->id ?? '') }}
    {{ Form::hiddenInput('currency_id', 'currency_id', old('currency_id') ?? $model->currency_id ?? '') }}

    @cannot('editRepayment', $model)
        <fieldset disabled>
    @endcannot
    <div class="col-lg-11 col-md-11 col-sm-11">

        @if (!empty($cardsInfo))  {{-- todo show old() values --}}
        @foreach ($cardsInfo as $info)
            <div class="panel panel-default panel-body">
            <div class="form-group repayment-row" id="request_info_{{ $info->id }}">

                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="form-group input-group-sm">
                            <label class="control-label">Номер карты/кошелька:</label>

                            <input class="form-control" type="text" id="card_number"
                                   name="card_number_{{ $info->id }}" disabled
                                   value="{{ $info->card_number ?? '' }}"
                            >
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2">
                        <div class="form-group input-group-sm">
                            <label class="control-label">Сумма:</label>
                            <input class="form-control" type="number" id="amount"
                                   name="amount_{{ $info->id }}" disabled
                                   value="{{ $info->amount }}"
                            >
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group input-group-sm">
                            <label class="control-label">Комментарий:</label>

                            <input class="form-control" type="text"
                                   name="comment_{{ $info->id }}" disabled
                                   value="{{ $info->note?? '' }}"
                            >
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="form-group input-group-sm repayment-button">
                        <button role="button" class="btn" id="add_box_{{ $info->id }}">Добавить операцию</button>
                    </div>
                </div>
                </div>
                <div id="repayment_block_{{ $info->id }}">
                        @if (!empty($repayment))
                    @foreach($repayment->where('send_info_id', $info->id)->all() as $row)
                        <div class="form-group row repayment-row">
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group input-group-sm">
                                    <label class="control-label">Карта ПКЦ:</label>
                                    {{ Form::selectList('box',
                                                           'box_id[]',
                                                           'disabled' ,
                                                           $boxes,
                                                           $row->box_id ?? '',
                                                           'id',
                                                           'unique_number') }}
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <div class="form-group input-group-sm">
                                    <label class="control-label">Сумма:</label>
                                    <input type="hidden" name="send_info_id[]" value="{{ $row->send_info_id }}">
                                    <input class="form-control" name="repayment_amount[]"
                                           type="number" required="required"
                                           value="{{ -1 * $row->amount }}" disabled/>
                                </div>
                            </div>
                        </div>
                    @endforeach
                        @endif
                </div>
                </div>
            </div>

            <br>
        @endforeach
        @endif

        <div class="form-group row">
            <textarea class="form-control" rows="3" name="note" id="note" placeholder="Оставьте комментарий...">{{ old('note') ?? $model->note ?? '' }}</textarea>
        </div>

    </div>
    @cannot('editRepayment', $model)
        </fieldset>
    @endcannot

    <div class="row">
        <div class="col-lg-12 panel panel-default panel-body">
            <div class="div-left">
                @cannot('editRepayment', $model)
                    <fieldset disabled>
                @endcannot
                <button type="submit" name="status" formaction="{{ route('send.store') }}" id="close" value="5" class="btn btn-primary">Выполнено</button>
                <button type="submit" name="status" formaction="{{ route('send.store') }}" id="payback" value="4" class="btn btn-primary">Возврат</button>
                @cannot('editRepayment', $model)
                    </fieldset>
                @endcannot
            </div>
            <div class="div-right">
                <a href="{{ route('send.index') }}" class="btn btn-primary" role="button">Отмена</a>
            </div>
        </div>
    </div>

</form>
