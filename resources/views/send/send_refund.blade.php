<form role="form" action="{{ route('send.store') }}" method="post" id="send_cash_payment">
    {{ csrf_field() }}
    {{ Form::hiddenInput('id', 'id', old('id') ?? $model->id ?? '') }}

    @cannot('editRefund', $model)
        <fieldset disabled>
    @endcannot
    @include('components.payment', ['totalAmount' => ($paybackAmount ?? 0),
                                    'currency' =>  'RUB',
                                    'amountData' => $paybackData ?? '',
                                    'debts' => $refundDebts ?? '',
                                    'sign' => 'refund'])

    @cannot('editRefund', $model)
        </fieldset>
    @endcannot
    <div class="col-lg-12 panel panel-default panel-body">
        <div class="div-left">
            <button type="submit" name="status" value="6" class="btn btn-primary"
            @cannot('editRefund', $model) disabled @endcannot>Завершить</button>
        </div>
        <div class="div-right">
            <a href="{{ route('send.index') }}" class="btn btn-primary" role="button">Отмена</a>
        </div>
    </div>

</form>

