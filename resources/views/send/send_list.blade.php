@extends('layouts.app')

@section('title', 'Отправка')

@section('content')

    @can('addRequest', \App\Models\Operations\Send::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('send.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    @can('viewList', \App\Models\Operations\Send::class)
    <div class="row">
        <div class="col-lg-12">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Дата</th>
                    <th>Вид услуги</th>
                    <th>Сумма</th>
                    <th>Валюта</th>
                    <th>Статус</th>
                    <th>Отделение</th>
                    <th>Автор</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($models as $send)
                    @can('viewItem', $send)
                        <tr id="row{{ $send->id }}">
                            <td><a href="{{ route('send.edit', ['id' => $send->id, 'status' => $send->status->id]) }}">Открыть</a></td>
                            <td>{{ $send->id }}</td>
                            <td>{{ $send->create_date_time }}</td>
                            <td>{{ $send->serviceType->descr }}</td>
                            <td>{{ ($send->amount ?? 0) + ($send->commission ?? 0) }}</td>
                            <td>{{ $send->currency->descr }}</td>
                            <td>{{ $send->status->descr }}</td>
                            <td>{{ $send->office->item }}</td>
                            <td>{{ $send->user->last_name }}</td>
                        </tr>
                    @endcan
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    @endcan
@endsection