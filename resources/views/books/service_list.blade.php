@extends('layouts.app')

@section('title', 'Услуги')

@section('content')

    @can('write', \App\Models\Books\Service::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('service.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-10">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Категория</th>
                    <th>Услуга</th>
                    <th>Вид</th>
                    <th>В архиве?</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($services as $service)
                    <tr id="row{{ $service->id }}">
                        <td><a href="{{ route('service.edit', ['id' => $service->id] ) }}">Открыть</a></td>
                        <td>{{ $service->id }}</td>
                        <td>{{ $service->category->descr }}</td>
                        <td>{{ $service->descr }}</td>
                        <td>
                            @foreach($service->types as $type)
                                {{ $type->descr . '; ' }}
                            @endforeach
                        </td>
                        <td>{{ ( $service->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ url('service/' . $service->id) }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\Service::class)
                                        disabled
                                        @endcannot
                                >
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection