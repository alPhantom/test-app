@extends('layouts.app')

@section('title', 'Города')

@section('content')

    @can('write', \App\Models\Books\City::class)
    <div class="row">
        <div class="col-lg-12">
            <p>
                <a href="{{ route('city.create') }}" class="btn btn-primary" role="button">Добавить</a>
            </p>
        </div>
    </div>
    @endcan

    <div class="row">
        <div class="col-lg-6">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Город</th>
                    <th>В архиве?</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($cities as $city)
                    <tr id="row{{ $city->id }}">
                        <td><a href="{{ route('city.edit', ['id' => $city->id] ) }}">Открыть</a></td>
                        <td>{{ $city->id }}</td>
                        <td>{{ $city->descr }}</td>
                        <td>{{ ( $city->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ url('city/' . $city->id) }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\City::class)
                                        disabled
                                        @endcannot
                                >
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection