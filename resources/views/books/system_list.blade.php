@extends('layouts.app')

@section('title', 'Системы')

@section('content')

    @can('write', \App\Models\Books\System::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('system.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-10">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Категория</th>
                    <th>Система</th>
                    <th>В архиве?</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($systems as $system)
                    <tr id="row{{ $system->id }}">
                        <td><a href="{{ route('system.edit', ['id' => $system->id] ) }}">Открыть</a></td>
                        <td>{{ $system->id }}</td>
                        <td>{{ $system->category->descr }}</td>
                        <td>{{ $system->descr }}</td>
                        <td>{{ ( $system->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            Удалить
                            <form action="{{ route('system.destroy', ['id' => $system->id]) }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\System::class)
                                            disabled
                                        @endcannot>
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection