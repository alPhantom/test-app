@extends('layouts.app')

@section('title', 'Отделения')

@section('content')

    @can('write', \App\Models\Books\Office::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('office.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-12">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Номер</th>
                    <th>Группа</th>
                    <th>Группа тарифов</th>
                    <th>Город</th>
                    <th>Адрес</th>
                    <th>Телефон</th>
                    <th>UAH</th>
                    <th>USD</th>
                    <th>EUR</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($offices as $office)
                    <tr id="row{{ $office->id }}">
                        <td><a href="{{ route('office.edit', ['id' => $office->id] ) }}">Открыть</a></td>
                        <td>{{ $office->id }}</td>
                        <td>{{ $office->item }}</td>
                        <td>{{ $office->officegroup->descr }}</td>
                        <td>{{ $office->tariffgroup->descr }}</td>
                        <td>{{ $office->city->descr }}</td>
                        <td>{{ $office->address }}</td>
                        <td>{{ $office->vodafone }}</td>
                        <td>{{ round( $office->border_uah, 2 ) }}</td>
                        <td>{{ round( $office->border_usd, 2 ) }}</td>
                        <td>{{ round( $office->border_eur, 2 ) }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ url('office/' . $office->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\Office::class)
                                        disabled
                                        @endcannot
                                >
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection