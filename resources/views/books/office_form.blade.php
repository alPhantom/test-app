@extends('layouts.app')

@section('title', 'Отделения Добавление/Редактирование')

@section('content')

    <form role="form" action="{{ route('office.store') }}" method="POST">
        {{ csrf_field() }}


        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @cannot('write', \App\Models\Books\Office::class) <fieldset disabled> @endcannot
                            <div class="form-group invisible">
                                <input
                                        type="hidden"
                                        name="id"
                                        id="id"
                                        value="{{ $office->id or '' }}"
                                >
                            </div>
                            <div class="form-group input-group-sm required">
                                <label class="control-label" for="item">Номер:</label>
                                <input
                                        type="number"
                                        class="form-control"
                                        name="item"
                                        id="item"
                                        min="1"
                                        max="9999"
                                        step=1
                                        required="required"
                                        value="{{ $office->item or '' }}"
                                >
                            </div>
                            <div class="form-group input-group-sm">
                                <label class="control-label" for="descr">Наименование:</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        name="descr"
                                        id="descr"
                                        maxlength="80"
                                        value="{{ $office->descr or '' }}"
                                >
                            </div>
                            <div class="form-group required">
                                <label class="control-label" for="group_id">Группа отделения:</label>
                                <select class="form-control" name="group_id" id="group_id" required >
                                    <option value="">Выберите из списка</option>
                                    @foreach( $officeGroups as $officeGroup )
                                        <option value="{{ $officeGroup->id }}"
                                                @if (!empty ($office->group_id) && $officeGroup->id === $office->group_id) selected @endif>
                                            {{ $officeGroup->descr }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            @cannot('write', \App\Models\Books\Office::class) </fieldset> @endcannot
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->

                <div class="panel panel-default">
                    <div class="panel-body">
                        @cannot('write', \App\Models\Books\Office::class) <fieldset disabled> @endcannot
                            <div class="form-group required">
                                <label class="control-label" for="city_id">Город:</label>
                                <select class="form-control" name="city_id" id="city_id" required >
                                    <option value="">Выберите из списка</option>
                                    @foreach( $cities as $city )
                                        <option value="{{ $city->id }}"
                                                @if (!empty ($office->city_id) && $city->id === $office->city_id) selected @endif>
                                            {{ $city->descr }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group input-group-sm">
                                <label class="control-label" for="address">Адрес:</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        name="address"
                                        id="address"
                                        maxlength="160"
                                        value="{{ $office->address or '' }}"
                                >
                            </div>
                            <div class="form-group input-group-sm">
                                <label class="control-label" for="guide">Ориентир:</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        name="guide"
                                        id="guide"
                                        maxlength="160"
                                        value="{{ $office->guide or '' }}"
                                >
                            </div>
                            <div class="form-group input-group-sm">
                                <label class="control-label" for="vodafone">Телефон водафон:</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        name="vodafone"
                                        id="vodafone"
                                        maxlength="12"
                                        value="{{ $office->vodafone or '' }}"
                                >
                            </div>
                            <div class="form-group input-group-sm">
                                <label class="control-label" for="lugacom">Телефон лугаком:</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        name="lugacom"
                                        id="lugacom"
                                        maxlength="12"
                                        value="{{ $office->lugacom or '' }}"
                                >
                            </div>
                            @cannot('write', \App\Models\Books\Office::class) </fieldset> @endcannot
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->



            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @cannot('write', \App\Models\Books\Office::class) <fieldset disabled> @endcannot
                            <div class="form-group input-group-sm required">
                                <label class="control-label" for="border_uah">Лимит UAH:</label>
                                <input
                                        type="number"
                                        class="form-control"
                                        name="border_uah"
                                        id="border_uah"
                                        min="0"
                                        step=0.01
                                        required="required"
                                        value="{{ $office->border_uah or '' }}"
                                >
                            </div>
                            <div class="form-group input-group-sm required">
                                <label class="control-label" for="border_usd">Лимит USD:</label>
                                <input
                                        type="number"
                                        class="form-control"
                                        name="border_usd"
                                        id="border_usd"
                                        min="0"
                                        step=0.01
                                        required="required"
                                        value="{{ $office->border_usd or '' }}"
                                >
                            </div>
                            <div class="form-group input-group-sm required">
                                <label class="control-label" for="border_eur">Лимит USD:</label>
                                <input
                                        type="number"
                                        class="form-control"
                                        name="border_eur"
                                        id="border_eur"
                                        min="0"
                                        step=0.01
                                        required="required"
                                        value="{{ $office->border_eur or '' }}"
                                >
                            </div>
                            @cannot('write', \App\Models\Books\Office::class) </fieldset> @endcannot
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        @cannot('write', \App\Models\Books\Office::class) <fieldset disabled> @endcannot
                        <div class="form-group required">
                            <label class="control-label" for="tariff_group_id">Группа тарифов:</label>
                            <select class="form-control" name="tariff_group_id" id="tariff_group_id" required >
                                <option value="">Выберите из списка</option>
                                @foreach( $tariffGroups as $tariffGroup )
                                    <option value="{{ $tariffGroup->id }}"
                                            @if (!empty ($office->tariff_group_id) && $tariffGroup->id === $office->tariff_group_id) selected @endif>
                                        {{ $tariffGroup->descr }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        @cannot('write', \App\Models\Books\Office::class) </fieldset> @endcannot
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->

            </div>
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <button type="submit" name="submit" value="save" class="btn btn-primary"
                                @cannot('write', \App\Models\Books\Office::class) disabled @endcannot>Сохранить</button>
                        <a href="{{ route('office.index') }}" class="btn btn-primary" role="button">Отмена</a>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

    </form>
@endsection