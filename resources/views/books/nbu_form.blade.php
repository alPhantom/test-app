@extends('layouts.app')

@section('title', 'Курсы НБУ')

@section('content')

    <form role="form" action="{{ route('nbu.store') }}" method="POST">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ csrf_field() }}
                        @cannot('write', \App\Models\Books\Nbu::class) <fieldset disabled> @endcannot
                        <div class="form-group invisible">
                            <input
                                    type="hidden"
                                    name="id"
                                    id="id"
                                    value="{{ $nbu->id or '' }}"
                            >
                        </div>
                        <div class="form-group input-group-sm required">
                            <label class="control-label" for="rate_uah">Гривна:</label>
                            <input
                                    type="number"
                                    class="form-control"
                                    name="rate_uah"
                                    id="rate_uah"
                                    min="0"
                                    step=0.01
                                    required="required"
                                    value="{{ $nbu->rate_uah or '' }}"
                            >
                        </div>
                        <div class="form-group input-group-sm required">
                            <label class="control-label" for="rate_usd">Доллар:</label>
                            <input
                                    type="number"
                                    class="form-control"
                                    name="rate_usd"
                                    id="rate_usd"
                                    min="0"
                                    step=0.01
                                    required="required"
                                    value="{{ $nbu->rate_usd or '' }}"
                            >
                        </div>
                        <div class="form-group input-group-sm required">
                            <label class="control-label" for="rate_eur">Евро:</label>
                            <input
                                    type="number"
                                    class="form-control"
                                    name="rate_eur"
                                    id="rate_eur"
                                    min="0"
                                    step=0.01
                                    required="required"
                                    value="{{ $nbu->rate_eur or '' }}"
                            >
                        </div>
                        <div class="form-group">
                            Поля, отмеченные звездочкой, обязательны для заполнения.
                        </div>
                    @cannot('write', \App\Models\Books\Nbu::class) </fieldset> @endcannot
                    </div>
                    <!-- /.panel-body -->
                    <div class="panel-footer">
                        <button type="submit" name="submit" value="save" class="btn btn-primary"
                        @cannot('write', \App\Models\Books\Nbu::class) disabled @endcannot>Сохранить</button>
                    </div>
                    <!-- /.panel-footer -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </form>

@endsection