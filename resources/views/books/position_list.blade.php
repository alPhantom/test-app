@extends('layouts.app')

@section('title', 'Должности')

@section('content')

    @can('write', \App\Models\Books\Position::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('position.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-8">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Название</th>
                    <th>В архиве?</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($positions as $position)
                    <tr id="row{{ $position->id }}">
                        <td><a href="{{ route('position.edit', ['id' => $position->id] ) }}">Открыть</a></td>
                        <td>{{ $position->id }}</td>
                        <td>{{ $position->descr }}</td>
                        <td>{{ ( $position->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ route('position.destroy', ['id' => $position->id]) }}" method="POST">
                                {{ csrf_field() }}

                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\Position::class)
                                        disabled
                                        @endcannot>
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection