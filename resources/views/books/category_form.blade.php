@extends('layouts.app')

@section('title', 'Категория Добавление/Редактирование')

@section('content')

    <form role="form" action="{{ route('category.store') }}" method="POST">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ csrf_field() }}
                        @cannot('write', \App\Models\Books\Category::class) <fieldset disabled> @endcannot
                            <div class="form-group invisible">
                                <input
                                        type="hidden"
                                        name="id"
                                        id="id"
                                        value="{{ $category->id or '' }}"
                                >
                            </div>
                            <div class="form-group input-group-sm required">
                                <label class="control-label" for="descr">Наименование категории:</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        name="descr"
                                        id="descr"
                                        required="required"
                                        maxlength="80"
                                        autofocus
                                        value="{{ $category->descr or '' }}"
                                >
                            </div>
                            <div class="form-group">
                                Поля, отмеченные звездочкой, обязательны для заполнения.
                            </div>
                            @cannot('write', \App\Models\Books\Category::class) </fieldset> @endcannot
                    </div>
                    <!-- /.panel-body -->
                    <div class="panel-footer">
                        <button type="submit" name="submit" value="save" class="btn btn-primary"
                                @cannot('write', \App\Models\Books\Category::class) disabled @endcannot>Сохранить</button>
                        <a href="{{ route('category.index') }}" class="btn btn-primary" role="button">Отмена</a>
                    </div>
                    <!-- /.panel-footer -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </form>
@endsection