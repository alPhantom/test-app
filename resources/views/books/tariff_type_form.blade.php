@extends('layouts.app')

@section('title', 'Типы тарифов Добавление/Редактирование')

@section('content')

    <form role="form" action="{{ route('tariff_type.store') }}" method="POST">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ csrf_field() }}
                        @cannot('write', \App\Models\Books\TariffType::class) <fieldset disabled> @endcannot
                        <div class="form-group invisible">
                            <input
                                    type="hidden"
                                    name="id"
                                    id="id"
                                    value="{{ $tariffType->id or '' }}"
                            >
                        </div>
                        <div class="form-group input-group-sm required">
                            <label class="control-label" for="descr">Название типа тарифа:</label>
                            <input
                                    type="text"
                                    class="form-control"
                                    name="descr"
                                    id="descr"
                                    required="required"
                                    maxlength="80"
                                    autofocus
                                    value="{{ $tariffType->descr or '' }}"
                            >
                        </div>
                        <div class="form-group required">
                            <label class="control-label" for="percent_unit">Процент или Денежная единица:</label>
                            <select class="form-control" name="percent_unit" id="percent_unit" required >
                                <option value="">Выберите из списка</option>
                                @foreach ( $percentUnit as $value => $name )
                                    <option @if ($selectedPercentUnit === $value) selected @endif value="{{ $value }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group required">
                            <label class="control-label" for="plus_minus">Увеличивает или уменьшает сумму операции::</label>
                            <select class="form-control" name="plus_minus" id="plus_minus" required >
                                <option value="">Выберите из списка</option>
                                @foreach ( $plusMinus as $value => $name )
                                    <option @if ($selectedPlusMinus === $value) selected @endif value="{{ $value }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="checkbox">
                            <label class="form-check-label">
                                <input
                                        type="checkbox"
                                        class="form-check-input"
                                        name="is_archive"
                                        id="is_archive"
                                        @if ( ! empty( $tariffType->is_archive ) && ($tariffType->is_archive === 1 ) )
                                        checked
                                        @endif
                                        @cannot('delete',\App\Models\Books\Marketing::class) disabled @endcannot
                                >
                                Архив
                            </label>
                        </div>
                        <div class="form-group">
                            Поля, отмеченные звездочкой, обязательны для заполнения.
                        </div>
                        @cannot('write', \App\Models\Books\TariffType::class) </fieldset> @endcannot
                    </div>
                    <!-- /.panel-body -->
                    <div class="panel-footer">
                        <button type="submit" name="submit" value="save" class="btn btn-primary"
                        @cannot('write', \App\Models\Books\TariffType::class) disabled @endcannot>Сохранить</button>
                        <a href="{{ route('tariff_type.index') }}" class="btn btn-primary" role="button">Отмена</a>
                    </div>
                    <!-- /.panel-footer -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </form>
@endsection