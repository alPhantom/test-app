@extends('layouts.app')

@section('title', 'Группы тарифов')

@section('content')

    @can('write', \App\Models\Books\TariffGroup::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('tariff_group.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-6">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Имя группы</th>
                    <th>В архиве?</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($tariffGroups as $tariffGroup)
                    <tr id="row{{ $tariffGroup->id }}">
                        <td><a href="{{ route('tariff_group.edit', ['id' => $tariffGroup->id] ) }}">Открыть</a></td>
                        <td>{{ $tariffGroup->id }}</td>
                        <td>{{ $tariffGroup->descr }}</td>
                        <td>{{ ( $tariffGroup->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ url('tariff_group/' . $tariffGroup->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\TariffGroup::class)
                                        disabled
                                        @endcannot
                                >
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection