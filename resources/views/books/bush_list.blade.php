@extends('layouts.app')

@section('title', 'Кусты')

@section('content')

    @can('write', \App\Models\Books\Bush::class)
    <div class="row">
        <div class="col-lg-12">
            <p>
                <a href="{{ route('bush.create') }}" class="btn btn-primary" role="button">Добавить</a>
            </p>
        </div>
    </div>
    @endcan

    <div class="row">
        <div class="col-lg-6">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Куст</th>
                    <th>В архиве?</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($bushes as $bush)
                    <tr id="row{{ $bush->id }}">
                        <td><a href="{{ route('bush.edit', ['id' => $bush->id] ) }}">Открыть</a></td>
                        <td>{{ $bush->id }}</td>
                        <td>{{ $bush->descr }}</td>
                        <td>{{ ( $bush->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ url('bush/' . $bush->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\Bush::class)
                                        disabled
                                        @endcannot>
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection