@extends('layouts.app')

@section('title', 'Должности - Добавление/Редактирование')

@section('content')

    <form role="form" action="{{ route('position.store') }}" method="POST">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ csrf_field() }}
                        @cannot('write', \App\Models\Books\Position::class) <fieldset disabled> @endcannot
                            <div class="form-group invisible">
                                <input
                                        type="hidden"
                                        name="id"
                                        id="id"
                                        value="{{ $position->id or '' }}"
                                >
                            </div>
                            <div class="form-group input-group-sm required">
                                <label class="control-label" for="descr">Название:</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        name="descr"
                                        id="descr"
                                        required="required"
                                        maxlength="80"
                                        autofocus
                                        value="{{ $position->descr or '' }}"
                                >
                            </div>
                            <div class="checkbox">
                                <label class="form-check-label">
                                    <input
                                            type="checkbox"
                                            class="form-check-input"
                                            name="is_archive"
                                            id="is_archive"
                                            @if ( ! empty( $position->is_archive ) && ($position->is_archive === 1 ) )
                                            checked
                                            @endif
                                    >
                                    Архив
                                </label>
                            </div>
                            <div class="form-group">
                                Поля, отмеченные звездочкой, обязательны для заполнения.
                            </div>
                            @cannot('write', \App\Models\Books\Position::class) </fieldset> @endcannot
                    </div>
                    <!-- /.panel-body -->
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-lg-6 col-md-6" align="left">
                            <button type="submit" name="submit" value="save" class="btn btn-primary"
                                    @cannot('write', \App\Models\Books\Position::class) disabled @endcannot>Сохранить</button>
                            <a href="{{ route('position.index') }}" class="btn btn-primary" role="button">Отмена</a>
                        </div>
                        <div class="col-lg-6 col-md-6" align="right">
                            <button type="submit"
                                    formaction="@if(!empty($position)) {{ route('position.destroy', ['id' => $position->id])}} @endif"
                                    id="button_delete" class="btn btn-danger"
                                    @cannot('delete',\App\Models\Books\Position::class)
                                    disabled
                                    @else @if(empty($position->id)) disabled @endif
                                    @endcannot>
                                Удалить
                            </button>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
    </form>
@endsection