@extends('layouts.app')

@section('title', 'Тарифы')

@section('content')

    @can('write', \App\Models\Books\Tariff::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('tariff.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-12">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Услуга</th>
                    <th>Валюта</th>
                    <th>Тип тарифа</th>
                    <th>Тариф</th>
                    <th>Диапазон</th>
                    {{--<th>Сумма макс</th>--}}
                    <th>Дата С</th>
                    <th>Дата По</th>
                    <th>Группы</th>
                    {{--<th>В архиве?</th>--}}
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($tariffs as $tariff)
                    <tr id="row{{ $tariff->id }}">
                        <td><a href="{{ route('tariff.edit', ['id' => $tariff->id] ) }}">Открыть</a></td>
                        <td>{{ $tariff->id }}</td>
                        <td>{{ $tariff->serviceType->service->descr .' ' . $tariff->serviceType->type->descr }}</td>
                        <td>{{ $tariff->currency->descr }}</td>
                        <td>{{ $tariff->tariffType->descr }}</td>
                        <td>{{ $tariff->tariff }}</td>
                        <td>{{ $tariff->sum_min . ' - ' . $tariff->sum_max }}</td>
                        {{--<td>{{ $tariff->sum_max }}</td>--}}
                        <td>{{ $tariff->date_begin }}</td>
                        <td>{{ $tariff->date_end }}</td>
                        <td>
                            @foreach($tariff->groups as $group)
                                {{ $group->descr . '; ' }}
                            @endforeach
                        </td>
                        {{--<td>{{ ( $tariff->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>--}}
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ route('tariff.destroy', ['id' => $tariff->id]) }}" method="POST">
                                {{ csrf_field() }}

                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\Tariff::class)
                                        disabled
                                        @endcannot>
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection