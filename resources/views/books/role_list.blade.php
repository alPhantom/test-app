@extends('layouts.app')

@section('title', 'Роли')

@section('content')

    @can('write', \App\Models\Role::class)
    <div class="row">
        <div class="col-lg-12">
            <p>
                <a href="{{ route('role.create') }}" class="btn btn-primary" role="button">Добавить</a>
            </p>
        </div>
    </div>
    @endcan

    <div class="row">
        <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
            <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Роль</th>
                    <th>Комментарий</th>
                    <th>Удалить</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($roles as $role)
                <tr id="row{{ $role->id }}">
                    <td><a href="{{ route('role.edit', ['id' => $role->id] ) }}">Открыть</a></td>
                    <td>{{ $role->id }}</td>
                    <td>{{ $role->descr }}</td>
                    <td>{{ $role->note }}</td>
                    <!-- Кнопка Удалить -->
                    <td>
                        <form action="{{ route('role.delete', ['id' => $role->id] ) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-link">
                                <i class="fa fa-trash"></i> Удалить
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
