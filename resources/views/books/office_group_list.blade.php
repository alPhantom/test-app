@extends('layouts.app')

@section('title', 'Группа для отделений')

@section('content')

    @can('write', \App\Models\Books\OfficeGroup::class)
    <div class="row">
        <div class="col-lg-12">
            <p>
                <a href="{{ route('office_group.create') }}" class="btn btn-primary" role="button">Добавить</a>
            </p>
        </div>
    </div>
    @endcan

    <div class="row">
        <div class="col-lg-6">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Название</th>
                    <th>В архиве?</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($officeGroups as $officeGroup)
                    <tr id="row{{ $officeGroup->id }}">
                        <td><a href="{{ route('office_group.edit', ['id' => $officeGroup->id] ) }}">Открыть</a></td>
                        <td>{{ $officeGroup->id }}</td>
                        <td>{{ $officeGroup->descr }}</td>
                        <td>{{ ( $officeGroup->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ url('office_group/' . $officeGroup->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\OfficeGroup::class)
                                        disabled
                                        @endcannot
                                >
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection