@extends('layouts.app')

@section('title', 'Страны')

@section('content')

    @can('write', \App\Models\Books\Country::class)
    <div class="row">
        <div class="col-lg-12">
            <p>
                <a href="{{ route('country.create') }}" class="btn btn-primary" role="button">Добавить</a>
            </p>
        </div>
    </div>
    @endcan

    <div class="row">
        <div class="col-lg-6">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Страна</th>
                    <th>В архиве?</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($countries as $country)
                    <tr id="row{{ $country->id }}">
                        <td><a href="{{ route('country.edit', ['id' => $country->id] ) }}">Открыть</a></td>
                        <td>{{ $country->id }}</td>
                        <td>{{ $country->descr }}</td>
                        <td>{{ ( $country->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ url('country/' . $country->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\Country::class)
                                        disabled
                                        @endcannot
                                >
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection