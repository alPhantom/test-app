@extends('layouts.app')

@section('title', 'Статьи ДДС')

@section('content')

    @can('write', \App\Models\Books\SectionCashflow::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('sectioncf.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-9">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Название</th>
                    <th>Транзитный</th>
                    <th>В архиве</th>
                </thead>
                <tbody>

                @foreach ($sections as $section)
                    <tr id="row{{ $section->id }}">
                        <td><a href="{{ route('sectioncf.edit', ['id' => $section->id] ) }}">Открыть</a></td>
                        <td>{{ $section->section_code }}</td>
                        <td>{{ $section->descr }}</td>
                        <td>{{ $section->transit == 1 ? 'Да' : 'Нет' }}</td>
                        <td>{{ $section->is_archive == 1 ? 'Да' : 'Нет' }}</td>

                    {{--<td>{{ ( $tariff->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>--}}
                    <!-- Кнопка Удалить -->
                        {{--<td>--}}
                            {{--<form action="{{ route('sectioncf.destroy', ['id' => $section->id]) }}" method="POST">--}}
                                {{--{{ csrf_field() }}--}}

                                {{--<button type="submit" class="btn btn-link"--}}
                                        {{--@cannot('delete', \App\Models\Books\SectionCashflow::class)--}}
                                        {{--disabled--}}
                                        {{--@endcannot>--}}
                                    {{--<i class="fa fa-trash"></i> Удалить--}}
                                {{--</button>--}}
                            {{--</form>--}}
                        {{--</td>--}}
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection