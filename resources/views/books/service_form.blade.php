@extends('layouts.app')

@section('js') <script src="{{ asset('js/core/dropdown_checkbox.js') }}" defer></script> @endsection

@section('css') <link href="{{ asset('css/dropdown_checkbox.css') }}" rel="stylesheet"> @endsection

@section('title', 'Услуги - Добавление/Редактирование')

@section('content')

    <form role="form" action="{{ route('service.store') }}" method="POST">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ csrf_field() }}
                        @cannot('write', \App\Models\Books\Service::class) <fieldset disabled> @endcannot

                            {{Form::hiddenInput('id', 'id', $service->id ?? '')}}

                            <div class="form-group input-group-sm">
                                {{Form::dropdownCheckbox(
                                    'type', 'Тип', '',
                                     $types,
                                     $selectedTypes ?? '',
                                     $selectedIds ?? '',
                                     'id', 'descr' )}}
                            </div>

                            <div class="form-group input-group-sm required">
                                <label class="control-label" for="category">Категория:</label>
                                {{Form::selectList('category',
                                                    'category_id',
                                                    'required' ,
                                                    $categories,
                                                    $service->category_id ?? '',
                                                    'id',
                                                    'descr'
                                )}}

                            </div>

                            {{Form::textInput('descr','descr', $service->descr ?? '',
                                'Наименование сервиса', 'required', 'required maxlength=80 autofocus')}}

                            {{Form::isArchive($service->is_archive ?? '')}}

                            <div class="form-group">
                                Поля, отмеченные звездочкой, обязательны для заполнения.
                            </div>
                            @cannot('write', \App\Models\Books\Service::class) </fieldset> @endcannot
                    </div>
                </div>

            {{Form::saveCancelDelete( Gate::denies('write', \App\Models\Books\Service::class),
                                        'service.index', $service->id ?? '',
                                        Gate::denies('delete', \App\Models\Books\Service::class),
                                        'service.destroy')}}
            </div>
            <!-- /.col-lg-6 -->
        </div>
    </form>
@endsection