@extends('layouts.app')

@section('js') <script src="{{ asset('js/core/dropdown_checkbox.js') }}" defer></script> @endsection

@section('css') <link href="{{ asset('css/dropdown_checkbox.css') }}" rel="stylesheet"> @endsection

@section('title', 'Тарифы - Добавление/Редактирование')

@section('content')

    <form role="form" action="{{ route('tariff.store') }}" method="POST">
        @cannot('write', \App\Models\Books\Tariff::class) <fieldset disabled> @endcannot
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ csrf_field() }}
                            {{Form::hiddenInput('id', 'id', $tariff->id ?? '')}}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group input-group-sm required">
                                        <label class="control-label" for="service_type_id">Услуга:</label>
                                        <select id="service_type_id" name="service_type_id" class="form-control" required>
                                            <option value="">Выберите из списка</option>
                                            @foreach($services as $item)
                                                <option value="{{$item->id}}"
                                                        @if ((!empty(old('service_type_id')) && $item->id == old('service_type_id')) ||
                                                            (!empty ($tariff->service_type_id) && $item->id === $tariff->service_type_id))
                                                        selected
                                                        @endif
                                                >
                                                    {{$item->service->descr .' ' . $item->type->descr}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group input-group-sm required">
                                        <label class="control-label" for="currency">Валюта:</label>
                                        {{Form::selectList('currency',
                                                        'currency_id',
                                                        'required' ,
                                                        $currencies,
                                                        old('currency_id') ?? $tariff->currency_id ?? '',
                                                        'id',
                                                        'descr'
                                        )}}

                                    </div>
                                </div>
                            </div>
                            <div class="form-group input-group-sm required row">
                                <div class="col-lg-6">
                                    <label class="control-label" for="tariff">Значение:</label>

                                    <input
                                            type="number"
                                            id="tariff"
                                            class="form-control"
                                            name="tariff"
                                            value="{{ old('tariff') ?? $tariff->tariff ?? ''}}"
                                            autocomplete="false"
                                            required/>
                                </div>
                                <div class="col-lg-6">
                                    <label class="control-label" for="currency">Тип:</label>
                                    {{Form::selectList('tariff_type',
                                                'tariff_type_id',
                                                'required' ,
                                                 $tariffTypes,
                                                 old('tariff_type_id') ?? $tariff->tariff_type_id ?? '',
                                                'id',
                                                'descr'
                                )}}
                                </div>

                            </div>

                        <div class="form-group input-group-sm required row"></div>
                            <div class="form-group">
                                <textarea class="form-control" name="note" id="note" rows="3" placeholder="Комментарий.."></textarea>
                            </div>
                    </div>
                </div>
            </div> <!-- /.col-lg-6 -->
            <div class="col-lg-6 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Ограничения
                    </div>
                    <div class="panel-body">
                        <div class="form-group input-group-sm required row">
                            <div class="col-lg-3 col-md-3">
                                <label class="control-label">Диапазон:</label>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <input
                                    type="number"
                                    class="form-control"
                                    id="sum_min"
                                    name="sum_min"
                                    value="{{ old('sum_min') ?? $tariff->sum_min ?? ''}}"
                                    autocomplete="false"
                                    required
                                />
                            </div>

                            <div class="col-lg-1 col-md-1">
                                <label style="padding-top: 5px;"> — </label>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <input
                                        type="number"
                                        class="form-control"
                                        id="sum_max"
                                        name="sum_max"
                                        value="{{ old('sum_max') ?? $tariff->sum_max ?? ''}}"
                                        autocomplete="false"
                                />
                            </div>
                        </div>

                        <div class="form-group input-group-sm required row">
                            <div class="col-lg-3 col-md-3">
                                <label class="control-label">Период c:</label>
                            </div>
                            <div class="col-lg-9 col-md-9">
                                <input
                                        type="datetime-local"
                                        class="form-control"
                                        id="date_begin"
                                        name="date_begin"
                                        min="2015-01-01"
                                        max="{{ date('Y-m-d') }}"
                                        value="{{ old('date_begin') ?? date( 'Y-m-d\TH:i',
                                                            strtotime( $tariff->date_begin ?? date('Y-m-d'))) }}"
                                        autocomplete="false"
                                        required
                                />
                            </div>
                        </div>

                        <div class="form-group input-group-sm row">
                            <div class="col-lg-3 col-md-3">
                                <label class="control-label">Период по:</label>
                            </div>
                            <div class="col-lg-9 col-md-9">
                                <input
                                        type="datetime-local"
                                        class="form-control"
                                        id="date_end"
                                        name="date_end"
                                        value=@if(!empty($tariff->date_end))
                                                   "{{ date( 'Y-m-d\TH:i', strtotime( $tariff->date_end)) }}"
                                               @endif
                                        autocomplete="false"
                                />
                            </div>
                        </div>

                        <div class="form-group input-group-sm">
                            {{Form::dropdownCheckbox(
                                'group', 'Для группы отделений', '',
                                 $groups,
                                 $selectedGroups ?? '',
                                 old('group') ?? $selectedIds ?? '',
                                 'id', 'descr' )}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
            @cannot('write', \App\Models\Books\Tariff::class) </fieldset> @endcannot

            <div class="row">
                <div class="col-lg-12">
                    {{Form::saveCancelDelete( Gate::denies('write', \App\Models\Books\Tariff::class),
                                                'tariff.index', $tariff->id ?? '',
                                                Gate::denies('delete', \App\Models\Books\Tariff::class),
                                                'tariff.destroy')}}
                </div>
            </div>

    </form>
@endsection