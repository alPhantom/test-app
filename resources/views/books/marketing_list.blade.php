@extends('layouts.app')

@section('title', 'Маркетинг')

@section('content')

    @can('write', \App\Models\Books\Marketing::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('marketing.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-8">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Источник привлечения</th>
                    <th>В архиве?</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($marketings as $marketing)
                    <tr id="row{{ $marketing->id }}">
                        <td><a href="{{ route('marketing.edit', ['id' => $marketing->id] ) }}">Открыть</a></td>
                        <td>{{ $marketing->id }}</td>
                        <td>{{ $marketing->attraction }}</td>
                        <td>{{ ( $marketing->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ url('marketing/' . $marketing->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\Marketing::class)
                                        disabled
                                        @endcannot
                                >
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection