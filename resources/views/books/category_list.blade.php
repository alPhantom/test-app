@extends('layouts.app')

@section('title', 'Категории')

@section('content')

    @can('write', \App\Models\Books\Category::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('category.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-8">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Название категории</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($categories as $category)
                    <tr id="row{{ $category->id }}">
                        <td><a href="{{ route('category.edit', ['id' => $category->id] ) }}">Открыть</a></td>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->descr }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ url('category/' . $category->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\Category::class)
                                        disabled
                                        @endcannot
                                >
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection