@extends('layouts.app')

@section('title', 'Города Добавление/Редактирование')

@section('content')

    <form role="form" action="{{ route('city.store') }}" method="POST">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ csrf_field() }}
                        @cannot('write', \App\Models\Books\City::class) <fieldset disabled> @endcannot

                            {{Form::hiddenInput('id', 'id', $city->id ?? '')}}

                            {{Form::textInput('descr','descr', $city->descr ?? '', 'Город', 'required', 'required maxlength=80 autofocus')}}

                            {{Form::isArchive($city->is_archive ?? '')}}

                        <div class="form-group">
                            Поля, отмеченные звездочкой, обязательны для заполнения.
                        </div>
                        @cannot('write', \App\Models\Books\City::class) </fieldset> @endcannot
                    </div>
                </div>

                {{Form::saveCancelDelete( Gate::denies('write', \App\Models\Books\City::class),
                                            'city.index', $city->id ?? '', Gate::denies('delete', \App\Models\Books\City::class),
                                            'city.destroy')}}

            </div>
            <!-- /.col-lg-6 -->
        </div>
    </form>
@endsection