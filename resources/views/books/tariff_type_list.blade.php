@extends('layouts.app')

@section('title', 'Типы тарифов')

@section('content')

    @can('write', \App\Models\Books\TariffType::class)
    <div class="row">
        <div class="col-lg-12">
            <p>
                <a href="{{ route('tariff_type.create') }}" class="btn btn-primary" role="button">Добавить</a>
            </p>
        </div>
    </div>
    @endcan

    <div class="row">
        <div class="col-lg-12">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Имя типа тарифа</th>
                    <th>Проценты/Ден.ед.</th>
                    <th>Плюс/Минус</th>
                    <th>Дата создания</th>
                    <th>В архиве?</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($tariffTypes as $tariffType)
                    <tr id="row{{ $tariffType->id }}">
                        <td><a href="{{ route('tariff_type.edit', ['id' => $tariffType->id] ) }}">Открыть</a></td>
                        <td>{{ $tariffType->id }}</td>
                        <td>{{ $tariffType->descr }}</td>
                        <td>{{ ( $tariffType->percent_unit == 'percent' ) ? 'Процент' : 'Ден.ед.' }}</td>
                        <td>{{ ( $tariffType->plus_minus == 'plus' ) ? 'Плюс' : 'Минус' }}</td>
                        <td>{{ $tariffType->ins_time }}</td>
                        <td>{{ ( $tariffType->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>
                        <!-- Кнопка Удалить -->
                        <td>
                            <form action="{{ url('tariff_type/' . $tariffType->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-link"
                                        @cannot('delete', \App\Models\Books\TariffType::class)
                                        disabled
                                        @endcannot
                                >
                                    <i class="fa fa-trash"></i> Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection