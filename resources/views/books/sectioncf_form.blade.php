@extends('layouts.app')

@section('title', 'Статьи ДДС - Добавление/Редактирование')

@section('content')

    <form role="form" action="{{ route('sectioncf.store') }}" method="POST">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ csrf_field() }}
                        @cannot('write', \App\Models\Books\SectionCashflow::class) <fieldset disabled> @endcannot

                            {{Form::hiddenInput('id', 'id', $section->id ?? '')}}

                            <div class="form-group input-group-sm required">
                                <label class="control-label" for="category">Код:</label>
                                <input
                                        type="number"
                                        name="section_code"
                                        class="form-control"
                                        required
                                        autocomplete="off"
                                        value="{{ old('section_code') ?? $section->section_code ?? '' }}"
                                />
                            </div>

                            <div class="form-group input-group-sm required">
                                <label class="control-label" for="category">Название:</label>
                                <input
                                        type="text"
                                        name="descr"
                                        class="form-control"
                                        required
                                        autocomplete="off"
                                        value="{{ old('descr') ?? $section->descr ?? '' }}"
                                />
                            </div>

                            <div class="form-group input-group-sm">
                                <label class="form-check-label">
                                    <input
                                            type="checkbox"
                                            class="form-check-input"
                                            name="transit"
                                            id="transit"
                                            @if ( ($section->transit ?? 0) === 1 )
                                            checked
                                            @endif
                                    />
                                    Транзитный
                                </label>
                            </div>

                            {{Form::isArchive($section->is_archive ?? '')}}

                            <div class="form-group">
                                <textarea
                                    class="form-control"
                                    rows="3" name="note" id="note"
                                    placeholder="Комментарий...">{{ old('note') ?? $section->note ?? '' }}</textarea>
                            </div>

                            <div class="form-group">
                                Поля, отмеченные звездочкой, обязательны для заполнения.
                            </div>
                            @cannot('write', \App\Models\Books\SectionCashflow::class) </fieldset> @endcannot
                    </div>
                </div>

                {{Form::saveCancel( 'sectioncf.index', Gate::denies('write', \App\Models\Books\SectionCashflow::class))}}
            </div>
            <!-- /.col-lg-6 -->
        </div>
    </form>
@endsection