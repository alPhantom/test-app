
@extends('layouts.app')

@section('js')

    <script src="{{ asset('include/jstree/jstree.min.js') }}" defer></script>
    <script src="{{ asset('js/role.js') }}" defer></script>

@endsection

@section('css')
    <link href="{{ asset('include/jstree/themes/default/style.min.css') }}" rel="stylesheet">
@endsection

@section('title', 'Роли')

@section('content')
    <div class="container col-lg-12 col-md-12 col-sm-12">
        <div class="row">
            <div class="panel panel-danger panel-heading" id="error" hidden></div>
            @cannot('write', \App\Models\Role::class) <fieldset disabled> @endcannot
            <div class="col-lg-6">
                <div class="panel-body">
                    <div class="form-group input-group-sm row required">

                        <div class="col-lg-3 col-md-3">
                            <label class="control-label" for="role">Роль: </label>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <input
                                    id="role"
                                    type="text"
                                    class="form-control"
                                    name="role"
                                    value="{{$role->descr or ''}}"
                            />
                        </div>
                    </div>
                    <div class="form-group input-group-sm row required">

                        <div class="col-lg-3 col-md-3">
                            <label class="control-label" for="note">Описание: </label>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <input
                                    id="note"
                                    type="text"
                                    class="form-control"
                                    name="note"
                                    value="{{$role->note or ''}}"
                            />
                        </div>
                    </div>
                </div>
            </div>
            @cannot('write', \App\Models\Role::class) </fieldset> @endcannot
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="node_tree" class="column categories">
                            <img id="img_load" src="{{ asset('images/loading.gif') }}" alt="" />

                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="panel panel-default" id="description" hidden>
                    <div  class="panel-body">
                        @cannot('write', \App\Models\Role::class) <fieldset disabled> @endcannot
                        <br>

                        <input
                                id="id"
                                type="hidden"
                                class="form-control"
                                name="id"
                                value="{{$role->id or ''}}"
                        />

                        <div id="access_block" class="form-group">
                            <label>Доступ:</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="access" id="none" value="none" checked="">Не доступно
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="access" id="read" value="read">Чтение
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="access" id="write" value="write">Изменение
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="access" id="delete" value="delete">Полный доступ
                                </label>
                            </div>
                        </div>
                        @cannot('write', \App\Models\Role::class) </fieldset> @endcannot
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                <div class="col-lg-6 col-md-6" align="left">
                    <button type="button" id="button_save" class="btn btn-primary">
                        Сохранить
                    </button>
                    <a href="{{route('roles')}}" class="btn btn-primary">Отмена</a>
                </div>
                <div class="col-lg-6 col-md-6" align="right">
                    <form action="delete" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" id="button_delete" class="btn btn-danger"
                                @cannot('delete',\App\Models\Role::class)
                                    disabled
                                    @else @if(empty($role->id)) disabled @endif
                                @endcannot>
                            Удалить
                        </button>
                    </form>
                </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

    </div>
@endsection
