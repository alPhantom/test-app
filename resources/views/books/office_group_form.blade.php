@extends('layouts.app')

@section('title', 'Группа для отделений Добавление/Редактирование')

@section('content')

    <form role="form" action="{{ route('office_group.store') }}" method="POST">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ csrf_field() }}
                        @cannot('write', \App\Models\Books\OfficeGroup::class) <fieldset disabled> @endcannot
                        <div class="form-group invisible">
                            <input
                                    type="hidden"
                                    name="id"
                                    id="id"
                                    value="{{ $officeGroup->id or '' }}"
                            >
                        </div>
                        <div class="form-group input-group-sm required">
                            <label class="control-label" for="descr">Наименование:</label>
                            <input
                                    type="text"
                                    class="form-control"
                                    name="descr"
                                    id="descr"
                                    required="required"
                                    maxlength="80"
                                    autofocus
                                    value="{{ $officeGroup->descr or '' }}"
                            >
                        </div>
                        <div class="checkbox">
                            <label class="form-check-label">
                                <input
                                        type="checkbox"
                                        class="form-check-input"
                                        name="is_archive"
                                        id="is_archive"
                                        @if ( ! empty( $officeGroup->is_archive ) && ($officeGroup->is_archive === 1 ) )
                                        checked
                                        @endif
                                        @cannot('delete',\App\Models\Books\OfficeGroup::class) disabled @endcannot
                                >
                                Архив
                            </label>
                        </div>
                        <div class="form-group">
                            Поля, отмеченные звездочкой, обязательны для заполнения.
                        </div>
                        @cannot('write', \App\Models\Books\OfficeGroup::class) </fieldset> @endcannot
                    </div>
                    <!-- /.panel-body -->
                    <div class="panel-footer">
                        <button type="submit" name="submit" value="save" class="btn btn-primary"
                            @cannot('write', \App\Models\Books\OfficeGroup::class) disabled @endcannot>Сохранить</button>
                        <a href="{{ route('office_group.index') }}" class="btn btn-primary" role="button">Отмена</a>
                    </div>
                    <!-- /.panel-footer -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </form>
@endsection