@extends('layouts.app')

@section('js')

    <script src="{{ asset('include/jstree/jstree.min.js') }}" defer></script>
    <script src="{{ asset('js/app_tree.js') }}" defer></script>

@endsection

@section('css')
    <link href="{{ asset('include/jstree/themes/default/style.min.css') }}" rel="stylesheet">
    <style>
        select{
            font-family: fontAwesome
        }
    </style>

@endsection

@section('content')
    <div class="container col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-3">
                            <button type="button" id="button_create" class="btn btn-primary">
                                <i class="glyphicon glyphicon-plus"></i> Добавить
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="node_tree" class="column categories">
                            <img id="img_load" src="{{ asset('images/loading.gif') }}" alt="" />
                            <div class="panel panel-danger panel-heading" id="error" hidden>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default" id="description" hidden>
                    <div  class="panel-body">
                        <br>
                        {{--<div id="description" hidden>--}}
                        <div class="form-group input-group-sm row" >
                            <div class="col-lg-3">
                                <label class="control-label" for="note">Название: </label>
                            </div>
                            <div class="col-lg-8">
                                <input
                                        id="note"
                                        type="text"
                                        class="form-control"
                                        name="note"
                                />
                            </div>
                        </div>

                        <div class="form-group input-group-sm row">
                            <div class="col-lg-3">
                                <label class="control-label" for="descr">Название(лат.): </label>
                            </div>
                            <div class="col-lg-8">
                                <input
                                        id="descr"
                                        type="text"
                                        class="form-control"
                                        name="descr"
                                />
                            </div>
                        </div>

                        <div class="form-group input-group-sm row">
                            <div class="col-lg-3">
                                <label>
                                    <input type="checkbox" id="show" for="show" value="1"> В меню
                                </label>
                            </div>
                            <div class="col-lg-8">
                                <input
                                        id="menu_descr"
                                        type="text"
                                        class="form-control"
                                        name="menu_descr"
                                />
                            </div>
                        </div>

                        <div class="form-group input-group-sm row">
                            <div class="col-lg-3">
                                <label class="control-label" for="icon">Иконка: </label>
                            </div>
                            <div class="col-lg-8">
                                <select id="icon" class="form-control">

                                    <option value=''></option>

                                    <option value='fa fa-archive fa-fw'>&#xf187; fa-archive</option>
                                    <option value='fa fa-area-chart fa-fw'>&#xf1fe; fa-area-chart</option>
                                    <option value='fa fa-arrow-circle-down fa-fw'>&#xf0ab; fa-arrow-circle-down</option>
                                    <option value='fa fa-arrow-circle-left fa-fw'>&#xf0a8; fa-arrow-circle-left</option>
                                    <option value='fa fa-arrow-circle-right fa-fw'>&#xf0a9; fa-arrow-circle-right</option>
                                    <option value='fa fa-arrow-circle-up fa-fw'>&#xf0aa; fa-arrow-circle-up</option>
                                    <option value='fa fa-asl-interpreting fa-fw'>&#xf2a3; fa-asl-interpreting</option>
                                    <option value='fa fa-assistive-listening-systems fa-fw'>&#xf2a2; fa-assistive-listening-systems</option>
                                    <option value='fa fa-asterisk fa-fw'>&#xf069; fa-asterisk</option>
                                    <option value='fa fa-balance-scale fa-fw'>&#xf24e; fa-balance-scale</option>
                                    <option value='fa fa-ban fa-fw'>&#xf05e; fa-ban</option>
                                    <option value='fa fa-bandcamp fa-fw'>&#xf2d5; fa-bandcamp</option>
                                    <option value='fa fa-bank fa-fw'>&#xf19c; fa-bank</option>
                                    <option value='fa fa-bar-chart fa-fw'>&#xf080; fa-bar-chart</option>
                                    <option value='fa fa-barcode fa-fw'>&#xf02a; fa-barcode</option>
                                    <option value='fa fa-bell fa-fw'>&#xf0f3; fa-bell</option>
                                    <option value='fa fa-birthday-cake fa-fw'>&#xf1fd; fa-birthday-cake</option>
                                    <option value='fa fa-bitbucket fa-fw'>&#xf171; fa-bitbucket</option>
                                    <option value='fa fa-bitcoin fa-fw'>&#xf15a; fa-bitcoin</option>
                                    <option value='fa fa-black-tie fa-fw'>&#xf27e; fa-black-tie</option>
                                    <option value='fa fa-book fa-fw'>&#xf02d; fa-book</option>
                                    <option value='fa fa-bookmark fa-fw'>&#xf02e; fa-bookmark</option>
                                    <option value='fa fa-braille fa-fw'>&#xf2a1; fa-braille</option>
                                    <option value='fa fa-briefcase fa-fw'>&#xf0b1; fa-briefcase</option>
                                    <option value='fa fa-building fa-fw'>&#xf1ad; fa-building</option>
                                    <option value='fa fa-calculator fa-fw'>&#xf1ec; fa-calculator</option>
                                    <option value='fa fa-calendar fa-fw'>&#xf073; fa-calendar</option>
                                    <option value='fa fa-cart-plus fa-fw'>&#xf217; fa-cart-plus</option>
                                    <option value='fa fa-cc-discover fa-fw'>&#xf1f2; fa-cc-discover</option>
                                    <option value='fa fa-cc-mastercard fa-fw'>&#xf1f1; fa-cc-mastercard</option>
                                    <option value='fa fa-cc-paypal fa-fw'>&#xf1f4; fa-cc-paypal</option>
                                    <option value='fa fa-cc-stripe fa-fw'>&#xf1f5; fa-cc-stripe</option>
                                    <option value='fa fa-cc-visa fa-fw'>&#xf1f0; fa-cc-visa</option>
                                    <option value='fa fa-certificate fa-fw'>&#xf0a3; fa-certificate</option>
                                    <option value='fa fa-chain fa-fw'>&#xf0c1; fa-chain</option>
                                    <option value='fa fa-chain-broken fa-fw'>&#xf127; fa-chain-broken</option>
                                    <option value='fa fa-check fa-fw'>&#xf00c; fa-check</option>
                                    <option value='fa fa-circle fa-fw'>&#xf111; fa-circle</option>
                                    <option value='fa fa-close fa-fw'>&#xf00d; fa-close</option>
                                    <option value='fa fa-cloud fa-fw'>&#xf0c2; fa-cloud</option>
                                    <option value='fa fa-columns fa-fw'>&#xf0db; fa-columns</option>
                                    <option value='fa fa-comment fa-fw'>&#xf075; fa-comment</option>
                                    <option value='fa fa-copy fa-fw'>&#xf0c5; fa-copy</option>
                                    <option value='fa fa-copyright fa-fw'>&#xf1f9; fa-copyright</option>
                                    <option value='fa fa-credit-card fa-fw'>&#xf09d; fa-credit-card</option>
                                    <option value='fa fa-crop fa-fw'>&#xf125; fa-crop</option>
                                    <option value='fa fa-cube fa-fw'>&#xf1b2; fa-cube</option>
                                    <option value='fa fa-dashboard fa-fw'>&#xf0e4; fa-dashboard</option>
                                    <option value='fa fa-dashcube fa-fw'>&#xf210; fa-dashcube</option>
                                    <option value='fa fa-database fa-fw'>&#xf1c0; fa-database</option>
                                    <option value='fa fa-deafness fa-fw'>&#xf2a4; fa-deafness</option>
                                    <option value='fa fa-dedent fa-fw'>&#xf03b; fa-dedent</option>
                                    <option value='fa fa-desktop fa-fw'>&#xf108; fa-desktop</option>
                                    <option value='fa fa-dollar fa-fw'>&#xf155; fa-dollar</option>
                                    <option value='fa fa-dot-circle-o fa-fw'>&#xf192; fa-dot-circle-o</option>
                                    <option value='fa fa-download fa-fw'>&#xf019; fa-download</option>
                                    <option value='fa fa-edit fa-fw'>&#xf044; fa-edit</option>
                                    <option value='fa fa-eur fa-fw'>&#xf153; fa-eur</option>
                                    <option value='fa fa-expand fa-fw'>&#xf065; fa-expand</option>
                                    <option value='fa fa-fax fa-fw'>&#xf1ac; fa-fax</option>
                                    <option value='fa fa-feed fa-fw'>&#xf09e; fa-feed</option>
                                    <option value='fa fa-file fa-fw'>&#xf15b; fa-file</option>
                                    <option value='fa fa-file-archive-o fa-fw'>&#xf1c6; fa-file-archive-o</option>
                                    <option value='fa fa-file-excel-o fa-fw'>&#xf1c3; fa-file-excel-o</option>
                                    <option value='fa fa-file-image-o fa-fw'>&#xf1c5; fa-file-image-o</option>
                                    <option value='fa fa-filter fa-fw'>&#xf0b0; fa-filter</option>
                                    <option value='fa fa-fire fa-fw'>&#xf06d; fa-fire</option>
                                    <option value='fa fa-flag fa-fw'>&#xf024; fa-flag</option>
                                    <option value='fa fa-folder fa-fw'>&#xf07b; fa-folder</option>
                                    <option value='fa fa-hashtag fa-fw'>&#xf292; fa-hashtag</option>
                                    <option value='fa fa-image fa-fw'>&#xf03e; fa-image</option>
                                    <option value='fa fa-inbox fa-fw'>&#xf01c; fa-inbox</option>
                                    <option value='fa fa-info fa-fw'>&#xf129; fa-info</option>
                                    <option value='fa fa-info-circle fa-fw'>&#xf05a; fa-info-circle</option>
                                    <option value='fa fa-language fa-fw'>&#xf1ab; fa-language</option>
                                    <option value='fa fa-mail-forward fa-fw'>&#xf064; fa-mail-forward</option>
                                    <option value='fa fa-map fa-fw'>&#xf279; fa-map</option>
                                    <option value='fa fa-map-marker fa-fw'>&#xf041; fa-map-marker</option>
                                    <option value='fa fa-minus fa-fw'>&#xf068; fa-minus</option>
                                    <option value='fa fa-mobile fa-fw'>&#xf10b; fa-mobile</option>
                                    <option value='fa fa-paypal fa-fw'>&#xf1ed; fa-paypal</option>
                                    <option value='fa fa-pencil fa-fw'>&#xf040; fa-pencil</option>
                                    <option value='fa fa-percent fa-fw'>&#xf295; fa-percent</option>
                                    <option value='fa fa-phone fa-fw'>&#xf095; fa-phone</option>
                                    <option value='fa fa-power-off fa-fw'>&#xf011; fa-power-off</option>
                                    <option value='fa fa-print fa-fw'>&#xf02f; fa-print</option>
                                    <option value='fa fa-question fa-fw'>&#xf128; fa-question</option>
                                    <option value='fa fa-random fa-fw'>&#xf074; fa-random</option>
                                    <option value='fa fa-rebel fa-fw'>&#xf1d0; fa-rebel</option>
                                    <option value='fa fa-refresh fa-fw'>&#xf021; fa-refresh</option>
                                    <option value='fa fa-registered fa-fw'>&#xf25d; fa-registered</option>
                                    <option value='fa fa-remove fa-fw'>&#xf00d; fa-remove</option>
                                    <option value='fa fa-save fa-fw'>&#xf0c7; fa-save</option>
                                    <option value='fa fa-send fa-fw'>&#xf1d8; fa-send</option>
                                    <option value='fa fa-sign-in fa-fw'>&#xf090; fa-sign-in</option>
                                    <option value='fa fa-sign-out fa-fw'>&#xf08b; fa-sign-out</option>
                                    <option value='fa fa-spinner fa-fw'>&#xf110; fa-spinner</option>
                                    <option value='fa fa-sticky-note fa-fw'>&#xf249; fa-sticky-note</option>
                                    <option value='fa fa-stop fa-fw'>&#xf04d; fa-stop</option>
                                    <option value='fa fa-stop-circle fa-fw'>&#xf28d; fa-stop-circle</option>
                                    <option value='fa fa-support fa-fw'>&#xf1cd; fa-support</option>
                                    <option value='fa fa-table fa-fw'>&#xf0ce; fa-table</option>
                                    <option value='fa fa-undo fa-fw'>&#xf0e2; fa-undo</option>
                                    <option value='fa fa-unlink fa-fw'>&#xf127; fa-unlink</option>
                                    <option value='fa fa-unlock fa-fw'>&#xf09c; fa-unlock</option>
                                    <option value='fa fa-user-md fa-fw'>&#xf0f0; fa-user-md</option>
                                    <option value='fa fa-users fa-fw'>&#xf0c0; fa-users</option>
                                    <option value='fa fa-vcard fa-fw'>&#xf2bb; fa-vcard</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group input-group-sm row">
                            <div class="col-lg-3">
                                <button type="button" id="button_update" class="btn btn-success">
                                    Сохранить
                                </button>
                            </div>
                            <div class="col-lg-3">
                                <button type="button" id="button_delete" class="btn btn-danger"
                                @cannot('delete', \App\Models\Menu::class) disabled @endcannot>
                                    Удалить
                                </button>
                            </div>
                        </div>

                        {{--</div>--}}

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>
@endsection
