@extends('layouts.app')

@section('title', 'Маркетинг Добавление/Редактирование')

@section('content')

    <form role="form" action="{{ route('marketing.store') }}" method="POST">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ csrf_field() }}
                        @cannot('write', \App\Models\Books\Marketing::class) <fieldset disabled> @endcannot
                        <div class="form-group invisible">
                            <input
                                    type="hidden"
                                    name="id"
                                    id="id"
                                    value="{{ $marketing->id or '' }}"
                            >
                        </div>
                        <div class="form-group input-group-sm required">
                            <label class="control-label" for="attraction">Город:</label>
                            <input
                                    type="text"
                                    class="form-control"
                                    name="attraction"
                                    id="attraction"
                                    required="required"
                                    maxlength="80"
                                    autofocus
                                    value="{{ $marketing->attraction or '' }}"
                            >
                        </div>
                        <div class="checkbox">
                            <label class="form-check-label">
                                <input
                                        type="checkbox"
                                        class="form-check-input"
                                        name="is_archive"
                                        id="is_archive"
                                        @if ( ! empty( $marketing->is_archive ) && ($marketing->is_archive === 1 ) )
                                        checked
                                        @endif
                                        @cannot('delete',\App\Models\Books\Marketing::class) disabled @endcannot
                                >
                                Архив
                            </label>
                        </div>
                        <div class="form-group">
                            Поля, отмеченные звездочкой, обязательны для заполнения.
                        </div>
                    @cannot('write', \App\Models\Books\Marketing::class) </fieldset> @endcannot
                    </div>
                    <!-- /.panel-body -->
                    <div class="panel-footer">
                        <button type="submit" name="submit" value="save" class="btn btn-primary"
                        @cannot('write', \App\Models\Books\Marketing::class) disabled @endcannot>Сохранить</button>
                        <a href="{{ route('marketing.index') }}" class="btn btn-primary" role="button">Отмена</a>
                    </div>
                    <!-- /.panel-footer -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </form>
@endsection