@extends('layouts.app')

@section('title', 'Боксы Добавление/Редактирование')

@section('content')

    <form role="form" action="{{ route('box.store') }}" method="POST">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ csrf_field() }}
                        @cannot('write', \App\Models\Box::class) <fieldset disabled> @endcannot
                            <div class="form-group invisible">
                                <input
                                        type="hidden"
                                        name="id"
                                        id="id"
                                        value="{{ $box->id or '' }}"
                                >
                            </div>
                            <div class="form-group required">
                                <label class="control-label" for="system">Система:</label>
                                <select class="form-control" name="system" id="system" required >
                                    <option value="">Выберите из списка</option>
                                    @foreach($systems as $system)
                                        <option value="{{$system->id}}"
                                                @if(!empty ($box->system_id) && $system->id === $box->system_id) selected @endif>
                                            {{$system->descr}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group required">
                                <label class="control-label" for="currency">Валюта:</label>
                                <select class="form-control" name="currency" id="currency" required >
                                    <option value="">Выберите из списка</option>
                                    @foreach( $currencies as $currency )
                                        <option value="{{$currency->id}}"
                                                @if(!empty ($box->currency_id) && $currency->id === $box->currency_id) selected @endif>
                                            {{$currency->descr}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group required">
                                <label class="control-label" for="office">Отделение:</label>
                                <select class="form-control" name="office" id="office" required >
                                    <option value="">Выберите из списка</option>
                                    @foreach( $offices as $office )
                                        <option value="{{$office->id}}"
                                                @if(!empty ($box->office_id) && $office->id === $box->office_id) selected @endif>
                                            {{$office->item}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group input-group-sm required">
                                <label class="control-label" for="unique_number">Уникальный номер:</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        name="unique_number"
                                        id="unique_number"
                                        required="required"
                                        maxlength="30"
                                        value="{{ $box->unique_number or '' }}"
                                >
                            </div>
                            <div class="form-group input-group-sm required">
                                <label class="control-label" for="balance">Баланс:</label>
                                <input
                                        type="number"
                                        class="form-control"
                                        name="balance"
                                        id="balance"
                                        min="0"
                                        step=0.01
                                        required="required"
                                        value="{{ $box->balance or '' }}"
                                >
                            </div>
                            <div class="form-group input-group-sm">
                                <label class="control-label" for="note">Комментарий:</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        name="note"
                                        id="note"
                                        maxlength="600"
                                        value="{{ $box->note or '' }}"
                                >
                            </div>
                            <div class="checkbox">
                                <label class="form-check-label">
                                    <input
                                            type="checkbox"
                                            class="form-check-input"
                                            name="is_archive"
                                            id="is_archive"
                                            @if ( ! empty( $box->is_archive ) && ( $box->is_archive === 1 ) )
                                            checked
                                            @endif
                                            @cannot('delete',\App\Models\Box::class) disabled @endcannot
                                    >
                                    Архив
                                </label>
                            </div>
                            <div class="form-group">
                                Поля, отмеченные звездочкой, обязательны для заполнения.
                            </div>
                            @cannot('write', \App\Models\Box::class) </fieldset> @endcannot
                    </div>
                    <!-- /.panel-body -->
                    <div class="panel-footer">
                        <button type="submit" name="submit" value="save" class="btn btn-primary"
                                @cannot('write', \App\Models\Box::class) disabled @endcannot>Сохранить</button>
                        <a href="{{ route('box.index') }}" class="btn btn-primary" role="button">Отмена</a>
                    </div>
                    <!-- /.panel-footer -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </form>
@endsection