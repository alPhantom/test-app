<div class="form-group input-group-sm row request">
    <div class="col-lg-4">
        <label class="control-label" for="last_name">Фамилия:</label>
    </div>
    <div class="col-lg-8">
        <input
                type="text"
                class="form-control"
                name="last_name"
                id="last_name"
                maxlength="80"
                autocomplete="off"
                value="{{ old('last_name') ?? $model->last_name ?? '' }}"
        />
    </div>
</div>

<div class="form-group input-group-sm row request">
    <div class="col-lg-4">
        <label class="control-label" for="first_name">Имя:</label>
    </div>
    <div class="col-lg-8">
        <input
                type="text"
                class="form-control"
                name="first_name"
                id="first_name"
                maxlength="80"
                autocomplete="off"
                value="{{ old('first_name') ?? $model->first_name ?? '' }}"
        />
    </div>
</div>

<div class="form-group input-group-sm row request">
    <div class="col-lg-4">
        <label class="control-label" for="middle_name">Отчество:</label>
    </div>
    <div class="col-lg-8">
        <input
                type="text"
                class="form-control"
                name="middle_name"
                id="middle_name"
                maxlength="80"
                autocomplete="off"
                value="{{ old('middle_name') ?? $model->middle_name ?? '' }}"
        />
    </div>
</div>

<div class="form-group input-group-sm row request">
    <div class="col-lg-4">
        <label class="control-label" for="phone">Телефон:</label>
    </div>
    <div class="col-lg-8">
        <input
                type="text"
                class="form-control"
                name="phone"
                id="phone"
                maxlength="80"
                autocomplete="off"
                value="{{ old('phone') ?? $model->phone ?? '' }}"
        />
    </div>
</div>

<div class="form-group input-group-sm row request">
    <div class="col-lg-4">
        <label class="control-label" for="middle_name">Источник привлечения:</label>
    </div>
    <div class="col-lg-8">
        {{ Form::selectList('marketing',
                           'marketing_id',
                           '' ,
                           $marketings,
                           old('marketing_id') ?? $model->marketing_id ?? '',
                           'id',
                           'attraction') }}
    </div>
</div>