<div class="form-group invisible">
    <input
            type="hidden"
            name="{{ $name }}"
            id="{{ $id }}"
            value="{{ $value }}"
    >
</div>