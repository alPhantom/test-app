<div class="panel panel-default">
    <div class="panel-body">
        <button type="submit" name="submit" value="save" class="btn btn-primary"
        @if($disabled) disabled @endif>
            Сохранить
        </button>
        <a href="{{ route($route) }}" class="btn btn-primary" role="button">Отмена</a>
    </div>
</div>