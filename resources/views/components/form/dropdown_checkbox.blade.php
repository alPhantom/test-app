
<dl class="dropdown">
    <dt>
        <label class="control-label">{{ $label }}:</label>
        <input
                class="form-control readonly"
                id="checked"
                name="checked_values"
                type="text"
                autocomplete="off"
                value="{{ old('checked_values') ?? $savedValues }}"
                placeholder="Выберите из списка"
                {{ $attributes }}
        />
    </dt>
    <dd>
        <div class="mutli_select">
            <ul>
                @foreach($collection as $item)
                    <li>
                        <input
                             type="checkbox"
                             value="{{ $item->$id }}"
                             id="{{ $name . '_' . $item->$id }}"
                             name="{{ $name }}[]"
                             data-text="{{ $item->$value }}"
                             @if (is_array($savedIds))
                                @if(in_array($item->$id, $savedIds) !== false) checked @endif
                             @else
                                @if(strpos($savedIds, '' . $item->$id) !== false) checked @endif
                             @endif
                        /> {{ $item->$value }}
                    </li>
                @endforeach
            </ul>
        </div>
    </dd>
</dl>
