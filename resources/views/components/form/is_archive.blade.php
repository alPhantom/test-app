<div class="form-group input-group-sm">
    <label class="form-check-label">
        <input
                type="checkbox"
                class="form-check-input"
                name="is_archive"
                id="is_archive"
                @if ( $isArchive === 1 )
                checked
                @endif
                @if ($disabled) disabled @endif
        >
        Архив
    </label>
</div>