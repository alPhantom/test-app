<div class="form-group input-group-sm {{ $class }}">
        <label class="control-label" for="{{ $id }}">{{ $label }}:</label>
        <input
                type="text"
                class="form-control"
                name="{{ $name }}"
                id="{{ $id }}"
                value="{{ old($name) ?? $value }}"
                {{ $attributes }}
        />
</div>