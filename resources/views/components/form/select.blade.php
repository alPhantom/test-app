
<select id="{{$id}}" name="{{$name}}" class="form-control" {{ $attributes }}>
    <option value="">Выберите из списка</option>
    @if($collection !== null)
        @foreach($collection as $item)
            <option value="{{$item->$value}}"
                    @if($need !== '' && $item->$value == $need) selected @endif>
                {{$item->$text}}
            </option>
        @endforeach
    @endif
</select>