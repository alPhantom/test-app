<div class="panel panel-default">
    <div class="panel-body">
        <div class="col-lg-6 col-md-6" align="left">
            <button type="submit"
                    name="submit"
                    value="save"
                    class="btn btn-primary"
                    @if($saveDisabled) disabled @endif
            >
                Сохранить
            </button>
            <a href="{{ route($cancelRoute) }}" class="btn btn-primary" role="button">Отмена</a>
        </div>
        <div class="col-lg-6 col-md-6" align="right">
            <button type="submit"
                    formaction="@if(!empty($id)) {{ route($deleteRoute, ['id' => $id])}} @endif"
                    id="button_delete" class="btn btn-danger"
                    @if(empty($id) || $deleteDisabled) disabled @endif
            >
                Удалить
            </button>
        </div>
    </div>
</div>