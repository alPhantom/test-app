<table class="table border-less table-responsive table-condensed text-center font-weight-bold table-rate">
    <tr>
        <th rowspan="2"></th>
        <th colspan="6" class="text-center border-bottom rate-bottom">Розница</th>
    </tr>
    <tr>
        <td>Покупка</td>
        <td>Отклонение -</td>
        <td class="rate-right">Отклонение +</td>
        <td>Продажа</td>
        <td>Отклонение -</td>
        <td>Отклонение +</td>
    </tr>

    @include('components.rate.rate_rows', ['collection' => $retails, 'type' => $typeRetail])
    @includeWhen($retails->count() === 0, 'components.rate.rate_rows_empty', ['collection' => $directions, 'type' => $typeRetail])

</table>
{{--<br>--}}
<table class="table border-less table-responsive table-condensed text-center font-weight-bold table-rate">
    <tr>
        <th rowspan="2"></th>
        <th colspan="6" class="text-center border-bottom rate-bottom">Опт</th>
    </tr>
    <tr>
        <td>Покупка</td>
        <td>Отклонение -</td>
        <td class="rate-right">Отклонение +</td>
        <td>Продажа</td>
        <td>Отклонение -</td>
        <td >Отклонение +</td>
    </tr>

    @include('components.rate.rate_rows', ['collection' => $wholesales, 'type' => $typeWholesale])
    @includeWhen($wholesales->count() === 0, 'components.rate.rate_rows_empty', ['collection' => $directions, 'type' => $typeWholesale])
</table>