<div class="modal fade" id="group_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width: 700px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-cogs"></i> Настройки групп отделений</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-2">
                        <button id="add_group_button" class="btn btn-primary">Добавить</button>
                    </div>
                    <div class="col-lg-10">
                        <div id="group_error_block" class="panel panel-danger panel-heading" style="margin-bottom: 0; margin-top: 0; padding-top: 6px; padding-bottom: 6px" hidden>
                            Error!
                        </div>
                    </div>
                </div>
                <br>
                <div id="group_block">
                @foreach($groups as $group)
                    <div class="form-group input-group-sm row">
                        <input type="hidden" name="id" value="{{ $group->id }}">
                        <input type="hidden" name="descr_old" value="{{ $group->descr }}">
                        <input type="hidden" name="is_archive_old" value="{{ $group->is_archive }}">
                        <div class="col-lg-4">
                            <input
                                 type="text"
                                 class="form-control"
                                 name="descr"
                                 autocomplete="off"
                                 value="{{ $group->descr }}"
                                 disabled
                            >
                        </div>
                        <div class="col-lg-2">
                            <label class="form-check-label">
                                <input
                                        type="checkbox"
                                        class="form-check-input"
                                        name="is_archive"
                                        @if ( $group->is_archive === 1 )
                                        checked
                                        @endif
                                        disabled
                                >
                                Архив
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <button class="btn btn-default" name="change_group_button">
                                Изменить
                            </button>
                        </div>

                        <div class="col-lg-2" align="right">
                            @if ($group->id !== 0)
                            <button class="btn btn-default" name="delete_group_button">
                                Удалить
                            </button>
                            @endif
                        </div>
                    </div>

                @endforeach
                    <input type="hidden" name="group_id_for_delete">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="cancel_group_button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                {{--<button type="button" id="save_group_button" class="btn btn-primary">Сохранить</button>--}}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>