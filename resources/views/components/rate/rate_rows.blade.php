
@foreach($collection as $row)
    <tr id="{{ $type . '_' . $row->exchange_direction_id }}" class="rate-row">

        <td>{{ $row->exchangeDirection->descr }}
            <input type="hidden" name="exchange_direction_id[]" value="{{ $row->exchange_direction_id ?? '' }}"></td>
        <td>
            <input
                    type="number"
                    class="form-control"
                    name="{{ $type }}_buy[]"
                    id="{{ $type .'_buy_'.$row->exchange_direction_id }}"
                    min="0"
                    step=0.01
                    value="{{ number_format($row->buy, 5) }}"
                    required="required"
            >
        </td>
        <td>
            <input
                    type="number"
                    class="form-control"
                    name="{{$type}}_buy_minus[]"
                    id="{{ $type .'_buy_minus_'.$row->exchange_direction_id }}"
                    min="0"
                    step=0.01
                    value="{{ number_format($row->buy_minus, 5) }}"
                    required="required"
            >
        </td>
        <td>
            <input
                    type="number"
                    class="form-control"
                    name="{{$type}}_buy_plus[]"
                    id="{{ $type .'_buy_plus_'.$row->exchange_direction_id }}"
                    min="0"
                    step=0.01
                    value="{{ number_format($row->buy_plus, 5) }}"
                    required="required"
            >
        </td>
        <td>
            <input
                    type="number"
                    class="form-control"
                    name="{{$type}}_sale[]"
                    id="{{ $type .'_sale_'.$row->exchange_direction_id }}"
                    min="0"
                    step=0.01
                    value="{{ number_format($row->sale, 5) }}"
                    required="required"
            >
        </td>
        <td>
            <input
                    type="number"
                    class="form-control"
                    name="{{$type}}_sale_minus[]"
                    id="{{ $type .'_sale_minus_'.$row->exchange_direction_id }}"
                    min="0"
                    step=0.01
                    value="{{ number_format($row->sale_minus, 5) }}"
                    required="required"
            >
        </td>
        <td>
            <input
                    type="number"
                    class="form-control"
                    name="{{$type}}_sale_plus[]"
                    id="{{ $type .'_sale_plus_'.$row->exchange_direction_id }}"
                    min="0"
                    step=0.01
                    value="{{ number_format($row->sale_plus, 5) }}"
                    required="required"
            >
        </td>
    </tr>
@endforeach