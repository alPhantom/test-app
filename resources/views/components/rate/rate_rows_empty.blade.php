
@foreach($collection as $row)
    <tr id="{{ $type . '_' . $row->id }}" class="rate-row">

        <td>{{ $row->descr }}
            <input type="hidden" name="exchange_direction_id[]" value="{{ $row->id ?? '' }}"></td>
        <td>
            <input
                    type="number"
                    class="form-control"
                    name="{{ $type }}_buy[]"
                    id="{{ $type .'_buy_'.$row->id }}"
                    min="0"
                    step=0.01
                    required="required"
            >
        </td>
        <td>
            <input
                    type="number"
                    class="form-control"
                    name="{{$type}}_buy_minus[]"
                    id="{{ $type .'_buy_minus_'.$row->id }}"
                    min="0"
                    step=0.01
                    required="required"
            >
        </td>
        <td>
            <input
                    type="number"
                    class="form-control"
                    name="{{$type}}_buy_plus[]"
                    id="{{ $type .'_buy_plus_'.$row->id }}"
                    min="0"
                    step=0.01
                    required="required"
            >
        </td>
        <td>
            <input
                    type="number"
                    class="form-control"
                    name="{{$type}}_sale[]"
                    id="{{ $type .'_sale_'.$row->id }}"
                    min="0"
                    step=0.01
                    required="required"
            >
        </td>
        <td>
            <input
                    type="number"
                    class="form-control"
                    name="{{$type}}_sale_minus[]"
                    id="{{ $type .'_sale_minus_'.$row->id }}"
                    min="0"
                    step=0.01
                    required="required"
            >
        </td>
        <td>
            <input
                    type="number"
                    class="form-control"
                    name="{{$type}}_sale_plus[]"
                    id="{{ $type .'_sale_plus_'.$row->id }}"
                    min="0"
                    step=0.01
                    required="required"
            >
        </td>
    </tr>
@endforeach