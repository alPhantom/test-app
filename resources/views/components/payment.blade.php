<div>
<div class="row">
    <div class="col-lg-6 col-md-6" align="center">
        <label class="control-label total-amount">
            Сумма: <span id="{{ $sign ?? '' }}full_amount"> {{ $totalAmount }}</span> <span id="operation_currency">{{ $currency }}</span>
        </label>
    </div>
    @if(!empty($debts))
        <div class="col-lg-offset-2 col-md-offset-2 col-lg-2 col-md-2">
            <label class="control-label rate-send">Розница</label>
        </div>
        <div class="col-lg-2 col-md-2">
            <label class="control-label rate-send">Опт</label>
        </div>
    @else
        <div class="col-lg-2 col-md-2">
            <label class="control-label rate-send">Курс</label>
        </div>
    @endif
</div>

<br>

<div id="{{ $sign ?? '' }}payment_block">

    @if(!empty($amountData))
        @foreach($amountData as $row)

            <div class="row " id="{{ $row->currency ?? '' }}_payment">
                <div class="col-lg-6 col-md-6">
                    <div class="form-group input-group">
                        <span class="input-group-addon">{{ $row->currency  ?? '' }}</span>
                        <input
                                type="hidden"
                                name="box_id[]"
                                value="{{ $row->box_id ?? '' }}"
                        />
                        <input
                                type="number"
                                name="payment[]"
                                id="{{ ($sign ?? '') . $row->currency }}_amount"
                                class="form-control"
                                placeholder="Сумма в {{ $row->currency ?? ''}}"
                                value="@if (!empty($row->amount )){{ $row->amount > 0 ? $row->amount : - $row->amount }}@endif"
                                data-border="{{ $row->border ?? '' }}"
                                data-currency="{{ $row->from_currency ?? '' }}"
                        />
                        <input
                                type="hidden"
                                name="rate[]"
                                value=""
                        />
                        <input
                                type="hidden"
                                name="rub_rate[]"
                                value=""
                        />
                    </div>
                </div>

                @if(!empty($debts))
                <div class="col-lg-2 col-md-2">
                    <label class="rate-send">{{ $row->currency . '/' . $currency }}</label>
                </div>
                <div class="col-lg-2 col-md-2">
                    <label class="rate-send" data-rub_rate="{{ round($row->retail_rub ?? 0, 5) }}" id="{{ ($sign ?? '') . $row->currency ?? ''}}_retail">{{ round($row->retail ?? '', 5) }}</label>
                </div>
                <div class="col-lg-2 col-md-2">
                    <label class="rate-send" data-rub_rate="{{ round($row->wholesale_rub ?? 0, 5) }}" id="{{ ($sign ?? '') . $row->currency ?? ''}}_wholesale">{{ round($row->wholesale ?? '', 5) }}</label>
                </div>
                @else
                <div class="col-lg-2 col-md-2">
                    <label class="rate-send">{{ $row->rate ?? '' }}</label>
                </div>
                @endif

            </div>

        @endforeach
    @endif
</div>

<div class="row">
    @if(!empty($debts))
    <div class="col-lg-6 col-md-12">
        <div>
            <label class="control-label">Осталось оплатить:</label>
        </div>
        <div class="form-group input-group row" id="debt" align="center">

                @foreach($debts as $descr => $amount)

                    <div class="col-lg-3 col-md-3">
                        <label class="help-block">{{ $descr }}</label>
                        <input
                                type="number"
                                id="{{ ($sign ?? '') . $descr }}_rest"
                                class="form-control"
                                data-currency="{{ $descr }}"
                                value="{{ round($amount, 4) ?? 0 }}"
                                disabled
                        />
                    </div>
                @endforeach

        </div>
    </div>
        @else <br>
    @endif
</div>
</div>