<div class="form-group row commission-btn">
    <button type="button" class="btn" id="calc_commission" style="width:100%;">Рассчитать комиссию</button>
</div>
<div class="commission-block row" >
    <div class="form-group" id="commission_block">

        @if($tariffs !== null)
            @foreach($tariffs as $item)
                <div class="row">
                    <div class="col-lg-5">
                        <label class="help-block"> {{ $item->tariff->descr }}</label>
                    </div>
                    <div class="col-lg-3">
                        <label class="help-block"> {{ $item->changed_tariff }}</label>
                    </div>
                    <div class="col-lg-4">
                        <label class="help-block"> {{ $item->explanation }}</label>
                    </div>
                </div>
            @endforeach
                <div class="form-group input-group">
                    <span class="input-group-addon"> Сумма с комиссией</span>
                    <input type="number"
                           class="form-control readonly"
                           id="commission_amount"
                           name="commission_amount"
                           required
                           value="{{ $commission_amount }}">
                </div>
        @endif

    </div>
    <br>
</div>