@extends('layouts.app')

@section('title', 'Доп. услуги/Колибри')

@section('content')

    @can('addRequest', \App\Models\Operations\Additional::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('additional.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    @can('viewList', \App\Models\Operations\Additional::class)
        <div class="row">
            <div class="col-lg-12">
                <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                    <thead>
                    <tr>
                        <th>Открыть</th>
                        <th>Код</th>
                        <th>Дата</th>
                        <th>Вид услуги</th>
                        <th>Сумма</th>
                        <th>Валюта</th>
                        <th>Статус</th>
                        <th>Отделение</th>
                        <th>Автор</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($models as $additional)
                        @can('viewItem', $additional)
                            <tr id="row{{ $additional->id }}">
                                <td><a href="{{ route('additional.edit', ['id' => $additional->id, 'status' => $additional->status->id]) }}">Открыть</a></td>
                                <td>{{ $additional->id }}</td>
                                <td>{{ $additional->create_date_time }}</td>
                                <td>{{ $additional->serviceType->descr }}</td>
                                <td>{{ ($additional->amount ?? 0) + ($additional->commission ?? 0) }}</td>
                                <td>{{ $additional->currency->descr }}</td>
                                <td>{{ $additional->status->descr }}</td>
                                <td>{{ $additional->office->item }}</td>
                                <td>{{ $additional->user->last_name }}</td>
                            </tr>
                        @endcan
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    @endcan
@endsection