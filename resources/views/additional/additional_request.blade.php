<form role="form" method="post" id="ticket_request">
    {{ csrf_field() }}
    @if(!empty($model))
        @cannot('editRequest', $model)
            <fieldset disabled>
                @endcannot
                @endif

                <div class="col-lg-6 col-md-6">
                    {{ Form::hiddenInput('id', 'id', old('id') ?? $model->id ?? '') }}

                    @include('components.client', ['model' => $model ?? null, 'marketings'=> $marketings])

                    <br>
                    <div class="form-group input-group-sm required row request">
                        <div class="col-lg-4">
                            <label class="control-label" for="last_name">Вид операции:</label>
                        </div>
                        <div class="col-lg-8">
                            {{ Form::selectList('cross_service',
                                               'cross_service_id',
                                               'required' ,
                                               $serviceTypes ?? null,
                                               old('cross_service_id') ?? $model->cross_service_id ?? '',
                                               'id',
                                               'descr') }}
                        </div>
                    </div>

                    <div class="form-group input-group-sm required row request">
                        <div class="col-lg-4">
                            <label class="control-label">Валюта:</label>
                        </div>
                        <div class="col-lg-8">
                            {{ Form::selectList('currency',
                                               'currency_id',
                                               'required' ,
                                               $currencies,
                                               old('currency_id') ?? $model->currency_id?? '',
                                               'id',
                                               'descr') }}
                        </div>
                    </div>

                    <div class="form-group input-group-sm row">
                        <div class="col-lg-4">
                            <label class="control-label">Сумма:</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="number"
                                   name="total_amount"
                                   id="total_amount"
                                   class="form-control"
                                   value="{{ old('total_amount') ?? $model->amount ?? 0 }}">
                        </div>
                    </div>


                    @include('components.tariff', ['tariffs' => $tariffs ?? null,
                                                   'commission_amount' => ($model->amount ?? 0) + ($model->commission ?? 0)])

                </div>

                <div class="col-lg-12">
                    <div class="form-group row">
                        <textarea
                                class="form-control"
                                rows="2" name="note" id="note"
                                placeholder="Комментарий...">{{ old('note') ?? $model->note ?? '' }}</textarea>
                    </div>
                </div>
                @if(!empty($model))
                    @cannot('editRequest', $model)
            </fieldset>
        @endcannot
    @endif
    <div class="col-lg-12 panel panel-default panel-body">
        <div class="div-left">
            @if(!empty($model))
                @cannot('editRequest', $model)
                    <fieldset disabled>
                        @endcannot
                        @endif
                        <button type="submit" name="status" formaction="{{ route('additional.store') }}" id="to_save" value="1" class="btn btn-primary">Сохранить</button>
                        <button type="submit" name="status" formaction="{{ route('additional.store') }}" id="to_pay" value="2" class="btn btn-primary">К оплате</button>
                        <button type="submit" name="status" formaction="{{ route('additional.store') }}" id="to_delete" value="7" class="btn btn-primary">Архив</button>
                        @if(!empty($model))
                            @cannot('editRequest', $model)
                    </fieldset>
                @endcannot
            @endif
        </div>
        <div class="div-right">
            <a href="{{ route('additional.index') }}" class="btn btn-primary" role="button">Отмена</a>
        </div>
    </div>
</form>
