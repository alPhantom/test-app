@extends('layouts.app')

@section('title', 'Приход/Расход - Добавление/Редактирование')

@section('js')

@endsection

@section('content')

    <form role="form" action="{{ route('income_expense.store') }}" method="POST">
        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ csrf_field() }}
                        @cannot('write', \App\Models\IncomeExpense::class)
                            <fieldset disabled> @endcannot

                                {{Form::hiddenInput('id', 'id', $movement->id ?? '')}}

                                <div class="form-group required">
                                    <label class="control-label">Операция: </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="income" id="income" value="yes">Приход
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="income" id="expense" value="no">Расход
                                    </label>
                                </div>

                                <div class="form-group input-group-sm required row">
                                    <div class="col-lg-4">
                                        <label class="control-label" for="category">Код:</label>
                                    </div>
                                    <div class="col-lg-8">
                                        {{Form::selectList('section_id',
                                                            'section',
                                                            'required' ,
                                                            $sections ?? null,
                                                            old('section_id') ?? $movement->section_id ?? '',
                                                            'id',
                                                            'descr'
                                        )}}
                                    </div>
                                </div>

                                <div class="form-group input-group-sm row">
                                    <div class="col-lg-4">
                                        <label class="control-label" for="category">Основание:</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <input
                                                type="text"
                                                name="cause"
                                                class="form-control"
                                                readonly
                                                value="{{ old('cause') ?? $movement->cause ?? '' }}"
                                        />
                                    </div>
                                </div>

                                <div class="form-group input-group-sm required row">
                                    <div class="col-lg-4">
                                        <label class="control-label" for="category">ЦФО:</label>
                                    </div>
                                    <div class="col-lg-8">
                                        {{Form::selectList( 'responsible_office_id',
                                                            'responsible_office_id',
                                                            'required' ,
                                                            $offices ?? null,
                                                            old('responsible_office_id') ?? $movement->responsible_office_id ?? '',
                                                            'id',
                                                            'item'
                                        )}}
                                    </div>
                                </div>

                                <div class="form-group input-group-sm required row">
                                    <div class="col-lg-4">
                                        <label class="control-label" for="category">ФИО:</label>
                                    </div>
                                    <div class="col-lg-8">
                                        {{Form::selectList( 'responsible_user_id',
                                                            'responsible_user_id',
                                                            'required' ,
                                                            $users ?? null,
                                                            old('responsible_user_id') ?? $movement->responsible_user_id ?? '',
                                                            'id',
                                                            'fullName'
                                        )}}
                                    </div>
                                </div>

                                <div class="form-group input-group-sm required row">
                                    <div class="col-lg-4">
                                        {{ Form::selectList('box_id',
                                                            'box_id',
                                                            'required' ,
                                                            $boxes,
                                                            old('box_id') ?? $cf->box_id ?? '',
                                                            'id',
                                                            'unique_number'
                                        ) }}
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group input-group">
                                            <input type="number" class="form-control" name="amount" id="amount"
                                                   value="{{ old('amount') ?? $cf->amount ?? ''}}"/>
                                            <span class="input-group-addon" id="rate"> {{ $cf->rate ?? '' }}</span>
                                            {{Form::hiddenInput('rate', 'rate', $cf->rate  ?? '')}}
                                            {{Form::hiddenInput('rub_rate', 'rub_rate', $cf->rub_rate ?? '')}}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                <textarea
                                        class="form-control"
                                        rows="3" name="note" id="note"
                                        placeholder="Комментарий...">{{ old('note') ?? $movement->note ?? '' }}</textarea>
                                </div>

                                @cannot('write', \App\Models\IncomeExpense::class) </fieldset> @endcannot
                    </div>
                </div>

                {{Form::saveCancel( 'income_expense.index', Gate::denies('write', \App\Models\IncomeExpense::class))}}
            </div>
            <!-- /.col-lg-6 -->
        </div>
    </form>
@endsection