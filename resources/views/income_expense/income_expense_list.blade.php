@extends('layouts.app')

@section('title', 'Приход/Расход')

@section('content')

    @can('write', \App\Models\IncomeExpense::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('income_expense.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-lg-9">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Операция</th>
                    <th>ДДС</th>
                    <th>Сумма</th>
                    <th>Валюта</th>
                    <th>ЦФО</th>
                    <th>ФИО</th>
                </thead>
                <tbody>

                @foreach ($movements as $movement)
                    <tr id="row{{ $movement->id }}">
                        <td><a href="{{ route('income_expense.edit', ['id' => $movement->id] ) }}">Открыть</a></td>
                        <td>{{ $movement->id }}</td>
                        <td>{{ $movement->descr }}</td>
                        <td>{{ $movement->descr }}</td>
                        <td>{{ $movement->descr }}</td>
                        <td>{{ $movement->descr }}</td>
                        <td>{{ $movement->transit == 1 ? 'Да' : 'Нет' }}</td>
                        <td>{{ $movement->is_archive == 1 ? 'Да' : 'Нет' }}</td>

                    {{--<td>{{ ( $tariff->is_archive == 1 ) ? 'Да' : 'Нет' }}</td>--}}
                    <!-- Кнопка Удалить -->
                        {{--<td>--}}
                        {{--<form action="{{ route('sectioncf.destroy', ['id' => $section->id]) }}" method="POST">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<button type="submit" class="btn btn-link"--}}
                        {{--@cannot('delete', \App\Models\Books\SectionCashflow::class)--}}
                        {{--disabled--}}
                        {{--@endcannot>--}}
                        {{--<i class="fa fa-trash"></i> Удалить--}}
                        {{--</button>--}}
                        {{--</form>--}}
                        {{--</td>--}}
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection