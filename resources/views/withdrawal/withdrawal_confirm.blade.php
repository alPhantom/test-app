<form role="form" action="{{ route('withdrawal.store') }}" method="post" id="withdrawal_confirm">
    {{ csrf_field() }}

    {{ Form::hiddenInput('id', 'id', old('id') ?? $withdrawal->id ?? '') }}
    {{ Form::hiddenInput('currency_id', 'currency_id', old('currency_id') ?? $withdrawal->currency_id ?? '') }}

    @cannot('editConfirm', $withdrawal)
        <fieldset disabled>
    @endcannot
    <div class="col-lg-12">
        <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
            <thead>
            <tr>
                <th>Код</th>
                <th>Бокс</th>
                <th>Номер карты клиента</th>
                <th>Время зачисления</th>
                <th>Сумма</th>
                <th>Незавершенка</th>
            </tr>
            </thead>
            <tbody>
                @if ( ! empty($withdrawalInfo) ) {{-- todo show old() values --}}
                    @foreach ($withdrawalInfo as $info)
                        <tr id="row{{ $info->id }}">
                            <td>{{ $info->id }}</td>
                            <td>{{ $info->box->unique_number }}</td>
                            <td>{{ $info->card_number }}</td>
                            <td>{{ $info->card_time }}</td>
                            <td>{{ $info->amount }}</td>
                            <td>
                                <select id="unfinished" name="unfinished_id[]" class="form-control" required>
                                    <option value="">Выберите из нового списка</option>
                                    @if ( $info->cashflow !== null )
                                        @foreach ( $info->cashflow as $cashflow )
                                            <option value="{{ $cashflow->id . 'delimiter' . $info->id }}"
                                                @if ($cashflow->withdrawal_info_id !== '' && $info->id == $cashflow->withdrawal_info_id) selected @endif>
                                                {{ $cashflow->amount . " | " . $cashflow->unfinished->coming_date . " | " . $cashflow->unfinished->note }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    @cannot('editConfirm', $withdrawal)
        </fieldset>
    @endcannot

    <div class="row">
        <div class="col-lg-12 panel panel-default panel-body">
            <div class="div-left">
                @cannot('editConfirm', $withdrawal)
                    <fieldset disabled>
                @endcannot
                <button type="submit" name="status" formaction="{{ route('withdrawal.store') }}" id="to_pay" value="2" class="btn btn-primary">К оплате</button>
                <button type="submit" name="status" formaction="{{ route('withdrawal.store') }}" id="to_save" value="1" class="btn btn-primary">Вернуть</button>
                @cannot('editConfirm', $withdrawal)
                    </fieldset>
                @endcannot
            </div>
            <div class="div-right">
                <a href="{{ route('withdrawal.index') }}" class="btn btn-primary" role="button">Отмена</a>
            </div>
        </div>
    </div>
</form>