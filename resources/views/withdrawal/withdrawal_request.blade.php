<form role="form" method="post" id="withdrawal_request">
    {{ csrf_field() }}
    @if ( ! empty($withdrawal) )
        @cannot('editRequest', $withdrawal)
            <fieldset disabled>
        @endcannot
    @endif

    <div class="col-lg-6 col-md-6">
        {{ Form::hiddenInput('id', 'id', old('id') ?? $withdrawal->id ?? '') }}

            <div class="form-group input-group-sm row request">
                <div class="col-lg-4">
                    <label class="control-label" for="last_name">Фамилия:</label>
                </div>
                <div class="col-lg-8">
                    <input
                            type="text"
                            class="form-control"
                            name="last_name"
                            id="last_name"
                            maxlength="60"
                            autocomplete="off"
                            value="{{ old('last_name') ?? $withdrawal->last_name ?? '' }}"
                    />
                </div>
            </div>

            <div class="form-group input-group-sm row request">
                <div class="col-lg-4">
                    <label class="control-label" for="first_name">Имя:</label>
                </div>
                <div class="col-lg-8">
                    <input
                            type="text"
                            class="form-control"
                            name="first_name"
                            id="first_name"
                            maxlength="60"
                            autocomplete="off"
                            value="{{ old('first_name') ?? $withdrawal->first_name ?? '' }}"
                    />
                </div>
            </div>

            <div class="form-group input-group-sm row request">
                <div class="col-lg-4">
                    <label class="control-label" for="middle_name">Отчество:</label>
                </div>
                <div class="col-lg-8">
                    <input
                            type="text"
                            class="form-control"
                            name="middle_name"
                            id="middle_name"
                            maxlength="60"
                            autocomplete="off"
                            value="{{ old('middle_name') ?? $withdrawal->middle_name ?? '' }}"
                    />
                </div>
            </div>

            <div class="form-group input-group-sm row request">
                <div class="col-lg-4">
                    <label class="control-label" for="phone">Телефон:</label>
                </div>
                <div class="col-lg-8">
                    <input
                            type="text"
                            class="form-control"
                            name="phone"
                            id="phone"
                            maxlength="12"
                            autocomplete="off"
                            value="{{ old('phone') ?? $withdrawal->phone ?? '' }}"
                    />
                </div>
            </div>

            <div class="form-group input-group-sm row request">
                <div class="col-lg-4">
                    <label class="control-label" for="marketing">Источник привлечения:</label>
                </div>
                <div class="col-lg-8">
                    {{ Form::selectList('marketing',
                                       'marketing_id',
                                       '' ,
                                       $marketings,
                                       old('marketing_id') ?? $withdrawal->marketing_id ?? '',
                                       'id',
                                       'attraction') }}
                </div>
            </div>

            <br>
            <div class="form-group input-group-sm required row request">
                <div class="col-lg-4">
                    <label class="control-label" for="cross_service">Вид операции:</label>
                </div>
                <div class="col-lg-8">
                    {{ Form::selectList('cross_service',
                                       'cross_service_id',
                                       'required' ,
                                       $serviceTypes,
                                       old('cross_service_id') ?? $withdrawal->cross_service_id?? '',
                                       'id',
                                       'descr') }}
                </div>
            </div>

            <div class="form-group input-group-sm required row request">
                <div class="col-lg-4">
                    <label class="control-label" for="currency">Валюта:</label>
                </div>
                <div class="col-lg-8">
                    {{ Form::selectList('currency',
                                       'currency_id',
                                       'required' ,
                                       $currencies,
                                       old('currency_id') ?? $withdrawal->currency_id?? '',
                                       'id',
                                       'descr') }}
                </div>
            </div>

            <div class="form-group input-group-sm required row request">
                <div class="col-lg-4">
                    <label class="control-label" for="total_amount">Сумма:</label>
                </div>
                <div class="col-lg-8">
                    <input
                            type="number"
                            class="form-control readonly"
                            name="total_amount"
                            id="total_amount"
                            readonly
                            value="{{ $withdrawal->amount ?? 0 }}"
                    />
                </div>
            </div>

            @include('components.tariff', ['tariffs' => $tariffs ?? null,
                                           'commission_amount' => ($send->amount ?? 0) + ($send->commission ?? 0)])

    </div>
    <!-- /.col-lg-6 -->

    <!--------------------------------- Блок добавления финансовых операций ------------------------->

    <div class="col-lg-6 col-md-6">

        <div class="row">
            <div class="col-lg-4">
                <button role="button" class="btn" id="add_card">Добавить операцию</button>
            </div>
        </div>
        <div class="form-group" id="card_info_block">
            <br>
            @if (!empty($withdrawalInfo)) {{-- todo show old() values --}}
                @foreach ($withdrawalInfo as $info)
                    <div class="form-group row card-block required">
                    <div class="row">
                        <div class="col-lg-5 col-md-4">
                            <label class="control-label" for="box">Карта/кошелек ПКЦ:</label>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            {{Form::selectList( 'box',
                                                'box_id[]',
                                                'required',
                                                $boxes,
                                                old('box_id') ?? $info->box_id ?? '',
                                                'id',
                                                'unique_number'
                            ) }}
                        </div>
                        @can('editRequest', $withdrawal)
                        <div class="col-lg-1 col-md-2 col-sm-1 col-xs-1">
                            <div class="delete-card-btn pointer">
                                <i class="fa fa-trash fa-2x"></i>
                            </div>
                        </div>
                        @endcan
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-5">
                            <label class="control-label" for="card_number">Карта/кошелек клиента:</label>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <input class="form-control"
                                   type="text"
                                   id="card_number"
                                   name="card_number[]"
                                   required
                                   maxlength="30"
                                   autocomplete="off"
                                   value="{{ $info->card_number }}"
                            >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-5">
                            <label class="control-label" for="card_time">Дата и время зачисления:</label>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <input class="form-control"
                                   type="datetime-local"
                                   id="card_time"
                                   name="card_time[]"
                                   autocomplete="off"
                                   value=@if( !empty( $info->card_time ) )
                                           "{{ date( 'Y-m-d\TH:i', strtotime( $info->card_time ) ) }}"
                                    @endif
                            >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-5">
                            <label class="control-label" for="amount">Сумма:</label>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <input class="form-control"
                                   type="number"
                                   id="amount"
                                   name="amount[]"
                                   min="0"
                                   required="required"
                                   autocomplete="off"
                                   value="{{ $info->amount }}"
                            >
                        </div>
                    </div>
                </div>
                @endforeach
            @endif
        </div>
    </div>

    <div class="col-lg-12">
        <div class="form-group row">
            <textarea
                    class="form-control"
                    rows="2" name="note" id="note"
                    placeholder="Комментарий...">{{ old('note') ?? $withdrawal->note ?? '' }}</textarea>
        </div>
    </div>
    @if( ! empty($withdrawal) )
        @cannot('editRequest', $withdrawal)
            </fieldset>
        @endcannot
    @endif

    <div class="row">
        <div class="col-lg-12 panel panel-default panel-body">
            <div class="div-left">
                @if( ! empty($withdrawal) )
                    @cannot('editRequest', $withdrawal)
                        <fieldset disabled>
                    @endcannot
                @endif
                <button type="submit" name="status" formaction="{{ route('withdrawal.store') }}" id="to_save" value="1" class="btn btn-primary">Сохранить</button>
                <button type="submit" name="status" formaction="{{ route('withdrawal.store') }}" id="to_repay" value="3" class="btn btn-primary">К выполнению</button>
                <button type="submit" name="status" formaction="{{ route('withdrawal.store') }}" id="to_delete" value="7" class="btn btn-primary">В архив</button>
                @if( ! empty($withdrawal) )
                    @cannot('editRequest', $withdrawal)
                        </fieldset>
                    @endcannot
                @endif
            </div>
            <div class="div-right">
                <a href="{{ route('withdrawal.index') }}" class="btn btn-primary" role="button">Отмена</a>
            </div>
        </div>
    </div>
</form>