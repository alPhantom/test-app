@extends('layouts.app')

@section('js')
    <script src="{{ asset('js/withdrawal/withdrawal.js') }}" defer></script>
    <script src="{{ asset('js/tariff.js') }}" defer></script>
    <script src="{{ asset('js/payment.js') }}" defer></script>
@endsection

@section('css')
    <link href="{{ asset('css/send.css') }}" rel="stylesheet">
@endsection

@section('title', 'Снятие')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                @if ( ! empty( $withdrawal ) )
                    @can('readRequest', $withdrawal)
                        <li class="active">
                            <a href="#request" data-toggle="tab" aria-expanded="true">Заявка</a>
                        </li>
                    @endcan
                    @can('readConfirm', $withdrawal)
                        <li class="">
                            <a href="#confirm" data-toggle="tab" aria-expanded="false">Подтверждение</a>
                        </li>
                    @endcan
                    @can('readPayment', $withdrawal)
                        <li class="">
                            <a href="#payment" data-toggle="tab" aria-expanded="false">Оплата</a>
                        </li>
                    @endcan
                @else
                    <li class="active">
                        <a href="#request" data-toggle="tab" aria-expanded="true">Заявка</a>
                    </li>
                @endif
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                @if ( ! empty($withdrawal) )
                    @can('readRequest', $withdrawal)
                        <div class="tab-pane fade active in" id="request">
                            <br>
                            @include('withdrawal.withdrawal_request', [])
                        </div>
                    @endcan
                    @can('readConfirm', $withdrawal)
                        <div class="tab-pane fade" id="confirm">
                            <br>
                            @include('withdrawal.withdrawal_confirm', [])
                        </div>
                    @endcan
                    @can('readPayment', $withdrawal)
                        <div class="tab-pane fade" id="payment">
                            <br>
                            @include('withdrawal.withdrawal_payment', [])
                        </div>
                    @endcan
                @else
                    <div class="tab-pane fade active in" id="request">
                        <br>
                        @include('withdrawal.withdrawal_request', [])
                    </div>
                @endif
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@endsection