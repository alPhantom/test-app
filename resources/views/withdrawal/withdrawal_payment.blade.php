<form role="form" action="{{ route('withdrawal.store') }}" method="post" id="withdrawal_cash_payment">
    {{ csrf_field() }}
    {{ Form::hiddenInput('id', 'id', old('id') ?? $send->id ?? '') }}

    @cannot('editPayment', $withdrawal)
        <fieldset disabled>
            @endcannot

            @include('components.payment', ['totalAmount' => ($withdrawal->amount ?? 0) + ($withdrawal->commission ?? 0),
                                            'currency' =>  $withdrawal->currency->descr ?? '',
                                            'amountData' => $amountData ?? '',
                                            'debts' => $debts ?? ''])

            @cannot('editPayment', $withdrawal)
        </fieldset>
    @endcannot
    <div class="col-lg-12 panel panel-default panel-body">
        <div class="div-left">
            <button type="submit" name="status" id="to_close" value="5"
                    @cannot('editPayment', $withdrawal) disabled @endcannot
                    class="btn btn-primary">Оплатить</button>
        </div>
        <div class="div-right">
            <a href="{{ route('withdrawal.index') }}" class="btn btn-primary" role="button">Отмена</a>
        </div>
    </div>
</form>