@extends('layouts.app')

@section('title', 'Снятие')

@section('content')

    @can('addRequest', \App\Models\Operations\Withdrawal::class)
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a href="{{ route('withdrawal.create') }}" class="btn btn-primary" role="button">Добавить</a>
                </p>
            </div>
        </div>
    @endcan

    @can('viewList', \App\Models\Operations\Withdrawal::class)
    <div class="row">
        <div class="col-lg-12">
            <table width="100%" class="table table-striped table-bordered table-hover show-search-pagination">
                <thead>
                <tr>
                    <th>Открыть</th>
                    <th>Код</th>
                    <th>Статус</th>
                    <th>Дата</th>
                    <th>Сервис</th>
                    <th>Сумма</th>
                    <th>Сумма с комиссей</th>
                    <th>Валюта</th>
                    <th>Телефон</th>
                    <th>ФИО</th>
                    {{--<th>Удалить</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach ($withdrawals as $withdrawal)
                    @can('viewItem', $withdrawal)
                        <tr id="row{{ $withdrawal->id }}">
                            <td><a href="{{ route('withdrawal.edit', ['id' => $withdrawal->id, 'status' => $withdrawal->status->id] ) }}">Открыть</a></td>
                            <td>{{ $withdrawal->id }}</td>
                            <td>{{ $withdrawal->status->descr }}</td>
                            <td>{{ $withdrawal->create_date_time }}</td>
                            <td>{{ $withdrawal->serviceType->service->descr }}</td>
                            <td>{{ $withdrawal->vamount->total_amount ?? 0 }}</td>
                            <td>{{ ($withdrawal->vamount->total_amount ?? 0) + ($withdrawal->vamount->total_commission ?? 0) }}</td>
                            <td>{{ $withdrawal->currency->descr }}</td>
                            <td>{{ $withdrawal->phone }}</td>
                            <td>{{ $withdrawal->last_name . ' ' . $withdrawal->first_name . ' ' . $withdrawal->middle_name }}</td>
                        </tr>
                    @endcan
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endcan
@endsection