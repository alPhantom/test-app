@extends('layouts.app')

@section('js')
    <script src="{{ asset('js/user.js') }}" defer></script>
@endsection

@section('title', 'Пользователи - Добавление/Редактирование')

@section('content')
<br>
    <form role="form" id="user_form" action="{{ route('user.store') }}" method="POST">

        {{--@cannot('write', \App\User::class) <fieldset disabled> @endcannot--}}

        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">

                        {{ csrf_field() }}

                            <div class="form-group invisible">
                                <input
                                        type="hidden"
                                        name="id"
                                        id="id"
                                        value="{{ $user->id or '' }}"
                                >
                            </div>

                        <div class="form-group input-group-sm required row">
                            <div class="col-lg-4">
                                <label class="control-label" for="last_name">Фамилия:</label>
                            </div>
                            <div class="col-lg-8">
                                <input
                                        type="text"
                                        class="form-control"
                                        name="last_name"
                                        id="last_name"
                                        required="required"
                                        maxlength="80"
                                        autocomplete="off"
                                        value="{{ $user->last_name or '' }}"
                                />
                            </div>
                        </div>

                        <div class="form-group input-group-sm required row">
                            <div class="col-lg-4">
                                <label class="control-label" for="first_name">Имя:</label>
                            </div>
                            <div class="col-lg-8">
                                <input
                                        type="text"
                                        class="form-control"
                                        name="first_name"
                                        id="first_name"
                                        required="required"
                                        maxlength="80"
                                        autocomplete="off"
                                        value="{{ $user->first_name or '' }}"
                                />
                            </div>
                        </div>

                        <div class="form-group input-group-sm required row">
                            <div class="col-lg-4">
                                <label class="control-label" for="middle_name">Отчество:</label>
                            </div>
                            <div class="col-lg-8">
                                <input
                                        type="text"
                                        class="form-control"
                                        name="middle_name"
                                        id="middle_name"
                                        required="required"
                                        maxlength="80"
                                        autocomplete="off"
                                        value="{{ $user->middle_name or '' }}"
                                />
                            </div>
                        </div>

                        <div class="form-group input-group-sm required row">
                            <div class="col-lg-4">
                                <label class="control-label" for="birthday">Дата рождения:</label>
                            </div>
                            <div class="col-lg-8">
                                <input
                                        class="form-control"
                                        type="date"
                                        id="birthday"
                                        name="birthday"
                                        required
                                        min="1940-01-01"
                                        max="{{ date('Y-m-d') }}"
                                        value="{{ $user->birthday or '' }}"
                                />
                            </div>
                        </div>

                        <div class="form-group input-group-sm required row">
                            <div class="col-lg-4">
                                <label class="control-label" for="optionsGender">Пол: </label>
                            </div>
                            <div class="col-lg-8">
                                <label class="radio-inline">
                                    <input
                                            type="radio"
                                            name="gender"
                                            id="male"
                                            value="m"
                                            @if(!empty($user->gender) && $user->gender === 'm' ) checked="" @endif
                                    />М
                                </label>
                                <label class="radio-inline">
                                    <input
                                            type="radio"
                                            name="gender"
                                            id="female"
                                            value="f"
                                            @if(!empty($user->gender) && $user->gender === 'f' ) checked="" @endif
                                    />Ж
                                </label>
                            </div>
                        </div>

                        <div class="form-group input-group-sm required row">
                            <div class="col-lg-4">
                                <label class="control-label" for="optionsGender">Работает: </label>
                            </div>
                            <div class="col-lg-8">
                                <label class="radio-inline">
                                    <input
                                            type="radio"
                                            name="works"
                                            id="works_yes"
                                            value="yes"
                                            @if(!empty($user->works) && $user->works === 'yes' ) checked="" @endif
                                    />Да
                                </label>
                                <label class="radio-inline">
                                    <input
                                            type="radio"
                                            name="works"
                                            id="works_no"
                                            value="no"
                                            @if(!empty($user->works) && $user->works === 'no' ) checked="" @endif
                                    />Нет
                                </label>
                            </div>
                        </div>

                        <div class="form-group input-group-sm row">
                            <div class="col-lg-8">
                                <label for="position">Должность:</label>
                                <select id="position" name="position_id" class="form-control">
                                    <option value="">Выберите из списка</option>
                                    @foreach($positions as $position)
                                        <option value="{{$position->id}}"
                                                @if(!empty ($user->position_id) && $position->id === $user->position_id) selected @endif>
                                            {{$position->descr}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label for="office">Отделение:</label>
                                {{Form::selectList('office', 'office_id', '', $offices ?? null, $user->office_id ?? '', 'id', 'item' )}}
                            </div>
                        </div>

                        <div class="form-group input-group-sm required row">
                            <div class="col-lg-4">
                                <label for="role">Права доступа:</label>
                            </div>
                            <div class="col-lg-8">
                                <select id="role" name="role_id" class="form-control">
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}"
                                                @if(!empty ($user->role_id) && $role->id === $user->role_id) selected @endif>
                                            {{$role->note}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> <!-- /.panel-body -->
                </div> <!-- /.panel -->

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Вход
                    </div>
                    <div class="panel-body">

                        <div class="form-group input-group-sm required row">
                            <div class="col-lg-6">
                                <label for="login_phone">Логин:</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        name="login_phone"
                                        id="login_phone"
                                        readonly
                                        maxlength="80"
                                        autocomplete="off"
                                        value="{{ $user->login_phone or '' }}"
                                />
                            </div>
                        </div>
                        <div class="form-group input-group-sm row" id="password_row">
                            <div class="col-lg-6">
                                <label for="password">Пароль:</label>
                                <input
                                        type="password"
                                        class="form-control"
                                        id="password"
                                        name="password"
                                        maxlength="80"
                                        autocomplete="off"
                                        rel="to-replace"
                                        placeholder="Изменить пароль"
                                />
                            </div>
                            <div class="col-lg-6">
                                <label id="pass_label" for="new_password" hidden >Повторите пароль:</label>
                                <input
                                        type="hidden"
                                        class="form-control"
                                        id="new_password"
                                        maxlength="80"
                                        autocomplete="off"
                                        rel="to-replace"
                                />
                            </div>
                        </div>
                    </div> <!-- /.panel-body -->
                </div> <!-- /.panel -->
            </div> <!-- /.col-lg-6 -->

            <div class="col-lg-6">
                <div class="row">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Контактная информация
                        </div>
                        <div class="panel-body">

                            <div class="form-group input-group-sm required row">
                                <div class="col-lg-6">
                                    <label class="control-label" for="last_name">Телефон:</label>
                                    <input
                                            type="text"
                                            class="form-control"
                                            name="phone"
                                            id="phone"
                                            required="required"
                                            maxlength="80"
                                            autocomplete="off"
                                            value="{{ $user->phone or '' }}"
                                    />
                                </div>
                                <div class="col-lg-6">
                                    <label for="email">e-mail:</label>
                                    <input
                                            type="text"
                                            class="form-control"
                                            name="email"
                                            id="email"
                                            maxlength="80"
                                            autocomplete="off"
                                            value="{{ $user->email or '' }}"
                                    />
                                </div>
                            </div>
                            <div class="form-group input-group-sm row">
                                <div class="col-lg-3">
                                    <label class="control-label" for="country">Страна:</label>
                                    <select id="country" name="country_id" class="form-control">
                                        <option value="">Выберите из списка</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}"
                                                    @if(!empty ($user->fact_country_id) && $country->id === $user->fact_country_id) selected @endif>
                                                {{$country->descr}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="control-label" for="city">Город:</label>
                                    <select id="city" name="city_id" class="form-control">
                                        <option value="">Выберите из списка</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}"
                                                    @if(!empty ($user->fact_city_id) && $city->id === $user->fact_city_id) selected @endif>
                                                {{$city->descr}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-5">
                                    <label for="fact_address">Адрес:</label>
                                    <input
                                            type="text"
                                            class="form-control"
                                            name="fact_address"
                                            id="fact_address"
                                            maxlength="80"
                                            autocomplete="off"
                                            value="{{ $user->fact_address or '' }}"
                                    />
                                </div>
                            </div>
                        </div> <!-- /.panel-body -->
                    </div> <!-- /.panel-info -->

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Паспортные данные
                        </div>
                        <div class="panel-body">

                            <div class="form-group input-group-sm row">
                                <div class="col-lg-4">
                                    <label for="tin">ИНН:</label>
                                </div>
                                <div class="col-lg-8">
                                    <input
                                            type="text"
                                            class="form-control"
                                            name="tin"
                                            id="tin"
                                            maxlength="80"
                                            autocomplete="off"
                                            value="{{ $user->tin or '' }}"
                                    />
                                </div>
                            </div>

                            <div class="form-group input-group-sm row">
                                <div class="col-lg-4">
                                    <label for="passport_number">Серия и номер:</label>
                                </div>
                                <div class="col-lg-8">
                                    <input
                                            type="text"
                                            class="form-control"
                                            name="passport_number"
                                            id="passport_number"
                                            maxlength="80"
                                            autocomplete="off"
                                            value="{{ $user->passport_number or '' }}"
                                    />
                                </div>
                            </div>
                            <div class="form-group input-group-sm row">
                                <div class="col-lg-7">
                                    <label for="passport_by">Кем выдан:</label>
                                    <input
                                            type="text"
                                            class="form-control"
                                            name="passport_by"
                                            id="passport_by"
                                            maxlength="80"
                                            autocomplete="off"
                                            value="{{ $user->passport_by or '' }}"
                                    />
                                </div>
                                <div class="col-lg-5">
                                    <label for="passport_when">Когда:</label>
                                    <input
                                            class="form-control"
                                            type="date"
                                            id="passport_when"
                                            name="passport_when"
                                            min="1940-01-01"
                                            value="{{ $user->passport_when or '' }}"
                                            max="{{ date('Y-m-d') }}"
                                    />
                                </div>
                            </div>
                            <div class="form-group input-group-sm row">

                                <div class="col-lg-3">
                                    <label for="formal_address">Прописка:</label>
                                </div>
                                <div class="col-lg-9">
                                    <input
                                            type="text"
                                            class="form-control"
                                            name="formal_address"
                                            id="formal_address"
                                            maxlength="80"
                                            autocomplete="off"
                                            value="{{ $user->formal_address or ''}}"
                                    />
                                </div>
                            </div>
                        </div> <!-- /.panel-body -->
                    </div> <!-- /.panel-info -->
                </div>
            </div> <!-- /.col-lg-6 -->
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <button type="submit" name="submit" value="save" class="btn btn-primary"
                                @cannot('write', \App\User::class) disabled @endcannot>Сохранить</button>
                        <a href="{{ route('user.index') }}" class="btn btn-primary" role="button">Отмена</a>
                    </div>
                </div> <!-- /.panel -->
            </div> <!-- /.col-lg-12 -->
        </div>
    </form>
@endsection